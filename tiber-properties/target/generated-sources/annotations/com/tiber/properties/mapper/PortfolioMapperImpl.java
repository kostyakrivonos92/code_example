package com.tiber.properties.mapper;

import com.tiber.properties.entity.PortfolioEntity;
import com.tiber.shared.propertylayer.domain.Portfolio;
import javax.annotation.processing.Generated;
import org.springframework.stereotype.Component;

@Generated(
    value = "org.mapstruct.ap.MappingProcessor",
    date = "2022-04-06T16:49:00+0300",
    comments = "version: 1.4.2.Final, compiler: javac, environment: Java 14 (Oracle Corporation)"
)
@Component
public class PortfolioMapperImpl implements PortfolioMapper {

    @Override
    public Portfolio mapEntityToDomain(PortfolioEntity propertyEntities) {
        if ( propertyEntities == null ) {
            return null;
        }

        String name = null;

        name = propertyEntities.getName();

        Portfolio portfolio = new Portfolio( name );

        portfolio.setId( propertyEntities.getId() );
        portfolio.setStatus( propertyEntities.getStatus() );
        portfolio.setCreatedAt( propertyEntities.getCreatedAt() );
        portfolio.setUpdatedAt( propertyEntities.getUpdatedAt() );

        return portfolio;
    }

    @Override
    public PortfolioEntity mapDomainToEntity(Portfolio propertyEntity) {
        if ( propertyEntity == null ) {
            return null;
        }

        PortfolioEntity portfolioEntity = new PortfolioEntity();

        portfolioEntity.setId( propertyEntity.getId() );
        portfolioEntity.setCreatedAt( propertyEntity.getCreatedAt() );
        portfolioEntity.setUpdatedAt( propertyEntity.getUpdatedAt() );
        portfolioEntity.setStatus( propertyEntity.getStatus() );
        portfolioEntity.setName( propertyEntity.getName() );

        return portfolioEntity;
    }
}
