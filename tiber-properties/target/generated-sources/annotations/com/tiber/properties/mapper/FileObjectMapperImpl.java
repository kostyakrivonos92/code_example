package com.tiber.properties.mapper;

import com.tiber.properties.integration.bridgeapi.domain.BridgeMlsMedia;
import com.tiber.shared.file.domain.FileObject;
import com.tiber.shared.file.domain.MlsMedia;
import com.tiber.shared.file.entity.FileObjectEntity;
import com.tiber.shared.file.entity.FileObjectEntityList;
import java.util.ArrayList;
import java.util.List;
import javax.annotation.processing.Generated;
import org.springframework.stereotype.Component;

@Generated(
    value = "org.mapstruct.ap.MappingProcessor",
    date = "2022-04-06T16:49:00+0300",
    comments = "version: 1.4.2.Final, compiler: javac, environment: Java 14 (Oracle Corporation)"
)
@Component
public class FileObjectMapperImpl implements FileObjectMapper {

    @Override
    public FileObject fromMlsMediaToFileObject(MlsMedia mlsMedia) {
        if ( mlsMedia == null ) {
            return null;
        }

        FileObject fileObject = new FileObject();

        fileObject.setTag( mlsMedia.getTag() );
        fileObject.setOrigin( mlsMedia.getOrigin() );
        fileObject.setMediaCategory( mlsMedia.getMediaCategory() );
        fileObject.setMimeType( mlsMedia.getMimeType() );

        return fileObject;
    }

    @Override
    public List<FileObject> fromMlsMediaListToFileObjectList(List<MlsMedia> mlsMedia) {
        if ( mlsMedia == null ) {
            return null;
        }

        List<FileObject> list = new ArrayList<FileObject>( mlsMedia.size() );
        for ( MlsMedia mlsMedia1 : mlsMedia ) {
            list.add( fromMlsMediaToFileObject( mlsMedia1 ) );
        }

        return list;
    }

    @Override
    public List<FileObject> fileObjectEntityListToFileObjectList(FileObjectEntityList fileObjectEntityList) {
        if ( fileObjectEntityList == null ) {
            return null;
        }

        List<FileObject> list = new ArrayList<FileObject>( fileObjectEntityList.size() );
        for ( FileObjectEntity fileObjectEntity : fileObjectEntityList ) {
            list.add( fileObjectEntityToFileObject( fileObjectEntity ) );
        }

        return list;
    }

    @Override
    public List<MlsMedia> bridgeMlsMediaListToMlsMediaList(List<BridgeMlsMedia> bridgeMlsMediaList) {
        if ( bridgeMlsMediaList == null ) {
            return null;
        }

        List<MlsMedia> list = new ArrayList<MlsMedia>( bridgeMlsMediaList.size() );
        for ( BridgeMlsMedia bridgeMlsMedia : bridgeMlsMediaList ) {
            list.add( bridgeMlsMediaToMlsMedia( bridgeMlsMedia ) );
        }

        return list;
    }

    protected FileObject fileObjectEntityToFileObject(FileObjectEntity fileObjectEntity) {
        if ( fileObjectEntity == null ) {
            return null;
        }

        FileObject fileObject = new FileObject();

        fileObject.setId( fileObjectEntity.getId() );
        fileObject.setFileName( fileObjectEntity.getFileName() );
        fileObject.setFileUrl( fileObjectEntity.getFileUrl() );
        fileObject.setTag( fileObjectEntity.getTag() );
        fileObject.setOrigin( fileObjectEntity.getOrigin() );
        fileObject.setMediaCategory( fileObjectEntity.getMediaCategory() );
        fileObject.setMimeType( fileObjectEntity.getMimeType() );
        fileObject.setCreatedAt( fileObjectEntity.getCreatedAt() );

        return fileObject;
    }

    protected MlsMedia bridgeMlsMediaToMlsMedia(BridgeMlsMedia bridgeMlsMedia) {
        if ( bridgeMlsMedia == null ) {
            return null;
        }

        MlsMedia mlsMedia = new MlsMedia();

        mlsMedia.setMediaURL( bridgeMlsMedia.getMediaURL() );
        mlsMedia.setMediaObjectID( bridgeMlsMedia.getMediaObjectID() );
        mlsMedia.setOrder( bridgeMlsMedia.getOrder() );
        mlsMedia.setMimeType( bridgeMlsMedia.getMimeType() );
        mlsMedia.setShortDescription( bridgeMlsMedia.getShortDescription() );
        mlsMedia.setMediaCategory( bridgeMlsMedia.getMediaCategory() );
        mlsMedia.setResourceRecordKey( bridgeMlsMedia.getResourceRecordKey() );
        mlsMedia.setResourceName( bridgeMlsMedia.getResourceName() );
        mlsMedia.setClassName( bridgeMlsMedia.getClassName() );

        return mlsMedia;
    }
}
