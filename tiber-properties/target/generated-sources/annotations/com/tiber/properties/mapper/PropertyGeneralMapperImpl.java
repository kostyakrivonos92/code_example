package com.tiber.properties.mapper;

import com.tiber.properties.entity.DealEntity;
import com.tiber.properties.entity.PortfolioEntity;
import com.tiber.properties.entity.PropertyCommentEntity;
import com.tiber.properties.entity.PropertyCommentEntityList;
import com.tiber.properties.entity.PropertyOnMarketEntity;
import com.tiber.properties.entity.RecipientEntity;
import com.tiber.shared.file.domain.FileObject;
import com.tiber.shared.file.entity.FileObjectEntity;
import com.tiber.shared.file.entity.FileObjectEntityList;
import com.tiber.shared.propertylayer.domain.Deal;
import com.tiber.shared.propertylayer.domain.Portfolio;
import com.tiber.shared.propertylayer.domain.PropertyComment;
import com.tiber.shared.propertylayer.domain.PropertyDetails;
import com.tiber.shared.propertylayer.domain.PropertyGeneral;
import com.tiber.shared.propertylayer.domain.PropertyOnMarket;
import com.tiber.shared.propertylayer.domain.PropertySummary;
import com.tiber.shared.propertylayer.domain.Recipient;
import com.tiber.shared.propertylayer.domain.Underwriting;
import com.tiber.shared.util.AnalogueRenovationTag;
import com.tiber.shared.vocabulary.ContractType;
import com.tiber.shared.vocabulary.InspectionBeforeOffer;
import com.tiber.shared.vocabulary.OccupancyStatus;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import javax.annotation.processing.Generated;
import org.springframework.stereotype.Component;

@Generated(
    value = "org.mapstruct.ap.MappingProcessor",
    date = "2022-04-06T16:49:00+0300",
    comments = "version: 1.4.2.Final, compiler: javac, environment: Java 14 (Oracle Corporation)"
)
@Component
public class PropertyGeneralMapperImpl implements PropertyGeneralMapper {

    @Override
    public PropertyOnMarketEntity propertyOnMarketToPropertyOnMarketEntity(PropertyOnMarket propertyEntity) {
        if ( propertyEntity == null ) {
            return null;
        }

        PropertyOnMarketEntity propertyOnMarketEntity = new PropertyOnMarketEntity();

        propertyOnMarketEntity.setId( propertyEntity.getId() );
        propertyOnMarketEntity.setUuid( propertyEntity.getUuid() );
        propertyOnMarketEntity.setAddress( propertyEntity.getAddress() );
        propertyOnMarketEntity.setFullAddress( propertyEntity.getFullAddress() );
        propertyOnMarketEntity.setCity( propertyEntity.getCity() );
        propertyOnMarketEntity.setState( propertyEntity.getState() );
        propertyOnMarketEntity.setZip( propertyEntity.getZip() );
        propertyOnMarketEntity.setMsa( propertyEntity.getMsa() );
        propertyOnMarketEntity.setCounty( propertyEntity.getCounty() );
        propertyOnMarketEntity.setBedrooms( propertyEntity.getBedrooms() );
        propertyOnMarketEntity.setBathrooms( propertyEntity.getBathrooms() );
        propertyOnMarketEntity.setOneQuarterBathrooms( propertyEntity.getOneQuarterBathrooms() );
        propertyOnMarketEntity.setHalfBathrooms( propertyEntity.getHalfBathrooms() );
        propertyOnMarketEntity.setThreeQuarterBathrooms( propertyEntity.getThreeQuarterBathrooms() );
        propertyOnMarketEntity.setAreaSqFt( propertyEntity.getAreaSqFt() );
        propertyOnMarketEntity.setYearBuilt( propertyEntity.getYearBuilt() );
        propertyOnMarketEntity.setLatitude( propertyEntity.getLatitude() );
        propertyOnMarketEntity.setLongitude( propertyEntity.getLongitude() );
        propertyOnMarketEntity.setPropertyType( propertyEntity.getPropertyType() );
        propertyOnMarketEntity.setPropertySubType( propertyEntity.getPropertySubType() );
        propertyOnMarketEntity.setTiberStatus( propertyEntity.getTiberStatus() );
        propertyOnMarketEntity.setStatusChangeDate( propertyEntity.getStatusChangeDate() );
        propertyOnMarketEntity.setDeals( dealListToDealEntityCollection( propertyEntity.getDeals() ) );
        propertyOnMarketEntity.setLink( propertyEntity.getLink() );
        propertyOnMarketEntity.setComments( propertyCommentListToPropertyCommentEntityList( propertyEntity.getComments() ) );
        propertyOnMarketEntity.setCreatedAt( propertyEntity.getCreatedAt() );
        propertyOnMarketEntity.setUpdatedAt( propertyEntity.getUpdatedAt() );

        return propertyOnMarketEntity;
    }

    @Override
    public PropertyOnMarket propertyOnMarketEntityToPropertyOnMarket(PropertyOnMarketEntity propertyEntity) {
        if ( propertyEntity == null ) {
            return null;
        }

        PropertyOnMarket propertyOnMarket = new PropertyOnMarket();

        propertyOnMarket.setId( propertyEntity.getId() );
        propertyOnMarket.setUuid( propertyEntity.getUuid() );
        propertyOnMarket.setAddress( propertyEntity.getAddress() );
        propertyOnMarket.setFullAddress( propertyEntity.getFullAddress() );
        propertyOnMarket.setCity( propertyEntity.getCity() );
        propertyOnMarket.setState( propertyEntity.getState() );
        propertyOnMarket.setZip( propertyEntity.getZip() );
        propertyOnMarket.setMsa( propertyEntity.getMsa() );
        propertyOnMarket.setCounty( propertyEntity.getCounty() );
        propertyOnMarket.setBedrooms( propertyEntity.getBedrooms() );
        propertyOnMarket.setBathrooms( propertyEntity.getBathrooms() );
        propertyOnMarket.setOneQuarterBathrooms( propertyEntity.getOneQuarterBathrooms() );
        propertyOnMarket.setHalfBathrooms( propertyEntity.getHalfBathrooms() );
        propertyOnMarket.setThreeQuarterBathrooms( propertyEntity.getThreeQuarterBathrooms() );
        propertyOnMarket.setAreaSqFt( propertyEntity.getAreaSqFt() );
        propertyOnMarket.setYearBuilt( propertyEntity.getYearBuilt() );
        propertyOnMarket.setLatitude( propertyEntity.getLatitude() );
        propertyOnMarket.setLongitude( propertyEntity.getLongitude() );
        propertyOnMarket.setPropertyType( propertyEntity.getPropertyType() );
        propertyOnMarket.setPropertySubType( propertyEntity.getPropertySubType() );
        propertyOnMarket.setTiberStatus( propertyEntity.getTiberStatus() );
        propertyOnMarket.setStatusChangeDate( propertyEntity.getStatusChangeDate() );
        propertyOnMarket.setDeals( dealEntityCollectionToDealList( propertyEntity.getDeals() ) );
        propertyOnMarket.setLink( propertyEntity.getLink() );
        propertyOnMarket.setComments( propertyCommentEntityListToPropertyCommentList( propertyEntity.getComments() ) );
        propertyOnMarket.setCreatedAt( propertyEntity.getCreatedAt() );
        propertyOnMarket.setUpdatedAt( propertyEntity.getUpdatedAt() );

        return propertyOnMarket;
    }

    @Override
    public List<PropertySummary> propertyOnMarketListToPropertySummaryList(List<PropertyOnMarket> propertyOnMarketList) {
        if ( propertyOnMarketList == null ) {
            return null;
        }

        List<PropertySummary> list = new ArrayList<PropertySummary>( propertyOnMarketList.size() );
        for ( PropertyOnMarket propertyOnMarket : propertyOnMarketList ) {
            list.add( propertyOnMarketToPropertySummary( propertyOnMarket ) );
        }

        return list;
    }

    @Override
    public PropertySummary propertyOnMarketToPropertySummary(PropertyOnMarket propertyOnMarket) {
        if ( propertyOnMarket == null ) {
            return null;
        }

        PropertySummary propertySummary = new PropertySummary();

        propertySummary.setPrice( propertyOnMarketLatestDealPrice( propertyOnMarket ) );
        propertySummary.setUuid( propertyOnMarket.getUuid() );
        propertySummary.setAddress( propertyOnMarket.getAddress() );
        propertySummary.setCity( propertyOnMarket.getCity() );
        propertySummary.setState( propertyOnMarket.getState() );
        propertySummary.setZip( propertyOnMarket.getZip() );
        propertySummary.setMsa( propertyOnMarket.getMsa() );
        propertySummary.setCounty( propertyOnMarket.getCounty() );
        propertySummary.setBedrooms( propertyOnMarket.getBedrooms() );
        propertySummary.setBathrooms( propertyOnMarket.getBathrooms() );
        propertySummary.setHalfBathrooms( propertyOnMarket.getHalfBathrooms() );
        propertySummary.setAreaSqFt( propertyOnMarket.getAreaSqFt() );
        propertySummary.setYearBuilt( propertyOnMarket.getYearBuilt() );
        propertySummary.setHasActiveDeal( propertyOnMarket.getHasActiveDeal() );
        propertySummary.setLatestDeal( propertyOnMarket.getLatestDeal() );

        return propertySummary;
    }

    @Override
    public DealEntity dealToDealEntity(Deal deal) {
        if ( deal == null ) {
            return null;
        }

        DealEntity dealEntity = new DealEntity();

        if ( deal.getRenovationTag() != null ) {
            dealEntity.setRenovationTag( deal.getRenovationTag() );
        }
        else {
            dealEntity.setRenovationTag( AnalogueRenovationTag.UNKNOWN );
        }
        dealEntity.setId( deal.getId() );
        dealEntity.setUuid( deal.getUuid() );
        dealEntity.setPublicSource( deal.getPublicSource() );
        dealEntity.setResponsibleAnalystUuid( deal.getResponsibleAnalystUuid() );
        dealEntity.setClaimedByAnalystUuid( deal.getClaimedByAnalystUuid() );
        dealEntity.setStatus( deal.getStatus() );
        dealEntity.setAgentUuid( deal.getAgentUuid() );
        dealEntity.setIsForSale( deal.getIsForSale() );
        dealEntity.setSourceModificationTimestamp( deal.getSourceModificationTimestamp() );
        dealEntity.setPortfolio( portfolioToPortfolioEntity( deal.getPortfolio() ) );
        dealEntity.setOrigin( deal.getOrigin() );
        dealEntity.setPrice( deal.getPrice() );
        dealEntity.setClosePrice( deal.getClosePrice() );
        dealEntity.setOnMarketDate( deal.getOnMarketDate() );
        dealEntity.setCloseDate( deal.getCloseDate() );
        dealEntity.setCumulativeDaysOnMarket( deal.getCumulativeDaysOnMarket() );
        dealEntity.setDaysOnMarket( deal.getDaysOnMarket() );
        dealEntity.setFiles( fileObjectListToFileObjectEntityList( deal.getFiles() ) );
        dealEntity.setCreatedAt( deal.getCreatedAt() );
        dealEntity.setUpdatedAt( deal.getUpdatedAt() );
        if ( deal.getOccupancyStatus() != null ) {
            dealEntity.setOccupancyStatus( Enum.valueOf( OccupancyStatus.class, deal.getOccupancyStatus() ) );
        }
        if ( deal.getOccupancyStatusAtClosing() != null ) {
            dealEntity.setOccupancyStatusAtClosing( Enum.valueOf( OccupancyStatus.class, deal.getOccupancyStatusAtClosing() ) );
        }
        if ( deal.getInspectionBeforeOffer() != null ) {
            dealEntity.setInspectionBeforeOffer( Enum.valueOf( InspectionBeforeOffer.class, deal.getInspectionBeforeOffer() ) );
        }
        if ( deal.getContractType() != null ) {
            dealEntity.setContractType( Enum.valueOf( ContractType.class, deal.getContractType() ) );
        }
        dealEntity.setRecipient( recipientToRecipientEntity( deal.getRecipient() ) );

        return dealEntity;
    }

    @Override
    public Deal dealEntityToDeal(DealEntity dealEntity) {
        if ( dealEntity == null ) {
            return null;
        }

        Deal deal = new Deal();

        if ( dealEntity.getRenovationTag() != null ) {
            deal.setRenovationTag( dealEntity.getRenovationTag() );
        }
        else {
            deal.setRenovationTag( AnalogueRenovationTag.UNKNOWN );
        }
        deal.setId( dealEntity.getId() );
        deal.setUuid( dealEntity.getUuid() );
        deal.setPublicSource( dealEntity.getPublicSource() );
        deal.setResponsibleAnalystUuid( dealEntity.getResponsibleAnalystUuid() );
        deal.setClaimedByAnalystUuid( dealEntity.getClaimedByAnalystUuid() );
        deal.setStatus( dealEntity.getStatus() );
        deal.setAgentUuid( dealEntity.getAgentUuid() );
        deal.setIsForSale( dealEntity.getIsForSale() );
        deal.setSourceModificationTimestamp( dealEntity.getSourceModificationTimestamp() );
        deal.setPortfolio( portfolioEntityToPortfolio( dealEntity.getPortfolio() ) );
        deal.setOrigin( dealEntity.getOrigin() );
        deal.setPrice( dealEntity.getPrice() );
        deal.setClosePrice( dealEntity.getClosePrice() );
        deal.setOnMarketDate( dealEntity.getOnMarketDate() );
        deal.setCloseDate( dealEntity.getCloseDate() );
        deal.setCumulativeDaysOnMarket( dealEntity.getCumulativeDaysOnMarket() );
        deal.setDaysOnMarket( dealEntity.getDaysOnMarket() );
        deal.setFiles( fileObjectEntityListToFileObjectList( dealEntity.getFiles() ) );
        deal.setCreatedAt( dealEntity.getCreatedAt() );
        deal.setUpdatedAt( dealEntity.getUpdatedAt() );
        if ( dealEntity.getOccupancyStatus() != null ) {
            deal.setOccupancyStatus( dealEntity.getOccupancyStatus().name() );
        }
        if ( dealEntity.getOccupancyStatusAtClosing() != null ) {
            deal.setOccupancyStatusAtClosing( dealEntity.getOccupancyStatusAtClosing().name() );
        }
        if ( dealEntity.getInspectionBeforeOffer() != null ) {
            deal.setInspectionBeforeOffer( dealEntity.getInspectionBeforeOffer().name() );
        }
        if ( dealEntity.getContractType() != null ) {
            deal.setContractType( dealEntity.getContractType().name() );
        }
        deal.setRecipient( recipientEntityToRecipient( dealEntity.getRecipient() ) );

        return deal;
    }

    @Override
    public RecipientEntity recipientToRecipientEntity(Recipient recipient) {
        if ( recipient == null ) {
            return null;
        }

        RecipientEntity recipientEntity = new RecipientEntity();

        return recipientEntity;
    }

    @Override
    public Recipient recipientEntityToRecipient(RecipientEntity recipientEntity) {
        if ( recipientEntity == null ) {
            return null;
        }

        Recipient recipient = new Recipient();

        return recipient;
    }

    @Override
    public PropertyCommentEntity commentToCommentEntity(PropertyComment propertyComment) {
        if ( propertyComment == null ) {
            return null;
        }

        PropertyCommentEntity propertyCommentEntity = new PropertyCommentEntity();

        propertyCommentEntity.setId( propertyComment.getId() );
        propertyCommentEntity.setText( propertyComment.getText() );
        propertyCommentEntity.setCreatedBy( propertyComment.getCreatedBy() );
        propertyCommentEntity.setCreatedAt( propertyComment.getCreatedAt() );

        return propertyCommentEntity;
    }

    @Override
    public PropertyComment commentEntityToComment(PropertyCommentEntity propertyComment) {
        if ( propertyComment == null ) {
            return null;
        }

        PropertyComment propertyComment1 = new PropertyComment();

        propertyComment1.setId( propertyComment.getId() );
        propertyComment1.setText( propertyComment.getText() );
        propertyComment1.setCreatedBy( propertyComment.getCreatedBy() );
        propertyComment1.setCreatedAt( propertyComment.getCreatedAt() );

        return propertyComment1;
    }

    @Override
    public FileObjectEntity fileObjectToFileObjectEntity(FileObject fileObject) {
        if ( fileObject == null ) {
            return null;
        }

        FileObjectEntity fileObjectEntity = new FileObjectEntity();

        fileObjectEntity.setId( fileObject.getId() );
        fileObjectEntity.setFileName( fileObject.getFileName() );
        fileObjectEntity.setFileUrl( fileObject.getFileUrl() );
        fileObjectEntity.setTag( fileObject.getTag() );
        fileObjectEntity.setOrigin( fileObject.getOrigin() );
        fileObjectEntity.setMediaCategory( fileObject.getMediaCategory() );
        fileObjectEntity.setMimeType( fileObject.getMimeType() );
        fileObjectEntity.setCreatedAt( fileObject.getCreatedAt() );

        return fileObjectEntity;
    }

    @Override
    public FileObject fileObjectEntityToFileObject(FileObjectEntity dealEntity) {
        if ( dealEntity == null ) {
            return null;
        }

        FileObject fileObject = new FileObject();

        fileObject.setId( dealEntity.getId() );
        fileObject.setFileName( dealEntity.getFileName() );
        fileObject.setFileUrl( dealEntity.getFileUrl() );
        fileObject.setTag( dealEntity.getTag() );
        fileObject.setOrigin( dealEntity.getOrigin() );
        fileObject.setMediaCategory( dealEntity.getMediaCategory() );
        fileObject.setMimeType( dealEntity.getMimeType() );
        fileObject.setCreatedAt( dealEntity.getCreatedAt() );

        return fileObject;
    }

    @Override
    public PropertyGeneral propertyToPropertyGeneral(PropertyOnMarket propertyOnMarket) {
        if ( propertyOnMarket == null ) {
            return null;
        }

        PropertyGeneral propertyGeneral = new PropertyGeneral();

        propertyGeneral.setUuid( propertyOnMarket.getUuid() );
        propertyGeneral.setAddress( propertyOnMarket.getAddress() );
        propertyGeneral.setCreatedAt( propertyOnMarket.getCreatedAt() );
        propertyGeneral.setUpdatedAt( propertyOnMarket.getUpdatedAt() );

        return propertyGeneral;
    }

    @Override
    public Underwriting dealToUnderwriting(Deal deal) {
        if ( deal == null ) {
            return null;
        }

        Underwriting underwriting = new Underwriting();

        if ( deal.getPrice() != null ) {
            underwriting.setPrice( BigDecimal.valueOf( deal.getPrice() ) );
        }
        underwriting.setOccupancyStatus( deal.getOccupancyStatus() );
        underwriting.setOccupancyStatusAtClosing( deal.getOccupancyStatusAtClosing() );
        underwriting.setInspectionBeforeOffer( deal.getInspectionBeforeOffer() );
        underwriting.setContractType( deal.getContractType() );

        return underwriting;
    }

    @Override
    public PropertyDetails propertyToPropertyDetails(PropertyOnMarket propertyOnMarket, Deal deal) {
        if ( propertyOnMarket == null && deal == null ) {
            return null;
        }

        PropertyDetails propertyDetails = new PropertyDetails();

        if ( propertyOnMarket != null ) {
            propertyDetails.setBedrooms( propertyOnMarket.getBedrooms() );
            propertyDetails.setAreaSqFt( propertyOnMarket.getAreaSqFt() );
            propertyDetails.setYearBuilt( propertyOnMarket.getYearBuilt() );
            propertyDetails.setBathrooms( propertyOnMarket.getBathrooms() );
            propertyDetails.setHalfBathrooms( propertyOnMarket.getHalfBathrooms() );
            propertyDetails.setOneQuarterBathrooms( propertyOnMarket.getOneQuarterBathrooms() );
            propertyDetails.setThreeQuarterBathrooms( propertyOnMarket.getThreeQuarterBathrooms() );
            propertyDetails.setPropertyType( propertyOnMarket.getPropertyType() );
            propertyDetails.setLink( propertyOnMarket.getLink() );
            List<PropertyComment> list1 = propertyOnMarket.getComments();
            if ( list1 != null ) {
                propertyDetails.setComments( new ArrayList<PropertyComment>( list1 ) );
            }
        }
        if ( deal != null ) {
            List<FileObject> list = deal.getFiles();
            if ( list != null ) {
                propertyDetails.setPhotos( new ArrayList<FileObject>( list ) );
            }
        }

        return propertyDetails;
    }

    protected Collection<DealEntity> dealListToDealEntityCollection(List<Deal> list) {
        if ( list == null ) {
            return null;
        }

        Collection<DealEntity> collection = new ArrayList<DealEntity>( list.size() );
        for ( Deal deal : list ) {
            collection.add( dealToDealEntity( deal ) );
        }

        return collection;
    }

    protected PropertyCommentEntityList propertyCommentListToPropertyCommentEntityList(List<PropertyComment> list) {
        if ( list == null ) {
            return null;
        }

        PropertyCommentEntityList propertyCommentEntityList = new PropertyCommentEntityList();
        for ( PropertyComment propertyComment : list ) {
            propertyCommentEntityList.add( commentToCommentEntity( propertyComment ) );
        }

        return propertyCommentEntityList;
    }

    protected List<PropertyComment> propertyCommentEntityListToPropertyCommentList(PropertyCommentEntityList propertyCommentEntityList) {
        if ( propertyCommentEntityList == null ) {
            return null;
        }

        List<PropertyComment> list = new ArrayList<PropertyComment>( propertyCommentEntityList.size() );
        for ( PropertyCommentEntity propertyCommentEntity : propertyCommentEntityList ) {
            list.add( commentEntityToComment( propertyCommentEntity ) );
        }

        return list;
    }

    private Integer propertyOnMarketLatestDealPrice(PropertyOnMarket propertyOnMarket) {
        if ( propertyOnMarket == null ) {
            return null;
        }
        Deal latestDeal = propertyOnMarket.getLatestDeal();
        if ( latestDeal == null ) {
            return null;
        }
        Integer price = latestDeal.getPrice();
        if ( price == null ) {
            return null;
        }
        return price;
    }

    protected PortfolioEntity portfolioToPortfolioEntity(Portfolio portfolio) {
        if ( portfolio == null ) {
            return null;
        }

        PortfolioEntity portfolioEntity = new PortfolioEntity();

        portfolioEntity.setId( portfolio.getId() );
        portfolioEntity.setCreatedAt( portfolio.getCreatedAt() );
        portfolioEntity.setUpdatedAt( portfolio.getUpdatedAt() );
        portfolioEntity.setStatus( portfolio.getStatus() );
        portfolioEntity.setName( portfolio.getName() );

        return portfolioEntity;
    }

    protected FileObjectEntityList fileObjectListToFileObjectEntityList(List<FileObject> list) {
        if ( list == null ) {
            return null;
        }

        FileObjectEntityList fileObjectEntityList = new FileObjectEntityList();
        for ( FileObject fileObject : list ) {
            fileObjectEntityList.add( fileObjectToFileObjectEntity( fileObject ) );
        }

        return fileObjectEntityList;
    }

    protected Portfolio portfolioEntityToPortfolio(PortfolioEntity portfolioEntity) {
        if ( portfolioEntity == null ) {
            return null;
        }

        String name = null;

        name = portfolioEntity.getName();

        Portfolio portfolio = new Portfolio( name );

        portfolio.setId( portfolioEntity.getId() );
        portfolio.setStatus( portfolioEntity.getStatus() );
        portfolio.setCreatedAt( portfolioEntity.getCreatedAt() );
        portfolio.setUpdatedAt( portfolioEntity.getUpdatedAt() );

        return portfolio;
    }

    protected List<FileObject> fileObjectEntityListToFileObjectList(FileObjectEntityList fileObjectEntityList) {
        if ( fileObjectEntityList == null ) {
            return null;
        }

        List<FileObject> list = new ArrayList<FileObject>( fileObjectEntityList.size() );
        for ( FileObjectEntity fileObjectEntity : fileObjectEntityList ) {
            list.add( fileObjectEntityToFileObject( fileObjectEntity ) );
        }

        return list;
    }
}
