package com.tiber.properties.exception;

import com.tiber.common.exception.InvalidStateException;

public class UnacceptableDealStatusException extends InvalidStateException {

    public UnacceptableDealStatusException(String status) {
        super(String.format("Deal have status: %s.", status));
        this.addContextValue("Deal Status", status);
    }

    public UnacceptableDealStatusException(String message, Throwable cause) {
        super(message, cause);
    }
}
