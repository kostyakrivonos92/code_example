package com.tiber.properties.exception;

import com.tiber.common.exception.NotFoundException;

public class DealNotFoundInPropertyException extends NotFoundException {

    public DealNotFoundInPropertyException(String propertyUuid, String dealUuid) {
        super("Deal not found");
        this.addContextValue("Property UUID", propertyUuid)
                .addContextValue("Deal UUID", dealUuid);
    }

    public DealNotFoundInPropertyException(String message) {
        super(message);
    }

    public DealNotFoundInPropertyException(String message, Throwable cause) {
        super(message, cause);
    }
}
