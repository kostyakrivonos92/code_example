package com.tiber.properties.repository;

import com.tiber.properties.entity.PropertyOnMarketEntity;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import java.util.Optional;


@Repository
public interface PropertyOnMarketRepository extends JpaRepository<PropertyOnMarketEntity, Long>, JpaSpecificationExecutor<PropertyOnMarketEntity> {

    Optional<PropertyOnMarketEntity> findByFullAddress(String fullAddress);

    Optional<PropertyOnMarketEntity> findByUuid(String uuid);

    @Query("from PropertyOnMarketEntity p join p.deals d where d.uuid = :dealUuid")
    Optional<PropertyOnMarketEntity> findByDealsContainsDealUuid(String dealUuid);
}
