package com.tiber.properties.repository;

import com.tiber.properties.entity.PortfolioEntity;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.Optional;

@Repository
public interface PortfolioRepository extends JpaRepository<PortfolioEntity, String> {

    Optional<PortfolioEntity> findByName(String portfolioName);
}
