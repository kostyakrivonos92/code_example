package com.tiber.properties.repository;

import com.tiber.properties.entity.DealEntity;
import com.tiber.properties.entity.PropertyOnMarketEntity;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Optional;


@Repository
public interface DealRepository extends JpaRepository<DealEntity, Long> {

    Optional<DealEntity> findByUuid(String uuid);

    List<DealEntity> findByPropertyOrderByUpdatedAtDesc(PropertyOnMarketEntity property);

    Optional<DealEntity> findTopByUuidIsNotNullOrderBySourceModificationTimestampDesc();
}
