package com.tiber.properties.specification;

import com.tiber.properties.entity.DealEntity;
import com.tiber.properties.entity.PropertyOnMarketEntity;
import com.tiber.shared.searchCriteria.domain.BaseSearchCriteria;
import com.tiber.shared.vocabulary.Label;
import com.tiber.shared.vocabulary.DealStatus;
import org.springframework.data.jpa.domain.Specification;

import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Join;
import javax.persistence.criteria.JoinType;
import javax.persistence.criteria.Predicate;
import javax.persistence.criteria.Root;
import java.io.Serial;
import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.Objects;
import java.util.Set;

import static com.tiber.shared.vocabulary.PropertyParameterName.*;

public class PropertyOnMarketSpecification implements Specification<PropertyOnMarketEntity> {

    @Serial
    private static final long serialVersionUID = -7425446816460766515L;

    private final BaseSearchCriteria baseSearchCriteria;

    public PropertyOnMarketSpecification(BaseSearchCriteria baseSearchCriteria) {
        this.baseSearchCriteria = baseSearchCriteria;
    }

    @Override
    public Predicate toPredicate(Root<PropertyOnMarketEntity> root, CriteriaQuery<?> criteriaQuery, CriteriaBuilder criteriaBuilder) {
        List<Predicate> predicates = new ArrayList<>();

        Label label = baseSearchCriteria.getLabel();
        // TODO: This filtering condition should be refactored and overwrited, filtering should depend on latest deal only
        if (!Objects.isNull(label)) {

            if (label == Label.ACTIVE) {
                Join<PropertyOnMarketEntity, DealEntity> deals = root.join("deals", JoinType.INNER);
                predicates.add(criteriaBuilder.isNull(deals.get("claimedByAnalystUuid")));
                predicates.add(criteriaBuilder.equal(deals.get("status"), DealStatus.ACTIVE));
            }

            if (label == Label.ALL) {
                Join<PropertyOnMarketEntity, DealEntity> deals = root.join("deals", JoinType.LEFT);
                predicates.add(criteriaBuilder.isNull(deals.get("claimedByAnalystUuid")));
            }

            if (label == Label.UNDERWRITTEN) {
                Join<PropertyOnMarketEntity, DealEntity> deals = root.join("deals", JoinType.INNER);
                predicates.add(criteriaBuilder.isNotNull(deals.get("claimedByAnalystUuid")));
                predicates.add(criteriaBuilder.equal(deals.get("status"), DealStatus.ACTIVE));
            }

            if (label == Label.OWNED) {
                Join<PropertyOnMarketEntity, DealEntity> deals = root.join("deals", JoinType.INNER);
                predicates.add(criteriaBuilder.equal(deals.get("status"), DealStatus.CLOSED));
            }
        }

        predicates.addAll(getEqualMapPredicates(root, criteriaBuilder));
        predicates.addAll(getNotNullMapPredicates(root, criteriaBuilder));
        predicates.addAll(getGreaterThanOrEqualPredicates(root, criteriaBuilder));
        predicates.addAll(getLessThanOrEqualPredicates(root, criteriaBuilder));
        predicates.addAll(getLikePredicates(root, criteriaBuilder));

        return criteriaBuilder.and(predicates.toArray(new Predicate[0]));
    }

    private List<Predicate> getEqualMapPredicates(Root<PropertyOnMarketEntity> root, CriteriaBuilder criteriaBuilder) {
        List<Predicate> equalPredicates = new ArrayList<>();
        Map<String, String> equalMap = baseSearchCriteria.getEqualMap();
        if (equalMap.isEmpty()) {
            return equalPredicates;
        }

        String address = equalMap.get(FULL_ADDRESS);
        if (!Objects.isNull(address)) {
            equalPredicates.add(criteriaBuilder.equal(criteriaBuilder.lower(root.get(FULL_ADDRESS)), address.toLowerCase()));
        }

        address = equalMap.get(ADDRESS);
        if (!Objects.isNull(address)) {
            equalPredicates.add(criteriaBuilder.equal(criteriaBuilder.lower(root.get(ADDRESS)), address.toLowerCase()));
        }

        String state = equalMap.get(STATE);
        if (!Objects.isNull(state)) {
            equalPredicates.add(criteriaBuilder.equal(criteriaBuilder.lower(root.get(STATE)), state));
        }

        String city = equalMap.get(CITY);
        if (!Objects.isNull(city)) {
            equalPredicates.add(criteriaBuilder.equal(criteriaBuilder.lower(root.get(CITY)), city));
        }

        String zip = equalMap.get(ZIP);
        if (!Objects.isNull(zip)) {
            equalPredicates.add(criteriaBuilder.equal(criteriaBuilder.lower(root.get(ZIP)), zip));
        }

        String bedrooms = equalMap.get(BEDROOMS);
        if (!Objects.isNull(bedrooms)) {
            Integer bedroomsNumber = Integer.valueOf(bedrooms);
            equalPredicates.add(criteriaBuilder.equal(root.get(BEDROOMS), bedroomsNumber));
        }

        String bathrooms = equalMap.get(BATHROOMS);
        if (!Objects.isNull(bathrooms)) {
            Integer bathroomsNumber = Integer.valueOf(bathrooms);
            equalPredicates.add(criteriaBuilder.equal(root.get(BATHROOMS), bathroomsNumber));
        }

        String tag = equalMap.get(TAG);
        if (!Objects.isNull(tag)) {
            equalPredicates.add(criteriaBuilder.equal(root.get(TAG), tag));
        }
        return equalPredicates;
    }

    private List<Predicate> getNotNullMapPredicates(Root<PropertyOnMarketEntity> root, CriteriaBuilder criteriaBuilder) {
        List<Predicate> equalPredicates = new ArrayList<>();
        Set<String> notNullMap = baseSearchCriteria.getNotNullSet();
        if (notNullMap.isEmpty()) {
            return equalPredicates;
        }

        if (notNullMap.contains(RESPONSIBLE_ANALYST_UUID)) {
            Join<PropertyOnMarketEntity, DealEntity> deals = root.join("deals", JoinType.INNER);
            equalPredicates.add(criteriaBuilder.isNotNull(deals.get(RESPONSIBLE_ANALYST_UUID))
            );
        }
        return equalPredicates;
    }

    private List<Predicate> getGreaterThanOrEqualPredicates(Root<PropertyOnMarketEntity> root, CriteriaBuilder criteriaBuilder) {
        List<Predicate> greaterThanOrEqualPredicates = new ArrayList<>();
        Map<String, String> greaterThanOrEqualMap = baseSearchCriteria.getGreaterThanOrEqualMap();
        if (greaterThanOrEqualMap.isEmpty()) {
            return greaterThanOrEqualPredicates;
        }

        String latitude = greaterThanOrEqualMap.get(LATITUDE);
        if (!Objects.isNull(latitude)) {
            greaterThanOrEqualPredicates.add(criteriaBuilder.greaterThanOrEqualTo(root.get(LATITUDE),
                    Double.valueOf(latitude)));
        }

        String longitude = greaterThanOrEqualMap.get(LONGITUDE);
        if (!Objects.isNull(longitude)) {
            greaterThanOrEqualPredicates.add(criteriaBuilder.greaterThanOrEqualTo(root.get(LONGITUDE),
                    Double.valueOf(longitude)));
        }

        String areaSquareFeet = greaterThanOrEqualMap.get(SQUARE_FEET);
        if (!Objects.isNull(areaSquareFeet)) {
            greaterThanOrEqualPredicates.add(criteriaBuilder.greaterThanOrEqualTo(root.get(SQUARE_FEET),
                    Integer.valueOf(areaSquareFeet)));
        }

        String yearBuilt = greaterThanOrEqualMap.get(YEAR_BUILT);
        if (!Objects.isNull(yearBuilt)) {
            greaterThanOrEqualPredicates.add(criteriaBuilder.greaterThanOrEqualTo(root.get(YEAR_BUILT),
                    Integer.valueOf(yearBuilt)));
        }

        //TODO filter old deals
        String earliestCloseDate = greaterThanOrEqualMap.get(CLOSE_DATE);
        if (!Objects.isNull(earliestCloseDate)) {
            Join<PropertyOnMarketEntity, DealEntity> deals = root.join("deals", JoinType.INNER);
            greaterThanOrEqualPredicates.add(criteriaBuilder.greaterThanOrEqualTo(
                    deals.get(CLOSE_DATE),
                    LocalDate.parse(earliestCloseDate))
            );
        }
        return greaterThanOrEqualPredicates;
    }

    private List<Predicate> getLessThanOrEqualPredicates(Root<PropertyOnMarketEntity> root,
                                                         CriteriaBuilder criteriaBuilder) {
        List<Predicate> lessThanOrEqualPredicates = new ArrayList<>();
        Map<String, String> lessThanOrEqualMap = baseSearchCriteria.getLessThanOrEqualMap();
        if (lessThanOrEqualMap.isEmpty()) {
            return lessThanOrEqualPredicates;
        }

        String latitude = lessThanOrEqualMap.get(LATITUDE);
        if (!Objects.isNull(latitude)) {
            lessThanOrEqualPredicates.add(criteriaBuilder.lessThanOrEqualTo(root.get(LATITUDE),
                    Double.valueOf(latitude)));
        }

        String longitude = lessThanOrEqualMap.get(LONGITUDE);
        if (!Objects.isNull(longitude)) {
            lessThanOrEqualPredicates.add(criteriaBuilder.lessThanOrEqualTo(root.get(LONGITUDE),
                    Double.valueOf(longitude)));
        }

        String areaSquareFeet = lessThanOrEqualMap.get(SQUARE_FEET);
        if (!Objects.isNull(areaSquareFeet)) {
            lessThanOrEqualPredicates.add(criteriaBuilder.lessThanOrEqualTo(root.get(SQUARE_FEET),
                    Integer.valueOf(areaSquareFeet)));
        }

        String yearBuilt = lessThanOrEqualMap.get(YEAR_BUILT);
        if (!Objects.isNull(yearBuilt)) {
            lessThanOrEqualPredicates.add(criteriaBuilder.lessThanOrEqualTo(root.get(YEAR_BUILT),
                    Integer.valueOf(yearBuilt)));
        }

        String latestCloseDate = lessThanOrEqualMap.get(CLOSE_DATE);
        if (!Objects.isNull(latestCloseDate)) {
            Join<PropertyOnMarketEntity, DealEntity> deals = root.join("deals", JoinType.INNER);
            lessThanOrEqualPredicates.add(criteriaBuilder.lessThanOrEqualTo(
                    deals.get(CLOSE_DATE),
                    LocalDate.parse(latestCloseDate))
            );
        }
        return lessThanOrEqualPredicates;
    }

    private List<Predicate> getLikePredicates(Root<PropertyOnMarketEntity> root, CriteriaBuilder criteriaBuilder) {
        List<Predicate> likePredicates = new ArrayList<>();
        Map<String, String> likeMap = baseSearchCriteria.getLikeMap();
        String address = likeMap.get(FULL_ADDRESS);
        if (!Objects.isNull(address)) {
            likePredicates.add(criteriaBuilder.like(criteriaBuilder.lower(root.get(FULL_ADDRESS)), address.toLowerCase() + "%"));
        }
        return likePredicates;
    }
}
