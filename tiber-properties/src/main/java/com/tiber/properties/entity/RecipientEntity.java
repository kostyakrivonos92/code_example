package com.tiber.properties.entity;

import lombok.Data;

import java.io.Serial;
import java.io.Serializable;

@Data
public class RecipientEntity implements Serializable {

    @Serial
    private static final long serialVersionUID = 9201648542675641000L;

    private String fullName;

    private String phoneNumber;

    private String email;
}
