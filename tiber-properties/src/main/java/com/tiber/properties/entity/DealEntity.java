package com.tiber.properties.entity;

import com.tiber.shared.file.entity.FileObjectEntityList;
import com.tiber.shared.file.entity.FileObjectEntityListType;
import com.tiber.shared.status.entity.StatusEventEntityListType;
import com.tiber.shared.util.AnalogueRenovationTag;
import com.tiber.shared.vocabulary.ContractType;
import com.tiber.shared.vocabulary.DealStatus;
import com.tiber.shared.vocabulary.InspectionBeforeOffer;
import com.tiber.shared.vocabulary.OccupancyStatus;
import com.tiber.shared.vocabulary.Origin;
import lombok.Getter;
import lombok.Setter;
import org.hibernate.annotations.DynamicInsert;
import org.hibernate.annotations.Type;
import org.hibernate.annotations.TypeDef;
import org.springframework.data.annotation.CreatedDate;
import org.springframework.data.annotation.LastModifiedDate;
import org.springframework.data.jpa.domain.support.AuditingEntityListener;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EntityListeners;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import java.io.Serial;
import java.io.Serializable;
import java.time.Instant;
import java.time.LocalDate;

@Setter
@Getter
@Entity
@DynamicInsert
@EntityListeners(AuditingEntityListener.class)
@TypeDef(name = "RecipientEntityType", typeClass = RecipientEntityType.class)
@TypeDef(name = "FileObjectEntityListType", typeClass = FileObjectEntityListType.class)
@TypeDef(name = "StatusEventEntityListType", typeClass = StatusEventEntityListType.class)
@Table(name = "deal", schema = "property")
public class DealEntity implements Serializable {

    @Serial
    private static final long serialVersionUID = 4042122102331773210L;

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "property_id", nullable = false)
    private PropertyOnMarketEntity property;

    private String uuid;

    private String publicSource;

    private String responsibleAnalystUuid;

    private String claimedByAnalystUuid;

    @Enumerated(EnumType.STRING)
    private DealStatus status;

    private String agentUuid;

    private Boolean isForSale;

    @Enumerated(EnumType.STRING)
    private AnalogueRenovationTag renovationTag;

    private Instant sourceModificationTimestamp;

    @ManyToOne
    @JoinColumn(nullable = false)
    private PortfolioEntity portfolio;

    @Enumerated(EnumType.STRING)
    private Origin origin;

    private Integer price;

    private Integer closePrice;

    private LocalDate onMarketDate;

    private LocalDate closeDate;

    private Integer cumulativeDaysOnMarket;

    private Integer daysOnMarket;

    @Column(columnDefinition = "json")
    @Type(type = "FileObjectEntityListType")
    private FileObjectEntityList files;

    @CreatedDate
    private Instant createdAt;

    @LastModifiedDate
    private Instant updatedAt;

    @Enumerated(EnumType.STRING)
    private OccupancyStatus occupancyStatus;

    @Enumerated(EnumType.STRING)
    private OccupancyStatus occupancyStatusAtClosing;

    @Enumerated(EnumType.STRING)
    private InspectionBeforeOffer inspectionBeforeOffer;

    @Enumerated(EnumType.STRING)
    private ContractType contractType;

    @Column(columnDefinition = "json")
    @Type(type = "RecipientEntityType")
    private RecipientEntity recipient;
}
