package com.tiber.properties.entity;

import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.tiber.shared.util.InstantDeserializer;
import com.tiber.shared.util.InstantSerializer;
import lombok.Data;

import java.io.Serial;
import java.io.Serializable;
import java.time.Instant;

@Data
public class PropertyCommentEntity implements Serializable {

    @Serial
    private  static final long serialVersionUID = -7510094155158119451L;

    private int id;

    private String text;

    private String createdBy;

    @JsonDeserialize(using = InstantDeserializer.class)
    @JsonSerialize(using = InstantSerializer.class)
    private Instant createdAt;
}
