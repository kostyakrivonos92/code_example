package com.tiber.properties.entity;

import java.io.Serial;
import java.io.Serializable;
import java.util.ArrayList;

public class PropertyCommentEntityList extends ArrayList<PropertyCommentEntity> implements Serializable {

    @Serial
    private static final long serialVersionUID = 1467735770176279817L;
}
