package com.tiber.properties.entity;

import com.tiber.shared.user.entity.ModifiableEntity;
import lombok.Data;
import org.hibernate.annotations.DynamicInsert;
import org.springframework.data.annotation.CreatedBy;
import org.springframework.data.annotation.LastModifiedBy;

import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import java.io.Serial;
import java.util.Collection;

@Data
@Entity
@DynamicInsert
@Table(name = "[portfolio]", schema = "[property]")
public class PortfolioEntity extends ModifiableEntity<Integer> {

    @Serial
    private static final long serialVersionUID = 7546719037851591420L;

    @OneToMany(mappedBy = "portfolio", fetch = FetchType.LAZY)
    private Collection<DealEntity> deal;

    private String name;

    @CreatedBy
    protected String createdBy;

    @LastModifiedBy
    protected String updatedBy;
}
