package com.tiber.properties.entity;

import com.tiber.shared.vocabulary.TiberStatus;
import lombok.Getter;
import lombok.Setter;
import org.hibernate.annotations.DynamicInsert;
import org.hibernate.annotations.Generated;
import org.hibernate.annotations.GenerationTime;
import org.hibernate.annotations.Type;
import org.hibernate.annotations.TypeDef;
import org.springframework.data.annotation.CreatedDate;
import org.springframework.data.annotation.LastModifiedDate;
import org.springframework.data.jpa.domain.support.AuditingEntityListener;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EntityListeners;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import java.io.Serial;
import java.io.Serializable;
import java.time.Instant;
import java.util.Collection;


@Setter
@Getter
@Entity
@DynamicInsert
@EntityListeners(AuditingEntityListener.class)
@TypeDef(name = "PropertyCommentEntityListType", typeClass = PropertyCommentEntityListType.class)
@Table(name = "[property_on_market]", schema = "[property]")
public class PropertyOnMarketEntity implements Serializable {

    @Serial
    private static final long serialVersionUID = -4914249007460569193L;

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    protected Long id;

    private String uuid;

    private String address;

    private String fullAddress;

    private String city;

    private String state;

    private String zip;

    private String msa;

    private String county;

    private Integer bedrooms;

    private Integer bathrooms;

    private Integer oneQuarterBathrooms;

    private Integer halfBathrooms;

    private Integer threeQuarterBathrooms;

    private Integer areaSqFt;

    private Integer yearBuilt;

    private double latitude;

    private double longitude;

    private String propertyType;

    private String propertySubType;

    @Enumerated(EnumType.STRING)
    @Generated(value = GenerationTime.INSERT)
    private TiberStatus tiberStatus;

    private Instant statusChangeDate;

    @OneToMany(mappedBy = "property", cascade = CascadeType.ALL, fetch = FetchType.EAGER)
    private Collection<DealEntity> deals;

    private String link;

    @Column(columnDefinition = "json")
    @Type(type = "PropertyCommentEntityListType")
    private PropertyCommentEntityList comments;

    @CreatedDate
    protected Instant createdAt;

    @LastModifiedDate
    protected Instant updatedAt;
}
