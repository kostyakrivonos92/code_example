package com.tiber.properties.pageable;

import lombok.Getter;
import org.springframework.util.Assert;

import java.util.List;
import java.util.function.Function;
import java.util.stream.Collectors;

@Getter
public class CustomPage<T> {

    private final int limit;

    private final long offset;

    private final List<T> content;

    public CustomPage(int limit, long offset, List<T> content) {
        this.limit = limit;
        this.offset = offset;
        this.content = content;
    }

    /**
     * Returns a new {@link CustomPage} with the content of the current one mapped by the given {@link Function}.
     *
     * @param converter must not be {@literal null}.
     * @return a new {@link CustomPage} with the content of the current one mapped by the given {@link Function}.
     */
    public <U> CustomPage<U> map(Function<? super T, ? extends U> converter) {
        return new CustomPage<>(limit, offset, getConvertedContent(converter));
    }

    protected <U> List<U> getConvertedContent(Function<? super T, ? extends U> converter) {

        Assert.notNull(converter, "Function must not be null!");

        return content.stream().map(converter).collect(Collectors.toList());
    }
}
