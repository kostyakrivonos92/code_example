package com.tiber.properties.service;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.tiber.properties.specification.PropertyOnMarketSpecification;
import com.tiber.properties.mapper.PropertySourceMappingService;
import com.tiber.shared.file.domain.FileObject;
import com.tiber.shared.file.domain.MlsMedia;
import com.tiber.shared.propertylayer.domain.Deal;
import com.tiber.shared.propertylayer.domain.PropertyGeneral;
import com.tiber.shared.propertylayer.domain.PropertyOnMarket;
import com.tiber.shared.propertylayer.domain.PropertySummaryPage;
import com.tiber.shared.searchCriteria.domain.PagedBaseSearchCriteria;
import com.tiber.shared.status.domain.StatusEvent;
import com.tiber.shared.util.AnalogueRenovationTag;
import com.tiber.shared.vocabulary.DealStatus;
import com.tiber.shared.vocabulary.Origin;
import io.reactivex.rxjava3.annotations.NonNull;
import io.reactivex.rxjava3.core.Flowable;
import io.reactivex.rxjava3.core.Maybe;
import io.reactivex.rxjava3.core.Single;
import org.javatuples.Pair;
import org.springframework.lang.Nullable;

import java.nio.file.Path;
import java.util.List;
import java.util.Map;
import java.util.Optional;

/**
 * @deprecated TODO redesign according new vision
 */
public interface PropertyGeneralService {

    @NonNull PropertyOnMarket save(PropertyOnMarket propertyOnMarket);

    @NonNull Maybe<PropertyOnMarket> getPropertyByUuid(String propertyUuid);

    @NonNull Maybe<PropertyGeneral> getPropertyGeneral(String propertyUuid);

    @NonNull Maybe<PropertyOnMarket> getPropertyByDeal(String dealUuid);

    @NonNull Flowable<PropertyOnMarket> getProperties(PropertyOnMarketSpecification specification,
                                                      @Nullable Integer page,
                                                      @Nullable Integer pageSize,
                                                      @Nullable String sortOrder,
                                                      @Nullable String sortBy);

    Single<PropertySummaryPage> getPropertySummaryPage(PagedBaseSearchCriteria criteria);

    @NonNull Single<Long> getPropertiesCount(PropertyOnMarketSpecification specification);

    @NonNull Flowable<PropertyOnMarket> savePublic(Flowable<PropertyOnMarket> publicJsonArray, Origin origin);

    @NonNull Flowable<PropertyOnMarket> savePublicWithMedia(Flowable<Pair<PropertyOnMarket, List<MlsMedia>>> publicJsonArray, Origin origin);

    @NonNull Flowable<PropertyOnMarket> savePublicProperty(Flowable<PropertyOnMarket> propertyOnMarketFlowable);

    @NonNull List<FileObject> addPropertyPhoto(String propertyUuid, Map<String, Path> temporaryPathToFileMap);

    @NonNull List<FileObject> updatePropertyPhoto(String propertyUuid, Map<String, Path> temporaryPathToFileMap, List<Integer> photoIdForDeleting);

    @NonNull Optional<PropertyOnMarket> createFromSourceObject(Object publicSource, PropertySourceMappingService propertySourceMappingService) throws JsonProcessingException;

    @NonNull Maybe<PropertyOnMarket> applyLatestDealStatusEvent(String propertyUuid, StatusEvent<DealStatus> statusEvent);

    @NonNull Deal updateDealRenovationTag(String propertyUuid, String dealUuid, AnalogueRenovationTag renovationTag);
}
