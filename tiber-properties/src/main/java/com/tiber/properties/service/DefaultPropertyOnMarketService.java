package com.tiber.properties.service;

import com.tiber.properties.repository.PropertyOnMarketRepository;
import com.tiber.properties.mapper.PropertyGeneralMapper;
import com.tiber.shared.property.service.PropertyOnMarketService;
import com.tiber.shared.propertylayer.domain.PropertyOnMarket;
import java.util.Optional;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

/*
    TODO: this service should be refactored or eliminated. Theis is temporary solution to replace RX-based implementation
 */

@Service
@RequiredArgsConstructor
public class DefaultPropertyOnMarketService implements PropertyOnMarketService {

    private final PropertyOnMarketRepository propertyOnMarketRepository;

    private final PropertyGeneralMapper propertyMapper;

    @Override
    public Optional<PropertyOnMarket> getPropertyByDealUuid(String dealUuid) {
        return propertyOnMarketRepository.findByDealsContainsDealUuid(dealUuid)
                .map(propertyMapper::propertyOnMarketEntityToPropertyOnMarket);
    }
}
