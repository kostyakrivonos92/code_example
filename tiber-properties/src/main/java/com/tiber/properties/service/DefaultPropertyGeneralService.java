package com.tiber.properties.service;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.tiber.properties.integration.bridgeapi.domain.BridgeMlsProperty;
import com.tiber.properties.exception.DealNotFoundInPropertyException;
import com.tiber.properties.repository.DealRepository;
import com.tiber.properties.repository.PropertyOnMarketRepository;
import com.tiber.properties.specification.PropertyOnMarketSpecification;
import com.tiber.properties.entity.DealEntity;
import com.tiber.properties.entity.PropertyOnMarketEntity;
import com.tiber.properties.mapper.FileObjectMapper;
import com.tiber.properties.mapper.PropertyGeneralMapper;
import com.tiber.properties.mapper.PropertySourceMappingService;
import com.tiber.shared.events.*;
import com.tiber.shared.events.property.DealClosedEvent;
import com.tiber.shared.exception.PropertyNotFoundException;
import com.tiber.shared.file.domain.FileObject;
import com.tiber.shared.file.domain.MlsMedia;
import com.tiber.shared.propertylayer.domain.Deal;
import com.tiber.shared.propertylayer.domain.Portfolio;
import com.tiber.shared.propertylayer.domain.PropertyGeneral;
import com.tiber.shared.propertylayer.domain.PropertyOnMarket;
import com.tiber.shared.propertylayer.domain.PropertySummaryPage;
import com.tiber.shared.propertylayer.event.PropertyGeneralByUuidRequestEvent;
import com.tiber.shared.propertylayer.event.PropertyGeneralCountResponseEvent;
import com.tiber.shared.propertylayer.event.PropertyGeneralRequestEvent;
import com.tiber.shared.propertylayer.event.PropertyGeneralValueResponseEvent;
import com.tiber.shared.propertylayer.event.PropertyLayerCompleteResponseEvent;
import com.tiber.shared.propertylayer.event.PropertyLayerEvent;
import com.tiber.shared.propertylayer.event.PropertyOnMarketByDealRequestEvent;
import com.tiber.shared.propertylayer.service.PropertyLayerService;
import com.tiber.shared.searchCriteria.domain.BaseSearchCriteria;
import com.tiber.shared.searchCriteria.domain.PagedBaseSearchCriteria;
import com.tiber.shared.status.domain.StatusEvent;
import com.tiber.shared.util.AnalogueRenovationTag;
import com.tiber.shared.vocabulary.ContractType;
import com.tiber.shared.vocabulary.DealStatus;
import com.tiber.shared.vocabulary.InspectionBeforeOffer;
import com.tiber.shared.vocabulary.OccupancyStatus;
import com.tiber.shared.vocabulary.Origin;
import io.reactivex.rxjava3.annotations.NonNull;
import io.reactivex.rxjava3.core.Flowable;
import io.reactivex.rxjava3.core.Maybe;
import io.reactivex.rxjava3.core.Observable;
import io.reactivex.rxjava3.core.Single;
import io.reactivex.rxjava3.schedulers.Schedulers;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.javatuples.Pair;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Sort;
import org.springframework.lang.Nullable;
import org.springframework.stereotype.Service;

import javax.annotation.PostConstruct;
import java.nio.file.Path;
import java.time.Instant;
import java.time.LocalDate;
import java.util.Collections;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.UUID;

/**
 * @deprecated TODO remove duplication code
 */
@Slf4j
@Service
@RequiredArgsConstructor(onConstructor_ = @Autowired)
class DefaultPropertyGeneralService implements PropertyGeneralService {

    private final PropertyOnMarketRepository propertyOnMarketRepository;

    private final DealRepository dealRepository;

    private final PortfolioService portfolioService;

    private final PropertyGeneralMapper propertyGeneralMapper;

    private final PropertyLayerService propertyLayerService;

    private final ModificationTimestampService<BridgeMlsProperty> modificationTimestampService;

    private final AddressService addressService;

    private final DealService dealService;

    private final FileObjectMapper fileObjectMapper;

    private final EventBus<PropertyLayerEvent> eventBus;

    private final ObjectMapper objectMapper = new ObjectMapper();

    private final TiberEventPublisher<BaseEvent<?>> eventPublisher;

    @PostConstruct
    private void init() {
        log.trace("Init property layer service");
        eventBus.events()
                .ofType(PropertyGeneralRequestEvent.class)
                .observeOn(Schedulers.io())
                .flatMap(propertyGeneralEvent -> {
                    BaseSearchCriteria criteria = propertyGeneralEvent.getCriteria();
                    PropertyOnMarketSpecification specification = new PropertyOnMarketSpecification(criteria);

                    Flowable<PropertyOnMarket> propertyOnMarketFlowable;

                    if (criteria instanceof PagedBaseSearchCriteria) {
                        propertyOnMarketFlowable = getProperties(specification,
                                ((PagedBaseSearchCriteria) criteria).getPage(),
                                ((PagedBaseSearchCriteria) criteria).getPageSize(),
                                ((PagedBaseSearchCriteria) criteria).getSort(),
                                ((PagedBaseSearchCriteria) criteria).getSortBy());
                    } else {
                        propertyOnMarketFlowable = getProperties(specification);
                    }

                    return Flowable.concat(
                                    getPropertiesCount(specification)
                                            .map(event1 -> new PropertyGeneralCountResponseEvent(propertyGeneralEvent.getId(), event1))
                                            .toFlowable(),
                                    propertyOnMarketFlowable
                                            .map(event -> new PropertyGeneralValueResponseEvent(propertyGeneralEvent.getId(), event))
                                            .doOnNext(event -> log.trace("send {}", event.getPropertyOnMarket())))
                            .doOnError(throwable -> log.error("Error retrieving property from repository {}", throwable.getLocalizedMessage()))
                            .onErrorComplete()
                            .doOnComplete(() -> eventBus.push(new PropertyLayerCompleteResponseEvent(propertyGeneralEvent.getId())));
                })
                .doOnNext(eventBus::push)
                .doOnError(throwable -> log.error("Error retrieving property {}", throwable.getLocalizedMessage()))
                .retry()
                .subscribe();

        eventBus.events()
                .ofType(PropertyGeneralByUuidRequestEvent.class)
                .observeOn(Schedulers.io())
                .flatMap(propertyGeneralEvent -> getPropertyByUuid(propertyGeneralEvent.getUuid())
                        .map(event -> new PropertyGeneralValueResponseEvent(propertyGeneralEvent.getId(), event))
                        .onErrorComplete()
                        .doOnComplete(() -> eventBus.push(new PropertyLayerCompleteResponseEvent(propertyGeneralEvent.getId())))
                        .toFlowable())
                .observeOn(Schedulers.computation())
                .doOnNext(eventBus::push)
                .doOnError(throwable -> log.error("Error retrieving property"))
                .retry()
                .subscribe();

        eventBus.events()
                .ofType(ClaimDealToUnderwriterRequestEvent.class)
                .observeOn(Schedulers.io())
                .flatMap(event -> getPropertyByUuid(event.getPropertyUuid())
                        .map(propertyOnMarket -> {
                            Deal latestDeal = propertyOnMarket.getLatestDeal();
                            if (latestDeal.getStatus().equals(DealStatus.ACTIVE)) {
                                latestDeal.claimToAnalystUuidByAnalystUuid(event.getResponsibleAnalystUuid(), event.getClaimedByAnalystUuid());
                                PropertyOnMarket propertyOnMarketUpdated = save(propertyOnMarket);
                                return new PropertyGeneralValueResponseEvent(event.getId(), propertyOnMarketUpdated);
                            }
                            return new PropertyLayerCompleteResponseEvent(event.getId());
                        })
                        .onErrorComplete()
                        .doOnComplete(() -> eventBus.push(new PropertyLayerCompleteResponseEvent(event.getId())))
                        .toFlowable())
                .observeOn(Schedulers.computation())
                .doOnNext(eventBus::push)
                .doOnError(throwable -> log.error("Error claiming property"))
                .retry()
                .subscribe();

        eventBus.events()
                .ofType(DealStatusChangeRequestEvent.class)
                .observeOn(Schedulers.io())
                .flatMap(event -> applyLatestDealStatusEvent(event.getPropertyUuid(), event.getStatusEvent())
                        .map(propertyOnMarket -> new PropertyGeneralValueResponseEvent(event.getId(), propertyOnMarket))
                        .onErrorComplete()
                        .doOnComplete(() -> eventBus.push(new PropertyLayerCompleteResponseEvent(event.getId())))
                        .toFlowable())
                .observeOn(Schedulers.computation())
                .doOnNext(eventBus::push)
                .doOnError(throwable -> log.error("Error claiming property"))
                .retry()
                .subscribe();

        eventBus.events()
                .ofType(PropertyOnMarketByDealRequestEvent.class)
                .observeOn(Schedulers.io())
                .flatMap(propertyGeneralEvent ->
                        getPropertyByDeal(propertyGeneralEvent.getDealUuid())
                                .map(event -> new PropertyGeneralValueResponseEvent(propertyGeneralEvent.getId(), event))
                                .onErrorComplete()
                                .doOnComplete(() -> eventBus.push(new PropertyLayerCompleteResponseEvent(propertyGeneralEvent.getId())))
                                .toFlowable())
                .observeOn(Schedulers.computation())
                .doOnNext(event1 -> Observable.defer(() -> Observable.just(event1)).subscribe(eventBus::push))
                .doOnError(throwable -> log.error("Error retrieving property"))
                .retry()
                .subscribe();
    }

    @Override
    public @NonNull PropertyOnMarket save(PropertyOnMarket propertyGeneral) {
        PropertyOnMarketEntity propertyOnMarketEntity = propertyGeneralMapper.propertyOnMarketToPropertyOnMarketEntity(propertyGeneral);
        propertyOnMarketEntity.getDeals().forEach(dealEntity -> dealEntity.setProperty(propertyOnMarketEntity));
        return propertyGeneralMapper.propertyOnMarketEntityToPropertyOnMarket(propertyOnMarketRepository.save(propertyOnMarketEntity));
    }

    @Override
    public @NonNull Maybe<PropertyOnMarket> getPropertyByUuid(String propertyUuid) {
        return Maybe.defer(() -> Maybe.fromOptional(propertyOnMarketRepository.findByUuid(propertyUuid))
                .map(propertyGeneralMapper::propertyOnMarketEntityToPropertyOnMarket));
    }

    @Override
    public @NonNull Maybe<PropertyGeneral> getPropertyGeneral(String propertyUuid) {
        PropertyOnMarket propertyOnMarket = propertyOnMarketRepository.findByUuid(propertyUuid)
                .map(propertyGeneralMapper::propertyOnMarketEntityToPropertyOnMarket)
                .orElseThrow(() -> new PropertyNotFoundException(propertyUuid));

        Deal deal = propertyOnMarket.getLatestDeal();

        PropertyGeneral propertyGeneral = propertyGeneralMapper.propertyToPropertyGeneral(propertyOnMarket);
        propertyGeneral.setPropertyDetails(propertyGeneralMapper.propertyToPropertyDetails(propertyOnMarket, deal));
        propertyGeneral.setUnderwriting(propertyGeneralMapper.dealToUnderwriting(deal));
        propertyGeneral.setOrigin(deal.getOrigin());
        return Maybe.just(propertyGeneral);
    }

    @Override
    public @NonNull Maybe<PropertyOnMarket> getPropertyByDeal(String dealUuid) {
        return Maybe.defer(() -> Maybe.fromOptional(propertyOnMarketRepository.findByDealsContainsDealUuid(dealUuid))
                .map(propertyGeneralMapper::propertyOnMarketEntityToPropertyOnMarket));
    }

    @Override
    public @NonNull Flowable<PropertyOnMarket> getProperties(PropertyOnMarketSpecification specification,
                                                             @Nullable Integer page,
                                                             @Nullable Integer pageSize,
                                                             @Nullable String sortDirectionString,
                                                             @Nullable String sortBy) {
        if (page == null || pageSize == null) {
            return getProperties(specification);
        }

        PageRequest pageRequest = (sortDirectionString != null && sortBy != null) ?
                PageRequest.of(page, pageSize, Sort.Direction.fromString(sortDirectionString.toUpperCase()), sortBy)
                : PageRequest.of(page, pageSize);

        return Flowable.defer(() -> Flowable.fromIterable(propertyOnMarketRepository
                .findAll(specification, pageRequest)
                .map(propertyGeneralMapper::propertyOnMarketEntityToPropertyOnMarket)));
    }

    private @NonNull Flowable<PropertyOnMarket> getProperties(PropertyOnMarketSpecification specification) {
        return Flowable.defer(() -> Flowable.fromIterable(propertyOnMarketRepository.findAll(specification))
                .map(propertyGeneralMapper::propertyOnMarketEntityToPropertyOnMarket));
    }

    @Override
    public Single<PropertySummaryPage> getPropertySummaryPage(PagedBaseSearchCriteria criteria) {
        PropertySummaryPage propertySummaryPage = new PropertySummaryPage(criteria.getPage(), criteria.getPageSize(), 0L, new LinkedList<>());
        Single<PropertySummaryPage> propertySummaryPageSingle = propertyLayerService.getPropertyPage(criteria)
                .map(propertyOnMarketPage -> {
                    propertySummaryPage.getProperties()
                            .addAll(propertyGeneralMapper.propertyOnMarketListToPropertySummaryList(propertyOnMarketPage.getProperties()));
                    propertySummaryPage.setMainCounter(propertyOnMarketPage.getMainCounter());
                    propertySummaryPage.setPage(propertyOnMarketPage.getPage());
                    propertySummaryPage.setPageSize(propertyOnMarketPage.getPageSize());
                    return propertySummaryPage;
                });
        return propertySummaryPageSingle;
    }

    @Override
    public @NonNull Single<Long> getPropertiesCount(PropertyOnMarketSpecification specification) {
        return Single.defer(() -> {
            long count = propertyOnMarketRepository.count(specification);
            return Single.just(count);
        });
    }

    @Override
    public @NonNull Flowable<PropertyOnMarket> savePublic(Flowable<PropertyOnMarket> publicJsonArray, Origin origin) {
        Portfolio portfolio = createPortfolio(origin);
        Flowable<PropertyOnMarket> savedPropertiesFlowable = publicJsonArray
                .map(propertyOnMarket -> {
                    Deal latestDeal = propertyOnMarket.getLatestDeal();
                    latestDeal.setPortfolio(portfolio);
                    latestDeal.setOrigin(origin);
                    latestDeal.init();
                    modificationTimestampService.getModificationTimestamp(latestDeal.getPublicSource())
                            .ifPresentOrElse(latestDeal::setSourceModificationTimestamp,
                                    () -> latestDeal.setSourceModificationTimestamp(Instant.now()));
                    return propertyOnMarket;
                })
                .map(propertyOnMarket -> {
                    PropertyOnMarketEntity propertyOnMarketEntity = propertyGeneralMapper.propertyOnMarketToPropertyOnMarketEntity(propertyOnMarket);
                    Optional<PropertyOnMarketEntity> propertyOnMarketEntityOptional = propertyOnMarketRepository.findByFullAddress(propertyOnMarketEntity.getFullAddress());
                    Deal newLatestDeal = propertyOnMarket.getLatestDeal();
                    if (propertyOnMarketEntityOptional.isEmpty()) {
                        propertyOnMarket.setUuid(UUID.randomUUID().toString());
                        propertyOnMarket.setFullAddress(addressService.generateUniqueAddress(propertyOnMarket.getAddress(), propertyOnMarket.getCity(), propertyOnMarket.getState(), propertyOnMarket.getZip()));
                        PropertyOnMarket propertyOnMarketToSave = propertyGeneralMapper.propertyOnMarketEntityToPropertyOnMarket(propertyOnMarketEntity);
                        return save(propertyOnMarketToSave);
                    }

                    PropertyOnMarketEntity propertyOnMarketEntityToUpdate = propertyOnMarketEntityOptional.get();
                    List<DealEntity> actualDealList = dealRepository.findByPropertyOrderByUpdatedAtDesc(propertyOnMarketEntityToUpdate);
                    DealEntity latestDeal = actualDealList.get(0);
                    if (!latestDeal.getStatus().equals(DealStatus.ACTIVE)
                            && (newLatestDeal.getCloseDate() == null || latestDeal.getCloseDate().isBefore(newLatestDeal.getCloseDate()))) {
                        PropertyOnMarket propertyOnMarketToSave = propertyGeneralMapper.propertyOnMarketEntityToPropertyOnMarket(propertyOnMarketEntityToUpdate);
                        propertyOnMarketToSave.getDeals().add(newLatestDeal);
                        return save(propertyOnMarketToSave);
                    } else {
                        LocalDate closeDate = newLatestDeal.getCloseDate();
                        latestDeal.setCloseDate(closeDate);
                        if (closeDate != null && latestDeal.getStatus().equals(DealStatus.ACTIVE)) {
                            latestDeal.setStatus(DealStatus.LOST);
                        }
                        latestDeal.setClosePrice(newLatestDeal.getClosePrice());
                        latestDeal.setPublicSource(newLatestDeal.getPublicSource());
                        latestDeal.setCumulativeDaysOnMarket(newLatestDeal.getCumulativeDaysOnMarket());
                        latestDeal.setDaysOnMarket(newLatestDeal.getDaysOnMarket());
                        latestDeal.setUpdatedAt(Instant.now());
                        latestDeal.setOnMarketDate(newLatestDeal.getOnMarketDate());
                        latestDeal.setPrice(newLatestDeal.getPrice());
                        latestDeal.setSourceModificationTimestamp(newLatestDeal.getSourceModificationTimestamp());
                        PropertyOnMarketEntity savedPropertyOnMarket = propertyOnMarketRepository.save(propertyOnMarketEntityToUpdate);
                        return propertyGeneralMapper.propertyOnMarketEntityToPropertyOnMarket(savedPropertyOnMarket);
                    }
                });
        return savedPropertiesFlowable;
    }

    @Override
    public @NonNull Flowable<PropertyOnMarket> savePublicWithMedia(Flowable<Pair<PropertyOnMarket, List<MlsMedia>>> publicJsonArray, Origin origin) {
        Portfolio portfolio = createPortfolio(origin);
        Flowable<PropertyOnMarket> savedPropertiesFlowable = publicJsonArray
                .map(pair -> {
                    Deal latestDeal = pair.getValue0().getLatestDeal();
                    latestDeal.setPortfolio(portfolio);
                    latestDeal.setOrigin(origin);
                    latestDeal.init();
                    modificationTimestampService.getModificationTimestamp(latestDeal.getPublicSource())
                            .ifPresentOrElse(latestDeal::setSourceModificationTimestamp,
                                    () -> latestDeal.setSourceModificationTimestamp(Instant.now()));
                    return pair;
                })
                .map(pair -> {
                    PropertyOnMarketEntity propertyOnMarketEntity = propertyGeneralMapper.propertyOnMarketToPropertyOnMarketEntity(pair.getValue0());
                    Optional<PropertyOnMarketEntity> propertyOnMarketEntityOptional = propertyOnMarketRepository.findByFullAddress(propertyOnMarketEntity.getFullAddress());
                    Deal newLatestDeal = pair.getValue0().getLatestDeal();
                    if (propertyOnMarketEntityOptional.isEmpty()) {
                        propertyOnMarketEntity.setUuid(UUID.randomUUID().toString());
                        pair.getValue0().setFullAddress(addressService.generateUniqueAddress(pair.getValue0().getAddress(), pair.getValue0().getCity(), pair.getValue0().getState(), pair.getValue0().getZip()));
                        PropertyOnMarket propertyOnMarketToSave = propertyGeneralMapper.propertyOnMarketEntityToPropertyOnMarket(propertyOnMarketEntity);
                        return Pair.with(save(propertyOnMarketToSave), pair.getValue1());
                    }

                    PropertyOnMarketEntity propertyOnMarketEntityToUpdate = propertyOnMarketEntityOptional.get();
                    List<DealEntity> actualDealList = dealRepository.findByPropertyOrderByUpdatedAtDesc(propertyOnMarketEntityToUpdate);
                    DealEntity latestDeal = actualDealList.get(0);
                    if (!latestDeal.getStatus().equals(DealStatus.ACTIVE)
                            && (newLatestDeal.getCloseDate() == null || latestDeal.getCloseDate().isBefore(newLatestDeal.getCloseDate()))) {
                        PropertyOnMarket propertyOnMarketToSave = propertyGeneralMapper.propertyOnMarketEntityToPropertyOnMarket(propertyOnMarketEntityToUpdate);
                        propertyOnMarketToSave.getDeals().add(newLatestDeal);
                        return Pair.with(save(propertyOnMarketToSave), pair.getValue1());
                    } else {
                        LocalDate closeDate = newLatestDeal.getCloseDate();
                        latestDeal.setCloseDate(closeDate);
                        if (closeDate != null && latestDeal.getStatus().equals(DealStatus.ACTIVE)) {
                            latestDeal.setStatus(DealStatus.LOST);
                        }
                        latestDeal.setClosePrice(newLatestDeal.getClosePrice());
                        latestDeal.setPublicSource(newLatestDeal.getPublicSource());
                        latestDeal.setCumulativeDaysOnMarket(newLatestDeal.getCumulativeDaysOnMarket());
                        latestDeal.setDaysOnMarket(newLatestDeal.getDaysOnMarket());
                        latestDeal.setUpdatedAt(Instant.now());
                        latestDeal.setOnMarketDate(newLatestDeal.getOnMarketDate());
                        latestDeal.setPrice(newLatestDeal.getPrice());
                        latestDeal.setSourceModificationTimestamp(newLatestDeal.getSourceModificationTimestamp());
                        PropertyOnMarketEntity savedPropertyOnMarketEntity = propertyOnMarketRepository.save(propertyOnMarketEntityToUpdate);
                        PropertyOnMarket savedPropertyOnMarket = propertyGeneralMapper.propertyOnMarketEntityToPropertyOnMarket(savedPropertyOnMarketEntity);
                        return Pair.with(savedPropertyOnMarket, pair.getValue1());
                    }
                })
                .map(pair -> {
                    dealService.addFileToDealByUrlAsync(pair.getValue0().getLatestDeal().getUuid(), pair.getValue1());
                    return pair.getValue0();
                });
        return savedPropertiesFlowable;
    }

    @Override
    public @NonNull Flowable<PropertyOnMarket> savePublicProperty(Flowable<PropertyOnMarket> propertyOnMarketFlowable) {
        Portfolio portfolio = createPortfolio(Origin.MANUAL);
        Flowable<PropertyOnMarket> savedPropertiesFlowable = propertyOnMarketFlowable
                .map(propertyOnMarket -> {
                    Deal latestDeal = propertyOnMarket.getLatestDeal();
                    latestDeal.setPortfolio(portfolio);
                    latestDeal.setOrigin(Origin.MANUAL);
                    latestDeal.init();
                    latestDeal.setSourceModificationTimestamp(Instant.now());
                    return propertyOnMarket;
                })
                .map(propertyOnMarket -> {
                    Optional<PropertyOnMarketEntity> propertyOnMarketEntityOptional = propertyOnMarketRepository.findByFullAddress(propertyOnMarket.getFullAddress());
                    Deal newLatestDeal = propertyOnMarket.getLatestDeal();
                    if (propertyOnMarketEntityOptional.isEmpty()) {
                        propertyOnMarket.setUuid(UUID.randomUUID().toString());
                        propertyOnMarket.setFullAddress(addressService.generateUniqueAddress(propertyOnMarket.getAddress(), propertyOnMarket.getCity(), propertyOnMarket.getState(), propertyOnMarket.getZip()));
                        PropertyOnMarketEntity propertyOnMarketEntity = propertyGeneralMapper.propertyOnMarketToPropertyOnMarketEntity(propertyOnMarket);
                        PropertyOnMarket propertyOnMarketToSave = propertyGeneralMapper.propertyOnMarketEntityToPropertyOnMarket(propertyOnMarketEntity);
                        return save(propertyOnMarketToSave);
                    }

                    PropertyOnMarketEntity propertyOnMarketEntityToUpdate = propertyOnMarketEntityOptional.get();
                    List<DealEntity> actualDealList = dealRepository.findByPropertyOrderByUpdatedAtDesc(propertyOnMarketEntityToUpdate);
                    DealEntity latestDeal = actualDealList.get(0);
                    if (!latestDeal.getStatus().equals(DealStatus.ACTIVE)
                            && (newLatestDeal.getCloseDate() == null || latestDeal.getCloseDate().isBefore(newLatestDeal.getCloseDate()))) {
                        PropertyOnMarket propertyOnMarketToSave = propertyGeneralMapper.propertyOnMarketEntityToPropertyOnMarket(propertyOnMarketEntityToUpdate);
                        propertyOnMarketToSave.getDeals().add(newLatestDeal);
                        return save(propertyOnMarketToSave);
                    } else {
                        LocalDate closeDate = newLatestDeal.getCloseDate();
                        latestDeal.setCloseDate(closeDate);
                        if (closeDate != null && latestDeal.getStatus().equals(DealStatus.ACTIVE)) {
                            latestDeal.setStatus(DealStatus.LOST);
                        }
                        latestDeal.setClosePrice(newLatestDeal.getClosePrice());
                        latestDeal.setPublicSource(newLatestDeal.getPublicSource());
                        latestDeal.setCumulativeDaysOnMarket(newLatestDeal.getCumulativeDaysOnMarket());
                        latestDeal.setDaysOnMarket(newLatestDeal.getDaysOnMarket());
                        latestDeal.setUpdatedAt(Instant.now());
                        latestDeal.setOnMarketDate(newLatestDeal.getOnMarketDate());
                        latestDeal.setPrice(newLatestDeal.getPrice());
                        latestDeal.setSourceModificationTimestamp(newLatestDeal.getSourceModificationTimestamp());
                        latestDeal.setContractType(ContractType.valueOf(newLatestDeal.getContractType()));
                        latestDeal.setOccupancyStatus(OccupancyStatus.valueOf(newLatestDeal.getOccupancyStatus()));
                        latestDeal.setOccupancyStatusAtClosing(OccupancyStatus.valueOf(newLatestDeal.getOccupancyStatusAtClosing()));
                        latestDeal.setInspectionBeforeOffer(InspectionBeforeOffer.valueOf(newLatestDeal.getInspectionBeforeOffer()));
                        latestDeal.setRecipient(propertyGeneralMapper.recipientToRecipientEntity(newLatestDeal.getRecipient()));
                        PropertyOnMarketEntity savedPropertyOnMarketEntity = propertyOnMarketRepository.save(propertyOnMarketEntityToUpdate);
                        PropertyOnMarket savedPropertyOnMarket = propertyGeneralMapper.propertyOnMarketEntityToPropertyOnMarket(savedPropertyOnMarketEntity);
                        return savedPropertyOnMarket;
                    }
                });
        return savedPropertiesFlowable;
    }

    @Override
    public @NonNull List<FileObject> addPropertyPhoto(String propertyUuid, Map<String, Path> temporaryPathToFileMap) {
        return propertyOnMarketRepository.findByUuid(propertyUuid)
                .map(propertyOnMarketEntity -> {
                    PropertyOnMarket propertyOnMarket = propertyGeneralMapper.propertyOnMarketEntityToPropertyOnMarket(propertyOnMarketEntity);
                    DealEntity deal = propertyGeneralMapper.dealToDealEntity(propertyOnMarket.getLatestDeal());
                    deal.setProperty(propertyOnMarketEntity);
                    return dealService.addFileToDeal(deal, temporaryPathToFileMap).getFiles();
                })
                .orElseThrow(() -> new PropertyNotFoundException(propertyUuid));
    }

    @Override
    public @NonNull List<FileObject> updatePropertyPhoto(String propertyUuid, Map<String, Path> temporaryPathToFileMap, List<Integer> photoIdForDeleting) {
        return propertyOnMarketRepository.findByUuid(propertyUuid)
                .map(propertyOnMarketEntity -> {
                    PropertyOnMarket propertyOnMarket = propertyGeneralMapper.propertyOnMarketEntityToPropertyOnMarket(propertyOnMarketEntity);
                    DealEntity deal = propertyGeneralMapper.dealToDealEntity(propertyOnMarket.getLatestDeal());
                    deal.setProperty(propertyOnMarketEntity);

                    if (photoIdForDeleting != null && !photoIdForDeleting.isEmpty()) {
                        dealService.deletePhotoFromDeal(deal, photoIdForDeleting);
                    }
                    if (temporaryPathToFileMap != null && !temporaryPathToFileMap.isEmpty()) {
                        dealService.addFileToDeal(deal, temporaryPathToFileMap);
                    }
                    return fileObjectMapper.fileObjectEntityListToFileObjectList(deal.getFiles());
                })
                .orElseThrow(() -> new PropertyNotFoundException(propertyUuid));
    }

    private Portfolio createPortfolio(Origin origin) {
        return Optional.of(origin.name())
                .map(portfolioService::getOrCreate)
                .orElseGet(portfolioService::getDefault);
    }

    @Override
    public @NonNull Optional<PropertyOnMarket> createFromSourceObject(Object publicSource, PropertySourceMappingService propertySourceMappingService) throws JsonProcessingException {
        String publicPropertySourceString = objectMapper.writeValueAsString(publicSource);
        return propertySourceMappingService.mapDeal(publicPropertySourceString)
                .flatMap(dealLocal -> {
                    dealLocal.setPublicSource(publicPropertySourceString);
                    dealLocal.init();
                    return propertySourceMappingService.mapProperty(publicPropertySourceString)
                            .map(propertyOnMarket -> {
                                propertyOnMarket.setDeals(Collections.singletonList(dealLocal));
                                return propertyOnMarket;
                            });
                });
    }

    //TODO Implement logic for checking possibility to change status from the current to the new
    @Override
    public @NonNull Maybe<PropertyOnMarket> applyLatestDealStatusEvent(String propertyUuid, StatusEvent<DealStatus> statusEvent) {
        return getPropertyByUuid(propertyUuid)
                .map(propertyOnMarket -> {
                    Deal latestDeal = propertyOnMarket.getLatestDeal();
                    if (!latestDeal.getStatus().equals(DealStatus.CLOSED)) {
                        //TODO implement keeping StatusEvents in the Deal
                        latestDeal.setStatus(statusEvent.getStatus());
                        latestDeal.setCloseDate(LocalDate.now());

                        if(statusEvent.getStatus() == DealStatus.CLOSED) {
                            //create budget for closed deal
                            eventPublisher.publishEvent(new DealClosedEvent(latestDeal));
                        }
                    }
                    return save(propertyOnMarket);
                });
    }

    @Override
    public @NonNull Deal updateDealRenovationTag(String propertyUuid, String dealUuid, AnalogueRenovationTag renovationTag) {
        final Optional<PropertyOnMarketEntity> propertyOnMarketEntityOptional = propertyOnMarketRepository.findByUuid(propertyUuid);
        final Deal existingDeal = propertyOnMarketEntityOptional
                .map(propertyOnMarket -> propertyOnMarket.getDeals().stream()
                        .filter(deal -> deal.getUuid().equals(dealUuid))
                        .findAny()
                        .orElseThrow(() -> new DealNotFoundInPropertyException(dealUuid, propertyUuid))
                )
                .map(propertyGeneralMapper::dealEntityToDeal)
                .orElseThrow(() -> new PropertyNotFoundException(propertyUuid));
        existingDeal.setRenovationTag(renovationTag);
        final DealEntity entity = propertyGeneralMapper.dealToDealEntity(existingDeal);
        entity.setProperty(propertyOnMarketEntityOptional.get());
        final DealEntity savedDeal = dealRepository.save(entity);
        return propertyGeneralMapper.dealEntityToDeal(savedDeal);
    }
}
