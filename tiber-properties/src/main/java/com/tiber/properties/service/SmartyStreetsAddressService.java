package com.tiber.properties.service;

import com.smartystreets.api.ClientBuilder;
import com.smartystreets.api.exceptions.SmartyException;
import com.smartystreets.api.us_street.Candidate;
import com.smartystreets.api.us_street.Client;
import com.smartystreets.api.us_street.Lookup;
import com.smartystreets.api.us_street.MatchType;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Profile;
import org.springframework.stereotype.Service;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

/**
 * @depricated This implementation has more than one responsibility - generate address and validate if exist
 */
@Slf4j
@Service
@Profile({"stage", "prod", "local"})
class SmartyStreetsAddressService implements AddressService {

    private final String authId;
    private final String authToken;
    private final String license;
    private final int maxCandidate;

    public SmartyStreetsAddressService(@Value("${smartystreets.authId}") String authId,
                                       @Value("${smartystreets.authToken}") String authToken,
                                       @Value("${smartystreets.licenses}") String license,
                                       @Value("${smartystreets.maxCandidate}") int maxCandidate) {
        this.authId = authId;
        this.authToken = authToken;
        this.license = license;
        this.maxCandidate = maxCandidate;
    }

    @Override
    public String generateUniqueAddress(String street, String city, String state, String zipCode) {
        ArrayList<String> licenses = new ArrayList<>();
        licenses.add(license);
        Client client = new ClientBuilder(authId, authToken).buildUsStreetApiClient();

        Lookup lookup = new Lookup();
        lookup.setStreet(street);
        lookup.setCity(city);
        lookup.setState(state);
        lookup.setZipCode(zipCode);
        lookup.setMaxCandidates(maxCandidate);
        lookup.setMatch(MatchType.STRICT);

        try {
            client.send(lookup);
        } catch (SmartyException | IOException e) {
            log.error(e.getMessage());
        }
        List<Candidate> result = lookup.getResult();
        if (result.isEmpty()) {
            return new ComposingAddressService().generateUniqueAddress(street, city, state, zipCode);
        }

        return result.stream()
                .map(c -> String.format(
                        "%s %s, %s, %s %s",
                        c.getComponents().getPrimaryNumber(),
                        c.getComponents().getStreetName(),
                        c.getComponents().getCityName(),
                        c.getComponents().getState(),
                        c.getComponents().getZipCode()
                ))
                .findFirst()
                .get();
    }
}
