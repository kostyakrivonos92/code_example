package com.tiber.properties.service;

import com.tiber.properties.integration.bridgeapi.domain.MlsProperty;

import java.time.Instant;
import java.util.Optional;

interface ModificationTimestampService<T extends MlsProperty> {

    Optional<Instant> getModificationTimestamp(String mlsProperty);
}
