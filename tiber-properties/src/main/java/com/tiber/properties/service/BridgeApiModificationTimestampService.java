package com.tiber.properties.service;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.tiber.properties.integration.bridgeapi.domain.BridgeMlsProperty;
import lombok.extern.log4j.Log4j2;
import org.springframework.stereotype.Service;

import java.time.Instant;
import java.util.Optional;

@Log4j2
@Service
public class BridgeApiModificationTimestampService implements ModificationTimestampService<BridgeMlsProperty> {

    @Override
    public Optional<Instant> getModificationTimestamp(String bridgeMlsPropertyString) {
        try {
            ObjectMapper objectMapper = new ObjectMapper();
            BridgeMlsProperty bridgeMlsProperty = objectMapper.readValue(bridgeMlsPropertyString, BridgeMlsProperty.class);
            return Optional.ofNullable(bridgeMlsProperty.getBridgeModificationTimestamp());
        } catch (JsonProcessingException e) {
            log.debug("Not a Bridge API property.");
        }
        return Optional.empty();
    }
}
