package com.tiber.properties.service;

import com.tiber.properties.repository.DealRepository;
import com.tiber.properties.entity.DealEntity;
import com.tiber.properties.mapper.FileObjectMapper;
import com.tiber.properties.mapper.PropertyGeneralMapper;
import com.tiber.shared.file.domain.FileObject;
import com.tiber.shared.file.domain.MlsMedia;
import com.tiber.shared.file.entity.FileObjectEntityList;
import com.tiber.shared.file.mapper.FileObjectEntityMapper;
import com.tiber.shared.file.service.FileObjectService;
import com.tiber.shared.file.service.ManageFileService;
import com.tiber.shared.propertylayer.domain.Deal;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.nio.file.Path;
import java.time.Instant;
import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.concurrent.CompletableFuture;
import java.util.concurrent.Executor;
import java.util.concurrent.Executors;
import java.util.stream.Collectors;

@Slf4j
@Service
@RequiredArgsConstructor(onConstructor_ = @Autowired)
public class DefaultDealService implements DealService {

    private final DealRepository dealRepository;

    private final ManageFileService manageFileService;

    private final FileObjectService fileObjectService;

    private final FileObjectEntityMapper fileObjectEntityMapper;

    private final PropertyGeneralMapper propertyGeneralMapper;

    private final FileObjectMapper fileObjectMapper;

    private final Executor fileSaveExecutor = Executors.newWorkStealingPool(10);

    @Override
    public Optional<Deal> getLastAdded() {
        return dealRepository.findTopByUuidIsNotNullOrderBySourceModificationTimestampDesc()
                .map(propertyGeneralMapper::dealEntityToDeal);
    }

    @Override
    public void addFileToDealByUrlAsync(String dealUuid, List<MlsMedia> mlsMediaList) {
        CompletableFuture.runAsync(() -> {
                    dealRepository.findByUuid(dealUuid).ifPresent(deal -> {
                        List<FileObject> fileObjectList = mlsMediaList.stream().map(mlsMedia -> {
                                    FileObject fileObject = fileObjectMapper.fromMlsMediaToFileObject(mlsMedia);
                                    Path uploadedMediaFileUrl = manageFileService.downloadMediaFile(dealUuid, mlsMedia.getMediaURL());
                                    fileObject.setId(mlsMedia.getOrder());
                                    fileObject.setFileUrl(uploadedMediaFileUrl.toString());
                                    fileObject.setFileName(uploadedMediaFileUrl.getFileName().toString());
                                    fileObject.setOrigin(deal.getOrigin());
                                    fileObject.setTag(mlsMedia.getTag());
                                    fileObject.setCreatedAt(Instant.now());
                                    return fileObject;
                                })
                                .collect(Collectors.toList());
                        deal.setFiles(fileObjectEntityMapper.fileObjectListToFileObjectEntityList(fileObjectList));
                        dealRepository.save(deal);
                        log.info("Fetched files for deal {}", dealUuid);
                    });
                }
                , fileSaveExecutor);
    }

    @Override
    public Deal addFileToDeal(DealEntity deal, Map<String, Path> temporaryPathToFileMap) {
        int lastId = fileObjectService.getLastId(deal.getFiles());
        List<FileObject> fileObjectList = fileObjectService.createFileObjectList(deal.getUuid(), lastId, temporaryPathToFileMap, null);
        deal.setFiles(fileObjectEntityMapper.fileObjectListToFileObjectEntityList(fileObjectList));
        log.info("Added files to deal {}", deal.getUuid());
        return propertyGeneralMapper.dealEntityToDeal(dealRepository.save(deal));
    }

    @Override
    public Deal deletePhotoFromDeal(DealEntity deal, List<Integer> photoIdForDeleting) {
        FileObjectEntityList fileObjectList = deal.getFiles();

        if (fileObjectList != null && !fileObjectList.isEmpty()) {
            FileObjectEntityList editedFileObjectList = fileObjectService.deletePhotoFromStorage(deal.getFiles(), photoIdForDeleting);
            deal.setFiles(editedFileObjectList);
        }
        return propertyGeneralMapper.dealEntityToDeal(deal);
    }
}
