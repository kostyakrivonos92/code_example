package com.tiber.properties.service;

import com.tiber.shared.propertylayer.domain.Portfolio;

public interface PortfolioService {

    Portfolio getOrCreate(String name);

    Portfolio getDefault();
}
