package com.tiber.properties.service;

interface AddressService {

    String generateUniqueAddress(String street, String city, String state, String zipCode);
}
