package com.tiber.properties.service;

import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Profile;
import org.springframework.stereotype.Service;

@Slf4j
@Service
@Profile({"default", "dev"})
@RequiredArgsConstructor(onConstructor_ = @Autowired)
class ComposingAddressService implements AddressService {

    @Override
    public String generateUniqueAddress(String street, String city, String state, String zipCode) {
        return String.format("%s, %s, %s %s", street, city, state, zipCode);
    }
}
