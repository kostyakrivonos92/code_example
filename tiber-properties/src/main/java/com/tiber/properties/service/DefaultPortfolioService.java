package com.tiber.properties.service;

import com.tiber.properties.repository.PortfolioRepository;
import com.tiber.properties.entity.PortfolioEntity;
import com.tiber.properties.mapper.PortfolioMapper;
import com.tiber.shared.propertylayer.domain.Portfolio;
import lombok.RequiredArgsConstructor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.persistence.EntityNotFoundException;

@Service
@RequiredArgsConstructor(onConstructor_ = @Autowired)
class DefaultPortfolioService implements PortfolioService {

    private final PortfolioRepository portfolioRepository;
    private final PortfolioMapper portfolioMapper;
    private final String BASE_PORTFOLIO = "TBR_CAPITAL";
    private final String BASE_PORTFOLIO_MISSED = "Base portfolio is not exist.";

    @Override
    public Portfolio getOrCreate(String name) {
        PortfolioEntity portfolioEntity = portfolioRepository.findByName(name)
                .orElseGet(() -> {
                    Portfolio portfolio = new Portfolio(name);
                    return portfolioRepository.save(portfolioMapper.mapDomainToEntity(portfolio));
                });
        return portfolioMapper.mapEntityToDomain(portfolioEntity);

    }

    @Override
    public Portfolio getDefault() {
        return portfolioRepository.findByName(BASE_PORTFOLIO)
                .map(portfolioMapper::mapEntityToDomain)
                .orElseThrow(() -> new EntityNotFoundException(BASE_PORTFOLIO_MISSED));
    }
}
