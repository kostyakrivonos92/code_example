package com.tiber.properties.service;

import com.tiber.properties.entity.DealEntity;
import com.tiber.shared.file.domain.MlsMedia;
import com.tiber.shared.propertylayer.domain.Deal;

import java.nio.file.Path;
import java.util.List;
import java.util.Map;
import java.util.Optional;

public interface DealService {

    Optional<Deal> getLastAdded();

    void addFileToDealByUrlAsync(String dealUuid, List<MlsMedia> mlsMediaList);

    Deal addFileToDeal(DealEntity deal, Map<String, Path> temporaryPathToFileMap);

    Deal deletePhotoFromDeal(DealEntity deal, List<Integer> photoIdForDeleting);
}
