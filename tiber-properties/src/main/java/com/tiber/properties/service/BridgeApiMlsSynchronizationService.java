package com.tiber.properties.service;

import com.tiber.properties.integration.bridgeapi.client.BridgeApiService;
import com.tiber.properties.integration.bridgeapi.domain.BridgeMlsProperty;
import com.tiber.properties.mapper.PropertySourceMappingService;
import com.tiber.properties.mapper.FileObjectMapper;
import com.tiber.shared.file.domain.MlsMedia;
import com.tiber.shared.propertylayer.domain.Deal;
import com.tiber.shared.propertylayer.domain.PropertyOnMarket;
import com.tiber.shared.vocabulary.Origin;
import io.reactivex.rxjava3.core.Flowable;
import lombok.extern.log4j.Log4j2;
import org.javatuples.Pair;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Service;

import java.time.Instant;
import java.util.List;
import java.util.Optional;
import java.util.concurrent.atomic.AtomicBoolean;

@Log4j2
@Service
public class BridgeApiMlsSynchronizationService implements MlsSynchronizationService {

    private final BridgeApiService bridgeApiService;

    private final DealService dealService;

    private final PropertyGeneralService propertyGeneralService;

    private final PropertySourceMappingService propertySourceMappingService;

    private final FileObjectMapper fileObjectMapper;

    private final Integer minYearBuilt;

    private final AtomicBoolean fetchInProgress = new AtomicBoolean(false);

    @Autowired
    public BridgeApiMlsSynchronizationService(BridgeApiService bridgeApiService,
                                              PropertyGeneralService propertyGeneralService,
                                              DealService dealService,
                                              FileObjectMapper fileObjectMapper,
                                              @Qualifier("BRIDGE") PropertySourceMappingService propertySourceMappingService,
                                              @Value("${property.minYearBuilt}") Integer minYearBuilt) {
        this.bridgeApiService = bridgeApiService;
        this.propertyGeneralService = propertyGeneralService;
        this.dealService = dealService;
        this.fileObjectMapper = fileObjectMapper;
        this.propertySourceMappingService = propertySourceMappingService;
        this.minYearBuilt = minYearBuilt;
    }

    @Override
    @Scheduled(fixedDelayString = "${propertyUpdate.fixeDelayMillis}")
    public void synchronize() {
        if (fetchInProgress.get()) {
            return;
        }
        fetchInProgress.set(true);
        Optional<Deal> lastAddedTimestamp = dealService.getLastAdded();
        Flowable<Pair<PropertyOnMarket, List<MlsMedia>>> publicPropertyGeneralFlowable = lastAddedTimestamp
                .map(deal -> fetch(deal.getSourceModificationTimestamp()))
                .orElseGet(this::replicate);
        propertyGeneralService.savePublicWithMedia(publicPropertyGeneralFlowable, Origin.BRIDGE_API)
                .doOnError(log::error)
                .onErrorComplete()
                .doOnComplete(() -> {
                    fetchInProgress.set(false);
                    log.info("Bridge replication/fetch completed");
                })
                .subscribe();
    }

    @SuppressWarnings("UnnecessaryLocalVariable")
    private Flowable<Pair<PropertyOnMarket, List<MlsMedia>>> fetch(Instant modificationTimestamp) {
        log.info("Bridge fetch started");
        Flowable<Pair<PropertyOnMarket, List<MlsMedia>>> publicPropertyGeneralFlowable = bridgeApiService
                .fetchEntities(minYearBuilt, modificationTimestamp, 200, 0, "BridgeModificationTimestamp")
                .map((BridgeMlsProperty publicSource) ->
                        propertyGeneralService
                                .createFromSourceObject(publicSource, propertySourceMappingService)
                                .map(propertyOnMarket1 -> Pair.with(propertyOnMarket1,
                                        fileObjectMapper.bridgeMlsMediaListToMlsMediaList(publicSource.getMedia()))))
                .filter(Optional::isPresent)
                .map(Optional::get);
        return publicPropertyGeneralFlowable;
    }

    @SuppressWarnings("UnnecessaryLocalVariable")
    private Flowable<Pair<PropertyOnMarket, List<MlsMedia>>> replicate() {
        log.info("Bridge replication started");
        Flowable<Pair<PropertyOnMarket, List<MlsMedia>>> publicPropertyGeneralFlowable = bridgeApiService
                .replicateEntities(minYearBuilt)
                .map((BridgeMlsProperty publicSource) ->
                        propertyGeneralService
                                .createFromSourceObject(publicSource, propertySourceMappingService)
                                .map(propertyOnMarket1 -> Pair.with(propertyOnMarket1,
                                        fileObjectMapper.bridgeMlsMediaListToMlsMediaList(publicSource.getMedia()))))
                .filter(Optional::isPresent)
                .map(Optional::get);
        return publicPropertyGeneralFlowable;
    }
}
