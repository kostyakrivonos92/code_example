package com.tiber.properties.mapper;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.DeserializationFeature;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.tiber.shared.propertylayer.domain.Deal;
import com.tiber.shared.propertylayer.domain.PropertyOnMarket;
import org.springframework.stereotype.Service;

import java.util.Optional;

@Service("MANUAL")
public class DefaultPropertySourceMappingService implements PropertySourceMappingService {

    @Override
    public Optional<PropertyOnMarket> mapProperty(String source) {
        ObjectMapper mapper = new ObjectMapper().configure(DeserializationFeature.FAIL_ON_UNKNOWN_PROPERTIES, false);
        try {
            return Optional.of(mapper.readValue(source, PropertyOnMarket.class));
        } catch (JsonProcessingException e) {
            return Optional.empty();
        }
    }

    @Override
    public Optional<Deal> mapDeal(String source) {
        ObjectMapper mapper = new ObjectMapper().configure(DeserializationFeature.FAIL_ON_UNKNOWN_PROPERTIES, false);
        try {
            return Optional.of(mapper.readValue(source, Deal.class));
        } catch (JsonProcessingException e) {
            return Optional.empty();
        }
    }
}
