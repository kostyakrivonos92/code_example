package com.tiber.properties.mapper;

import com.tiber.properties.integration.bridgeapi.domain.BridgeMlsMedia;
import com.tiber.shared.file.domain.FileObject;
import com.tiber.shared.file.domain.MlsMedia;
import com.tiber.shared.file.entity.FileObjectEntityList;
import org.mapstruct.Mapper;

import java.util.List;

@Mapper(componentModel = "spring")
public interface FileObjectMapper {

    FileObject fromMlsMediaToFileObject(MlsMedia mlsMedia);

    List<FileObject> fromMlsMediaListToFileObjectList(List<MlsMedia> mlsMedia);

    List<FileObject> fileObjectEntityListToFileObjectList(FileObjectEntityList fileObjectEntityList);

    List<MlsMedia> bridgeMlsMediaListToMlsMediaList(List<BridgeMlsMedia> bridgeMlsMediaList);
}
