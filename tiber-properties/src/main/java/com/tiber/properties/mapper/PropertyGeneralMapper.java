package com.tiber.properties.mapper;

import com.tiber.properties.entity.DealEntity;
import com.tiber.properties.entity.PropertyCommentEntity;
import com.tiber.properties.entity.PropertyOnMarketEntity;
import com.tiber.properties.entity.RecipientEntity;
import com.tiber.shared.file.domain.FileObject;
import com.tiber.shared.file.entity.FileObjectEntity;
import com.tiber.shared.propertylayer.domain.Deal;
import com.tiber.shared.propertylayer.domain.PropertyComment;
import com.tiber.shared.propertylayer.domain.PropertyDetails;
import com.tiber.shared.propertylayer.domain.PropertyGeneral;
import com.tiber.shared.propertylayer.domain.PropertyOnMarket;
import com.tiber.shared.propertylayer.domain.PropertySummary;
import com.tiber.shared.propertylayer.domain.Recipient;
import com.tiber.shared.propertylayer.domain.Underwriting;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

@Mapper(componentModel = "spring")
public interface PropertyGeneralMapper {

    PropertyOnMarketEntity propertyOnMarketToPropertyOnMarketEntity(PropertyOnMarket propertyEntity);

    PropertyOnMarket propertyOnMarketEntityToPropertyOnMarket(PropertyOnMarketEntity propertyEntity);

    List<PropertySummary> propertyOnMarketListToPropertySummaryList(List<PropertyOnMarket> propertyOnMarketList);

    @Mapping(source = "latestDeal.price", target = "price")
    PropertySummary propertyOnMarketToPropertySummary(PropertyOnMarket propertyOnMarket);

    @Mapping(target = "renovationTag", defaultValue = "UNKNOWN")
    DealEntity dealToDealEntity(Deal deal);

    @Mapping(target = "renovationTag", defaultValue = "UNKNOWN")
    Deal dealEntityToDeal(DealEntity dealEntity);

    RecipientEntity recipientToRecipientEntity(Recipient recipient);

    Recipient recipientEntityToRecipient(RecipientEntity recipientEntity);

    PropertyCommentEntity commentToCommentEntity(PropertyComment propertyComment);

    PropertyComment commentEntityToComment(PropertyCommentEntity propertyComment);

    FileObjectEntity fileObjectToFileObjectEntity(FileObject fileObject);

    FileObject fileObjectEntityToFileObject(FileObjectEntity dealEntity);

    PropertyGeneral propertyToPropertyGeneral(PropertyOnMarket propertyOnMarket);

    Underwriting dealToUnderwriting(Deal deal);

    @Mapping(target = "photos", source = "deal.files")
    PropertyDetails propertyToPropertyDetails(PropertyOnMarket propertyOnMarket, Deal deal);

    default List<Deal> dealEntityCollectionToDealList(Collection<DealEntity> collection) {
        if (collection == null) {
            return null;
        }
        List<Deal> list = new ArrayList<>(collection.size());
        for (DealEntity dealEntity : collection) {
            list.add(dealEntityToDeal(dealEntity));
        }
        list.sort((deal1, deal2) -> deal2.getUpdatedAt().compareTo(deal1.getUpdatedAt()));
        return list;
    }
}

