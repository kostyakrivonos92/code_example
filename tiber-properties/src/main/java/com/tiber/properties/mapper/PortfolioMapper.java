package com.tiber.properties.mapper;

import com.tiber.properties.entity.PortfolioEntity;
import com.tiber.shared.propertylayer.domain.Portfolio;
import org.mapstruct.Mapper;

@Mapper(componentModel = "spring")
public interface PortfolioMapper {

    Portfolio mapEntityToDomain(PortfolioEntity propertyEntities);

    PortfolioEntity mapDomainToEntity(Portfolio propertyEntity);
}
