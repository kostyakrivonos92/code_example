package com.tiber.properties.mapper;

import com.tiber.shared.propertylayer.domain.Deal;
import com.tiber.shared.propertylayer.domain.PropertyOnMarket;

import java.util.Optional;

public interface PropertySourceMappingService {

    Optional<PropertyOnMarket> mapProperty(String source);

    Optional<Deal> mapDeal(String source);
}
