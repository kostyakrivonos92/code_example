package com.tiber.properties.mapper;

import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.tiber.shared.propertylayer.domain.Deal;
import com.tiber.shared.propertylayer.domain.PropertyOnMarket;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;

import java.time.LocalDate;
import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.List;
import java.util.Optional;

import static com.tiber.shared.vocabulary.BridgePropertyParameterName.BATHROOMS;
import static com.tiber.shared.vocabulary.BridgePropertyParameterName.BATHROOMS_HALF;
import static com.tiber.shared.vocabulary.BridgePropertyParameterName.BEDROOMS;
import static com.tiber.shared.vocabulary.BridgePropertyParameterName.CITY;
import static com.tiber.shared.vocabulary.BridgePropertyParameterName.CLOSE_DATE;
import static com.tiber.shared.vocabulary.BridgePropertyParameterName.COORDINATES;
import static com.tiber.shared.vocabulary.BridgePropertyParameterName.COUNTY;
import static com.tiber.shared.vocabulary.BridgePropertyParameterName.CUMULATIVE_DAYS_ON_MARKET;
import static com.tiber.shared.vocabulary.BridgePropertyParameterName.DAYS_ON_MARKET;
import static com.tiber.shared.vocabulary.BridgePropertyParameterName.LATITUDE;
import static com.tiber.shared.vocabulary.BridgePropertyParameterName.LONGITUDE;
import static com.tiber.shared.vocabulary.BridgePropertyParameterName.LOT_SIZE_ACRES;
import static com.tiber.shared.vocabulary.BridgePropertyParameterName.LOT_SIZE_SQUARE_FEET;
import static com.tiber.shared.vocabulary.BridgePropertyParameterName.ON_MARKET_DATE;
import static com.tiber.shared.vocabulary.BridgePropertyParameterName.PRICE;
import static com.tiber.shared.vocabulary.BridgePropertyParameterName.PROPERTY_SUB_TYPE;
import static com.tiber.shared.vocabulary.BridgePropertyParameterName.PROPERTY_TYPE;
import static com.tiber.shared.vocabulary.BridgePropertyParameterName.STATE;
import static com.tiber.shared.vocabulary.BridgePropertyParameterName.STREET;
import static com.tiber.shared.vocabulary.BridgePropertyParameterName.STREET_NUMBER;
import static com.tiber.shared.vocabulary.BridgePropertyParameterName.YEAR_BUILT;
import static com.tiber.shared.vocabulary.BridgePropertyParameterName.ZIP;

@Slf4j
@Service("BRIDGE")
public class BridgeApiPropertySourceMappingService implements PropertySourceMappingService {

    @Override
    public Optional<PropertyOnMarket> mapProperty(String source) {
        ObjectMapper objectMapper = new ObjectMapper();
        HashMap<String, Object> publicPropertyHashMap = null;
        try {
            publicPropertyHashMap = objectMapper.readValue(source, new TypeReference<>() {
            });
            PropertyOnMarket propertyOnMarket = new PropertyOnMarket();
            propertyOnMarket.setCity(publicPropertyHashMap.containsKey(CITY) ? publicPropertyHashMap.get(CITY).toString(): "");
            propertyOnMarket.setState(publicPropertyHashMap.containsKey(STATE) ? publicPropertyHashMap.get(STATE).toString() : "");
            propertyOnMarket.setCounty(publicPropertyHashMap.containsKey(COUNTY) ? publicPropertyHashMap.get(COUNTY).toString() : "");
            propertyOnMarket.setZip(publicPropertyHashMap.containsKey(ZIP) ? publicPropertyHashMap.get(ZIP).toString(): "");
            propertyOnMarket.setBedrooms(publicPropertyHashMap.containsKey(BEDROOMS) ? Integer.parseInt(publicPropertyHashMap.get(BEDROOMS).toString()) : null);
            propertyOnMarket.setBathrooms(Integer.parseInt(publicPropertyHashMap.get(BATHROOMS).toString()));
            //TODO temporary solution, rewrite when implement google address format
            String fullAddress = String.format("%s %s, %s, %s %s", publicPropertyHashMap.get(STREET_NUMBER), publicPropertyHashMap.get(STREET),
                    publicPropertyHashMap.get(CITY), publicPropertyHashMap.get(STATE), publicPropertyHashMap.get(ZIP));
            propertyOnMarket.setAddress(String.format("%s %s", publicPropertyHashMap.get(STREET_NUMBER), publicPropertyHashMap.get(STREET)));
            propertyOnMarket.setFullAddress(fullAddress);
            Optional.ofNullable(publicPropertyHashMap.get(BATHROOMS_HALF))
                    .ifPresent(bathroomsHalf -> propertyOnMarket.setHalfBathrooms(Integer.parseInt(bathroomsHalf.toString())));
            final Float lotSizeAcres = Optional.ofNullable(publicPropertyHashMap.get(LOT_SIZE_ACRES))
                    .map(String::valueOf)
                    .map(Float::parseFloat)
                    .orElse(0.f);
            Optional.ofNullable(publicPropertyHashMap.get(LOT_SIZE_SQUARE_FEET))
                    .ifPresentOrElse(areaSquareFeet -> propertyOnMarket.setAreaSqFt(Integer.parseInt(areaSquareFeet.toString())),
                            () -> {
                                propertyOnMarket.setAreaSqFt(Math.round(lotSizeAcres * 43560));
                            });

            propertyOnMarket.setYearBuilt(Integer.parseInt(publicPropertyHashMap.get(YEAR_BUILT).toString()));
            Optional.ofNullable(publicPropertyHashMap.get(LONGITUDE))
                    .ifPresent(longitude -> propertyOnMarket.setLongitude(Double.parseDouble(longitude.toString())));
            Optional.ofNullable(publicPropertyHashMap.get(LATITUDE))
                    .ifPresent(latitude -> propertyOnMarket.setLatitude(Double.parseDouble(latitude.toString())));
            Optional.ofNullable(publicPropertyHashMap.get(COORDINATES))
                    .ifPresent(coordinates -> {
                        if (coordinates instanceof Collection) {
                            final List<?> coordinatesParsed = new ArrayList<>((Collection<?>) coordinates);
                            propertyOnMarket.setLongitude((Double) coordinatesParsed.get(0));
                            propertyOnMarket.setLatitude((Double) coordinatesParsed.get(1));
                        }
                    });
            propertyOnMarket.setPropertyType(publicPropertyHashMap.get(PROPERTY_TYPE).toString());
            propertyOnMarket.setPropertySubType(publicPropertyHashMap.get(PROPERTY_SUB_TYPE).toString());
            return Optional.of(propertyOnMarket);
        } catch (Exception e) {
            log.error("Can't map public: {}", source, e);
            return Optional.empty();
        }
    }

    @Override
    public Optional<Deal> mapDeal(String source) {
        ObjectMapper objectMapper = new ObjectMapper();
        HashMap<String, Object> publicPropertyHashMap = null;
        try {
            publicPropertyHashMap = objectMapper.readValue(source, new TypeReference<>() {
            });
            Deal deal = new Deal();
            final Integer price = Optional.ofNullable(publicPropertyHashMap.get(PRICE))
                    .map(priceString -> Integer.parseInt(priceString.toString()))
                    .orElse(0);
            deal.setPrice(price);
            Optional.ofNullable(publicPropertyHashMap.get(ON_MARKET_DATE))
                    .ifPresent(onMarketDate -> deal.setOnMarketDate(LocalDate.parse(onMarketDate.toString())));
            Optional.ofNullable(publicPropertyHashMap.get(CLOSE_DATE))
                    .ifPresent(closeDate -> deal.setCloseDate(LocalDate.parse(closeDate.toString())));
            Optional.ofNullable(publicPropertyHashMap.get(CUMULATIVE_DAYS_ON_MARKET))
                    .ifPresent(cumulativeDaysOnMarket ->
                            deal.setCumulativeDaysOnMarket(Integer.parseInt(cumulativeDaysOnMarket.toString())));
            Optional.ofNullable(publicPropertyHashMap.get(DAYS_ON_MARKET))
                    .ifPresent(daysOnMarket -> deal.setDaysOnMarket(Integer.parseInt(daysOnMarket.toString())));
            deal.setIsForSale(publicPropertyHashMap.get(PROPERTY_TYPE).equals("Residential"));
            return Optional.of(deal);
        } catch (Exception e) {
            log.error("Can't map public deal: {}", source, e);
            return Optional.empty();
        }
    }
}
