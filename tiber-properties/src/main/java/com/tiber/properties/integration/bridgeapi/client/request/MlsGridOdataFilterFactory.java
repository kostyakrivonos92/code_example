package com.tiber.properties.integration.bridgeapi.client.request;

import org.apache.olingo.client.api.ODataClient;
import org.apache.olingo.client.api.uri.FilterFactory;
import org.apache.olingo.client.api.uri.URIFilter;
import org.apache.olingo.client.core.uri.AndFilter;

import java.time.Instant;
import java.util.Arrays;
import java.util.Collection;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Optional;

/**
 * @deprecated TODO
 */
//@Component("MLS")
public class MlsGridOdataFilterFactory implements OdataFilterFactory {

    private final FilterFactory filterFactory;


    private final List<String> availableFieldsForFilter = Arrays.asList("ModificationTimestamp",
            "OriginatingSystemName",
            "StandardStatus",
            "ListingId",
            "MlgCanView"
    );

    //    @Autowired
    public MlsGridOdataFilterFactory(ODataClient oDataClient) {
        filterFactory = oDataClient.getFilterFactory();
    }

    @Override
    public Optional<URIFilter> createAddByMaps(Map<String, Object> geFilters,
                                               Map<String, String> eqFilters,
                                               Map<String, Collection<String>> orEqFilters,
                                               Map<String, Instant> geDateFilters,
                                               Map<String, String> containsFilter) {
        return getUriFilter(geFilters, eqFilters, geDateFilters, containsFilter, filterFactory);
    }

    private Optional<URIFilter> getUriFilter(Map<String, Object> geFilters,
                                             Map<String, String> eqFilters,
                                             Map<String, Instant> geDateFilters,
                                             Map<String, String> containsFilter,
                                             FilterFactory filterFactory) {

        HashMap<String, String> allowedEqFilters = new HashMap<>();
        eqFilters.keySet().stream()
                .filter(availableFieldsForFilter::contains)
                .forEach(key -> allowedEqFilters.put(key, eqFilters.get(key)));

        HashMap<String, String> allowedGeFilters = new HashMap<>();
        eqFilters.keySet().stream()
                .filter(availableFieldsForFilter::contains)
                .forEach(key -> allowedGeFilters.put(key, eqFilters.get(key)));


        Optional<URIFilter> geUriFilter = geFilters.keySet().stream()
                .map(key -> filterFactory.ge(key, geFilters.get(key)))
                .reduce(AndFilter::new);
        Optional<URIFilter> eqUriFilter = eqFilters.keySet().stream()
                .map(key -> filterFactory.eq(key, eqFilters.get(key)))
                .reduce(AndFilter::new);
        Optional<URIFilter> containsUriFilter = containsFilter.keySet().stream()
                .map(key -> filterFactory.match(filterFactory.getArgFactory()
                        .contains(filterFactory.getArgFactory().property(key),
                                filterFactory.getArgFactory().literal(containsFilter.get(key)))))
                .reduce(AndFilter::new);
        Optional<URIFilter> geDateUriFilter = geDateFilters.keySet().stream()
                .map(key -> filterFactory.ge(
                        filterFactory.getArgFactory().date(filterFactory.getArgFactory().property(key)).build(),
                        geDateFilters.get(key)))
                .reduce(AndFilter::new);

        @SuppressWarnings("redundant")
        Optional<URIFilter> uriFilters = combineFilters(Arrays.asList(geUriFilter, eqUriFilter, containsUriFilter, geDateUriFilter));
        return uriFilters;
    }

    private Optional<URIFilter> combineFilters(List<Optional<URIFilter>> uriFilter) {
        return uriFilter.stream()
                .filter(Optional::isPresent)
                .map(Optional::get)
                .reduce(AndFilter::new);
    }
}
