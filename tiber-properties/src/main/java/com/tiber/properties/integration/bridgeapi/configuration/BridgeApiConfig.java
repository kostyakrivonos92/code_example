package com.tiber.properties.integration.bridgeapi.configuration;

import org.apache.olingo.client.api.ODataClient;
import org.apache.olingo.client.core.ODataClientFactory;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

@Configuration
public class BridgeApiConfig {

    @Bean
    public ODataClient getODataClient() {
        return ODataClientFactory.getClient();
    }
}
