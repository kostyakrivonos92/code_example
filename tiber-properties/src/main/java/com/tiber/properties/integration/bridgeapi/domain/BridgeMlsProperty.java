package com.tiber.properties.integration.bridgeapi.domain;

import com.fasterxml.jackson.annotation.JsonAlias;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.tiber.properties.integration.bridgeapi.utility.BridgeMlsInstantDeserializer;
import com.tiber.properties.integration.bridgeapi.utility.BridgeMlsInstantSerializer;
import com.tiber.shared.util.LocalDateDeserializer;
import com.tiber.shared.util.LocalDateSerializer;
import lombok.Data;

import java.time.Instant;
import java.time.LocalDate;
import java.util.List;

@Data
@JsonIgnoreProperties(ignoreUnknown = true)
public class BridgeMlsProperty implements MlsProperty {

    private String listingKey;

    private String listingId;

    private String originatingSystemKey;

    private Integer yearBuilt;

    private Integer yearBuiltEffective;

    private Integer bedroomsTotal;

    private Integer roomsTotal;

    private Integer bathroomsTotalInteger;

    private Integer bathroomsFull;

    private Integer bathroomsHalf;

    private Integer bathroomsOneQuarter;

    private Integer bathroomsThreeQuarter;

    private Integer fireplacesTotal;

    private List<String> fireplaceFeatures;

    private Boolean garageYN;

    private Integer garageSpaces;

    private Boolean heatingYN;

    private List<String> heating;

    private Boolean mobileHomeRemainsYN;

    private Integer parkingTotal;

    private Boolean poolPrivateYN;

    private List<String> poolFeatures;

    private List<String> roof;

    private Integer stories;

    private Integer storiesTotal;

    private List<String> architecturalStyle;

    private List<String> electric;

    private String electricOnPropertyYN;

    private String fuelExpense;

    private List<BridgeMlsMedia> media;

    private String postalCode;

    private String city;

    private String country;

    private String countyOrParish;

    private String stateOrProvince;

    private String unparsedAddress;

    private String streetName;

    private String streetNumber;

    private String unitNumber;

    private Double longitude;

    private Double latitude;

    private List<Double> coordinates;

    private String carrierRoute;

    private Integer listPrice;

    private Integer previousListPrice;

    private Integer originalListPrice;

    private Integer closePrice;

    private Integer listPriceLow;

    @JsonDeserialize(using = BridgeMlsInstantDeserializer.class)
    @JsonSerialize(using = BridgeMlsInstantSerializer.class)
    private Instant priceChangeTimestamp;

    private Integer taxAssessedValue;

    private Integer taxAnnualAmount;

    private Integer taxYear;

    private List<String> taxStatusCurrent;

    private String taxLot;

    private String taxBlock;

    private String taxTract;

    private String taxLegalDescription;

    @JsonAlias("STELLAR_PublicRemarks")
    private String publicRemarks;

    private String privateRemarks;

    private String standardStatus;

    @JsonDeserialize(using = BridgeMlsInstantDeserializer.class)
    @JsonSerialize(using = BridgeMlsInstantSerializer.class)
    private Instant statusChangeTimestamp;

    @JsonDeserialize(using = BridgeMlsInstantDeserializer.class)
    @JsonSerialize(using = BridgeMlsInstantSerializer.class)
    private Instant modificationTimestamp;

    @JsonDeserialize(using = BridgeMlsInstantDeserializer.class)
    @JsonSerialize(using = BridgeMlsInstantSerializer.class)
    private Instant originalEntryTimestamp;

    @JsonDeserialize(using = BridgeMlsInstantDeserializer.class)
    @JsonSerialize(using = BridgeMlsInstantSerializer.class)
    private Instant pendingTimestamp;

    @JsonDeserialize(using = BridgeMlsInstantDeserializer.class)
    @JsonSerialize(using = BridgeMlsInstantSerializer.class)
    private Instant bridgeModificationTimestamp;

    private Integer lotSizeSquareFeet;

    private String ListAgentKey;

    private String listAgentKeyNumeric;

    private String listAgentFullName;

    private String listAgentFirstName;

    private String listAgentMiddleName;

    private String listAgentLastName;

    private String listAgentHomePhone;

    private String listOfficeKey;

    private String listOfficeKeyNumeric;

    private String listOfficeMlsId;

    private String listOfficeName;

    private String listOfficePhone;

    @JsonDeserialize(using = LocalDateDeserializer.class)
    @JsonSerialize(using = LocalDateSerializer.class)
    private LocalDate onMarketDate;

    @JsonDeserialize(using = LocalDateDeserializer.class)
    @JsonSerialize(using = LocalDateSerializer.class)
    private LocalDate closeDate;

    private Integer cumulativeDaysOnMarket;

    private Integer daysOnMarket;

    private String propertyType;

    private String propertySubType;
}
