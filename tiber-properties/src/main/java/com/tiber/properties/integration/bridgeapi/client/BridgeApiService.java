package com.tiber.properties.integration.bridgeapi.client;

import com.tiber.properties.integration.bridgeapi.domain.BridgeMlsProperty;
import io.reactivex.rxjava3.core.Flowable;

import java.time.Instant;

public interface BridgeApiService {

    Flowable<BridgeMlsProperty> fetchEntities(Integer yearBuilt, Instant modificationTimeStamp, Integer top, Integer skip, String order);

    Flowable<BridgeMlsProperty> replicateEntities(Integer yearBuilt);
}
