package com.tiber.properties.integration.bridgeapi.domain;

import lombok.AllArgsConstructor;
import lombok.Data;

import java.util.List;

@Data
@AllArgsConstructor
public class BridgeMlsPropertyResponse {

    Integer count;

    Integer top;

    Integer skip;

    List<BridgeMlsProperty> properties;
}
