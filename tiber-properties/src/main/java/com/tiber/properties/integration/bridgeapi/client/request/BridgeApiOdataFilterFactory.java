package com.tiber.properties.integration.bridgeapi.client.request;

import org.apache.olingo.client.api.ODataClient;
import org.apache.olingo.client.api.uri.FilterFactory;
import org.apache.olingo.client.api.uri.URIFilter;
import org.apache.olingo.client.core.uri.AndFilter;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.time.Instant;
import java.util.Arrays;
import java.util.Collection;
import java.util.List;
import java.util.Map;
import java.util.Optional;

@Component("BridgeApi")
public class BridgeApiOdataFilterFactory implements OdataFilterFactory {

    private final FilterFactory filterFactory;

    @Autowired
    public BridgeApiOdataFilterFactory(ODataClient oDataClient) {
        filterFactory = oDataClient.getFilterFactory();
    }

    @Override
    public Optional<URIFilter> createAddByMaps(Map<String, Object> geFilters,
                                               Map<String, String> eqFilters,
                                               Map<String, Collection<String>> orEqFilters,
                                               Map<String, Instant> geDateFilters,
                                               Map<String, String> containsFilter) {
        return getUriFilter(geFilters, eqFilters, orEqFilters, geDateFilters, containsFilter, filterFactory);
    }

    private Optional<URIFilter> getUriFilter(Map<String, Object> geFilters,
                                             Map<String, String> eqFilters,
                                             Map<String, Collection<String>> orEqFilters,
                                             Map<String, Instant> geDateFilters,
                                             Map<String, String> containsFilter,
                                             FilterFactory filterFactory) {
        Optional<URIFilter> geUriFilter = geFilters.keySet().stream()
                .map(key -> filterFactory.ge(key, geFilters.get(key)))
                .reduce(AndFilter::new);
        Optional<URIFilter> eqUriFilter = eqFilters.keySet().stream()
                .map(key -> filterFactory.eq(key, eqFilters.get(key)))
                .reduce(AndFilter::new);
        Optional<URIFilter> orEqUriFilter = orEqFilters.keySet().stream()
                .map(key -> orEqFilters.get(key).stream()
                        .map(eqValue -> filterFactory.eq(key, eqValue))
                        .reduce(filterFactory::or)
                )
                .filter(Optional::isPresent)
                .map(Optional::get)
                .reduce(AndFilter::new);
        Optional<URIFilter> containsUriFilter = containsFilter.keySet().stream()
                .map(key -> filterFactory.match(filterFactory.getArgFactory()
                        .contains(filterFactory.getArgFactory().property(key),
                                filterFactory.getArgFactory().literal(containsFilter.get(key)))))
                .reduce(AndFilter::new);
        Optional<URIFilter> geDateUriFilter = geDateFilters.keySet().stream()
                .map(key -> filterFactory.ge(
                        filterFactory.getArgFactory().date(filterFactory.getArgFactory().property(key)).build(),
                        geDateFilters.get(key)))
                .reduce(AndFilter::new);

        @SuppressWarnings("redundant")
        Optional<URIFilter> uriFilters = combineFilters(Arrays.asList(geUriFilter, eqUriFilter, orEqUriFilter, containsUriFilter, geDateUriFilter));
        return uriFilters;
    }

    private Optional<URIFilter> combineFilters(List<Optional<URIFilter>> uriFilter) {
        return uriFilter.stream()
                .filter(Optional::isPresent)
                .map(Optional::get)
                .reduce(AndFilter::new);
    }
}
