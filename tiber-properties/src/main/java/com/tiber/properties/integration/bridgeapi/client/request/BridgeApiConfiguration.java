package com.tiber.properties.integration.bridgeapi.client.request;


import lombok.Data;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.stereotype.Component;

import java.util.Map;

@Data
@Component
@ConfigurationProperties(prefix = "bridgeapi")
public class BridgeApiConfiguration {

    @Value("${bridgeapi.server.token}")
    private String accessToken;

    private String url;

    private Map<String, String> datasetMap;
}
