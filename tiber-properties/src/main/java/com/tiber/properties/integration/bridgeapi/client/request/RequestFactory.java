package com.tiber.properties.integration.bridgeapi.client.request;

import org.apache.olingo.client.api.communication.request.retrieve.ODataPropertyRequest;
import org.apache.olingo.client.api.domain.ClientProperty;
import org.apache.olingo.client.api.uri.URIFilter;
import org.springframework.lang.Nullable;

import java.net.URI;
import java.util.Collection;

public interface RequestFactory {

    Collection<ODataPropertyRequest<ClientProperty>> createRequestForPropertySet(URIFilter uriFilter,
                                                                                 Class<?> mappingClass, Integer top, Integer skip, String order);

    Collection<ODataPropertyRequest<ClientProperty>> createRequestForReplicatePropertySet(@Nullable URIFilter uriFilter,
                                                                                          Class<?> mappingClass);

    ODataPropertyRequest<ClientProperty> createRequestFromNextLink(URI uri);
}
