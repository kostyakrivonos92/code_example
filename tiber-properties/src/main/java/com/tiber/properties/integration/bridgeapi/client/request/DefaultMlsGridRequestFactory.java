package com.tiber.properties.integration.bridgeapi.client.request;

import org.apache.commons.lang3.StringUtils;
import org.apache.olingo.client.api.ODataClient;
import org.apache.olingo.client.api.communication.request.retrieve.ODataPropertyRequest;
import org.apache.olingo.client.api.domain.ClientProperty;
import org.apache.olingo.client.api.uri.FilterFactory;
import org.apache.olingo.client.api.uri.QueryOption;
import org.apache.olingo.client.api.uri.URIBuilder;
import org.apache.olingo.client.api.uri.URIFilter;
import org.springframework.beans.factory.annotation.Value;

import java.lang.reflect.Field;
import java.net.URI;
import java.util.Arrays;
import java.util.Collection;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

/**
 * @deprecated TODO
 */
//@Component("MLS")
class DefaultMlsGridRequestFactory extends AbstractRequestFactory {

    private final ODataClient client;

    private final String accessToken;

    private final String url;

    private final String[] datasetIds;

    //    @Autowired
    DefaultMlsGridRequestFactory(ODataClient client,
                                 @Value("${mlsgrid.server.token}") String accessToken,
                                 @Value("${mlsgrid.url}") String url,
                                 @Value("${mlsgrid.dataset_ids}") String[] datasetIds) {
        this.client = client;
        this.accessToken = accessToken;
        this.url = url;
        this.datasetIds = datasetIds;
    }

    @Override
    public Collection<ODataPropertyRequest<ClientProperty>> createRequestForPropertySet(URIFilter uriFilter,
                                                                                        Class<?> mappingClass,
                                                                                        Integer top, Integer skip, String order) {
        String fieldsToLoad = Arrays.stream(mappingClass.getDeclaredFields())
                .map(Field::getName)
                .map(StringUtils::capitalize)
                .collect(Collectors.joining(","));
        FilterFactory filterFactory = client.getFilterFactory();

        URIBuilder uriBuilder = client
                .newURIBuilder(url)
                .appendEntitySetSegment("Property")
                .addQueryOption(QueryOption.SELECT, fieldsToLoad);

        @SuppressWarnings("redundant")
        final List<ODataPropertyRequest<ClientProperty>> resultCollection = Arrays.stream(datasetIds).map(datasetId -> {
            URIFilter originatingSystemNameFilter = filterFactory.eq("OriginatingSystemName", datasetId);
            Optional.ofNullable(uriFilter)
                    .map(filter -> filterFactory.and(uriFilter, originatingSystemNameFilter))
                    .ifPresent(uriBuilder::filter);

            ODataPropertyRequest<ClientProperty> request = client.getRetrieveRequestFactory()
                    .getPropertyRequest(uriBuilder
                            .build());
            request.addCustomHeader("Accept-Encoding", "gzip");
            addAuthorizationHeader(request, accessToken);
            return request;
        })
                .collect(Collectors.toList());
        return resultCollection;
    }

    @Override
    public ODataPropertyRequest<ClientProperty> createRequestFromNextLink(URI uri) {
        throw new UnsupportedOperationException("Not implemented");
    }

    @Override
    public Collection<ODataPropertyRequest<ClientProperty>> createRequestForReplicatePropertySet(URIFilter uriFilter, Class<?> mappingClass) {
        throw new UnsupportedOperationException("Not implemented");
    }
}
