package com.tiber.properties.integration.bridgeapi.domain;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import lombok.Data;

@Data
@JsonIgnoreProperties(ignoreUnknown = true)
public class BridgeMlsMedia {
    private String mediaURL;
    private String mediaObjectID;
    private Integer order;
    private String mimeType;
    private String shortDescription;
    private String mediaCategory;
    private String resourceRecordKey;
    private String resourceName;
    private String className;
}
