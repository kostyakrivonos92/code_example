package com.tiber.properties.integration.bridgeapi.utility;

import com.fasterxml.jackson.core.JsonGenerator;
import com.fasterxml.jackson.databind.SerializerProvider;
import com.fasterxml.jackson.databind.ser.std.StdSerializer;

import java.io.IOException;
import java.io.Serial;
import java.time.Instant;

public class BridgeMlsInstantSerializer extends StdSerializer<Instant> {

    @Serial
    private static final long serialVersionUID = 849930934597381630L;

    public BridgeMlsInstantSerializer() {
        this(null);
    }

    public BridgeMlsInstantSerializer(Class<Instant> t) {
        super(t);
    }

    @Override
    public void serialize(Instant value, JsonGenerator gen, SerializerProvider provider) throws IOException {
        gen.writeString(value.toString());
    }
}
