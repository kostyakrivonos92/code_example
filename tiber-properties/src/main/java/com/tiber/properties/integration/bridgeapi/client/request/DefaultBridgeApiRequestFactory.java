package com.tiber.properties.integration.bridgeapi.client.request;

import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang3.StringUtils;
import org.apache.olingo.client.api.ODataClient;
import org.apache.olingo.client.api.communication.request.retrieve.ODataPropertyRequest;
import org.apache.olingo.client.api.domain.ClientProperty;
import org.apache.olingo.client.api.uri.QueryOption;
import org.apache.olingo.client.api.uri.URIBuilder;
import org.apache.olingo.client.api.uri.URIFilter;
import org.javatuples.Pair;
import org.springframework.lang.Nullable;
import org.springframework.stereotype.Component;

import java.lang.reflect.Field;
import java.net.URI;
import java.util.Arrays;
import java.util.Collection;
import java.util.List;
import java.util.Map;
import java.util.Objects;
import java.util.Optional;
import java.util.stream.Collectors;

@Component
@Slf4j
class DefaultBridgeApiRequestFactory extends AbstractRequestFactory {

    private final ODataClient client;

    private final String accessToken;

    private final String url;

    private final Map<String, String> datasetMap;

    DefaultBridgeApiRequestFactory(ODataClient client,
                                   BridgeApiConfiguration bridgeApiConfiguration) {
        this.client = client;
        this.accessToken = bridgeApiConfiguration.getAccessToken();
        this.url = bridgeApiConfiguration.getUrl();
        this.datasetMap = bridgeApiConfiguration.getDatasetMap();
    }

    @Override
    public Collection<ODataPropertyRequest<ClientProperty>> createRequestForPropertySet(@Nullable URIFilter uriFilter,
                                                                                        Class<?> mappingClass,
                                                                                        Integer top, Integer skip,
                                                                                        String order) {
        final List<Pair<String, String>> fieldsDatasetIdPairCollection = getFieldsDatasetIdPairCollection();

        @SuppressWarnings("redundant")
        final List<ODataPropertyRequest<ClientProperty>> resultCollection = fieldsDatasetIdPairCollection.stream()
                .map(fieldsDatasetPair -> {
                    URIBuilder uriBuilder = client
                            .newURIBuilder(url + fieldsDatasetPair.getValue1())
                            .appendEntitySetSegment("Property")
                            .top(top)
                            .skip(skip)
                            .orderBy(order)
                            .addQueryOption(QueryOption.SELECT, fieldsDatasetPair.getValue0());
                    Optional.ofNullable(uriFilter).ifPresent(uriBuilder::filter);
                    ODataPropertyRequest<ClientProperty> request = client.getRetrieveRequestFactory()
                            .getPropertyRequest(uriBuilder.build());
                    addAuthorizationHeader(request, accessToken);
                    return request;
                })
                .collect(Collectors.toList());
        return resultCollection;
    }

    @Override
    public Collection<ODataPropertyRequest<ClientProperty>> createRequestForReplicatePropertySet(@Nullable URIFilter uriFilter,
                                                                                                 Class<?> mappingClass) {
        final List<Pair<String, String>> fieldsDatasetIdPairCollection = getFieldsDatasetIdPairCollection();
        @SuppressWarnings("redundant")
        final List<ODataPropertyRequest<ClientProperty>> resultCollection = fieldsDatasetIdPairCollection.stream()
                .map(fieldsDatasetIdPair -> {
                    URIBuilder uriBuilder = client
                            .newURIBuilder(url + fieldsDatasetIdPair.getValue1())
                            .appendEntitySetSegment("Property")
                            .appendEntitySetSegment("replication")
                            .addQueryOption(QueryOption.SELECT, fieldsDatasetIdPair.getValue0());
                    Optional.ofNullable(uriFilter).ifPresent(uriBuilder::filter);
                    ODataPropertyRequest<ClientProperty> request = client.getRetrieveRequestFactory()
                            .getPropertyRequest(uriBuilder.build());
                    addAuthorizationHeader(request, accessToken);
                    return request;
                })
                .collect(Collectors.toList());
        return resultCollection;
    }

    private List<Pair<String, String>> getFieldsDatasetIdPairCollection() {
        return datasetMap.entrySet().stream()
                .map(entry -> {
                    try {
                        final Class<?> classForGet = Class.forName(entry.getKey());
                        String fieldsToLoad = Arrays.stream(classForGet.getDeclaredFields())
                                .map(Field::getName)
                                .map(StringUtils::capitalize)
                                .collect(Collectors.joining(","));
                        return Pair.with(fieldsToLoad, entry.getValue());
                    } catch (ClassNotFoundException e) {
                        log.error("Can't process property entry: {}", entry);
                        return null;
                    }
                })
                .filter(Objects::nonNull)
                .collect(Collectors.toList());
    }


    @Override
    public ODataPropertyRequest<ClientProperty> createRequestFromNextLink(URI uri) {
        ODataPropertyRequest<ClientProperty> request = client.getRetrieveRequestFactory()
                .getPropertyRequest(uri);
        addAuthorizationHeader(request, accessToken);
        return request;
    }
}
