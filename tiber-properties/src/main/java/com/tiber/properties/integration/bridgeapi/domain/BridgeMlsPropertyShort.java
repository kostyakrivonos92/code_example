package com.tiber.properties.integration.bridgeapi.domain;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.databind.PropertyNamingStrategy;
import com.fasterxml.jackson.databind.annotation.JsonNaming;
import lombok.Data;

@Data
@JsonNaming(PropertyNamingStrategy.UpperCamelCaseStrategy.class)
@JsonIgnoreProperties(ignoreUnknown = true)
public class BridgeMlsPropertyShort {
    private String listingKey;
    private String listingId;
    private String originatingSystemKey;
    private Integer yearBuilt;
    private Integer bedroomsTotal;
    private Integer bathroomsTotalInteger;
    private String postalCode;
    private String city;
    private String stateOrProvince;
    private String countyOrParish;
    private String unparsedAddress;
    private Float lotSizeSquareFeet;
}
