package com.tiber.properties.integration.bridgeapi.client.request;

import org.apache.olingo.client.api.communication.request.retrieve.ODataPropertyRequest;
import org.apache.olingo.client.api.domain.ClientProperty;

abstract class AbstractRequestFactory implements RequestFactory {

    protected void addAuthorizationHeader(ODataPropertyRequest<ClientProperty> request, String accessToken) {
        request.addCustomHeader("Authorization", "Bearer " + accessToken);
    }
}
