package com.tiber.properties.integration.bridgeapi.utility;

import com.fasterxml.jackson.core.JsonParser;
import com.fasterxml.jackson.databind.DeserializationContext;
import com.fasterxml.jackson.databind.deser.std.StdDeserializer;

import java.io.IOException;
import java.time.Instant;

public class BridgeMlsInstantDeserializer extends StdDeserializer<Instant> {

    public BridgeMlsInstantDeserializer() {
        this(null);
    }

    public BridgeMlsInstantDeserializer(Class<Instant> t) {
        super(t);
    }

    @Override
    public Instant deserialize(JsonParser p, DeserializationContext ctxt) throws IOException {
        String valueAsString = p.getValueAsString();
        return Instant.parse(valueAsString);
    }
}
