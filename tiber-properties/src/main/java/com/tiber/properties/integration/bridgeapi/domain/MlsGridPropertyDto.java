package com.tiber.properties.integration.bridgeapi.domain;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.databind.PropertyNamingStrategy;
import com.fasterxml.jackson.databind.annotation.JsonNaming;
import lombok.Data;

/**
 * @deprecated  TODO
 */
@Data
@JsonNaming(PropertyNamingStrategy.UpperCamelCaseStrategy.class)
@JsonIgnoreProperties(ignoreUnknown = true)
public class MlsGridPropertyDto {

    //TODO extend with appropriate fields
    private String listingKey;
    private String listingId;
    private String originatingSystemName;
    private Integer yearBuilt;
    private Integer bedroomsTotal;
    private Integer bathroomsTotalInteger;
    private Integer bathroomsFull;
    private Integer bathroomsHalf;
}
