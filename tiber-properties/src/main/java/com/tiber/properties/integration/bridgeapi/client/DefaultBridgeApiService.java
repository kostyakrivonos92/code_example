package com.tiber.properties.integration.bridgeapi.client;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.PropertyNamingStrategy;
import com.tiber.properties.integration.bridgeapi.client.request.OdataFilterFactory;
import com.tiber.properties.integration.bridgeapi.client.request.RequestFactory;
import com.tiber.properties.integration.bridgeapi.domain.BridgeMlsProperty;
import io.reactivex.rxjava3.core.Flowable;
import io.reactivex.rxjava3.processors.FlowableProcessor;
import io.reactivex.rxjava3.processors.PublishProcessor;
import lombok.extern.log4j.Log4j2;
import org.apache.commons.lang3.StringUtils;
import org.apache.olingo.client.api.communication.request.retrieve.ODataPropertyRequest;
import org.apache.olingo.client.api.domain.ClientProperty;
import org.apache.olingo.client.api.uri.URIFilter;
import org.apache.olingo.commons.api.edm.EdmPrimitiveTypeException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

import java.net.URI;
import java.time.Instant;
import java.time.ZonedDateTime;
import java.util.Arrays;
import java.util.Collection;
import java.util.Collections;
import java.util.HashMap;
import java.util.Map;
import java.util.Optional;
import java.util.concurrent.CompletableFuture;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

import static com.tiber.shared.vocabulary.BridgePropertyParameterName.MODIFICATION_TIMESTAMP;
import static com.tiber.shared.vocabulary.BridgePropertyParameterName.PROPERTY_SUB_TYPE;
import static com.tiber.shared.vocabulary.BridgePropertyParameterName.PROPERTY_TYPE;
import static com.tiber.shared.vocabulary.BridgePropertyParameterName.YEAR_BUILT;
import static com.tiber.shared.vocabulary.BridgePropertyParameterValues.PROPERTY_SUB_TYPE_SFR;
import static com.tiber.shared.vocabulary.BridgePropertyParameterValues.PROPERTY_SUB_TYPE_SFR_OLD;
import static com.tiber.shared.vocabulary.BridgePropertyParameterValues.RESIDENTIAL;
import static com.tiber.shared.vocabulary.BridgePropertyParameterValues.RESIDENTIAL_LEASE;

@Log4j2
@Service
class DefaultBridgeApiService implements BridgeApiService {

    private final RequestFactory requestFactory;
    private final OdataFilterFactory odataFilterFactory;
    private final String ODATA_NEXT_LINK_ANNOTATION_NAME = "odata.nextLink";
    private final Integer yearsBackForAnalyzing;
    ExecutorService uploadExecutor = Executors.newSingleThreadExecutor();
    ExecutorService saveExecutor = Executors.newSingleThreadExecutor();

    @Autowired
    public DefaultBridgeApiService(RequestFactory requestFactory, @Qualifier("BridgeApi") OdataFilterFactory odataFilterFactory,
                                   @Value("${bridgeapi.yearsBackForAnalyzing}") Integer yearsBackForAnalyzing) {
        this.requestFactory = requestFactory;
        this.odataFilterFactory = odataFilterFactory;
        this.yearsBackForAnalyzing = yearsBackForAnalyzing;
    }

    @Override
    public Flowable<BridgeMlsProperty> fetchEntities(Integer yearBuilt, Instant modificationTimeStamp, Integer top, Integer skip, String order) {
        Map<String, Object> geFilters = getGeFilters(yearBuilt, modificationTimeStamp);
        Map<String, Collection<String>> orEqFilters = getOrEqFilters();
        Map<String, String> eqFilters = getEqFilters();
        URIFilter uriFilter = odataFilterFactory.createAddByMaps(geFilters, eqFilters, orEqFilters, Collections.emptyMap(), Collections.emptyMap()).orElse(null);
        final Collection<ODataPropertyRequest<ClientProperty>> requestForPropertySetCollection =
                requestFactory.createRequestForPropertySet(uriFilter, BridgeMlsProperty.class, top, skip, order);
        return Flowable.fromIterable(requestForPropertySetCollection)
                .flatMap(requestForPropertySet -> getBridgeMlsPropertyFlowable(requestForPropertySet).doOnError(log::error));
    }

    @Override
    public Flowable<BridgeMlsProperty> replicateEntities(Integer yearBuilt) {
        final Instant modificationTimeStamp = ZonedDateTime.now().minusYears(yearsBackForAnalyzing).toInstant();
        Map<String, Object> geFilters = getGeFilters(yearBuilt, modificationTimeStamp);
        Map<String, Collection<String>> orEqFilters = getOrEqFilters();
        Map<String, String> eqFilters = getEqFilters();
        URIFilter uriFilter = odataFilterFactory.createAddByMaps(geFilters, eqFilters, orEqFilters,
                Collections.emptyMap(), Collections.emptyMap()).orElse(null);
        final Collection<ODataPropertyRequest<ClientProperty>> requestForReplicatePropertySetCollection =
                requestFactory.createRequestForReplicatePropertySet(uriFilter, BridgeMlsProperty.class);
        return Flowable.fromIterable(requestForReplicatePropertySetCollection)
                .flatMap(requestForReplicatePropertySet -> getBridgeMlsPropertyFlowable(requestForReplicatePropertySet).doOnError(log::error));
    }

    private Map<String, Instant> getLastModificationFilter(Instant modificationTimeStamp) {
        Map<String, Instant> geDateFilters = Collections.singletonMap(StringUtils.capitalize(MODIFICATION_TIMESTAMP), modificationTimeStamp);
        return geDateFilters;
    }

    private Map<String, String> getEqFilters() {
        Map<String, String> eqFilters = new HashMap<>();
        return eqFilters;
    }

    private Map<String, Collection<String>> getOrEqFilters() {
        Map<String, Collection<String>> orEqFilters = new HashMap<>();
        orEqFilters.put(StringUtils.capitalize(PROPERTY_TYPE), Arrays.asList(RESIDENTIAL, RESIDENTIAL_LEASE));
        orEqFilters.put(StringUtils.capitalize(PROPERTY_SUB_TYPE), Arrays.asList(PROPERTY_SUB_TYPE_SFR, PROPERTY_SUB_TYPE_SFR_OLD));
        return orEqFilters;
    }

    private Map<String, Object> getGeFilters(Integer yearBuilt, Instant modificationTimeStamp) {
        Map<String, Object> geFilters = new HashMap<>();
        geFilters.put(StringUtils.capitalize(YEAR_BUILT), yearBuilt);
        geFilters.put(StringUtils.capitalize(MODIFICATION_TIMESTAMP), modificationTimeStamp);
        return geFilters;
    }

    private Flowable<BridgeMlsProperty> getBridgeMlsPropertyFlowable(ODataPropertyRequest<ClientProperty> requestForPropertySet) {
        log.debug("Getting deals from bridge {}", requestForPropertySet.getURI());
        ClientProperty clientProperty = requestForPropertySet.execute().getBody();
        log.debug("Deals is got from bridge");
        final FlowableProcessor<BridgeMlsProperty> bridgeMlsPropertyProcessor = PublishProcessor.create();
        final ObjectMapper mapper = new ObjectMapper();
        mapper.setPropertyNamingStrategy(PropertyNamingStrategy.UPPER_CAMEL_CASE);
        CompletableFuture.runAsync(
                        () -> clientProperty.getCollectionValue().asJavaCollection().stream()
                                .map(value -> {
                                    final BridgeMlsProperty bridgeMlsProperty = mapper.convertValue(value, BridgeMlsProperty.class);
                                    return bridgeMlsProperty;
                                })
                                .forEach(bridgeMlsPropertyProcessor::onNext),
                        saveExecutor)
                .whenComplete(((unused, throwable) -> {
                    if (throwable != null) {
                        log.error("Error during fetching deals from BridgeApi {}", throwable.getLocalizedMessage());
                        return;
                    }
                    Optional<URI> nextLink = getNextLink(clientProperty);
                    if (nextLink.isEmpty()) {
                        bridgeMlsPropertyProcessor.onComplete();
                    }
                }));
        Optional<URI> nextLink = getNextLink(clientProperty);
        nextLink.ifPresent(s -> getClientProperty(s, bridgeMlsPropertyProcessor, mapper));
        return bridgeMlsPropertyProcessor;
    }

    private void getClientProperty(URI nextLink,
                                   FlowableProcessor<BridgeMlsProperty> bridgeMlsPropertyProcessor,
                                   ObjectMapper mapper) {
        log.info("Upload from next link from Bridge API: " + nextLink);
        CompletableFuture.supplyAsync(() -> requestFactory.createRequestFromNextLink(nextLink).execute().getBody(),
                        uploadExecutor)
                .thenAcceptAsync(body -> {
                            Optional<URI> newNextLink = getNextLink(body);
                            newNextLink.ifPresent(s -> getClientProperty(s, bridgeMlsPropertyProcessor, mapper));
                            Collection<Object> objects = body.getCollectionValue().asJavaCollection();
                            objects.stream()
                                    .map(value -> mapper.convertValue(value, BridgeMlsProperty.class))
                                    .forEach(bridgeMlsPropertyProcessor::onNext);
                            if (newNextLink.isEmpty()) {
                                bridgeMlsPropertyProcessor.onComplete();
                            }
                        },
                        saveExecutor);
    }

    private Optional<URI> getNextLink(ClientProperty clientProperty) {
        return clientProperty.getAnnotations().stream()
                .filter(clientAnnotation1 -> clientAnnotation1.getTerm().equals(ODATA_NEXT_LINK_ANNOTATION_NAME))
                .findAny()
                .map(clientAnnotation -> {
                    try {
                        return clientAnnotation.getPrimitiveValue().toCastValue(String.class);
                    } catch (EdmPrimitiveTypeException e) {
                        e.printStackTrace();
                    }
                    return null;
                })
                .map(URI::create);
    }
}
