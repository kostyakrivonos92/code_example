package com.tiber.properties.integration.bridgeapi.client.request;

import org.apache.olingo.client.api.uri.URIFilter;

import java.time.Instant;
import java.util.Collection;
import java.util.Map;
import java.util.Optional;

public interface OdataFilterFactory {

    Optional<URIFilter> createAddByMaps(Map<String, Object> geFilters,
                                        Map<String, String> eqFilters,
                                        Map<String, Collection<String>> orEqFilters,
                                        Map<String, Instant> geDateFilters,
                                        Map<String, String> containsFilter);
}
