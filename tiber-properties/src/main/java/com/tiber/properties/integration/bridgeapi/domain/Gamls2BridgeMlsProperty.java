package com.tiber.properties.integration.bridgeapi.domain;

import com.fasterxml.jackson.annotation.JsonAlias;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.tiber.properties.integration.bridgeapi.utility.BridgeMlsInstantDeserializer;
import com.tiber.properties.integration.bridgeapi.utility.BridgeMlsInstantSerializer;
import com.tiber.shared.util.LocalDateDeserializer;
import com.tiber.shared.util.LocalDateSerializer;
import lombok.Data;

import java.time.Instant;
import java.time.LocalDate;
import java.util.List;

@Data
@JsonIgnoreProperties(ignoreUnknown = true)
public class Gamls2BridgeMlsProperty implements MlsProperty {

    private String listingKey;

    private String listingId;

    private String originatingSystemKey;

    private Integer yearBuilt;

    private Integer bedroomsTotal;

    private Integer bathroomsTotalInteger;

    private Integer bathroomsFull;

    private Integer bathroomsHalf;

    private Integer fireplacesTotal;

    private List<String> fireplaceFeatures;

    private Boolean garageYN;

    private Boolean heatingYN;

    private List<String> heating;

    private Integer parkingTotal;

    private List<String> poolFeatures;

    private List<String> roof;

    private Integer storiesTotal;

    private List<String> architecturalStyle;

    private List<String> electric;

    private String electricOnPropertyYN;

    private String fuelExpense;

    private List<BridgeMlsMedia> media;

    private String postalCode;

    private String city;

    private String country;

    private String countyOrParish;

    private String stateOrProvince;

    private String unparsedAddress;

    private String streetName;

    private String streetNumber;

    private String unitNumber;

    private List<Double> coordinates;

    private Integer listPrice;

    private Integer originalListPrice;

    private Integer closePrice;

    private Integer taxAnnualAmount;

    private Integer taxYear;

    @JsonAlias("STELLAR_PublicRemarks")
    private String publicRemarks;

    private String standardStatus;

    @JsonDeserialize(using = BridgeMlsInstantDeserializer.class)
    @JsonSerialize(using = BridgeMlsInstantSerializer.class)
    private Instant statusChangeTimestamp;

    @JsonDeserialize(using = BridgeMlsInstantDeserializer.class)
    @JsonSerialize(using = BridgeMlsInstantSerializer.class)
    private Instant modificationTimestamp;

    @JsonDeserialize(using = BridgeMlsInstantDeserializer.class)
    @JsonSerialize(using = BridgeMlsInstantSerializer.class)
    private Instant bridgeModificationTimestamp;

    private Integer lotSizeSquareFeet;

    private String listOfficeKey;

    private String listOfficeMlsId;

    @JsonDeserialize(using = LocalDateDeserializer.class)
    @JsonSerialize(using = LocalDateSerializer.class)
    private LocalDate onMarketDate;

    @JsonDeserialize(using = LocalDateDeserializer.class)
    @JsonSerialize(using = LocalDateSerializer.class)
    private LocalDate closeDate;

    private Integer cumulativeDaysOnMarket;

    private String propertyType;

    private String propertySubType;
}
