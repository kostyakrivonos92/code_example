update only partner.public.partners
set balance = 0,
    booster_amount = 0,
    referral_level=0,
    activity_points=0,
    allocation_amount = 0
where partner.public.partners.balance is null
   or partner.public.partners.booster_amount is null
   or partner.public.partners.referral_level is null
   or partner.public.partners.activity_points is null
   or partner.public.partners.allocation_amount is null;