package com.artbay.marketplace.partner.model.dto;

import lombok.*;

import java.util.List;
import java.util.UUID;

@Getter
@Setter
@Builder
@AllArgsConstructor
@NoArgsConstructor
public class MysteryBoxPrizeProbabilityShortDto {

    private UUID id;

    private Integer boxLevel;

    private List<OpenPrizeDto> prizes;
}
