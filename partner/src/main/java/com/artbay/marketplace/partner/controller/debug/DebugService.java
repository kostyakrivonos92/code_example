package com.artbay.marketplace.partner.controller.debug;

import com.artbay.marketplace.common.client.CryptoServiceClient;
import com.artbay.marketplace.common.client.auth.debug.FusionauthAuthDebugClient;
import com.artbay.marketplace.common.client.auth.internal.FusionauthInternalClient;
import com.artbay.marketplace.common.enums.Role;
import com.artbay.marketplace.common.exception.ErrorCode;
import com.artbay.marketplace.common.exception.GlobalException;
import com.artbay.marketplace.common.mapper.UserDtoMapper;
import com.artbay.marketplace.common.model.request.RegistrationRequest;
import com.artbay.marketplace.common.service.MoosendService;
import com.artbay.marketplace.partner.model.Partner;
import com.artbay.marketplace.partner.model.mapper.PartnerMapper;
import com.artbay.marketplace.partner.repository.PartnerRepository;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.MessageSource;
import org.springframework.context.annotation.Profile;
import org.springframework.context.i18n.LocaleContextHolder;
import org.springframework.stereotype.Service;

import javax.transaction.Transactional;
import java.util.Arrays;
import java.util.UUID;
import java.util.stream.Collectors;

//ONLY FOR DEBUG PURPOSES!!!

@Deprecated
@Slf4j
@Service
@RequiredArgsConstructor
@Profile({"!prod"})
public class DebugService {

    private final PartnerRepository partnerRepository;

    private final UserDtoMapper userDtoMapper;
    private final PartnerMapper partnerMapper;

    private final FusionauthInternalClient fusionauthInternalClient;
    private final FusionauthAuthDebugClient fusionauthAuthDebugClient;

    private final CryptoServiceClient cryptoServiceClient;
    private final MoosendService moosendService;

    @Value("${moosend.list.registered}")
    private String registeredList;
    @Value("${moosend.list.registered.artist}")
    private String registeredArtistList;

    @Qualifier("activeMessageSource")
    private final MessageSource messageSource;


    @Transactional
    public void createPartner(RegistrationRequest registrationRequest) {
        checkExistsPartnerAndDelete(registrationRequest);
        var partnerBuilder = Partner.builder()
                                 .id(registrationRequest.getUserId())
                                 .referrerId(fetchReferrerId(registrationRequest))
                                 .username(registrationRequest.getUsername())
                                 .firstName(registrationRequest.getFirstName())
                                 .lastName(registrationRequest.getLastName())
                                 .email(registrationRequest.getEmail())
                                 .country(registrationRequest.getCountry());

        var response = cryptoServiceClient.createWallet();
        partnerBuilder.cryptowalletPrivateKey(response.getPrivateKey());
        partnerBuilder.cryptowalletPublicKey(response.getPublicKey());

        var partner = partnerBuilder.build();
        partnerRepository.save(partner);

        var createUserRequest = userDtoMapper.fromRegistrationRequest(registrationRequest);
        createUserRequest.setRoles(Arrays.stream(Role.PARTNER.values())
                                       .map(Role.PARTNER::getRoleName)
                                       .collect(Collectors.toSet()));
        createUserRequest.setId(registrationRequest.getUserId());
        fusionauthAuthDebugClient.createUser(createUserRequest);

        if (partner.getIsArtist()) {
            moosendService.addToEmailList(partnerMapper.partnerToDto(partner), registeredArtistList);
        } else {
            moosendService.addToEmailList(partnerMapper.partnerToDto(partner), registeredList);
        }
    }

    public Partner getPartnerByEmail(String email) {
        return partnerRepository.findFirstByEmail(email);
    }

    private UUID fetchReferrerId(RegistrationRequest registrationRequest) {
        Partner referrer = partnerRepository.findFirstByUsernameIgnoreCase(registrationRequest.getReferrerUsername());
        if (referrer != null && referrer.getId() != null && referrer.getIsVerified()) {
            return referrer.getId();
        } else {
            throw new GlobalException(
                ErrorCode.ERROR_REFERRER_NOT_FOUND,
                messageSource.getMessage("REFERRER_NOT_FOUND", null, LocaleContextHolder.getLocale())
            );
        }
    }

    private void checkExistsPartnerAndDelete(RegistrationRequest registrationRequest) {
        var existsPartner = getPartnerByEmail(registrationRequest.getEmail());
        if (existsPartner != null) {
            if (existsPartner.getIsVerified()) {
                throw new GlobalException(
                    ErrorCode.EMAIL_EXISTS,
                    messageSource.getMessage("USER_ALREADY_EXISTS", null, LocaleContextHolder.getLocale())
                );
            } else {
                fusionauthInternalClient.removeUser(existsPartner.getId());
                partnerRepository.delete(existsPartner);
            }
        }
    }

}
