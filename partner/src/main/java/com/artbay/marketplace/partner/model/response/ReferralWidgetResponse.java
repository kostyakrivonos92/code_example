package com.artbay.marketplace.partner.model.response;

import lombok.Builder;
import lombok.Data;

@Data
@Builder
public class ReferralWidgetResponse {

    private long partnersRegisteredFirstLine;
    private long partnersRegisteredTotal;

    private long nftKeyholdersFirstLine;
    private long nftKeyholdersTotal;

    private double rewardsEarnedFirstLine;
    private double rewardsEarnedTotal;

}
