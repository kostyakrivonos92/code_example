package com.artbay.marketplace.partner.controller;

import com.artbay.marketplace.partner.model.response.ActivityChartResponse;
import com.artbay.marketplace.partner.model.response.ArtzWidgetResponse;
import com.artbay.marketplace.partner.model.response.ReferralWidgetResponse;
import com.artbay.marketplace.partner.service.DashboardService;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.format.annotation.DateTimeFormat;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import java.time.LocalDate;

import static com.artbay.marketplace.common.utils.ControllerUtil.getUserId;

@Slf4j
@RestController
@RequestMapping("/dashboard")
@RequiredArgsConstructor
public class DashboardController {

    private final DashboardService dashboardService;

    @GetMapping("/activity-chart")
    @PreAuthorize("hasAuthority('user')")
    public ActivityChartResponse getActivityChart(
        @RequestParam("dateFrom") @DateTimeFormat(iso = DateTimeFormat.ISO.DATE) LocalDate dateFrom,
        @RequestParam("dateTo") @DateTimeFormat(iso = DateTimeFormat.ISO.DATE) LocalDate dateTo
    ) {
        return dashboardService.getActivityChart(getUserId(), dateFrom, dateTo);
    }

    @GetMapping("/artz-widget")
    @PreAuthorize("hasAuthority('user')")
    public ArtzWidgetResponse getArtzWidgetData() {
        return dashboardService.getArtzWidgetData(getUserId());
    }

    @GetMapping("/referral-widget")
    @PreAuthorize("hasAuthority('user')")
    public ReferralWidgetResponse getReferralWidgetData() {
        return dashboardService.getReferralWidgetData(getUserId());
    }

}
