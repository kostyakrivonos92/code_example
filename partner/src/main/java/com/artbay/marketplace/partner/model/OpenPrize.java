package com.artbay.marketplace.partner.model;

import com.artbay.marketplace.common.model.enums.MysteryBoxPrizeType;
import lombok.*;

import java.util.UUID;

@Getter
@Setter
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class OpenPrize {

    private MysteryBoxPrizeType type;
    private Integer level;
    private Double countMin;
    private Double countMax;
    private Double probability;
    private Double amount;
    private UUID prizeNftId;

}
