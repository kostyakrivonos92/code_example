package com.artbay.marketplace.partner;

import com.artbay.marketplace.common.client.referral.ReferralTransactionClient;
import com.artbay.marketplace.common.model.TransactionDTO;
import com.artbay.marketplace.common.model.enums.TransactionEntityType;
import com.artbay.marketplace.partner.model.Partner;
import com.artbay.marketplace.partner.service.PartnerService;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.boot.ApplicationArguments;
import org.springframework.boot.ApplicationRunner;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;
import java.util.UUID;
import java.util.concurrent.Executors;
import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.TimeUnit;
import java.util.stream.Collectors;

@Component
@Slf4j
@RequiredArgsConstructor
public class PartnerApplicationStartupRunner implements ApplicationRunner {

    private final PartnerService partnerService;
    private final ReferralTransactionClient referralTransactionClient;
    private final ScheduledExecutorService executorService = Executors.newSingleThreadScheduledExecutor();

    @Override
    @Transactional
    public void run(ApplicationArguments args) {
        executorService.schedule(this::updateBalances, 20, TimeUnit.SECONDS);
    }


    private void updateBalances() {
        log.info("Updating user balances after restart");
        List<TransactionDTO> failedToUpdateBalanceTransactions =
            referralTransactionClient.getFailedToUpdateBalanceTransactions();
        List<UUID> partnerIds = failedToUpdateBalanceTransactions.stream()
                                    .map(TransactionDTO::getReceiverId)
                                    .distinct()
                                    .collect(Collectors.toList());
        log.info("Total # of users with incorrect balances is {}", partnerIds.size());

        List<Partner> partners = partnerService.getPartnersByIds(partnerIds);

        partners.forEach(partner -> {
            Double artUpBalance = referralTransactionClient.getBalanceForSpecificEntityType(
                partner.getId(),
                TransactionEntityType.BOOSTER_BALANCE
            );
            partner.setBoosterAmount(artUpBalance);

            Double apBalance = referralTransactionClient.getBalanceForSpecificEntityType(
                partner.getId(),
                TransactionEntityType.ACTIVITY_POINTS_BALANCE
            );
            partner.setActivityPointsAmount(apBalance.intValue());

            partnerService.save(partner);
        });

        referralTransactionClient.markTransactionsAsExecuted(failedToUpdateBalanceTransactions);
        log.info("Finished updating balances");
    }

}


