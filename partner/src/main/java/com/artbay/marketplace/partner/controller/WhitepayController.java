package com.artbay.marketplace.partner.controller;

import com.artbay.marketplace.common.client.referral.ReferralWhitepayClient;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import java.util.Map;

import static com.artbay.marketplace.common.utils.ControllerUtil.getUserId;

@Slf4j
@RestController
@RequestMapping("/whitepay")
@RequiredArgsConstructor
public class WhitepayController {

    private final ReferralWhitepayClient whitepayClient;

    @GetMapping("/payment-status")
    public Map<String, String> getPaymentStatus() {
        return whitepayClient.getPaymentStatus(getUserId());
    }

    @GetMapping("/purchase-nft-key")
    public Map<String, String> purchaseNftKey() {
        return whitepayClient.purchaseNftKey(getUserId());
    }

    @GetMapping("/purchase-artz")
    public Map<String, String> purchaseArtz(@RequestParam Double amount) {
        return whitepayClient.purchaseArtz(getUserId(), amount);
    }
}
