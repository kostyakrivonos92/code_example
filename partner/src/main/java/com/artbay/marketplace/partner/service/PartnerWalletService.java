package com.artbay.marketplace.partner.service;

import com.artbay.marketplace.common.client.referral.ReferralExchangeRatesClient;
import com.artbay.marketplace.common.client.referral.ReferralPaymentClient;
import com.artbay.marketplace.common.model.PersonalWalletDto;
import com.artbay.marketplace.partner.model.Partner;
import com.artbay.marketplace.partner.repository.PartnerRepository;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

import java.util.UUID;

@Slf4j
@Service
@RequiredArgsConstructor
public class PartnerWalletService {

    @Value("${app.artz.token.address}")
    private String artzTokenAddress;

    private final PartnerRepository partnerRepository;
    private final ReferralPaymentClient referralPaymentClient;

    private final ReferralExchangeRatesClient referralExchangeRatesClient;

    public PersonalWalletDto getPersonalWalletData(UUID partnerId) {
        Partner partner = partnerRepository.getById(partnerId);
        return PersonalWalletDto.builder()
                   .currentRateUsd(getArtzPriceInUsd())
                   .walletPublicKey(partner.getCryptowalletPublicKey())
                   .tokenAddress(artzTokenAddress)
                   .build();
    }

    public String withdrawalTokensToExternalWallet(
        UUID partnerId,
        String externalWalletAddress,
        String mintAddress,
        Double amount
    ) {
        return referralPaymentClient.withdrawalTokensToExternalWallet(
            partnerId,
            amount,
            externalWalletAddress,
            mintAddress,
            false
        );
    }

    private Double getArtzPriceInUsd() {
        return referralExchangeRatesClient.getUsdArtzPair().getPrice();
    }

}
