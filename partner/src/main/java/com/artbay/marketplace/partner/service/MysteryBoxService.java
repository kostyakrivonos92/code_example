package com.artbay.marketplace.partner.service;

import com.artbay.marketplace.common.client.marketplace.internal.MarketplaceInternalClient;
import com.artbay.marketplace.common.client.partner.internal.PartnerInternalClient;
import com.artbay.marketplace.common.client.referral.ReferralExchangeRatesClient;
import com.artbay.marketplace.common.client.referral.internal.ReferralInternalClient;
import com.artbay.marketplace.common.exception.ErrorCode;
import com.artbay.marketplace.common.exception.GlobalException;
import com.artbay.marketplace.common.model.ArtworkDTO;
import com.artbay.marketplace.common.model.mysterybox.MysteryBoxDto;
import com.artbay.marketplace.partner.model.MysteryBox;
import com.artbay.marketplace.partner.model.MysteryBoxActivityHistory;
import com.artbay.marketplace.partner.model.MysteryBoxPrizeProbability;
import com.artbay.marketplace.partner.model.OpenPrize;
import com.artbay.marketplace.partner.model.mapper.MysteryBoxMapper;
import com.artbay.marketplace.partner.model.mapper.MysteryBoxPrizeMapper;
import com.artbay.marketplace.partner.model.response.MysteryBoxPageResponse;
import com.artbay.marketplace.partner.repository.MysteryBoxActivityHistoryRepository;
import com.artbay.marketplace.partner.repository.MysteryBoxPrizeProbabilityRepository;
import com.artbay.marketplace.partner.repository.MysteryBoxRepository;
import com.artbay.marketplace.partner.util.MysteryBoxPriceConstants;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.context.MessageSource;
import org.springframework.context.i18n.LocaleContextHolder;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

import java.util.*;
import java.util.stream.Collectors;

import static com.artbay.marketplace.partner.model.enums.MysteryBoxOperationType.MYSTERY_BOX_OPEN;
import static com.artbay.marketplace.partner.util.MysteryBoxActivityHistoryUtil.*;

@Slf4j
@Service
@RequiredArgsConstructor
public class MysteryBoxService {

    private final PartnerInternalClient partnerInternalClient;
    private final ReferralInternalClient referralInternalClient;
    private final ReferralExchangeRatesClient referralExchangeRatesClient;
    private final MarketplaceInternalClient marketplaceInternalClient;

    private final MysteryBoxRepository mysteryBoxRepository;
    private final MysteryBoxPrizeProbabilityRepository prizeProbabilityRepository;
    private final MysteryBoxActivityHistoryRepository activityHistoryRepository;

    private final MysteryBoxMapper mysteryBoxMapper;
    private final MysteryBoxPrizeMapper mysteryBoxPrizeMapper;
    private final MysteryBoxPriceConstants mysteryBoxPriceConstants;

    @Qualifier("activeMessageSource")
    private final MessageSource messageSource;

    public MysteryBox createMysteryBox(MysteryBoxDto boxDto) {
        var mysteryBox = mysteryBoxRepository.save(mysteryBoxMapper.dtoToEntity(boxDto));
        activityHistoryRepository.save(buildCreateMysteryBoxActivityHistory(
            mysteryBox.getOwnerId(),
            mysteryBox.getId(),
            mysteryBox.getLevel()
        ));
        return mysteryBox;
    }

    public void purchaseMysteryBox(int boxLevel, int count, UUID userId) {
        double boxPrice = MysteryBoxPriceConstants.PRICE_MYSTERY_BOX.get(boxLevel) /
                              referralExchangeRatesClient.getArtzUsdPair().getPrice();
        List<MysteryBox> boxList = new ArrayList<>();
        for (int box = 0; box < count; box = box + 1) {
            MysteryBox mysteryBox = MysteryBox.builder()
                                        .price(boxPrice)
                                        .ownerId(userId)
                                        .level(boxLevel)
                                        .build();
            boxList.add(mysteryBox);
        }

        referralInternalClient.buyMysteryBox(userId, boxPrice, count, boxLevel);

        List<MysteryBoxActivityHistory> activityHistory = new ArrayList<>();
        mysteryBoxRepository.saveAll(boxList)
            .forEach(mysteryBox -> activityHistory.add(buildBuyMysteryBoxActivityHistory(
                userId,
                mysteryBox.getId(),
                mysteryBox.getLevel(),
                mysteryBox.getPrice()
            )));
        activityHistoryRepository.saveAll(activityHistory);
    }

    public void exchangeMysteryBox(int boxLevel, UUID userId) {
        var boxList = mysteryBoxRepository.findByOwnerIdAndOpenedIsFalseAndExchangedIsFalse(userId);
        checkBoxCountForExchange(boxLevel, boxList);

        var boxesForExchange = getExchangeMysteryBoxes(boxLevel, boxList);
        MysteryBox mysteryBox = MysteryBox.builder()
                                    .ownerId(userId)
                                    .level(boxLevel)
                                    .build();

        boxesForExchange.add(mysteryBox);
        mysteryBoxRepository.saveAll(boxesForExchange);

        activityHistoryRepository.save(buildExchangeMysteryBoxActivityHistory(
            userId,
            mysteryBox.getId(),
            boxLevel
        ));
    }

    public OpenPrize openMysteryBox(int boxLevel, UUID userId) {
        var partner = partnerInternalClient.getPartnerById(userId);
        if (partner.isHasNftKey()) {
            var mysteryBox = mysteryBoxRepository.findByOwnerIdAndOpenedIsFalseAndExchangedIsFalse(userId).stream()
                                 .filter(box -> box.getLevel() == boxLevel)
                                 .findFirst();

            if (mysteryBox.isPresent()) {
                var prize = calculatePrize(boxLevel);
                ArtworkDTO prizeNft = null;
                double amount = 0.0;
                int prizeLevel = 0;
                UUID nftPrizeId = null;
                SplittableRandom random = new SplittableRandom();
                switch (prize.getType()) {
                    case AP: {
                        amount = Math.round(random.nextDouble(prize.getCountMin(), prize.getCountMax()));
                        referralInternalClient.accrualPrizeActivityPoints(userId, amount);
                        break;
                    }
                    case ARTZ: {
                        amount = random.nextDouble(prize.getCountMin(), prize.getCountMax()) /
                                     referralExchangeRatesClient.getArtzUsdPair().getPrice();
                        referralInternalClient.accrualPrizeArtzToken(userId, amount);
                        break;
                    }
                    case ARTUP: {
                        amount = 1;
                        prizeLevel = prize.getLevel();
                        marketplaceInternalClient.createMysteryBoxPrizeArtUpBooster(userId, prize.getLevel());
                        break;
                    }
                    case NFT: {
                        amount = 1;
                        prizeNft = marketplaceInternalClient.accrualMysteryBoxPrizeNft(userId, prize.getLevel());

                        if (prizeNft == null) {
                            return openMysteryBox(boxLevel, userId);
                        }
                        break;
                    }
                }
                if(prizeNft != null){
                    nftPrizeId = prizeNft.getId();
                    prize.setPrizeNftId(nftPrizeId);
                    prizeLevel = prizeNft.getLevel();
                }
                prize.setAmount(amount);
                mysteryBox.get().setOpened(true);
                mysteryBoxRepository.save(mysteryBox.get());
                activityHistoryRepository.save(buildOpenMysteryBoxActivityHistory(
                    userId,
                    mysteryBox.get().getId(),
                    boxLevel,
                    prize.getType(),
                    amount,
                    prizeLevel,
                    nftPrizeId
                ));
                return prize;
            } else {
                throw new GlobalException(ErrorCode.BAD_REQUEST, messageSource.getMessage(
                    "NOT_ENOUGH_MYSTERY_BOX",
                    null,
                    LocaleContextHolder.getLocale()
                ));
            }
        } else {
            throw new GlobalException(ErrorCode.ERROR_VALIDATION, messageSource.getMessage(
                "NOT_NFT_KEY",
                null,
                LocaleContextHolder.getLocale()
            ));
        }

    }

    public List<MysteryBoxPageResponse> getMysteryBoxPrice() {
        return mysteryBoxPriceConstants.getMysteryBoxPrice();
    }

    public Map<Integer, Long> getMysteryBoxCount(UUID userId) {
        return mysteryBoxRepository.findByOwnerIdAndOpenedIsFalseAndExchangedIsFalse(userId).stream()
                   .collect(
                       Collectors.groupingBy(
                           MysteryBox::getLevel,
                           Collectors.counting()
                       )
                   );
    }

    public Page<MysteryBoxActivityHistory> getActivityHistoryOpenedBox(UUID userId, Pageable pageable) {
        return activityHistoryRepository.findByUserIdAndOperationType(userId, MYSTERY_BOX_OPEN, pageable);
    }

    public OpenPrize calculatePrize(int boxLevel) {
        MysteryBoxPrizeProbability prizeProbability = prizeProbabilityRepository.findByBoxLevel(boxLevel);
        List<Double> probabilities = prizeProbability.getPrizes()
                                         .stream()
                                         .map(MysteryBoxPrizeProbability.Prize::getProbability)
                                         .collect(Collectors.toList());

        SplittableRandom random = new SplittableRandom();
        double closestPrizeValue = findClosestPrize(
            probabilities,
            random.nextDouble(0, 1)
        );
        var openPrize =  prizeProbability.getPrizes()
                   .stream()
                   .filter(prize -> prize.getProbability()
                                        .equals(closestPrizeValue))
                   .findFirst()
                   .get();

        return mysteryBoxPrizeMapper.entityToOpenPrize(openPrize);
    }

    public List<MysteryBoxPrizeProbability> getPrizesProbability() {
        return prizeProbabilityRepository.findAll();
    }

    private Double findClosestPrize(List<Double> arr, double target) {
        return arr.stream().min(Comparator.comparingDouble(i -> Math.abs(i - target))).get();
    }

    private Integer getExchangeMysteryBoxCount(Integer boxLevel) {
        if (boxLevel < 3) {
            return MysteryBoxPriceConstants.EXCHANGE_MYSTERY_BOX_COUNT.get(2);
        } else {
            return MysteryBoxPriceConstants.EXCHANGE_MYSTERY_BOX_COUNT.get(3);
        }
    }

    private void checkBoxCountForExchange(int boxLevel, List<MysteryBox> boxList) {
        var exchangeLevel = MysteryBoxPriceConstants.EXCHANGE_MYSTERY_BOX_LEVEL.get(boxLevel);
        var boxCount = getExchangeMysteryBoxCount(boxLevel);

        var countPresentBox = boxList.stream()
                                  .filter(box -> Objects.equals(box.getLevel(), exchangeLevel))
                                  .count();

        if (countPresentBox < boxCount) {
            throw new GlobalException(
                ErrorCode.INSUFFICIENT_FUNDS,
                messageSource.getMessage(
                    "NOT_ENOUGH_MYSTERY_BOX_COUNT",
                    null,
                    LocaleContextHolder.getLocale()
                )
            );
        }
    }

    private List<MysteryBox> getExchangeMysteryBoxes(int boxLevel, List<MysteryBox> boxList) {
        var exchangeLevel = MysteryBoxPriceConstants.EXCHANGE_MYSTERY_BOX_LEVEL.get(boxLevel);
        var boxCount = getExchangeMysteryBoxCount(boxLevel);

        return boxList.stream()
                   .filter(box -> Objects.equals(box.getLevel(), exchangeLevel))
                   .limit(boxCount)
                   .peek(box -> box.setExchanged(true))
                   .collect(Collectors.toList());
    }

}
