package com.artbay.marketplace.partner.util;

import com.artbay.marketplace.partner.model.MysteryBoxActivityHistory;
import com.artbay.marketplace.partner.model.enums.MysteryBoxOperationType;
import com.artbay.marketplace.common.model.enums.MysteryBoxPrizeType;
import lombok.experimental.UtilityClass;

import java.util.UUID;

@UtilityClass
public class MysteryBoxActivityHistoryUtil {

    public static MysteryBoxActivityHistory buildCreateMysteryBoxActivityHistory(
        UUID userId,
        UUID mysteryBoxId,
        int level
    ) {
        return MysteryBoxActivityHistory.builder()
                   .userId(userId)
                   .mysteryBoxId(mysteryBoxId)
                   .newLevel(level)
                   .operationType(MysteryBoxOperationType.MYSTERY_BOX_PRIZE)
                   .build();
    }

    public static MysteryBoxActivityHistory buildBuyMysteryBoxActivityHistory(
        UUID userId,
        UUID mysteryBoxId,
        int level,
        double mysteryBoxPrice
    ) {
        return MysteryBoxActivityHistory.builder()
                   .userId(userId)
                   .mysteryBoxId(mysteryBoxId)
                   .amount(1.0)
                   .newLevel(level)
                   .price(mysteryBoxPrice)
                   .operationType(MysteryBoxOperationType.MYSTERY_BOX_BUY)
                   .build();
    }

    public static MysteryBoxActivityHistory buildExchangeMysteryBoxActivityHistory(
        UUID userId,
        UUID mysteryBoxId,
        int level
    ) {
        return MysteryBoxActivityHistory.builder()
                   .userId(userId)
                   .mysteryBoxId(mysteryBoxId)
                   .newLevel(level)
                   .operationType(MysteryBoxOperationType.MYSTERY_BOX_EXCHANGE)
                   .build();
    }

    public static MysteryBoxActivityHistory buildOpenMysteryBoxActivityHistory(
        UUID userId,
        UUID mysteryBoxId,
        int level,
        MysteryBoxPrizeType prizeType,
        double amount,
        int prizeLevel,
        UUID nftPrizeId
    ) {
        return MysteryBoxActivityHistory.builder()
                   .userId(userId)
                   .mysteryBoxId(mysteryBoxId)
                   .newLevel(level)
                   .operationType(MysteryBoxOperationType.MYSTERY_BOX_OPEN)
                   .prizeType(prizeType)
                   .amount(amount)
                   .prizeLevel(prizeLevel)
                   .nftPrizeId(nftPrizeId)
                   .build();
    }
}
