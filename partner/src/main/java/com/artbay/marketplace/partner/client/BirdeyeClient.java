package com.artbay.marketplace.partner.client;

import com.artbay.marketplace.partner.model.response.BirdeyeHistoryPriceResponse;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestParam;

@FeignClient(name = "birdeye-service", url = "https://public-api.birdeye.so/public")
public interface BirdeyeClient {

    @GetMapping(value = "/history_price", consumes = "application/json")
    BirdeyeHistoryPriceResponse getHistoryPrice(
        @RequestParam("address") String tokenAddress,
        @RequestParam("time_from") long unixTimeFrom,
        @RequestParam("time_to") long unixTimeTo
    );

    @GetMapping(value = "/history_price", consumes = "application/json")
    String getHistoryPriceStr(
        @RequestParam("address") String tokenAddress,
        @RequestParam("time_from") long unixTimeFrom,
        @RequestParam("time_to") long unixTimeTo
    );

}
