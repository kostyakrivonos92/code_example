package com.artbay.marketplace.partner.config;

import com.artbay.marketplace.common.config.CommonAuthConfig;
import com.artbay.marketplace.common.config.CommonConfig;
import com.artbay.marketplace.common.config.JpaAuditingConfig;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Import;
import org.springframework.data.redis.connection.RedisConnectionFactory;
import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.data.redis.serializer.StringRedisSerializer;
import org.springframework.web.multipart.MultipartResolver;
import org.springframework.web.multipart.commons.CommonsMultipartResolver;

@Configuration
@Import({CommonConfig.class, JpaAuditingConfig.class, CommonAuthConfig.class})
public class PartnerModuleConfig {

    @Bean("birdeyeRedisTemplate")
    public RedisTemplate<String, Double> redisBirdeyeTemplate(RedisConnectionFactory connectionFactory) {
        //stores current and 24h historic price
        RedisTemplate<String, Double> template = new RedisTemplate<>();
        template.setConnectionFactory(connectionFactory);
        template.setKeySerializer(StringRedisSerializer.UTF_8);
        return template;
    }

    @Bean
    public MultipartResolver multipartResolver() {
        CommonsMultipartResolver multipartResolver
                = new CommonsMultipartResolver();
        multipartResolver.setMaxUploadSize(5242880);
        multipartResolver.setSupportedMethods();
        return multipartResolver;
    }

}
