package com.artbay.marketplace.partner.model.response;

import com.fasterxml.jackson.annotation.JsonRootName;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.List;

@Data
@Builder
@JsonRootName("data")
@NoArgsConstructor
@AllArgsConstructor
public class BirdeyeHistoryPriceResponse {

    private List<TimeAndValue> items;

    @Data
    @NoArgsConstructor
    public static class TimeAndValue {

        private String address;
        private long unixTime;
        private double value;

    }

}
