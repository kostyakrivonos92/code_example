package com.artbay.marketplace.partner.model.dto;

import com.artbay.marketplace.partner.model.enums.MysteryBoxOperationType;
import com.artbay.marketplace.common.model.enums.MysteryBoxPrizeType;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.time.Instant;
import java.util.UUID;

@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
public class MysteryBoxActivityHistoryDto {

    private UUID id;

    private UUID mysteryBoxId;

    private UUID userId;

    private MysteryBoxOperationType operationType;

    private Double price;

    private Integer previousLevel;

    private Integer newLevel;

    private Integer prizeLevel;

    private UUID prizeId;

    private MysteryBoxPrizeType prizeType;

    private Double amount;

    private Instant operationDate;
}
