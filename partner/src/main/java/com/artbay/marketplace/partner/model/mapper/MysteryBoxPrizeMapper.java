package com.artbay.marketplace.partner.model.mapper;

import com.artbay.marketplace.partner.model.dto.MysteryBoxPrizeProbabilityDto;
import com.artbay.marketplace.partner.model.MysteryBoxPrizeProbability;
import com.artbay.marketplace.partner.model.OpenPrize;
import com.artbay.marketplace.partner.model.dto.MysteryBoxPrizeProbabilityShortDto;
import com.artbay.marketplace.partner.model.dto.OpenPrizeDto;
import org.mapstruct.Mapper;
import org.mapstruct.NullValueCheckStrategy;
import org.mapstruct.NullValuePropertyMappingStrategy;
import org.mapstruct.ReportingPolicy;

import java.util.List;

@Mapper(
    unmappedTargetPolicy = ReportingPolicy.IGNORE,
    nullValuePropertyMappingStrategy = NullValuePropertyMappingStrategy.IGNORE,
    nullValueCheckStrategy = NullValueCheckStrategy.ALWAYS
)
public interface MysteryBoxPrizeMapper {

    MysteryBoxPrizeProbabilityDto entityToDto(MysteryBoxPrizeProbability probability);

    MysteryBoxPrizeProbability dtoToEntity(MysteryBoxPrizeProbabilityDto probabilityDto);

    OpenPrizeDto openPrizeToDto(OpenPrize openPrize);

    List<MysteryBoxPrizeProbabilityShortDto> entityListToDtoList(List<MysteryBoxPrizeProbability> probability);

    OpenPrize entityToOpenPrize(MysteryBoxPrizeProbability.Prize prize);
}
