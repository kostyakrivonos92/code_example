package com.artbay.marketplace.partner.model;

import com.artbay.marketplace.common.model.enums.MysteryBoxPrizeType;
import com.vladmihalcea.hibernate.type.json.JsonBinaryType;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.hibernate.annotations.Type;
import org.hibernate.annotations.TypeDef;

import javax.persistence.*;
import java.io.Serializable;
import java.util.List;
import java.util.UUID;

@Data
@AllArgsConstructor
@NoArgsConstructor
@Entity
@Table(name = "mystery_box_prize_probability")
@TypeDef(name = "jsonb", typeClass = JsonBinaryType.class)
public class MysteryBoxPrizeProbability {

    @Id
    @GeneratedValue
    @Column(name = "id")
    private UUID id;
    @Column(name = "box_level")
    private Integer boxLevel;
    @Type(type = "jsonb")
    @Column(columnDefinition = "jsonb")
    private List<Prize> prizes;

    @Data
    @NoArgsConstructor
    @AllArgsConstructor
    public static class Prize implements Serializable {

        @Enumerated(EnumType.STRING)
        private MysteryBoxPrizeType type;
        private Integer level;
        private Double countMin;
        private Double countMax;
        private Double probability;
        private Double amount;

    }

}
