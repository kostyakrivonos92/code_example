package com.artbay.marketplace.partner.model.mapper;

import com.artbay.marketplace.partner.model.MysteryBoxActivityHistory;
import com.artbay.marketplace.partner.model.dto.MysteryBoxActivityHistoryDto;
import org.mapstruct.*;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;

import java.util.List;

@Mapper(
    unmappedTargetPolicy = ReportingPolicy.IGNORE,
    nullValuePropertyMappingStrategy = NullValuePropertyMappingStrategy.IGNORE,
    nullValueCheckStrategy = NullValueCheckStrategy.ALWAYS
)
public abstract class MysteryBoxActivityHistoryMapper {

    @Mapping(target = "prizeId", source = "nftPrizeId")
    public abstract MysteryBoxActivityHistoryDto entityToDto(MysteryBoxActivityHistory activityHistory);

    public abstract MysteryBoxActivityHistory dtoToEntity(MysteryBoxActivityHistoryDto activityHistoryDto);

    public abstract List<MysteryBoxActivityHistoryDto> listEntityToListDto(List<MysteryBoxActivityHistory> activityHistoryList);

    public Page<MysteryBoxActivityHistoryDto> pageEntityToPageDto(Page<MysteryBoxActivityHistory> activityHistoryPage) {
        return new PageImpl<>(
            listEntityToListDto(activityHistoryPage.getContent()),
            activityHistoryPage.getPageable(),
            activityHistoryPage.getTotalElements()
        );
    }

}