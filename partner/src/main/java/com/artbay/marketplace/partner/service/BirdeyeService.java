package com.artbay.marketplace.partner.service;

import com.artbay.marketplace.partner.client.BirdeyeClient;
import com.artbay.marketplace.partner.model.response.BirdeyeHistoryPriceResponse;
import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import lombok.RequiredArgsConstructor;
import lombok.SneakyThrows;
import lombok.extern.slf4j.Slf4j;
import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Service;

import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.ZoneOffset;
import java.time.temporal.ChronoUnit;
import java.util.concurrent.TimeUnit;

@Slf4j
@Service
@RequiredArgsConstructor
public class BirdeyeService {

    private static final String BIRDEYE_DAY_BEFORE_PRICE_KEY = "artzPriceDayBefore";
    private static final String BIRDEYE_CURRENT_PRICE_KEY = "artzPriceNow";

    private final BirdeyeClient birdeyeClient;
    private final RedisTemplate<String, Double> birdeyeRedisTemplate;

    public Double getArtzPriceNow() {
        Double currentPrice = birdeyeRedisTemplate.opsForValue().get(BIRDEYE_CURRENT_PRICE_KEY);
        if (currentPrice == null) {
            refreshArtzPrices();
            currentPrice = birdeyeRedisTemplate.opsForValue().get(BIRDEYE_DAY_BEFORE_PRICE_KEY);
        }
        return currentPrice;
    }

    public Double getArtzPriceDayBefore() {
        Double priceDayBefore = birdeyeRedisTemplate.opsForValue().get(BIRDEYE_DAY_BEFORE_PRICE_KEY);
        if (priceDayBefore == null) {
            refreshArtzPrices();
            priceDayBefore = birdeyeRedisTemplate.opsForValue().get(BIRDEYE_DAY_BEFORE_PRICE_KEY);
        }
        return priceDayBefore;
    }

    @SneakyThrows
    @Scheduled(fixedRate = 1, timeUnit = TimeUnit.MINUTES)
    public void refreshArtzPrices() {
        LocalDateTime today = LocalDate.now().atStartOfDay();
        LocalDateTime yesterday = today.minus(1, ChronoUnit.DAYS);

        String historyPriceResponseString = birdeyeClient.getHistoryPriceStr(
            "A1KWZzcUrsX9MAsL8XtZkuS4RX6fjZazc7K5UoX42M3M",
            yesterday.toEpochSecond(ZoneOffset.UTC),
            today.toEpochSecond(ZoneOffset.UTC)
        );

        //я ебал все эти респонсы с датой и саксессом, приходится парсить их вручную
        var objectMapper = new ObjectMapper();
        JsonNode dataNode = objectMapper.readTree(historyPriceResponseString).get("data");
        var historyPriceResponse = objectMapper.treeToValue(dataNode, BirdeyeHistoryPriceResponse.class);

        historyPriceResponse.getItems().forEach(
            timeAndValue -> {
                long differenceBetweenNow = today.toEpochSecond(ZoneOffset.UTC) - timeAndValue.getUnixTime();
                long differenceBetweenDayBefore = yesterday.toEpochSecond(ZoneOffset.UTC) - timeAndValue.getUnixTime();
                if (differenceBetweenNow > differenceBetweenDayBefore) {
                    birdeyeRedisTemplate.opsForValue().set(
                        BIRDEYE_DAY_BEFORE_PRICE_KEY,
                        timeAndValue.getValue()
                    );
                } else {
                    birdeyeRedisTemplate.opsForValue().set(
                        BIRDEYE_CURRENT_PRICE_KEY,
                        timeAndValue.getValue()
                    );
                }
            }
        );
    }

}
