package com.artbay.marketplace.partner.model.response;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
public class MysteryBoxPageResponse {

    private Integer level;
    private Integer price;
}
