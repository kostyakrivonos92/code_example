package com.artbay.marketplace.partner.model.response;

import lombok.Builder;
import lombok.Data;

import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;

@Data
public class ActivityChartResponse {

    private List<Data> data = new ArrayList<>();

    @lombok.Data
    @Builder
    public static class Data {
        private LocalDate date;
        private Double value;
    }

}
