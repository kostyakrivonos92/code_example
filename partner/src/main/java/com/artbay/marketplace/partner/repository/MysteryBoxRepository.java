package com.artbay.marketplace.partner.repository;

import com.artbay.marketplace.partner.model.MysteryBox;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.UUID;

@Repository
public interface MysteryBoxRepository extends JpaRepository<MysteryBox, UUID>, JpaSpecificationExecutor<MysteryBox> {

    List<MysteryBox> findByOwnerIdAndOpenedIsFalseAndExchangedIsFalse(UUID ownerId);

}
