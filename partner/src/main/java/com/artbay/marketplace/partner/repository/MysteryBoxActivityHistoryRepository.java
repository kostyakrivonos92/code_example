package com.artbay.marketplace.partner.repository;

import com.artbay.marketplace.partner.model.MysteryBoxActivityHistory;
import com.artbay.marketplace.partner.model.enums.MysteryBoxOperationType;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.UUID;

@Repository
public interface MysteryBoxActivityHistoryRepository extends JpaRepository<MysteryBoxActivityHistory, UUID> {

    Page<MysteryBoxActivityHistory> findByUserIdAndOperationType(UUID userId, MysteryBoxOperationType operationType, Pageable pageable);

}
