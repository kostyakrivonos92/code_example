package com.artbay.marketplace.partner.controller;

import com.artbay.marketplace.common.CustomUserDetails;
import com.artbay.marketplace.common.client.partner.PartnerRootClient;
import com.artbay.marketplace.common.client.referral.ReferralPaymentClient;
import com.artbay.marketplace.common.client.referral.ReferralRootClient;
import com.artbay.marketplace.common.model.DetailedInfoDto;
import com.artbay.marketplace.common.model.PartnerDTO;
import com.artbay.marketplace.common.model.RefDTO;
import com.artbay.marketplace.common.model.ReferralStructureDto;
import com.artbay.marketplace.common.model.enums.TimeframeValue;
import com.artbay.marketplace.common.model.request.RegistrationRequest;
import com.artbay.marketplace.common.model.request.UpdateUserRequest;
import com.artbay.marketplace.common.validation.FileSize;
import com.artbay.marketplace.common.validation.LoadableFile;
import com.artbay.marketplace.partner.model.mapper.PartnerMapper;
import com.artbay.marketplace.partner.service.PartnerService;
import io.swagger.annotations.ApiOperation;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

import javax.validation.Valid;
import java.net.URI;
import java.util.List;
import java.util.Map;
import java.util.UUID;

import static com.artbay.marketplace.common.utils.ControllerUtil.getUserId;

@Slf4j
@RestController
@RequiredArgsConstructor
@Validated
public class PartnerController implements PartnerRootClient {

    private final ReferralRootClient referralRootClient;
    private final ReferralPaymentClient referralPaymentClient;

    private final PartnerService partnerService;
    private final PartnerMapper partnerMapper;

    @GetMapping("/self")
    @PreAuthorize("hasAuthority('user')")
    public PartnerDTO getDataSelf() {
        return partnerMapper.partnerToDtoIgnoreCryptowallets(partnerService.getPartnerById(getUserId()));
    }

    @Override
    @PreAuthorize("hasAnyAuthority('user', 'artist_verified', 'artist_unverified')")
    public void updateSelf(@RequestBody UpdateUserRequest updateUserRequest) {
         partnerService.updatePartner(getUserId(), updateUserRequest);
    }

    @GetMapping("/referral-structure")
    @PreAuthorize("hasAuthority('user')")
    @ApiOperation(value = "Return referral structure and sum rewards artz.")
    public Map<Long, ReferralStructureDto> getReferralStructureAndSumRewards() {
        return partnerService.getReferralStructure(getUserId());
    }

    @GetMapping("/referral-level")
    @PreAuthorize("hasAuthority('user')")
    public List<RefDTO> getReferralsOnLevel() {
        return partnerService.getReferralsOnLevel(getUserId(), 1);
    }

    @GetMapping("/referral-link")
    @PreAuthorize("hasAuthority('user')")
    public ResponseEntity<Map<String, String>> getReferralLink() {
        var userDetails = getUserDetails();
        return ResponseEntity.ok(Map.of("link", partnerService.getReferralLink(userDetails.getUsername())));
    }

    @PostMapping("/upload-avatar")
    @PreAuthorize("hasAuthority('user')")
    public ResponseEntity<Void> uploadAvatar(
        @Valid @FileSize(maxSizeInMB = 25) @LoadableFile(permittedTypes = "image") @RequestPart(value = "file") MultipartFile file
    ) {
        partnerService.uploadAvatar(getUserId(), file);
        return ResponseEntity.ok().build();
    }

    @Override
    @PostMapping("/register")
    public ResponseEntity<Void> registerPartner(@RequestBody @Valid RegistrationRequest registrationRequest) {
        registrationRequest.setEmail(registrationRequest.getEmail().toLowerCase());
        partnerService.createPartner(registrationRequest);
        return ResponseEntity.created(URI.create("/partner/self")).build();
    }

    @GetMapping(value = "/detailed-info", consumes = "application/json")
    @ApiOperation(value = "Get detailed info for widget by partner id.")
    public DetailedInfoDto getPartnerDetailedInfo(
        @RequestParam UUID partnerId,
        @RequestParam String timeframe
    ) {
        return partnerService.getPartnerDetailedInfo(getUserId(), partnerId, timeframe);
    }

    @GetMapping(value = "/detailed-info/level", consumes = "application/json")
    @ApiOperation(value = "Get detailed info for widget by partner id and level.")
    public DetailedInfoDto getDetailedInfoByLevel(
        @RequestParam String timeframe,
        @RequestParam long level
    ) {
        return referralRootClient.getDetailedInfoForLevel(
            getUserId(),
            TimeframeValue.valueOf(timeframe.toUpperCase()),
            level
        );
    }

    @GetMapping("/purchase/nft-key/artz")
    @PreAuthorize("hasAuthority('user')")
    public void purchaseNftKeyArtz() {
        var userId = getUserId();
        log.info(String.format("[PARTNER] Partner with id [%s], purchase Nft Key Artz", userId));
        referralPaymentClient.purchaseNftKeyArtz(userId);
    }

    @Override
    public void verified(UUID partnerId) {
        partnerService.verified(partnerId);
    }

    private CustomUserDetails getUserDetails() {
        return (CustomUserDetails) SecurityContextHolder.getContext()
                                       .getAuthentication()
                                       .getPrincipal();
    }

}
