package com.artbay.marketplace.partner.model.mapper;

import com.artbay.marketplace.common.model.DetailedInfoDto;
import com.artbay.marketplace.common.model.PartnerDTO;
import com.artbay.marketplace.common.model.RefDTO;
import com.artbay.marketplace.common.model.request.UpdateUserRequest;
import com.artbay.marketplace.partner.model.Partner;
import com.artbay.marketplace.partner.repository.PartnerRepository;
import org.mapstruct.*;
import org.springframework.beans.factory.annotation.Autowired;

import java.util.List;
import java.util.UUID;

@Mapper(
    unmappedTargetPolicy = ReportingPolicy.IGNORE,
    nullValuePropertyMappingStrategy = NullValuePropertyMappingStrategy.IGNORE,
    nullValueCheckStrategy = NullValueCheckStrategy.ALWAYS
)
public abstract class PartnerMapper {

    @Autowired
    private PartnerRepository partnerRepository;

    @Named("fullConversion")
    @Mapping(source = "referrerId", target = "referrer", qualifiedByName = "fromPartnerIdToRefDTO")
    public abstract PartnerDTO partnerToDto(Partner entity);

    @IterableMapping(qualifiedByName = "fullConversion")
    public abstract List<PartnerDTO> partnerToDto(List<Partner> entities);

    @Mapping(source = "referrerId", target = "referrer", qualifiedByName = "fromPartnerIdToRefDTO")
    @Mapping(target = "cryptowalletPrivateKey", ignore = true)
    public abstract PartnerDTO partnerToDtoIgnoreCryptowallets(Partner entity);

    @Mapping(source = "referrer.id", target = "referrerId")
    public abstract Partner dtoToEntity(PartnerDTO dto);

    public abstract List<Partner> dtosToEntities(List<PartnerDTO> dtos);

    @Mapping(source = "dto", target = "partnerDto")
    public abstract void setPartnerToDto(@MappingTarget DetailedInfoDto detailedInfoDto, PartnerDTO dto);

    public abstract void update(@MappingTarget Partner partner, UpdateUserRequest updateRequest);

    @Named("fromPartnerIdToRefDTO")
    public RefDTO fromPartnerIdToRefDTO(UUID partnerId) {
        if (partnerId != null) {
            var partner = partnerRepository.getById(partnerId);
            return RefDTO.builder()
                .id(partnerId)
                .username(partner.getUsername())
                .avatarLink(partner.getAvatarLink())
                .hasNftKey(partner.getHasNftKey())
                .build();
        }
        return null;
    }

}
