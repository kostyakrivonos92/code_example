package com.artbay.marketplace.partner.controller;

import com.artbay.marketplace.common.client.referral.ReferralMercuryoClient;
import com.artbay.marketplace.common.model.mercuryo.WidgetProperties;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import static com.artbay.marketplace.common.utils.ControllerUtil.getUserId;

@Slf4j
@RestController
@RequestMapping("/mercuryo")
@RequiredArgsConstructor
public class MercuryoController {

    private final ReferralMercuryoClient referralMercuryoClient;

    @GetMapping("/key-purchase-properties")
    @PreAuthorize("hasAuthority('user')")
    public WidgetProperties getWidgetPropertiesForKeyPurchase() {
        return referralMercuryoClient.getWidgetPropertiesForKeyPurchase(getUserId());
    }

    @GetMapping("/artz-purchase-properties")
    @PreAuthorize("hasAuthority('user')")
    public WidgetProperties getWidgetPropertiesForArtzPurchase(@RequestParam Integer amount) {
        return referralMercuryoClient.getWidgetPropertiesForArtzPurchase(getUserId(), amount);
    }

}
