package com.artbay.marketplace.partner.model.mapper;

import com.artbay.marketplace.common.model.mysterybox.MysteryBoxDto;
import com.artbay.marketplace.partner.model.MysteryBox;
import org.mapstruct.*;

@Mapper(
    unmappedTargetPolicy = ReportingPolicy.IGNORE,
    nullValuePropertyMappingStrategy = NullValuePropertyMappingStrategy.IGNORE,
    nullValueCheckStrategy = NullValueCheckStrategy.ALWAYS
)
public interface MysteryBoxMapper {

    MysteryBoxDto entityToDto(MysteryBox mysteryBox);

    @Mapping(target = "exchanged", ignore = true)
    @Mapping(target = "name", ignore = true)
    MysteryBox dtoToEntity(MysteryBoxDto mysteryBoxDto);

}
