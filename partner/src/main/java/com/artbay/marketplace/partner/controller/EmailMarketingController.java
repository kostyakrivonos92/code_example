package com.artbay.marketplace.partner.controller;

import com.artbay.marketplace.common.service.MoosendService;
import com.artbay.marketplace.partner.model.request.SubscribeEmailRequest;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

@Slf4j
@RestController
@RequiredArgsConstructor
public class EmailMarketingController {

    private final MoosendService moosendService;

    @Value("${moosend.list.subscribers-award}")
    private String subscribersAwardList;

    @PostMapping("/marketing/subscribe")
    public void subscribe(@RequestBody SubscribeEmailRequest request) {
        moosendService.addToEmailList(request.getEmail(), subscribersAwardList);
    }

}
