package com.artbay.marketplace.partner;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.autoconfigure.domain.EntityScan;
import org.springframework.cloud.client.discovery.EnableDiscoveryClient;
import org.springframework.cloud.openfeign.EnableFeignClients;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;
import org.springframework.data.redis.repository.configuration.EnableRedisRepositories;
import org.springframework.retry.annotation.EnableRetry;

@SpringBootApplication
@EnableDiscoveryClient
@EnableFeignClients({"com.artbay.marketplace.common.client", "com.artbay.marketplace.partner.client"})
@EntityScan(basePackages = "com.artbay.marketplace.partner.model")
@EnableJpaRepositories(basePackages = "com.artbay.marketplace.partner.repository")
@EnableRetry
@EnableRedisRepositories
public class PartnerApplication {

    public static void main(String[] args) {
        SpringApplication.run(PartnerApplication.class, args);
    }

}
