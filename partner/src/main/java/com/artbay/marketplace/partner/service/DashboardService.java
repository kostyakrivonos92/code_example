package com.artbay.marketplace.partner.service;

import com.artbay.marketplace.common.client.partner.internal.PartnerInternalClient;
import com.artbay.marketplace.common.client.referral.ReferralRootClient;
import com.artbay.marketplace.common.client.referral.ReferralTransactionClient;
import com.artbay.marketplace.common.model.PartnerDTO;
import com.artbay.marketplace.common.model.TransactionDTO;
import com.artbay.marketplace.partner.model.response.ActivityChartResponse;
import com.artbay.marketplace.partner.model.response.ArtzWidgetResponse;
import com.artbay.marketplace.partner.model.response.ReferralWidgetResponse;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;

import java.time.Instant;
import java.time.LocalDate;
import java.util.Map;
import java.util.UUID;

import static com.artbay.marketplace.common.model.enums.TransactionCurrency.ARTZ;
import static com.artbay.marketplace.common.model.enums.TransactionStatus.EXECUTED;
import static java.time.ZoneOffset.UTC;

@Slf4j
@Service
@RequiredArgsConstructor
public class DashboardService {

    private final ReferralTransactionClient referralTransactionClient;
    private final ReferralRootClient referralRootClient;
    private final PartnerInternalClient partnerInternalClient;

    private final PartnerService partnerService;
    private final BirdeyeService birdeyeService;

    public ActivityChartResponse getActivityChart(UUID userId, LocalDate dateFrom, LocalDate dateTo) {
        ActivityChartResponse response = new ActivityChartResponse();

        Instant instantFrom = dateFrom.atStartOfDay(UTC).toInstant();
        Instant instantTo = dateTo.atTime(23, 59, 59).toInstant(UTC);

        var transactions = referralTransactionClient.getTransactionHistoryByDates(userId, instantFrom, instantTo);
        dateFrom.datesUntil(dateTo.plusDays(1)).forEach(
            day -> {
                var startOfDay = day.atStartOfDay().toInstant(UTC);
                var endOfDay = day.atTime(23, 59, 59).toInstant(UTC);
                var artzSum = transactions.stream()
                                  .filter(transaction -> transaction.getCreationDate().isAfter(startOfDay))
                                  .filter(transaction -> transaction.getCreationDate().isBefore(endOfDay))
                                  .filter(transaction -> transaction.getStatus() == EXECUTED)
                                  .filter(transaction -> transaction.getCurrency() == ARTZ)
                                  .filter(transaction -> transaction.getAmount() > 0)
                                  .mapToDouble(TransactionDTO::getAmount).sum();
                response.getData().add(
                    ActivityChartResponse.Data.builder()
                        .date(day)
                        .value(artzSum)
                        .build()
                );
            }
        );
        return response;
    }

    public ArtzWidgetResponse getArtzWidgetData(UUID userId) {
        Double currentPrice = birdeyeService.getArtzPriceNow();
        Double priceDayBefore = birdeyeService.getArtzPriceDayBefore();

        PartnerDTO partner = partnerInternalClient.getPartnerById(userId);

        double changePercentage = priceDayBefore / 100 * currentPrice;
        if (currentPrice - priceDayBefore == 0.0) {
            changePercentage = 0;
        }

        return ArtzWidgetResponse.builder()
                   .dailyChangeUsdAmount(currentPrice - priceDayBefore)
                   .dailyChangePercentage(changePercentage)
                   .currentExchangeRate(currentPrice)
                   .walletAddress(partner.getCryptowalletPublicKey())
                   .build();
    }

    public ReferralWidgetResponse getReferralWidgetData(UUID userId) {
        var referralIdsOnFirstLine = referralRootClient.getReferralIdsOnSpecificLevel(userId, 1);
        var allReferralIds = referralRootClient.getReferralNetworkUserIdsForPartner(userId);

        long keyholdersFirstLine = referralIdsOnFirstLine.stream()
                                       .filter(partnerId -> partnerService.getPartnerById(partnerId).getHasNftKey())
                                       .count();
        long keyholdersTotal = allReferralIds.stream()
                                   .filter(partnerId -> partnerService.getPartnerById(partnerId).getHasNftKey())
                                   .count();

        var levelToEarningsMap = referralTransactionClient.getLevelToEarningsForPartner(userId);
        double earningsFirstLine = levelToEarningsMap == null ? 0 : levelToEarningsMap.entrySet().stream()
                                                                        .filter(entry -> entry.getKey() == 1)
                                                                        .mapToDouble(entry -> entry.getValue()
                                                                                                  .values()
                                                                                                  .stream()
                                                                                                  .mapToDouble(v -> v)
                                                                                                  .sum())
                                                                        .sum();

        double earningsTotal = levelToEarningsMap == null ? 0 : levelToEarningsMap
                                                                    .values()
                                                                    .stream()
                                                                    .flatMap(map -> map.entrySet().stream())
                                                                    .mapToDouble(Map.Entry::getValue)
                                                                    .sum();

        return ReferralWidgetResponse.builder()
                   .partnersRegisteredFirstLine(referralIdsOnFirstLine.size())
                   .partnersRegisteredTotal(allReferralIds.size())
                   .nftKeyholdersFirstLine(keyholdersFirstLine)
                   .nftKeyholdersTotal(keyholdersTotal)
                   .rewardsEarnedFirstLine(earningsFirstLine)
                   .rewardsEarnedTotal(earningsTotal)
                   .build();
    }

}
