package com.artbay.marketplace.partner.model.dto;

import com.artbay.marketplace.common.model.enums.MysteryBoxPrizeType;
import lombok.*;

import java.util.UUID;

@Getter
@Setter
@Builder
@AllArgsConstructor
@NoArgsConstructor
public class OpenPrizeDto {

    private MysteryBoxPrizeType type;
    private Integer level;
    private Double amount;
    private Double probability;
    private UUID prizeNftId;
}
