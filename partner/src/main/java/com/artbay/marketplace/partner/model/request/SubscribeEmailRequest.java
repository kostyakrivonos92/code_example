package com.artbay.marketplace.partner.model.request;

import lombok.Data;

import javax.validation.constraints.NotBlank;

@Data
public class SubscribeEmailRequest {

    @NotBlank
    private String email;

}
