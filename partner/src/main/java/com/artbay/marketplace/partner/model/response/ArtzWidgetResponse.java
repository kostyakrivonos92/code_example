package com.artbay.marketplace.partner.model.response;

import lombok.Builder;
import lombok.Data;

@Data
@Builder
public class ArtzWidgetResponse {

    private double currentExchangeRate;
    private double dailyChangeUsdAmount;
    private double dailyChangePercentage;
    private String walletAddress;

}
