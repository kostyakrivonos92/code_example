package com.artbay.marketplace.partner.model;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.springframework.data.annotation.CreatedDate;
import org.springframework.data.annotation.LastModifiedDate;
import org.springframework.data.jpa.domain.support.AuditingEntityListener;

import javax.persistence.*;
import java.time.Instant;
import java.util.UUID;

@Data
@AllArgsConstructor
@NoArgsConstructor
@Builder
@Entity
@EntityListeners(AuditingEntityListener.class)
@Table(name = "partners")
public class Partner {

    @Id
    @Column(name = "id")
    private UUID id;

    @Column(name = "is_artist", columnDefinition = "boolean default false")
    @Builder.Default
    private Boolean isArtist = false;

    @Column(name = "username")
    private String username;
    @Column(name = "email")
    private String email;
    @Column(name = "is_verified", columnDefinition = "boolean default false")
    @Builder.Default
    private Boolean isVerified = false;
    @Column(name = "avatar_link")
    private String avatarLink;
    @Column(name = "avatar_thumbnail_link")
    private String avatarThumbnailLink;
    @Column(name = "first_name")
    private String firstName;
    @Column(name = "last_name")
    private String lastName;
    @Column(name = "country")
    private String country;


    //Presale ONLY
    /**
     * @deprecated outdated
     */
    @Deprecated(since = "Presale passed", forRemoval = true)
    @Column(name = "allocation_amount", columnDefinition = "integer default 0")
    @Builder.Default
    private Integer allocationAmount = 0;

    @Column(name = "booster_amount", columnDefinition = "double default 0.0")
    @Builder.Default
    private Double boosterAmount = 0.0;
    @Column(name = "activity_points", columnDefinition = "integer default 0")
    @Builder.Default
    private Integer activityPointsAmount = 0;

    @Column(name = "has_nft_key")
    @Builder.Default
    private Boolean hasNftKey = false;
    @Column(name = "referral_level", columnDefinition = "integer default 0")
    @Builder.Default
    private Integer referralLevel = 0;

    @Column(name = "referrer_id")
    private UUID referrerId;
    @Column(name = "discord_id")
    private String discordId;

    @Column(name = "showing_private_info")
    @Builder.Default
    private Boolean showingPrivateInfo = false;

    @Column(name = "cryptowallet_private_key")
    private String cryptowalletPrivateKey;
    @Column(name = "cryptowallet_public_key")
    private String cryptowalletPublicKey;

    @CreatedDate
    @Column(name = "creation_date")
    private Instant creationDate;

    @LastModifiedDate
    @Column(name = "last_modified_date")
    private Instant lastModifiedDate;

    @Column(name = "tatum_subscription_id")
    private String tatumSubscriptionId;

    /**
     * @deprecated outdated, balance is calculated from transactions
     */
    @Deprecated(since = "delete after MVP", forRemoval = true)
    @Column(name = "balance", columnDefinition = "double default 0.0")
    @Builder.Default
    private Double balance = 0.0;

}
