package com.artbay.marketplace.partner.controller;

import com.artbay.marketplace.partner.model.dto.MysteryBoxActivityHistoryDto;
import com.artbay.marketplace.partner.model.dto.MysteryBoxPrizeProbabilityShortDto;
import com.artbay.marketplace.partner.model.dto.OpenPrizeDto;
import com.artbay.marketplace.partner.model.mapper.MysteryBoxActivityHistoryMapper;
import com.artbay.marketplace.partner.model.mapper.MysteryBoxPrizeMapper;
import com.artbay.marketplace.partner.model.response.MysteryBoxPageResponse;
import com.artbay.marketplace.partner.service.MysteryBoxService;
import io.swagger.annotations.ApiOperation;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.Map;

import static com.artbay.marketplace.common.utils.ControllerUtil.getUserId;

@Slf4j
@RestController
@RequestMapping("/mystery-box")
@RequiredArgsConstructor
public class MysteryBoxController {

    private final MysteryBoxService mysteryBoxService;

    private final MysteryBoxActivityHistoryMapper activityHistoryMapper;
    private final MysteryBoxPrizeMapper mysteryBoxPrizeMapper;

    @PutMapping("/purchase")
    @PreAuthorize("hasAuthority('user_allowed_purchase')")
    public void purchaseMysteryBox(@RequestParam int boxLevel, @RequestParam int count) {
        mysteryBoxService.purchaseMysteryBox(boxLevel, count, getUserId());
    }

    @PutMapping("/exchange")
    @PreAuthorize("hasAuthority('user_allowed_purchase')")
    public void exchangeMysteryBox(@RequestParam int boxLevel) {
        mysteryBoxService.exchangeMysteryBox(boxLevel, getUserId());
    }

    @GetMapping("/price")
    @PreAuthorize("hasAuthority('user')")
    @ApiOperation(value = "Get price for mystery box page")
    public List<MysteryBoxPageResponse> getMysteryBoxPrice() {
        return mysteryBoxService.getMysteryBoxPrice();
    }

    @GetMapping("/available")
    @PreAuthorize("hasAuthority('user')")
    @ApiOperation(value = "Get mystery box count")
    public Map<Integer, Long> getMysteryBoxCount() {
        return mysteryBoxService.getMysteryBoxCount(getUserId());
    }

    @GetMapping("/activity-history")
    @PreAuthorize("hasAuthority('user')")
    @ApiOperation(value = "Get activity history for mystery box")
    public Page<MysteryBoxActivityHistoryDto> getMysteryBoxActivityHistory(Pageable pageable) {
        return activityHistoryMapper.pageEntityToPageDto(mysteryBoxService.getActivityHistoryOpenedBox(getUserId(), pageable));
    }

    @PutMapping("/open")
    @PreAuthorize("hasAuthority('user_allowed_purchase')")
    public OpenPrizeDto openMysteryBox(@RequestParam int boxLevel){
        return mysteryBoxPrizeMapper.openPrizeToDto(mysteryBoxService.openMysteryBox(boxLevel, getUserId()));
    }

    @ApiOperation("Get mystery box prizes.")
    @GetMapping("/prize")
    @PreAuthorize("hasAuthority('user')")
    public List<MysteryBoxPrizeProbabilityShortDto> getPrizesProbability() {
        return mysteryBoxPrizeMapper.entityListToDtoList(mysteryBoxService.getPrizesProbability());
    }

}
