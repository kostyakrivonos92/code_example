package com.artbay.marketplace.partner.controller.internal;

import com.artbay.marketplace.common.client.partner.internal.PartnerInternalClient;
import com.artbay.marketplace.common.model.mysterybox.MysteryBoxDto;
import com.artbay.marketplace.common.model.PartnerDTO;
import com.artbay.marketplace.partner.model.mapper.MysteryBoxMapper;
import com.artbay.marketplace.partner.model.mapper.PartnerMapper;
import com.artbay.marketplace.partner.service.MysteryBoxService;
import com.artbay.marketplace.partner.service.PartnerService;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;
import java.util.Map;
import java.util.UUID;

@Slf4j
@RestController
@RequestMapping("/internal")
@RequiredArgsConstructor
public class InternalController implements PartnerInternalClient {

    private final PartnerService partnerService;
    private final MysteryBoxService mysteryBoxService;

    private final PartnerMapper partnerMapper;
    private final MysteryBoxMapper mysteryBoxMapper;

    @Override
    public PartnerDTO getPartnerByUsername(String username) {
        return partnerMapper.partnerToDto(partnerService.getPartnerByUsername(username));
    }

    @Override
    public PartnerDTO getPartnerByEmail(String email) {
        return partnerMapper.partnerToDto(partnerService.getPartnerByEmail(email));
    }

    @Override
    public PartnerDTO getPartnerById(UUID partnerId) {
        return partnerMapper.partnerToDto(partnerService.getPartnerById(partnerId));
    }

    @Override
    public PartnerDTO updatePartner(PartnerDTO partnerDTO) {
        return partnerMapper.partnerToDto(partnerService.save(partnerDTO));
    }

    @Override
    public PartnerDTO getPartnerByIdIsArtist(UUID partnerId) {
        return partnerMapper.partnerToDto(partnerService.getPartnerByIdIsArtist(partnerId));
    }

    @Override
    public void batchUpdatePartners(@RequestBody List<PartnerDTO> partnerDtos) {
        partnerService.batchUpdatePartners(partnerDtos);
    }

    @Override
    public List<PartnerDTO> getPartnerList(@RequestBody List<UUID> uuids) {
        return partnerMapper.partnerToDto(partnerService.getPartnerList(uuids));
    }

    @Override
    public PartnerDTO getPartnerByWallet(String walletAddress) {
        return partnerMapper.partnerToDto(partnerService.getPartnerByWallet(walletAddress));
    }

    @Override
    public MysteryBoxDto createMysteryBox(MysteryBoxDto mysteryBox, UUID userId) {
        return mysteryBoxMapper.entityToDto(mysteryBoxService.createMysteryBox(mysteryBox));
    }

    @Override
    public Map<Integer, Long> getMysteryBoxCount(UUID partnerId) {
        return mysteryBoxService.getMysteryBoxCount(partnerId);
    }

}
