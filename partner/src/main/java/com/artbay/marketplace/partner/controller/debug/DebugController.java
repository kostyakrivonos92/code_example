package com.artbay.marketplace.partner.controller.debug;


import com.artbay.marketplace.common.model.request.RegistrationRequest;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.context.annotation.Profile;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.net.URI;

@Slf4j
@RestController
@RequestMapping("/debug")
@RequiredArgsConstructor
@Validated
@Profile({"!prod"})
public class DebugController {

    private final DebugService debugService;

    @PostMapping("/register")
    public ResponseEntity<Void> registerPartner(@RequestBody @Valid RegistrationRequest registrationRequest) {
        registrationRequest.setEmail(registrationRequest.getEmail().toLowerCase());
        debugService.createPartner(registrationRequest);
        return ResponseEntity.created(URI.create("/partner/self")).build();
    }

}
