package com.artbay.marketplace.partner.repository;

import com.artbay.marketplace.partner.model.Partner;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.UUID;

@Repository
public interface PartnerRepository extends JpaRepository<Partner, UUID> {

    Partner findFirstByUsernameIgnoreCase(String username);

    Partner findFirstByEmail(String email);

    Partner findFirstByCryptowalletPublicKey(String walletAddress);

    Partner findAllByIdAndIsArtistTrue(UUID partnerId);

    boolean existsPartnerByUsername(String username);

}
