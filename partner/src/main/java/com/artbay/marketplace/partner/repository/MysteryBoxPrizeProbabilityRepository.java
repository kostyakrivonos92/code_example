package com.artbay.marketplace.partner.repository;

import com.artbay.marketplace.partner.model.MysteryBoxPrizeProbability;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.UUID;

@Repository
public interface MysteryBoxPrizeProbabilityRepository extends JpaRepository<MysteryBoxPrizeProbability, UUID> {

    MysteryBoxPrizeProbability findByBoxLevel(int boxLevel);

}
