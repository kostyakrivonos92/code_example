package com.artbay.marketplace.partner.util;

import com.artbay.marketplace.common.client.referral.ReferralExchangeRatesClient;
import com.artbay.marketplace.partner.model.response.MysteryBoxPageResponse;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Component;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

@Component
@RequiredArgsConstructor
public class MysteryBoxPriceConstants {

    private final ReferralExchangeRatesClient referralExchangeRatesClient;

    public static final double MYSTERY_BOX_L1 = 0.5;
    public static final double MYSTERY_BOX_L2 = 1;
    public static final double MYSTERY_BOX_L3 = 3;
    public static final double MYSTERY_BOX_L4 = 9;
    public static final double MYSTERY_BOX_L5 = 27;
    public static final double MYSTERY_BOX_L6 = 81;

    public static final Map<Integer, Double> PRICE_MYSTERY_BOX = Map.of(
        1, MYSTERY_BOX_L1,
        2, MYSTERY_BOX_L2,
        3, MYSTERY_BOX_L3,
        4, MYSTERY_BOX_L4,
        5, MYSTERY_BOX_L5,
        6, MYSTERY_BOX_L6
    );

    public List<MysteryBoxPageResponse> getMysteryBoxPrice() {
        List<MysteryBoxPageResponse> responseList = new ArrayList<>();
        var artzUsdPair = referralExchangeRatesClient.getArtzUsdPair().getPrice();
        for (int level = 1; level < 7; level = level + 1) {
            MysteryBoxPageResponse response;
            double upLevelPriceArtz = PRICE_MYSTERY_BOX.get(level) / artzUsdPair;
            response = new MysteryBoxPageResponse(level, (int) upLevelPriceArtz);

            responseList.add(response);
        }
        return responseList;
    }

    public static final int EXCHANGE_MYSTERY_BOX_L2 = 1;
    public static final int EXCHANGE_MYSTERY_BOX_L3 = 2;
    public static final int EXCHANGE_MYSTERY_BOX_L4 = 3;
    public static final int EXCHANGE_MYSTERY_BOX_L5 = 4;
    public static final int EXCHANGE_MYSTERY_BOX_L6 = 5;

    public static final Map<Integer, Integer> EXCHANGE_MYSTERY_BOX_LEVEL = Map.of(
        2, EXCHANGE_MYSTERY_BOX_L2,
        3, EXCHANGE_MYSTERY_BOX_L3,
        4, EXCHANGE_MYSTERY_BOX_L4,
        5, EXCHANGE_MYSTERY_BOX_L5,
        6, EXCHANGE_MYSTERY_BOX_L6
    );

    public static final int EXCHANGE_MYSTERY_BOX_L2_COUNT = 2;
    public static final int EXCHANGE_MYSTERY_BOX_L3_L6_COUNT = 3;

    public static final Map<Integer, Integer> EXCHANGE_MYSTERY_BOX_COUNT = Map.of(
        2, EXCHANGE_MYSTERY_BOX_L2_COUNT,
        3, EXCHANGE_MYSTERY_BOX_L3_L6_COUNT
    );

}
