package com.artbay.marketplace.partner.model.enums;

public enum MysteryBoxOperationType {
    MYSTERY_BOX_PRIZE,
    MYSTERY_BOX_BUY,
    MYSTERY_BOX_EXCHANGE,
    MYSTERY_BOX_OPEN
}
