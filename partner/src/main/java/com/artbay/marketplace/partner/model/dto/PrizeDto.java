package com.artbay.marketplace.partner.model.dto;

import com.artbay.marketplace.common.model.enums.MysteryBoxPrizeType;
import lombok.*;

@Getter
@Setter
@Builder
@AllArgsConstructor
@NoArgsConstructor
public class PrizeDto {


    private MysteryBoxPrizeType type;
    private Integer level;
    private Double countMin;
    private Double countMax;
    private Double probability;
    private Double amount;
}
