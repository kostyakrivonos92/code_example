package com.artbay.marketplace.partner.controller;

import com.artbay.marketplace.common.client.referral.ReferralRootClient;
import com.artbay.marketplace.common.client.referral.ReferralTransactionClient;
import com.artbay.marketplace.common.model.TransactionDTO;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import javax.validation.Valid;
import javax.validation.constraints.Min;
import java.util.HashMap;
import java.util.Map;

import static com.artbay.marketplace.common.model.enums.TransactionEntityType.ACTIVITY_POINTS_BALANCE;
import static com.artbay.marketplace.common.utils.ControllerUtil.getUserId;
import static com.artbay.marketplace.common.utils.constants.PartnerRankConstants.ACTIVITY_POINTS_TO_PARTNER_RANK;

@Slf4j
@RestController
@RequestMapping("/activity-points")
@RequiredArgsConstructor
public class ActivityPointsController {

    private static final String AMOUNT = "amount";
    private static final String RANK = "rank";
    private static final String NEXT = "next";
    private static final String CURRENT = "current";
    private static final String POINTS_LEFT = "pointsLeft";

    private final ReferralTransactionClient referralTransactionClient;
    private final ReferralRootClient referralRootClient;

    @GetMapping("/convert")
    @PreAuthorize("hasAuthority('user')")
    public void convertActivityPoints(@RequestParam @Valid @Min(100) Integer amount) {
        referralRootClient.convertActivityPoints(getUserId(), amount);
    }

    @GetMapping("/available")
    @PreAuthorize("hasAuthority('user')")
    public Map<String, Double> getAvailableActivityPoints() {
        var balance = referralTransactionClient.getBalanceForSpecificEntityType(
            getUserId(),
            ACTIVITY_POINTS_BALANCE
        );
        return Map.of(AMOUNT, balance);
    }

    @GetMapping("/used")
    @PreAuthorize("hasAuthority('user')")
    public Map<String, Double> getUsedActivityPoints() {
        var used = referralTransactionClient.getTotalUsedForSpecificEntityType(
            getUserId(),
            ACTIVITY_POINTS_BALANCE
        );
        return Map.of(AMOUNT, used);
    }

    @GetMapping("/total-earnings")
    @PreAuthorize("hasAuthority('user')")
    public Map<String, Double> getTotalActivityPoints() {
        var total = referralTransactionClient.getTotalEarningsForSpecificEntityType(
            getUserId(),
            ACTIVITY_POINTS_BALANCE
        );
        return Map.of(AMOUNT, total);
    }

    @GetMapping("/rank")
    @PreAuthorize("hasAuthority('user')")
    public Map<String, String> getRank() {
        var total = (referralTransactionClient.getTotalEarningsForSpecificEntityType(
            getUserId(),
            ACTIVITY_POINTS_BALANCE
        )).intValue();

        var currentRank = ACTIVITY_POINTS_TO_PARTNER_RANK.floorEntry(total);
        var nextRank = ACTIVITY_POINTS_TO_PARTNER_RANK.higherEntry(currentRank.getKey());

        var resultMap = new HashMap<>(Map.of(RANK, currentRank.getValue().name()));
        if (nextRank != null) {
            resultMap.put(CURRENT, String.valueOf(currentRank.getKey()));
            resultMap.put(NEXT, nextRank.getValue().name());
            resultMap.put(POINTS_LEFT, String.valueOf(nextRank.getKey() - total));
        }
        return resultMap;
    }

    @GetMapping("/converted")
    @PreAuthorize("hasAuthority('user')")
    public Map<String, Integer> getConvertedActivityPoints() {
        var convertedAmount = referralTransactionClient.getConvertedActivityPointsAmount(getUserId());
        return Map.of(AMOUNT, convertedAmount);
    }

    @GetMapping("/exchange-rates")
    @PreAuthorize("hasAuthority('user')")
    public Map<Integer, Double> getActivityPointsExchangeRate() {
        return referralTransactionClient.getActivityPointsExchangeRate();
    }

    @GetMapping("/transactions")
    @PreAuthorize("hasAuthority('user')")
    public Page<TransactionDTO> getTransactions(Pageable pageable) {
        return referralTransactionClient.getExpenseForEntityType(
            getUserId(),
            ACTIVITY_POINTS_BALANCE,
            pageable
        );
    }

}
