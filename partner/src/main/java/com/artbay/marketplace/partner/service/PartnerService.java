package com.artbay.marketplace.partner.service;

import com.artbay.marketplace.common.client.CryptoServiceClient;
import com.artbay.marketplace.common.client.TatumClient;
import com.artbay.marketplace.common.client.artist.internal.ArtistInternalClient;
import com.artbay.marketplace.common.client.auth.FusionauthAuthClient;
import com.artbay.marketplace.common.client.auth.internal.FusionauthInternalClient;
import com.artbay.marketplace.common.client.referral.ReferralRootClient;
import com.artbay.marketplace.common.client.referral.ReferralTransactionClient;
import com.artbay.marketplace.common.enums.Role;
import com.artbay.marketplace.common.exception.ErrorCode;
import com.artbay.marketplace.common.exception.GlobalException;
import com.artbay.marketplace.common.mapper.UserDtoMapper;
import com.artbay.marketplace.common.model.DetailedInfoDto;
import com.artbay.marketplace.common.model.PartnerDTO;
import com.artbay.marketplace.common.model.RefDTO;
import com.artbay.marketplace.common.model.ReferralStructureDto;
import com.artbay.marketplace.common.model.enums.TimeframeValue;
import com.artbay.marketplace.common.model.request.AddToReferralStructureRequest;
import com.artbay.marketplace.common.model.request.RegistrationRequest;
import com.artbay.marketplace.common.model.request.UpdateUserRequest;
import com.artbay.marketplace.common.model.tatum.CreateSubscriptionRequest;
import com.artbay.marketplace.common.service.MoosendService;
import com.artbay.marketplace.common.service.S3Service;
import com.artbay.marketplace.partner.model.Partner;
import com.artbay.marketplace.partner.model.mapper.PartnerMapper;
import com.artbay.marketplace.partner.repository.PartnerRepository;
import com.fasterxml.jackson.databind.ObjectMapper;
import lombok.RequiredArgsConstructor;
import lombok.SneakyThrows;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.io.FilenameUtils;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.MessageSource;
import org.springframework.context.i18n.LocaleContextHolder;
import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.util.UriComponentsBuilder;

import javax.transaction.Transactional;
import java.io.File;
import java.nio.file.Files;
import java.util.*;
import java.util.stream.Collectors;

import static com.artbay.marketplace.common.utils.FilePathGenerator.*;

@Slf4j
@Service
@RequiredArgsConstructor
public class PartnerService {

    @Value("${app.tatum.api-key}")
    private String tatumApiKey;
    @Value("${app.tatum.subscription-callback}")
    private String tatumSubscriptionCallback;
    @Value("${cloud.aws.default-bucket}")
    private String defaultBucketName;
    @Value("${app.cloudinary.delivery.url}")
    private String cloudinaryDeliveryUrl;
    @Value("${app.referral.domain}")
    private String referralDomain;
    @Value("${moosend.list.registered}")
    private String registeredList;
    @Value("${moosend.list.registered.artist}")
    private String registeredArtistList;

    private final PartnerRepository partnerRepository;

    private final UserDtoMapper userDtoMapper;
    private final PartnerMapper partnerMapper;

    private final ObjectMapper objectMapper;

    private final FusionauthInternalClient fusionauthInternalClient;
    private final FusionauthAuthClient fusionauthAuthClient;
    private final ReferralRootClient referralRootClient;
    private final ReferralTransactionClient referralTransactionClient;
    private final CryptoServiceClient cryptoServiceClient;
    private final TatumClient tatumClient;
    private final ArtistInternalClient artistInternalClient;

    private final S3Service s3Service;
    private final MoosendService moosendService;

    @Qualifier("activeMessageSource")
    private final MessageSource messageSource;

    public Partner getPartnerById(UUID id) {
        return partnerRepository.getById(id);
    }

    @Transactional
    public void updatePartner(UUID partnerId, UpdateUserRequest updateRequest) {
        fusionauthInternalClient.updateUser(partnerId, updateRequest);
        var partner = getPartnerById(partnerId);

        if (updateRequest.getUsername() != null && !updateRequest.getUsername().equals(partner.getUsername()) &&
                partnerRepository.existsPartnerByUsername(updateRequest.getUsername())) {
            throw new GlobalException(ErrorCode.ERROR_VALIDATION, messageSource.getMessage(
                "USERNAME_ALREADY_EXISTS",
                null,
                LocaleContextHolder.getLocale()
            ));
        }

        partnerMapper.update(partner, updateRequest);

        if (partner.getIsArtist()) {
            artistInternalClient.updateArtist(partnerId, updateRequest);
        }

        partnerRepository.save(partner);
    }

    @SneakyThrows
    public void uploadAvatar(UUID partnerId, MultipartFile file) {
        var tempFileName = UUID.randomUUID() + "." + FilenameUtils.getExtension(file.getOriginalFilename());
        var tempFile = new File(System.getProperty("java.io.tmpdir"), tempFileName);
        file.transferTo(tempFile);
        var partner = getPartnerById(partnerId);
        var path = generateAvatarFileName(tempFileName);
        s3Service.upload(defaultBucketName, path, tempFile);
        var avatarLink = generateCloudinaryLink(cloudinaryDeliveryUrl, path);

        var thumbnailLink = generateThumbnailLink(cloudinaryDeliveryUrl, path, false);

        partner.setAvatarLink(avatarLink);
        partner.setAvatarThumbnailLink(thumbnailLink);

        if (partner.getIsArtist()) {
            var artist = artistInternalClient.getArtistById(partnerId);
            artist.setAvatarLink(avatarLink);
            artist.setAvatarThumbnailLink(thumbnailLink);
            artistInternalClient.updateArtist(artist);
        }

        partnerRepository.save(partner);
        Files.delete(tempFile.toPath());
    }

    @Transactional
    public void createPartner(RegistrationRequest registrationRequest) {
        checkExistsPartnerAndDelete(registrationRequest);
        var partnerBuilder = Partner.builder()
                                 .referrerId(fetchReferrerId(registrationRequest))
                                 .username(registrationRequest.getUsername())
                                 .firstName(registrationRequest.getFirstName())
                                 .lastName(registrationRequest.getLastName())
                                 .email(registrationRequest.getEmail())
                                 .country(registrationRequest.getCountry());

        //if method called from artist module, we already have an id
        var partnerId = UUID.randomUUID();
        if (registrationRequest.getUserId() != null) {
            partnerId = registrationRequest.getUserId();
            partnerBuilder.isArtist(true);
        }
        partnerBuilder.id(partnerId);

        var response = cryptoServiceClient.createWallet();
        partnerBuilder.cryptowalletPrivateKey(response.getPrivateKey());
        partnerBuilder.cryptowalletPublicKey(response.getPublicKey());

        var partner = partnerBuilder.build();
        partnerRepository.save(partner);

        var createUserRequest = userDtoMapper.fromRegistrationRequest(registrationRequest);
        createUserRequest.setRoles(Arrays.stream(Role.PARTNER.values())
                                       .map(Role.PARTNER::getRoleName)
                                       .collect(Collectors.toSet()));
        createUserRequest.setId(partnerId);
        fusionauthAuthClient.createUser(createUserRequest);

        if (partner.getIsArtist()) {
            moosendService.addToEmailList(partnerMapper.partnerToDto(partner), registeredArtistList);
        } else {
            moosendService.addToEmailList(partnerMapper.partnerToDto(partner), registeredList);
        }
    }

    @Transactional
    public void verified(UUID partnerId) {
        var partner = getPartnerById(partnerId);

        if (isEligibleForApBonus(partner)) {
            referralTransactionClient.activateAPBonus(partner.getId());
            //hack to update partner's ap balance with bonus in current entity session
            partner.setActivityPointsAmount(20);
        }
        partner.setIsVerified(true);
        referralRootClient.addToStructure(new AddToReferralStructureRequest(partnerId, partner.getReferrerId()));
        partnerRepository.save(partner); //maybe not necessary
        partner.setTatumSubscriptionId(createTatumSubscription(partner));
        partnerRepository.save(partner);
    }

    public Map<Long, ReferralStructureDto> getReferralStructure(UUID partnerId) {
        return referralRootClient.getReferralStructure(partnerId);
    }

    public List<RefDTO> getReferralsOnLevel(UUID partnerId, int level) {
        var referrals = referralRootClient.getReferralIdsOnSpecificLevel(partnerId, level);
        return referrals.stream().map(partnerMapper::fromPartnerIdToRefDTO).collect(Collectors.toList());
    }

    public String getReferralLink(String username) {
        var uri = UriComponentsBuilder.newInstance()
                      .scheme("https")
                      .host(referralDomain)
                      .path("ref/" + username)
                      .build();
        return uri.toUriString();
    }

    public Partner getPartnerByUsername(String username) {
        return partnerRepository.findFirstByUsernameIgnoreCase(username);
    }

    public Partner getPartnerByEmail(String email) {
        return partnerRepository.findFirstByEmail(email);
    }

    public Partner getPartnerByIdIsArtist(UUID partnerId) {
        return partnerRepository.findAllByIdAndIsArtistTrue(partnerId);
    }

    public List<PartnerDTO> batchUpdatePartners(List<PartnerDTO> partnerDtos) {
        var partnersToUpdate = partnerMapper.dtosToEntities(partnerDtos);
        return partnerMapper.partnerToDto(partnerRepository.saveAll(partnersToUpdate));
    }

    public Partner save(PartnerDTO partnerDTO) {
        var partner = partnerMapper.dtoToEntity(partnerDTO);
        return partnerRepository.save(partner);
    }

    public Partner save(Partner partner) {
        return partnerRepository.save(partner);
    }

    public List<Partner> getPartnersByIds(List<UUID> partnerIds) {
        return partnerRepository.findAllById(partnerIds);
    }

    public DetailedInfoDto getPartnerDetailedInfo(UUID currentUserId, UUID partnerId, String timeframe) {
        var partnerDTO = partnerMapper.partnerToDto(getPartnerById(partnerId));
        if (currentUserId.equals(partnerDTO.getReferrer().getId())) {
            var detailedInfoDto = referralRootClient.getPartnerDetailedInfo(
                partnerId,
                TimeframeValue.valueOf(timeframe.toUpperCase())
            );
            partnerMapper.setPartnerToDto(detailedInfoDto, partnerDTO);
            return detailedInfoDto;
        }
        return new DetailedInfoDto();
    }

    public List<Partner> getPartnerList(List<UUID> uuids) {
        return partnerRepository.findAllById(uuids);
    }

    public Partner getPartnerByWallet(String walletAddress) {
        return partnerRepository.findFirstByCryptowalletPublicKey(walletAddress);
    }

    private UUID fetchReferrerId(RegistrationRequest registrationRequest) {
        Partner referrer = partnerRepository.findFirstByUsernameIgnoreCase(registrationRequest.getReferrerUsername());
        if (referrer != null && referrer.getId() != null && referrer.getIsVerified()) {
            return referrer.getId();
        } else {
            throw new GlobalException(
                ErrorCode.ERROR_REFERRER_NOT_FOUND,
                messageSource.getMessage("REFERRER_NOT_FOUND", null, LocaleContextHolder.getLocale())
            );
        }
    }

    private void checkExistsPartnerAndDelete(RegistrationRequest registrationRequest) {
        var existsPartner = getPartnerByEmail(registrationRequest.getEmail());
        if (existsPartner != null) {
            if (existsPartner.getIsVerified()) {
                throw new GlobalException(
                    ErrorCode.EMAIL_EXISTS,
                    messageSource.getMessage("USER_ALREADY_EXISTS", null, LocaleContextHolder.getLocale())
                );
            } else {
                fusionauthInternalClient.removeUser(existsPartner.getId());
                partnerRepository.delete(existsPartner);
            }
        }
    }

    private String createTatumSubscription(Partner partner) {
        CreateSubscriptionRequest request = CreateSubscriptionRequest.builder()
                                                .attributes(CreateSubscriptionRequest.Attributes.builder()
                                                                .address(partner.getCryptowalletPublicKey())
                                                                .url(tatumSubscriptionCallback)
                                                                .build())
                                                .build();
        try {
            String response = tatumClient.createSubscription(tatumApiKey, request);
            return objectMapper.readTree(response).get("id").asText();
        } catch (Exception e) {
            log.error("Error creating Tatum subscription for partner {}", partner, e);
        }
        return "ERROR";
    }

    private boolean isEligibleForApBonus(Partner partner) {
        return !partner.getIsVerified() &&
                   (partner.getDiscordId() != null ||
                        (partner.getReferrerId() != null && !Objects.equals(
                            partner.getReferrerId()
                                .toString(),
                            "82552e49-e918-4564-a1b9-3aa329775cbd"
                        )));
    }

}
