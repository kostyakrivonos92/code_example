package com.artbay.marketplace.partner.model;

import lombok.*;
import org.springframework.data.annotation.CreatedDate;
import org.springframework.data.annotation.LastModifiedDate;
import org.springframework.data.jpa.domain.support.AuditingEntityListener;

import javax.persistence.*;
import javax.validation.constraints.Max;
import javax.validation.constraints.Min;
import java.time.Instant;
import java.util.UUID;

@Getter
@Setter
@Builder
@AllArgsConstructor
@NoArgsConstructor
@Entity
@EntityListeners(AuditingEntityListener.class)
@Table(name = "mystery_box")
public class MysteryBox {

    @Id
    @GeneratedValue
    @Column(name = "id")
    private UUID id;

    @Column(name = "name")
    private String name;

    @Min(value = 1, message = "Artwork level should be between 0 and 6")
    @Max(value = 6, message = "Artwork level should be between 0 and 6")
    @Column(name = "level", columnDefinition = "integer default 1")
    private Integer level;

    @Column(name = "price", columnDefinition = "double default 0")
    private Double price;

    @Column(name = "owner_id")
    private UUID ownerId;

    @Column(name = "opened")
    @Builder.Default
    private Boolean opened = false;

    @Column(name = "exchanged")
    @Builder.Default
    private Boolean exchanged = false;

    @CreatedDate
    @Column(name = "creation_date")
    private Instant creationDate;

    @LastModifiedDate
    @Column(name = "last_modified_date")
    private Instant lastModifiedDate;

}
