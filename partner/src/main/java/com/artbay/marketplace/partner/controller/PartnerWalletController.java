package com.artbay.marketplace.partner.controller;

import com.artbay.marketplace.common.client.referral.ReferralSwapClient;
import com.artbay.marketplace.common.client.referral.ReferralTransactionClient;
import com.artbay.marketplace.common.model.PersonalWalletDto;
import com.artbay.marketplace.common.model.TransactionDTO;
import com.artbay.marketplace.common.model.jupiter.SwapInstruction;
import com.artbay.marketplace.common.model.jupiter.SwapTransaction;
import com.artbay.marketplace.common.twofactor.Required2fa;
import com.artbay.marketplace.partner.model.Partner;
import com.artbay.marketplace.partner.service.PartnerService;
import com.artbay.marketplace.partner.service.PartnerWalletService;
import io.swagger.annotations.ApiOperation;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.List;
import java.util.Map;
import java.util.UUID;

import static com.artbay.marketplace.common.utils.ControllerUtil.getUserId;

@Slf4j
@RestController
@RequestMapping("/personal-wallet")
@RequiredArgsConstructor
@Validated
public class PartnerWalletController {

    private final PartnerWalletService partnerWalletService;
    private final PartnerService partnerService;

    private final ReferralTransactionClient referralTransactionClient;
    private final ReferralSwapClient referralSwapClient;

    @Value("${app.wallet.platform.public-key}")
    private String platformPublicKey;

    @GetMapping
    public PersonalWalletDto getPersonalWallet() {
        return partnerWalletService.getPersonalWalletData(getUserId());
    }

    @Required2fa
    @GetMapping("/withdrawal/external-wallet")
    @PreAuthorize("hasAuthority('user_allowed_withdrawal')")
    @ApiOperation(value = "Withdraw any currency to external wallet.")
    public Map<String, String> withdrawalTokensToExternalWallet(
        @RequestParam String externalWalletAddress,
        @RequestParam String mintAddress,
        @RequestParam Double amount
    ) {
        return Map.of(
            "transactionId",
            partnerWalletService.withdrawalTokensToExternalWallet(
                getUserId(),
                externalWalletAddress,
                mintAddress,
                amount
            )
        );
    }

    @GetMapping("/transaction-history")
    @PreAuthorize("hasAuthority('user')")
    @ApiOperation(value = "Get transaction history for partner personal wallet.")
    public Page<TransactionDTO> transactionHistory(Pageable pageable) {
        return referralTransactionClient.getPartnerTransactionHistory(getUserId(), pageable);
    }

    @PostMapping("/swap")
    @PreAuthorize("hasAuthority('allowed_swap')")
    @ApiOperation(value = "Create swap transaction")
    public List<SwapTransaction> createSwap(@RequestBody @Valid SwapInstruction swapInstruction) {
        Partner partner = partnerService.getPartnerById(getUserId());
        swapInstruction.setUserPublicKey(partner.getCryptowalletPublicKey());
        swapInstruction.setFeeAccount(platformPublicKey);
        return referralSwapClient.swap(swapInstruction, getUserId(), partner.getCryptowalletPrivateKey(), false);
    }

    @PostMapping("/send-swap-transaction")
    @PreAuthorize("hasAuthority('allowed_swap')")
    @ApiOperation(value = "Send swap transaction to blockchain")
    public Map<String, String> sendSwapTransaction(@RequestBody SwapTransaction swapTransaction) {
        Partner partner = partnerService.getPartnerById(getUserId());
        return referralSwapClient.sendTransaction(
            swapTransaction,
            partner.getCryptowalletPublicKey(),
            partner.getCryptowalletPrivateKey()
        );
    }

    @PostMapping("/confirm-swap")
    @PreAuthorize("hasAuthority('allowed_swap')")
    @ApiOperation(value = "Confirm swap transaction (check in blockchain and set 'executed' status)")
    public void confirmSwap(@RequestParam UUID swapId) {
        referralSwapClient.confirm(swapId);
    }

}
