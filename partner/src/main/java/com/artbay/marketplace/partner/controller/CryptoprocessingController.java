package com.artbay.marketplace.partner.controller;

import com.artbay.marketplace.common.client.referral.ReferralCryptoprocessingClient;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;
import java.util.Map;

import static com.artbay.marketplace.common.utils.ControllerUtil.getUserId;

@Slf4j
@RestController
@RequestMapping("/cryptoprocessing")
@RequiredArgsConstructor
public class CryptoprocessingController {

    private final ReferralCryptoprocessingClient cryptoprocessingClient;

    @GetMapping("/payment-status")
    public Map<String, String> getPaymentStatus() {
        return cryptoprocessingClient.getPaymentStatus(getUserId());
    }

    @GetMapping("/available-currencies")
    public Map<String, List<String>> getAvailableCurrencies() {
        return cryptoprocessingClient.getAvailableCurrencies();
    }

    @GetMapping("/purchase-nft-key")
    public Map<String, Object> purchaseNftKey(
        @RequestParam String paymentCurrency
    ) {
        return cryptoprocessingClient.purchaseNftKey(getUserId(), paymentCurrency);
    }

    @GetMapping("/purchase-artz")
    public Map<String, Object> purchaseArtz(@RequestParam String paymentCurrency, @RequestParam Double amount) {
        return cryptoprocessingClient.purchaseArtz(getUserId(), paymentCurrency, amount);
    }

}
