package com.artbay.marketplace.partner.model;

import com.artbay.marketplace.partner.model.enums.MysteryBoxOperationType;
import com.artbay.marketplace.common.model.enums.MysteryBoxPrizeType;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.springframework.data.annotation.CreatedDate;
import org.springframework.data.jpa.domain.support.AuditingEntityListener;

import javax.persistence.*;
import java.time.Instant;
import java.util.UUID;

@Data
@AllArgsConstructor
@NoArgsConstructor
@Builder
@Entity
@Table(name = "mystery_box_activity_history")
@EntityListeners(AuditingEntityListener.class)
public class MysteryBoxActivityHistory {

    @Id
    @GeneratedValue
    @Column(name = "id")
    private UUID id;

    @Column(name = "mystery_box_id")
    private UUID mysteryBoxId;

    @Column(name = "user_id")
    private UUID userId;

    @Enumerated(EnumType.STRING)
    @Column(name = "operation_type")
    private MysteryBoxOperationType operationType;

    @Column(name = "price")
    private Double price;
    @Column(name = "previous_level")
    private Integer previousLevel;
    @Column(name = "new_level")
    private Integer newLevel;
    @Column(name = "prize_level")
    private Integer prizeLevel;
    @Column(name = "nft_prize_id")
    private UUID nftPrizeId;

    @Enumerated(EnumType.STRING)
    @Column(name = "prize_type")
    private MysteryBoxPrizeType prizeType;
    @Column(name = "amount", columnDefinition = "double default 0.0")
    @Builder.Default
    private Double amount = 0.0;

    @CreatedDate
    @Column(name = "operation_date")
    private Instant operationDate;

}
