package com.tiber.property.mapper;

import com.tiber.property.domain.DealSummary;
import com.tiber.shared.propertylayer.domain.Deal;
import com.tiber.shared.propertylayer.domain.PropertyOnMarket;
import com.tiber.shared.util.AnalogueRenovationTag;
import com.tiber.shared.vocabulary.DealStatus;
import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;
import javax.annotation.processing.Generated;
import org.springframework.stereotype.Component;

@Generated(
    value = "org.mapstruct.ap.MappingProcessor",
    date = "2022-04-06T16:49:03+0300",
    comments = "version: 1.4.2.Final, compiler: javac, environment: Java 14 (Oracle Corporation)"
)
@Component
public class DealSummaryMapperImpl implements DealSummaryMapper {

    @Override
    public List<DealSummary> mapToDealSummaryList(List<PropertyOnMarket> properties) {
        if ( properties == null ) {
            return null;
        }

        List<DealSummary> list = new ArrayList<DealSummary>( properties.size() );
        for ( PropertyOnMarket propertyOnMarket : properties ) {
            list.add( dealSummaryFromPropertyOnMarket( propertyOnMarket ) );
        }

        return list;
    }

    @Override
    public DealSummary dealSummaryFromPropertyOnMarket(PropertyOnMarket propertyOnMarket) {
        if ( propertyOnMarket == null ) {
            return null;
        }

        DealSummary dealSummary = new DealSummary();

        dealSummary.setPropertyUuid( propertyOnMarket.getUuid() );
        dealSummary.setUuid( propertyOnMarketLatestDealUuid( propertyOnMarket ) );
        Integer price = propertyOnMarketLatestDealPrice( propertyOnMarket );
        if ( price != null ) {
            dealSummary.setPrice( price.floatValue() );
        }
        dealSummary.setStatus( propertyOnMarketLatestDealStatus( propertyOnMarket ) );
        dealSummary.setCloseDate( propertyOnMarketLatestDealCloseDate( propertyOnMarket ) );
        dealSummary.setRenovationTag( propertyOnMarketLatestDealRenovationTag( propertyOnMarket ) );
        dealSummary.setAddress( propertyOnMarket.getAddress() );
        dealSummary.setCity( propertyOnMarket.getCity() );
        dealSummary.setState( propertyOnMarket.getState() );
        dealSummary.setZip( propertyOnMarket.getZip() );
        dealSummary.setMsa( propertyOnMarket.getMsa() );
        dealSummary.setCounty( propertyOnMarket.getCounty() );
        dealSummary.setBedrooms( propertyOnMarket.getBedrooms() );
        dealSummary.setBathrooms( propertyOnMarket.getBathrooms() );
        dealSummary.setHalfBathrooms( propertyOnMarket.getHalfBathrooms() );
        dealSummary.setAreaSqFt( propertyOnMarket.getAreaSqFt() );
        dealSummary.setYearBuilt( propertyOnMarket.getYearBuilt() );
        dealSummary.setLatitude( propertyOnMarket.getLatitude() );
        dealSummary.setLongitude( propertyOnMarket.getLongitude() );

        return dealSummary;
    }

    private String propertyOnMarketLatestDealUuid(PropertyOnMarket propertyOnMarket) {
        if ( propertyOnMarket == null ) {
            return null;
        }
        Deal latestDeal = propertyOnMarket.getLatestDeal();
        if ( latestDeal == null ) {
            return null;
        }
        String uuid = latestDeal.getUuid();
        if ( uuid == null ) {
            return null;
        }
        return uuid;
    }

    private Integer propertyOnMarketLatestDealPrice(PropertyOnMarket propertyOnMarket) {
        if ( propertyOnMarket == null ) {
            return null;
        }
        Deal latestDeal = propertyOnMarket.getLatestDeal();
        if ( latestDeal == null ) {
            return null;
        }
        Integer price = latestDeal.getPrice();
        if ( price == null ) {
            return null;
        }
        return price;
    }

    private DealStatus propertyOnMarketLatestDealStatus(PropertyOnMarket propertyOnMarket) {
        if ( propertyOnMarket == null ) {
            return null;
        }
        Deal latestDeal = propertyOnMarket.getLatestDeal();
        if ( latestDeal == null ) {
            return null;
        }
        DealStatus status = latestDeal.getStatus();
        if ( status == null ) {
            return null;
        }
        return status;
    }

    private LocalDate propertyOnMarketLatestDealCloseDate(PropertyOnMarket propertyOnMarket) {
        if ( propertyOnMarket == null ) {
            return null;
        }
        Deal latestDeal = propertyOnMarket.getLatestDeal();
        if ( latestDeal == null ) {
            return null;
        }
        LocalDate closeDate = latestDeal.getCloseDate();
        if ( closeDate == null ) {
            return null;
        }
        return closeDate;
    }

    private AnalogueRenovationTag propertyOnMarketLatestDealRenovationTag(PropertyOnMarket propertyOnMarket) {
        if ( propertyOnMarket == null ) {
            return null;
        }
        Deal latestDeal = propertyOnMarket.getLatestDeal();
        if ( latestDeal == null ) {
            return null;
        }
        AnalogueRenovationTag renovationTag = latestDeal.getRenovationTag();
        if ( renovationTag == null ) {
            return null;
        }
        return renovationTag;
    }
}
