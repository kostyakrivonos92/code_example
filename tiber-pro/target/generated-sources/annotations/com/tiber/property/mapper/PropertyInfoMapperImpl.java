package com.tiber.property.mapper;

import com.tiber.shared.inspection.domain.PropertyInfo;
import com.tiber.shared.inspection.entity.PropertyInfoEntity;
import com.tiber.shared.propertylayer.domain.PropertyOnMarket;
import javax.annotation.processing.Generated;
import org.springframework.stereotype.Component;

@Generated(
    value = "org.mapstruct.ap.MappingProcessor",
    date = "2022-04-06T16:49:03+0300",
    comments = "version: 1.4.2.Final, compiler: javac, environment: Java 14 (Oracle Corporation)"
)
@Component
public class PropertyInfoMapperImpl implements PropertyInfoMapper {

    @Override
    public PropertyInfo propertyInfoDoToEntity(PropertyInfoEntity entity) {
        if ( entity == null ) {
            return null;
        }

        PropertyInfo propertyInfo = new PropertyInfo();

        if ( entity.getBedrooms() != null ) {
            propertyInfo.setBedrooms( entity.getBedrooms() );
        }
        if ( entity.getBathrooms() != null ) {
            propertyInfo.setBathrooms( entity.getBathrooms() );
        }
        if ( entity.getHalfBathrooms() != null ) {
            propertyInfo.setHalfBathrooms( entity.getHalfBathrooms() );
        }
        if ( entity.getAreaSqFt() != null ) {
            propertyInfo.setAreaSqFt( entity.getAreaSqFt() );
        }
        if ( entity.getYearBuilt() != null ) {
            propertyInfo.setYearBuilt( entity.getYearBuilt() );
        }

        return propertyInfo;
    }

    @Override
    public PropertyInfoEntity propertyInfoEntityToDo(PropertyInfo propertyInfo) {
        if ( propertyInfo == null ) {
            return null;
        }

        PropertyInfoEntity propertyInfoEntity = new PropertyInfoEntity();

        propertyInfoEntity.setBedrooms( propertyInfo.getBedrooms() );
        propertyInfoEntity.setBathrooms( propertyInfo.getBathrooms() );
        propertyInfoEntity.setHalfBathrooms( propertyInfo.getHalfBathrooms() );
        propertyInfoEntity.setAreaSqFt( propertyInfo.getAreaSqFt() );
        propertyInfoEntity.setYearBuilt( propertyInfo.getYearBuilt() );

        return propertyInfoEntity;
    }

    @Override
    public PropertyInfo publicPropertyToDomain(PropertyOnMarket propertyOnMarket) {
        if ( propertyOnMarket == null ) {
            return null;
        }

        PropertyInfo propertyInfo = new PropertyInfo();

        if ( propertyOnMarket.getBedrooms() != null ) {
            propertyInfo.setBedrooms( propertyOnMarket.getBedrooms() );
        }
        if ( propertyOnMarket.getBathrooms() != null ) {
            propertyInfo.setBathrooms( propertyOnMarket.getBathrooms() );
        }
        if ( propertyOnMarket.getHalfBathrooms() != null ) {
            propertyInfo.setHalfBathrooms( propertyOnMarket.getHalfBathrooms() );
        }
        if ( propertyOnMarket.getAreaSqFt() != null ) {
            propertyInfo.setAreaSqFt( propertyOnMarket.getAreaSqFt() );
        }
        if ( propertyOnMarket.getYearBuilt() != null ) {
            propertyInfo.setYearBuilt( propertyOnMarket.getYearBuilt() );
        }

        return propertyInfo;
    }
}
