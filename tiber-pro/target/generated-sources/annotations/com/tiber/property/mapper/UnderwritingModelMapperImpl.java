package com.tiber.property.mapper;

import com.tiber.property.domain.AnaloguesSelection;
import com.tiber.property.domain.PropertyAnalogue;
import com.tiber.property.domain.StrategyDependentInitialData;
import com.tiber.property.domain.StrategyDependentResultingData;
import com.tiber.property.domain.UnderwritingModel;
import com.tiber.property.entity.AnaloguesSelectionEntity;
import com.tiber.property.entity.PropertyAnalogueEntity;
import com.tiber.property.entity.PropertyAnalogueEntityList;
import com.tiber.property.entity.StrategyDependentInitialDataEntity;
import com.tiber.property.entity.StrategyDependentResultingDataEntity;
import com.tiber.property.entity.UnderwritingModelEntity;
import com.tiber.property.entity.UnderwritingNoteEntity;
import com.tiber.property.status.UnderwritingModelStatus;
import com.tiber.shared.file.domain.FileObject;
import com.tiber.shared.file.entity.FileObjectEntity;
import com.tiber.shared.file.entity.FileObjectEntityList;
import com.tiber.shared.inspection.domain.PropertyInfo;
import com.tiber.shared.inspection.entity.PropertyInfoEntity;
import com.tiber.shared.note.domain.Note;
import com.tiber.shared.status.domain.StatusEvent;
import com.tiber.shared.status.entity.StatusEventEntity;
import com.tiber.shared.status.entity.StatusEventEntityList;
import com.tiber.shared.underwriting.domain.UnderwritingModelExternal;
import com.tiber.shared.util.RenovationType;
import java.math.BigDecimal;
import java.time.Instant;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import javax.annotation.processing.Generated;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

@Generated(
    value = "org.mapstruct.ap.MappingProcessor",
    date = "2022-04-06T16:49:03+0300",
    comments = "version: 1.4.2.Final, compiler: javac, environment: Java 14 (Oracle Corporation)"
)
@Component
public class UnderwritingModelMapperImpl implements UnderwritingModelMapper {

    @Autowired
    private UnderwritingModelMiscMapper underwritingModelMiscMapper;
    @Autowired
    private UnderwritingModelLeaseMapper underwritingModelLeaseMapper;
    @Autowired
    private UnderwritingModelTaxMapper underwritingModelTaxMapper;

    @Override
    public UnderwritingModel toDomain(UnderwritingModelEntity entity) {
        if ( entity == null ) {
            return null;
        }

        UnderwritingModel underwritingModel = new UnderwritingModel();

        underwritingModel.setId( entity.getId() );
        underwritingModel.setUuid( entity.getUuid() );
        underwritingModel.setPropertyUuid( entity.getPropertyUuid() );
        underwritingModel.setDealUuid( entity.getDealUuid() );
        underwritingModel.setSalesDate( entity.getSalesDate() );
        underwritingModel.setMinYield( entity.getMinYield() );
        underwritingModel.setDdRehab( BigDecimal.valueOf( entity.getDdRehab() ) );
        underwritingModel.setValuationPsp( BigDecimal.valueOf( entity.getValuationPsp() ) );
        underwritingModel.setAskingPrice( BigDecimal.valueOf( entity.getAskingPrice() ) );
        underwritingModel.setOffer( BigDecimal.valueOf( entity.getOffer() ) );
        underwritingModel.setAllInBasis( entity.getAllInBasis() );
        underwritingModel.setPriceToYield( entity.getPriceToYield() );
        underwritingModel.setStrategy( entity.getStrategy() );
        underwritingModel.setStrategyDependentResultingDataMap( renovationTypeStrategyDependentResultingDataEntityMapToRenovationTypeStrategyDependentResultingDataMap( entity.getStrategyDependentResultingDataMap() ) );
        underwritingModel.setStrategyDependentInitialDataMap( renovationTypeStrategyDependentInitialDataEntityMapToRenovationTypeStrategyDependentInitialDataMap( entity.getStrategyDependentInitialDataMap() ) );
        underwritingModel.setLease( underwritingModelLeaseMapper.toDomain( entity.getLease() ) );
        underwritingModel.setMisc( underwritingModelMiscMapper.toDomain( entity.getMisc() ) );
        underwritingModel.setTax( underwritingModelTaxMapper.toDomain( entity.getTax() ) );
        underwritingModel.setPropertyAnalogues( propertyAnalogueEntityListToPropertyAnalogueList( entity.getPropertyAnalogues() ) );
        underwritingModel.setAnaloguesSelection( analoguesSelectionEntityToAnaloguesSelection( entity.getAnaloguesSelection() ) );
        underwritingModel.setCreatedAt( entity.getCreatedAt() );
        underwritingModel.setUpdatedAt( entity.getUpdatedAt() );
        underwritingModel.setPropertyInfo( propertyInfoEntityToPropertyInfo( entity.getPropertyInfo() ) );
        underwritingModel.setStatusEventList( statusEventEntityListToStatusEventList( entity.getStatusEventList() ) );

        return underwritingModel;
    }

    @Override
    public UnderwritingModelEntity toEntity(UnderwritingModel model) {
        if ( model == null ) {
            return null;
        }

        UnderwritingModelEntity underwritingModelEntity = new UnderwritingModelEntity();

        underwritingModelEntity.setId( model.getId() );
        underwritingModelEntity.setCreatedAt( model.getCreatedAt() );
        underwritingModelEntity.setUpdatedAt( model.getUpdatedAt() );
        underwritingModelEntity.setUuid( model.getUuid() );
        underwritingModelEntity.setPropertyUuid( model.getPropertyUuid() );
        underwritingModelEntity.setDealUuid( model.getDealUuid() );
        underwritingModelEntity.setSalesDate( model.getSalesDate() );
        underwritingModelEntity.setMinYield( model.getMinYield() );
        if ( model.getDdRehab() != null ) {
            underwritingModelEntity.setDdRehab( model.getDdRehab().intValue() );
        }
        if ( model.getValuationPsp() != null ) {
            underwritingModelEntity.setValuationPsp( model.getValuationPsp().intValue() );
        }
        if ( model.getAskingPrice() != null ) {
            underwritingModelEntity.setAskingPrice( model.getAskingPrice().intValue() );
        }
        if ( model.getOffer() != null ) {
            underwritingModelEntity.setOffer( model.getOffer().intValue() );
        }
        underwritingModelEntity.setPropertyInfo( propertyInfoToPropertyInfoEntity( model.getPropertyInfo() ) );
        underwritingModelEntity.setAllInBasis( model.getAllInBasis() );
        underwritingModelEntity.setPriceToYield( model.getPriceToYield() );
        underwritingModelEntity.setStrategy( model.getStrategy() );
        underwritingModelEntity.setStrategyDependentResultingDataMap( renovationTypeStrategyDependentResultingDataMapToRenovationTypeStrategyDependentResultingDataEntityMap( model.getStrategyDependentResultingDataMap() ) );
        underwritingModelEntity.setStrategyDependentInitialDataMap( renovationTypeStrategyDependentInitialDataMapToRenovationTypeStrategyDependentInitialDataEntityMap( model.getStrategyDependentInitialDataMap() ) );
        underwritingModelEntity.setLease( underwritingModelLeaseMapper.toEntity( model.getLease() ) );
        underwritingModelEntity.setMisc( underwritingModelMiscMapper.toEntity( model.getMisc() ) );
        underwritingModelEntity.setTax( underwritingModelTaxMapper.toEntity( model.getTax() ) );
        underwritingModelEntity.setPropertyAnalogues( propertyAnalogueListToPropertyAnalogueEntityList( model.getPropertyAnalogues() ) );
        underwritingModelEntity.setAnaloguesSelection( analoguesSelectionToAnaloguesSelectionEntity( model.getAnaloguesSelection() ) );
        underwritingModelEntity.setStatusEventList( statusEventListToStatusEventEntityList( model.getStatusEventList() ) );

        return underwritingModelEntity;
    }

    @Override
    public AnaloguesSelectionEntity analoguesSelectionToAnaloguesSelectionEntity(AnaloguesSelection analoguesSelection) {
        if ( analoguesSelection == null ) {
            return null;
        }

        AnaloguesSelectionEntity analoguesSelectionEntity = new AnaloguesSelectionEntity();

        List<String> list = analoguesSelection.getSpec();
        if ( list != null ) {
            analoguesSelectionEntity.setSpec( new ArrayList<String>( list ) );
        }
        List<String> list1 = analoguesSelection.getStock();
        if ( list1 != null ) {
            analoguesSelectionEntity.setStock( new ArrayList<String>( list1 ) );
        }
        List<String> list2 = analoguesSelection.getAll();
        if ( list2 != null ) {
            analoguesSelectionEntity.setAll( new ArrayList<String>( list2 ) );
        }

        return analoguesSelectionEntity;
    }

    @Override
    public UnderwritingNoteEntity noteToUnderwritingNoteEntity(Note note) {
        if ( note == null ) {
            return null;
        }

        UnderwritingNoteEntity underwritingNoteEntity = new UnderwritingNoteEntity();

        underwritingNoteEntity.setId( note.getId() );
        underwritingNoteEntity.setCreatedAt( note.getCreatedAt() );
        underwritingNoteEntity.setText( note.getText() );
        underwritingNoteEntity.setCreatedBy( note.getCreatedBy() );
        underwritingNoteEntity.setFiles( fileObjectListToFileObjectEntityList( note.getFiles() ) );
        underwritingNoteEntity.setSystem( note.isSystem() );

        return underwritingNoteEntity;
    }

    @Override
    public Note underwritingNoteEntityToNote(UnderwritingNoteEntity note) {
        if ( note == null ) {
            return null;
        }

        String text = null;

        text = note.getText();

        Note note1 = new Note( text );

        if ( note.getId() != null ) {
            note1.setId( note.getId() );
        }
        note1.setCreatedBy( note.getCreatedBy() );
        note1.setFiles( fileObjectEntityListToFileObjectList( note.getFiles() ) );
        note1.setCreatedAt( note.getCreatedAt() );
        note1.setSystem( note.isSystem() );

        return note1;
    }

    @Override
    public UnderwritingModelExternal underwritingModelEntityToUnderwritingModelExternal(UnderwritingModelEntity underwritingModelEntity) {
        if ( underwritingModelEntity == null ) {
            return null;
        }

        UnderwritingModelExternal underwritingModelExternal = new UnderwritingModelExternal();

        underwritingModelExternal.setBedroom( underwritingModelEntityPropertyInfoBedrooms( underwritingModelEntity ) );
        underwritingModelExternal.setBathroom( underwritingModelEntityPropertyInfoBathrooms( underwritingModelEntity ) );
        underwritingModelExternal.setId( underwritingModelEntity.getId() );
        underwritingModelExternal.setUuid( underwritingModelEntity.getUuid() );
        underwritingModelExternal.setPropertyUuid( underwritingModelEntity.getPropertyUuid() );
        underwritingModelExternal.setDealUuid( underwritingModelEntity.getDealUuid() );
        underwritingModelExternal.setSalesDate( underwritingModelEntity.getSalesDate() );
        underwritingModelExternal.setMinYield( underwritingModelEntity.getMinYield() );
        underwritingModelExternal.setDdRehab( underwritingModelEntity.getDdRehab() );
        underwritingModelExternal.setAskingPrice( underwritingModelEntity.getAskingPrice() );
        underwritingModelExternal.setOffer( underwritingModelEntity.getOffer() );
        underwritingModelExternal.setStrategy( underwritingModelEntity.getStrategy() );
        underwritingModelExternal.setCreatedAt( underwritingModelEntity.getCreatedAt() );
        underwritingModelExternal.setUpdatedAt( underwritingModelEntity.getUpdatedAt() );
        underwritingModelExternal.setCreatedBy( underwritingModelEntity.getCreatedBy() );

        underwritingModelExternal.setProjectedSalesPrice( underwritingModelEntity.getStrategyDependentInitialDataMap().get(underwritingModelEntity.getStrategy()).getProjectedSalesPrice() );

        return underwritingModelExternal;
    }

    protected StrategyDependentResultingData strategyDependentResultingDataEntityToStrategyDependentResultingData(StrategyDependentResultingDataEntity strategyDependentResultingDataEntity) {
        if ( strategyDependentResultingDataEntity == null ) {
            return null;
        }

        StrategyDependentResultingData strategyDependentResultingData = new StrategyDependentResultingData();

        strategyDependentResultingData.setTargetYield( strategyDependentResultingDataEntity.getTargetYield() );
        strategyDependentResultingData.setLoanToCostRatio( strategyDependentResultingDataEntity.getLoanToCostRatio() );
        strategyDependentResultingData.setInternalRateOfReturn( strategyDependentResultingDataEntity.getInternalRateOfReturn() );

        return strategyDependentResultingData;
    }

    protected Map<RenovationType, StrategyDependentResultingData> renovationTypeStrategyDependentResultingDataEntityMapToRenovationTypeStrategyDependentResultingDataMap(Map<RenovationType, StrategyDependentResultingDataEntity> map) {
        if ( map == null ) {
            return null;
        }

        Map<RenovationType, StrategyDependentResultingData> map1 = new HashMap<RenovationType, StrategyDependentResultingData>( Math.max( (int) ( map.size() / .75f ) + 1, 16 ) );

        for ( java.util.Map.Entry<RenovationType, StrategyDependentResultingDataEntity> entry : map.entrySet() ) {
            RenovationType key = entry.getKey();
            StrategyDependentResultingData value = strategyDependentResultingDataEntityToStrategyDependentResultingData( entry.getValue() );
            map1.put( key, value );
        }

        return map1;
    }

    protected StrategyDependentInitialData strategyDependentInitialDataEntityToStrategyDependentInitialData(StrategyDependentInitialDataEntity strategyDependentInitialDataEntity) {
        if ( strategyDependentInitialDataEntity == null ) {
            return null;
        }

        StrategyDependentInitialData strategyDependentInitialData = new StrategyDependentInitialData();

        strategyDependentInitialData.setRent( strategyDependentInitialDataEntity.getRent() );
        strategyDependentInitialData.setRehabOverride( strategyDependentInitialDataEntity.getRehabOverride() );
        strategyDependentInitialData.setUwRehab( strategyDependentInitialDataEntity.getUwRehab() );
        strategyDependentInitialData.setProjectedSalesPrice( strategyDependentInitialDataEntity.getProjectedSalesPrice() );

        return strategyDependentInitialData;
    }

    protected Map<RenovationType, StrategyDependentInitialData> renovationTypeStrategyDependentInitialDataEntityMapToRenovationTypeStrategyDependentInitialDataMap(Map<RenovationType, StrategyDependentInitialDataEntity> map) {
        if ( map == null ) {
            return null;
        }

        Map<RenovationType, StrategyDependentInitialData> map1 = new HashMap<RenovationType, StrategyDependentInitialData>( Math.max( (int) ( map.size() / .75f ) + 1, 16 ) );

        for ( java.util.Map.Entry<RenovationType, StrategyDependentInitialDataEntity> entry : map.entrySet() ) {
            RenovationType key = entry.getKey();
            StrategyDependentInitialData value = strategyDependentInitialDataEntityToStrategyDependentInitialData( entry.getValue() );
            map1.put( key, value );
        }

        return map1;
    }

    protected PropertyAnalogue propertyAnalogueEntityToPropertyAnalogue(PropertyAnalogueEntity propertyAnalogueEntity) {
        if ( propertyAnalogueEntity == null ) {
            return null;
        }

        PropertyAnalogue propertyAnalogue = new PropertyAnalogue();

        propertyAnalogue.setUuid( propertyAnalogueEntity.getUuid() );
        propertyAnalogue.setPropertyUuid( propertyAnalogueEntity.getPropertyUuid() );
        propertyAnalogue.setAddress( propertyAnalogueEntity.getAddress() );
        propertyAnalogue.setCity( propertyAnalogueEntity.getCity() );
        propertyAnalogue.setState( propertyAnalogueEntity.getState() );
        propertyAnalogue.setZip( propertyAnalogueEntity.getZip() );
        propertyAnalogue.setBedrooms( propertyAnalogueEntity.getBedrooms() );
        propertyAnalogue.setBathrooms( propertyAnalogueEntity.getBathrooms() );
        propertyAnalogue.setHalfBathrooms( propertyAnalogueEntity.getHalfBathrooms() );
        propertyAnalogue.setAreaSqFt( propertyAnalogueEntity.getAreaSqFt() );
        propertyAnalogue.setYearBuilt( propertyAnalogueEntity.getYearBuilt() );
        propertyAnalogue.setPrice( BigDecimal.valueOf( propertyAnalogueEntity.getPrice() ) );
        propertyAnalogue.setRent( propertyAnalogueEntity.isRent() );
        propertyAnalogue.setLatitude( propertyAnalogueEntity.getLatitude() );
        propertyAnalogue.setLongitude( propertyAnalogueEntity.getLongitude() );
        propertyAnalogue.setDaysOnMarket( propertyAnalogueEntity.getDaysOnMarket() );
        propertyAnalogue.setCloseDate( propertyAnalogueEntity.getCloseDate() );
        propertyAnalogue.setDistance( propertyAnalogueEntity.getDistance() );
        propertyAnalogue.setRenovationTag( propertyAnalogueEntity.getRenovationTag() );
        propertyAnalogue.setCompsLabel( propertyAnalogueEntity.getCompsLabel() );
        propertyAnalogue.setGoogleUrl( propertyAnalogueEntity.getGoogleUrl() );
        propertyAnalogue.setZillowUrl( propertyAnalogueEntity.getZillowUrl() );
        propertyAnalogue.setMlsUrl( propertyAnalogueEntity.getMlsUrl() );

        return propertyAnalogue;
    }

    protected List<PropertyAnalogue> propertyAnalogueEntityListToPropertyAnalogueList(PropertyAnalogueEntityList propertyAnalogueEntityList) {
        if ( propertyAnalogueEntityList == null ) {
            return null;
        }

        List<PropertyAnalogue> list = new ArrayList<PropertyAnalogue>( propertyAnalogueEntityList.size() );
        for ( PropertyAnalogueEntity propertyAnalogueEntity : propertyAnalogueEntityList ) {
            list.add( propertyAnalogueEntityToPropertyAnalogue( propertyAnalogueEntity ) );
        }

        return list;
    }

    protected AnaloguesSelection analoguesSelectionEntityToAnaloguesSelection(AnaloguesSelectionEntity analoguesSelectionEntity) {
        if ( analoguesSelectionEntity == null ) {
            return null;
        }

        AnaloguesSelection analoguesSelection = new AnaloguesSelection();

        List<String> list = analoguesSelectionEntity.getSpec();
        if ( list != null ) {
            analoguesSelection.setSpec( new ArrayList<String>( list ) );
        }
        List<String> list1 = analoguesSelectionEntity.getStock();
        if ( list1 != null ) {
            analoguesSelection.setStock( new ArrayList<String>( list1 ) );
        }
        List<String> list2 = analoguesSelectionEntity.getAll();
        if ( list2 != null ) {
            analoguesSelection.setAll( new ArrayList<String>( list2 ) );
        }

        return analoguesSelection;
    }

    protected PropertyInfo propertyInfoEntityToPropertyInfo(PropertyInfoEntity propertyInfoEntity) {
        if ( propertyInfoEntity == null ) {
            return null;
        }

        PropertyInfo propertyInfo = new PropertyInfo();

        if ( propertyInfoEntity.getBedrooms() != null ) {
            propertyInfo.setBedrooms( propertyInfoEntity.getBedrooms() );
        }
        if ( propertyInfoEntity.getBathrooms() != null ) {
            propertyInfo.setBathrooms( propertyInfoEntity.getBathrooms() );
        }
        if ( propertyInfoEntity.getHalfBathrooms() != null ) {
            propertyInfo.setHalfBathrooms( propertyInfoEntity.getHalfBathrooms() );
        }
        if ( propertyInfoEntity.getAreaSqFt() != null ) {
            propertyInfo.setAreaSqFt( propertyInfoEntity.getAreaSqFt() );
        }
        if ( propertyInfoEntity.getYearBuilt() != null ) {
            propertyInfo.setYearBuilt( propertyInfoEntity.getYearBuilt() );
        }

        return propertyInfo;
    }

    protected StatusEvent<UnderwritingModelStatus> statusEventEntityToUnderwritingModelStatusStatusEvent(StatusEventEntity statusEventEntity) {
        if ( statusEventEntity == null ) {
            return null;
        }

        String comment = null;
        String eventAddresseeUuid = null;
        String emittedByUuid = null;
        UnderwritingModelStatus status = null;

        comment = statusEventEntity.getComment();
        eventAddresseeUuid = statusEventEntity.getEventAddresseeUuid();
        emittedByUuid = statusEventEntity.getEmittedByUuid();
        status = statusToUnderwritingModelStatus( statusEventEntity.getStatus() );

        StatusEvent<UnderwritingModelStatus> statusEvent = new StatusEvent<UnderwritingModelStatus>( status, emittedByUuid, comment, eventAddresseeUuid );

        statusEvent.setEmittedAt( statusEventEntity.getEmittedAt() );

        return statusEvent;
    }

    protected List<StatusEvent<UnderwritingModelStatus>> statusEventEntityListToStatusEventList(StatusEventEntityList statusEventEntityList) {
        if ( statusEventEntityList == null ) {
            return null;
        }

        List<StatusEvent<UnderwritingModelStatus>> list = new ArrayList<StatusEvent<UnderwritingModelStatus>>( statusEventEntityList.size() );
        for ( StatusEventEntity statusEventEntity : statusEventEntityList ) {
            list.add( statusEventEntityToUnderwritingModelStatusStatusEvent( statusEventEntity ) );
        }

        return list;
    }

    protected PropertyInfoEntity propertyInfoToPropertyInfoEntity(PropertyInfo propertyInfo) {
        if ( propertyInfo == null ) {
            return null;
        }

        PropertyInfoEntity propertyInfoEntity = new PropertyInfoEntity();

        propertyInfoEntity.setBedrooms( propertyInfo.getBedrooms() );
        propertyInfoEntity.setBathrooms( propertyInfo.getBathrooms() );
        propertyInfoEntity.setHalfBathrooms( propertyInfo.getHalfBathrooms() );
        propertyInfoEntity.setAreaSqFt( propertyInfo.getAreaSqFt() );
        propertyInfoEntity.setYearBuilt( propertyInfo.getYearBuilt() );

        return propertyInfoEntity;
    }

    protected StrategyDependentResultingDataEntity strategyDependentResultingDataToStrategyDependentResultingDataEntity(StrategyDependentResultingData strategyDependentResultingData) {
        if ( strategyDependentResultingData == null ) {
            return null;
        }

        StrategyDependentResultingDataEntity strategyDependentResultingDataEntity = new StrategyDependentResultingDataEntity();

        strategyDependentResultingDataEntity.setTargetYield( strategyDependentResultingData.getTargetYield() );
        strategyDependentResultingDataEntity.setLoanToCostRatio( strategyDependentResultingData.getLoanToCostRatio() );
        strategyDependentResultingDataEntity.setInternalRateOfReturn( strategyDependentResultingData.getInternalRateOfReturn() );

        return strategyDependentResultingDataEntity;
    }

    protected Map<RenovationType, StrategyDependentResultingDataEntity> renovationTypeStrategyDependentResultingDataMapToRenovationTypeStrategyDependentResultingDataEntityMap(Map<RenovationType, StrategyDependentResultingData> map) {
        if ( map == null ) {
            return null;
        }

        Map<RenovationType, StrategyDependentResultingDataEntity> map1 = new HashMap<RenovationType, StrategyDependentResultingDataEntity>( Math.max( (int) ( map.size() / .75f ) + 1, 16 ) );

        for ( java.util.Map.Entry<RenovationType, StrategyDependentResultingData> entry : map.entrySet() ) {
            RenovationType key = entry.getKey();
            StrategyDependentResultingDataEntity value = strategyDependentResultingDataToStrategyDependentResultingDataEntity( entry.getValue() );
            map1.put( key, value );
        }

        return map1;
    }

    protected StrategyDependentInitialDataEntity strategyDependentInitialDataToStrategyDependentInitialDataEntity(StrategyDependentInitialData strategyDependentInitialData) {
        if ( strategyDependentInitialData == null ) {
            return null;
        }

        StrategyDependentInitialDataEntity strategyDependentInitialDataEntity = new StrategyDependentInitialDataEntity();

        strategyDependentInitialDataEntity.setRent( strategyDependentInitialData.getRent() );
        strategyDependentInitialDataEntity.setRehabOverride( strategyDependentInitialData.getRehabOverride() );
        strategyDependentInitialDataEntity.setUwRehab( strategyDependentInitialData.getUwRehab() );
        strategyDependentInitialDataEntity.setProjectedSalesPrice( strategyDependentInitialData.getProjectedSalesPrice() );

        return strategyDependentInitialDataEntity;
    }

    protected Map<RenovationType, StrategyDependentInitialDataEntity> renovationTypeStrategyDependentInitialDataMapToRenovationTypeStrategyDependentInitialDataEntityMap(Map<RenovationType, StrategyDependentInitialData> map) {
        if ( map == null ) {
            return null;
        }

        Map<RenovationType, StrategyDependentInitialDataEntity> map1 = new HashMap<RenovationType, StrategyDependentInitialDataEntity>( Math.max( (int) ( map.size() / .75f ) + 1, 16 ) );

        for ( java.util.Map.Entry<RenovationType, StrategyDependentInitialData> entry : map.entrySet() ) {
            RenovationType key = entry.getKey();
            StrategyDependentInitialDataEntity value = strategyDependentInitialDataToStrategyDependentInitialDataEntity( entry.getValue() );
            map1.put( key, value );
        }

        return map1;
    }

    protected PropertyAnalogueEntity propertyAnalogueToPropertyAnalogueEntity(PropertyAnalogue propertyAnalogue) {
        if ( propertyAnalogue == null ) {
            return null;
        }

        PropertyAnalogueEntity propertyAnalogueEntity = new PropertyAnalogueEntity();

        propertyAnalogueEntity.setUuid( propertyAnalogue.getUuid() );
        propertyAnalogueEntity.setPropertyUuid( propertyAnalogue.getPropertyUuid() );
        propertyAnalogueEntity.setAddress( propertyAnalogue.getAddress() );
        propertyAnalogueEntity.setCity( propertyAnalogue.getCity() );
        propertyAnalogueEntity.setState( propertyAnalogue.getState() );
        propertyAnalogueEntity.setZip( propertyAnalogue.getZip() );
        propertyAnalogueEntity.setBedrooms( propertyAnalogue.getBedrooms() );
        propertyAnalogueEntity.setBathrooms( propertyAnalogue.getBathrooms() );
        propertyAnalogueEntity.setHalfBathrooms( propertyAnalogue.getHalfBathrooms() );
        propertyAnalogueEntity.setAreaSqFt( propertyAnalogue.getAreaSqFt() );
        propertyAnalogueEntity.setYearBuilt( propertyAnalogue.getYearBuilt() );
        if ( propertyAnalogue.getPrice() != null ) {
            propertyAnalogueEntity.setPrice( propertyAnalogue.getPrice().floatValue() );
        }
        propertyAnalogueEntity.setRent( propertyAnalogue.isRent() );
        propertyAnalogueEntity.setLatitude( propertyAnalogue.getLatitude() );
        propertyAnalogueEntity.setLongitude( propertyAnalogue.getLongitude() );
        propertyAnalogueEntity.setDaysOnMarket( propertyAnalogue.getDaysOnMarket() );
        propertyAnalogueEntity.setCloseDate( propertyAnalogue.getCloseDate() );
        propertyAnalogueEntity.setDistance( propertyAnalogue.getDistance() );
        propertyAnalogueEntity.setRenovationTag( propertyAnalogue.getRenovationTag() );
        propertyAnalogueEntity.setCompsLabel( propertyAnalogue.getCompsLabel() );
        propertyAnalogueEntity.setGoogleUrl( propertyAnalogue.getGoogleUrl() );
        propertyAnalogueEntity.setZillowUrl( propertyAnalogue.getZillowUrl() );
        propertyAnalogueEntity.setMlsUrl( propertyAnalogue.getMlsUrl() );

        return propertyAnalogueEntity;
    }

    protected PropertyAnalogueEntityList propertyAnalogueListToPropertyAnalogueEntityList(List<PropertyAnalogue> list) {
        if ( list == null ) {
            return null;
        }

        PropertyAnalogueEntityList propertyAnalogueEntityList = new PropertyAnalogueEntityList();
        for ( PropertyAnalogue propertyAnalogue : list ) {
            propertyAnalogueEntityList.add( propertyAnalogueToPropertyAnalogueEntity( propertyAnalogue ) );
        }

        return propertyAnalogueEntityList;
    }

    protected StatusEventEntity underwritingModelStatusStatusEventToStatusEventEntity(StatusEvent<UnderwritingModelStatus> statusEvent) {
        if ( statusEvent == null ) {
            return null;
        }

        String comment = null;
        String eventAddresseeUuid = null;
        Instant emittedAt = null;
        String emittedByUuid = null;
        String status = null;

        comment = statusEvent.getComment();
        eventAddresseeUuid = statusEvent.getEventAddresseeUuid();
        emittedAt = statusEvent.getEmittedAt();
        emittedByUuid = statusEvent.getEmittedByUuid();
        if ( statusEvent.getStatus() != null ) {
            status = statusEvent.getStatus().name();
        }

        String className = null;

        StatusEventEntity statusEventEntity = new StatusEventEntity( comment, eventAddresseeUuid, emittedAt, emittedByUuid, className, status );

        return statusEventEntity;
    }

    protected StatusEventEntityList statusEventListToStatusEventEntityList(List<StatusEvent<UnderwritingModelStatus>> list) {
        if ( list == null ) {
            return null;
        }

        StatusEventEntityList statusEventEntityList = new StatusEventEntityList();
        for ( StatusEvent<UnderwritingModelStatus> statusEvent : list ) {
            statusEventEntityList.add( underwritingModelStatusStatusEventToStatusEventEntity( statusEvent ) );
        }

        return statusEventEntityList;
    }

    protected FileObjectEntity fileObjectToFileObjectEntity(FileObject fileObject) {
        if ( fileObject == null ) {
            return null;
        }

        FileObjectEntity fileObjectEntity = new FileObjectEntity();

        fileObjectEntity.setId( fileObject.getId() );
        fileObjectEntity.setFileName( fileObject.getFileName() );
        fileObjectEntity.setFileUrl( fileObject.getFileUrl() );
        fileObjectEntity.setTag( fileObject.getTag() );
        fileObjectEntity.setOrigin( fileObject.getOrigin() );
        fileObjectEntity.setMediaCategory( fileObject.getMediaCategory() );
        fileObjectEntity.setMimeType( fileObject.getMimeType() );
        fileObjectEntity.setCreatedAt( fileObject.getCreatedAt() );

        return fileObjectEntity;
    }

    protected FileObjectEntityList fileObjectListToFileObjectEntityList(List<FileObject> list) {
        if ( list == null ) {
            return null;
        }

        FileObjectEntityList fileObjectEntityList = new FileObjectEntityList();
        for ( FileObject fileObject : list ) {
            fileObjectEntityList.add( fileObjectToFileObjectEntity( fileObject ) );
        }

        return fileObjectEntityList;
    }

    protected FileObject fileObjectEntityToFileObject(FileObjectEntity fileObjectEntity) {
        if ( fileObjectEntity == null ) {
            return null;
        }

        FileObject fileObject = new FileObject();

        fileObject.setId( fileObjectEntity.getId() );
        fileObject.setFileName( fileObjectEntity.getFileName() );
        fileObject.setFileUrl( fileObjectEntity.getFileUrl() );
        fileObject.setTag( fileObjectEntity.getTag() );
        fileObject.setOrigin( fileObjectEntity.getOrigin() );
        fileObject.setMediaCategory( fileObjectEntity.getMediaCategory() );
        fileObject.setMimeType( fileObjectEntity.getMimeType() );
        fileObject.setCreatedAt( fileObjectEntity.getCreatedAt() );

        return fileObject;
    }

    protected List<FileObject> fileObjectEntityListToFileObjectList(FileObjectEntityList fileObjectEntityList) {
        if ( fileObjectEntityList == null ) {
            return null;
        }

        List<FileObject> list = new ArrayList<FileObject>( fileObjectEntityList.size() );
        for ( FileObjectEntity fileObjectEntity : fileObjectEntityList ) {
            list.add( fileObjectEntityToFileObject( fileObjectEntity ) );
        }

        return list;
    }

    private Integer underwritingModelEntityPropertyInfoBedrooms(UnderwritingModelEntity underwritingModelEntity) {
        if ( underwritingModelEntity == null ) {
            return null;
        }
        PropertyInfoEntity propertyInfo = underwritingModelEntity.getPropertyInfo();
        if ( propertyInfo == null ) {
            return null;
        }
        Integer bedrooms = propertyInfo.getBedrooms();
        if ( bedrooms == null ) {
            return null;
        }
        return bedrooms;
    }

    private Integer underwritingModelEntityPropertyInfoBathrooms(UnderwritingModelEntity underwritingModelEntity) {
        if ( underwritingModelEntity == null ) {
            return null;
        }
        PropertyInfoEntity propertyInfo = underwritingModelEntity.getPropertyInfo();
        if ( propertyInfo == null ) {
            return null;
        }
        Integer bathrooms = propertyInfo.getBathrooms();
        if ( bathrooms == null ) {
            return null;
        }
        return bathrooms;
    }
}
