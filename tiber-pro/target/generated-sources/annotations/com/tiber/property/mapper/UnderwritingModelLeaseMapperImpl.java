package com.tiber.property.mapper;

import com.tiber.property.domain.UnderwritingModelLease;
import com.tiber.property.entity.UnderwritingModelLeaseEntity;
import javax.annotation.processing.Generated;
import org.springframework.stereotype.Component;

@Generated(
    value = "org.mapstruct.ap.MappingProcessor",
    date = "2022-04-06T16:49:03+0300",
    comments = "version: 1.4.2.Final, compiler: javac, environment: Java 14 (Oracle Corporation)"
)
@Component
public class UnderwritingModelLeaseMapperImpl implements UnderwritingModelLeaseMapper {

    @Override
    public UnderwritingModelLease toDomain(UnderwritingModelLeaseEntity source) {
        if ( source == null ) {
            return null;
        }

        UnderwritingModelLease underwritingModelLease = new UnderwritingModelLease();

        underwritingModelLease.setLease( source.isLease() );
        underwritingModelLease.setLeaseAmount( source.getLeaseAmount() );
        underwritingModelLease.setLeaseExpiration( source.getLeaseExpiration() );
        underwritingModelLease.setAllInBasis( source.getAllInBasis() );
        underwritingModelLease.setInPlacePty( source.getInPlacePty() );

        return underwritingModelLease;
    }

    @Override
    public UnderwritingModelLeaseEntity toEntity(UnderwritingModelLease source) {
        if ( source == null ) {
            return null;
        }

        UnderwritingModelLeaseEntity underwritingModelLeaseEntity = new UnderwritingModelLeaseEntity();

        underwritingModelLeaseEntity.setLease( source.isLease() );
        underwritingModelLeaseEntity.setLeaseAmount( source.getLeaseAmount() );
        underwritingModelLeaseEntity.setLeaseExpiration( source.getLeaseExpiration() );
        underwritingModelLeaseEntity.setAllInBasis( source.getAllInBasis() );
        underwritingModelLeaseEntity.setInPlacePty( source.getInPlacePty() );

        return underwritingModelLeaseEntity;
    }
}
