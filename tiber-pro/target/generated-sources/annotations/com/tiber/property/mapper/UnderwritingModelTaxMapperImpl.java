package com.tiber.property.mapper;

import com.tiber.property.domain.UnderwritingModelTax;
import com.tiber.property.entity.UnderwritingModelTaxEntity;
import javax.annotation.processing.Generated;
import org.springframework.stereotype.Component;

@Generated(
    value = "org.mapstruct.ap.MappingProcessor",
    date = "2022-04-06T16:49:03+0300",
    comments = "version: 1.4.2.Final, compiler: javac, environment: Java 14 (Oracle Corporation)"
)
@Component
public class UnderwritingModelTaxMapperImpl implements UnderwritingModelTaxMapper {

    @Override
    public UnderwritingModelTax toDomain(UnderwritingModelTaxEntity source) {
        if ( source == null ) {
            return null;
        }

        UnderwritingModelTax underwritingModelTax = new UnderwritingModelTax();

        underwritingModelTax.setAssessedValue( source.getAssessedValue() );
        underwritingModelTax.setMileageRate( source.getMileageRate() );
        underwritingModelTax.setSpecialAssessment( source.getSpecialAssessment() );

        return underwritingModelTax;
    }

    @Override
    public UnderwritingModelTaxEntity toEntity(UnderwritingModelTax source) {
        if ( source == null ) {
            return null;
        }

        UnderwritingModelTaxEntity underwritingModelTaxEntity = new UnderwritingModelTaxEntity();

        underwritingModelTaxEntity.setAssessedValue( source.getAssessedValue() );
        underwritingModelTaxEntity.setMileageRate( source.getMileageRate() );
        underwritingModelTaxEntity.setSpecialAssessment( source.getSpecialAssessment() );

        return underwritingModelTaxEntity;
    }
}
