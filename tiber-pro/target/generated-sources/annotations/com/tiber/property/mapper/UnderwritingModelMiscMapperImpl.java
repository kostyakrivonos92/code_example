package com.tiber.property.mapper;

import com.tiber.property.domain.UnderwritingModelMisc;
import com.tiber.property.entity.UnderwritingModelMiscEntity;
import javax.annotation.processing.Generated;
import org.springframework.stereotype.Component;

@Generated(
    value = "org.mapstruct.ap.MappingProcessor",
    date = "2022-04-06T16:49:03+0300",
    comments = "version: 1.4.2.Final, compiler: javac, environment: Java 14 (Oracle Corporation)"
)
@Component
public class UnderwritingModelMiscMapperImpl implements UnderwritingModelMiscMapper {

    @Override
    public UnderwritingModelMisc toDomain(UnderwritingModelMiscEntity source) {
        if ( source == null ) {
            return null;
        }

        UnderwritingModelMisc underwritingModelMisc = new UnderwritingModelMisc();

        underwritingModelMisc.setStrategy( source.getStrategy() );
        underwritingModelMisc.setHoa( source.getHoa() );
        underwritingModelMisc.setLeaseUpTime( source.getLeaseUpTime() );
        underwritingModelMisc.setConstructionQuality( source.getConstructionQuality() );

        return underwritingModelMisc;
    }

    @Override
    public UnderwritingModelMiscEntity toEntity(UnderwritingModelMisc source) {
        if ( source == null ) {
            return null;
        }

        UnderwritingModelMiscEntity underwritingModelMiscEntity = new UnderwritingModelMiscEntity();

        underwritingModelMiscEntity.setStrategy( source.getStrategy() );
        underwritingModelMiscEntity.setLeaseUpTime( source.getLeaseUpTime() );
        underwritingModelMiscEntity.setConstructionQuality( source.getConstructionQuality() );
        underwritingModelMiscEntity.setHoa( source.getHoa() );

        return underwritingModelMiscEntity;
    }
}
