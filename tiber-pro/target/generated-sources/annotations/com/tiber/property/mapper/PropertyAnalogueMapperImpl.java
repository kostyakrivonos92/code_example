package com.tiber.property.mapper;

import com.tiber.property.domain.PropertyAnalogue;
import com.tiber.shared.propertylayer.domain.Deal;
import com.tiber.shared.propertylayer.domain.PropertyOnMarket;
import com.tiber.shared.util.AnalogueRenovationTag;
import java.math.BigDecimal;
import java.time.LocalDate;
import javax.annotation.processing.Generated;
import org.springframework.stereotype.Component;

@Generated(
    value = "org.mapstruct.ap.MappingProcessor",
    date = "2022-04-06T16:49:03+0300",
    comments = "version: 1.4.2.Final, compiler: javac, environment: Java 14 (Oracle Corporation)"
)
@Component
public class PropertyAnalogueMapperImpl implements PropertyAnalogueMapper {

    @Override
    public PropertyAnalogue propertyAnalogueFromPropertyOnMarket(PropertyOnMarket propertyOnMarket) {
        if ( propertyOnMarket == null ) {
            return null;
        }

        PropertyAnalogue propertyAnalogue = new PropertyAnalogue();

        propertyAnalogue.setCloseDate( propertyOnMarketLatestDealCloseDate( propertyOnMarket ) );
        Integer price = propertyOnMarketLatestDealPrice( propertyOnMarket );
        if ( price != null ) {
            propertyAnalogue.setPrice( BigDecimal.valueOf( price ) );
        }
        propertyAnalogue.setRenovationTag( propertyOnMarketLatestDealRenovationTag( propertyOnMarket ) );
        propertyAnalogue.setUuid( propertyOnMarketLatestDealUuid( propertyOnMarket ) );
        propertyAnalogue.setPropertyUuid( propertyOnMarket.getUuid() );
        propertyAnalogue.setAddress( propertyOnMarket.getAddress() );
        propertyAnalogue.setCity( propertyOnMarket.getCity() );
        propertyAnalogue.setState( propertyOnMarket.getState() );
        propertyAnalogue.setZip( propertyOnMarket.getZip() );
        if ( propertyOnMarket.getBedrooms() != null ) {
            propertyAnalogue.setBedrooms( propertyOnMarket.getBedrooms() );
        }
        if ( propertyOnMarket.getBathrooms() != null ) {
            propertyAnalogue.setBathrooms( propertyOnMarket.getBathrooms() );
        }
        if ( propertyOnMarket.getHalfBathrooms() != null ) {
            propertyAnalogue.setHalfBathrooms( propertyOnMarket.getHalfBathrooms() );
        }
        if ( propertyOnMarket.getAreaSqFt() != null ) {
            propertyAnalogue.setAreaSqFt( propertyOnMarket.getAreaSqFt() );
        }
        if ( propertyOnMarket.getYearBuilt() != null ) {
            propertyAnalogue.setYearBuilt( propertyOnMarket.getYearBuilt() );
        }
        propertyAnalogue.setLatitude( propertyOnMarket.getLatitude() );
        propertyAnalogue.setLongitude( propertyOnMarket.getLongitude() );

        return propertyAnalogue;
    }

    private LocalDate propertyOnMarketLatestDealCloseDate(PropertyOnMarket propertyOnMarket) {
        if ( propertyOnMarket == null ) {
            return null;
        }
        Deal latestDeal = propertyOnMarket.getLatestDeal();
        if ( latestDeal == null ) {
            return null;
        }
        LocalDate closeDate = latestDeal.getCloseDate();
        if ( closeDate == null ) {
            return null;
        }
        return closeDate;
    }

    private Integer propertyOnMarketLatestDealPrice(PropertyOnMarket propertyOnMarket) {
        if ( propertyOnMarket == null ) {
            return null;
        }
        Deal latestDeal = propertyOnMarket.getLatestDeal();
        if ( latestDeal == null ) {
            return null;
        }
        Integer price = latestDeal.getPrice();
        if ( price == null ) {
            return null;
        }
        return price;
    }

    private AnalogueRenovationTag propertyOnMarketLatestDealRenovationTag(PropertyOnMarket propertyOnMarket) {
        if ( propertyOnMarket == null ) {
            return null;
        }
        Deal latestDeal = propertyOnMarket.getLatestDeal();
        if ( latestDeal == null ) {
            return null;
        }
        AnalogueRenovationTag renovationTag = latestDeal.getRenovationTag();
        if ( renovationTag == null ) {
            return null;
        }
        return renovationTag;
    }

    private String propertyOnMarketLatestDealUuid(PropertyOnMarket propertyOnMarket) {
        if ( propertyOnMarket == null ) {
            return null;
        }
        Deal latestDeal = propertyOnMarket.getLatestDeal();
        if ( latestDeal == null ) {
            return null;
        }
        String uuid = latestDeal.getUuid();
        if ( uuid == null ) {
            return null;
        }
        return uuid;
    }
}
