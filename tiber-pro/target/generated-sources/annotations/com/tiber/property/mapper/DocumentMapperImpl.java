package com.tiber.property.mapper;

import com.tiber.property.domain.Document;
import com.tiber.property.entity.DocumentEntity;
import java.util.ArrayList;
import java.util.List;
import javax.annotation.processing.Generated;
import org.springframework.stereotype.Component;

@Generated(
    value = "org.mapstruct.ap.MappingProcessor",
    date = "2022-04-06T16:49:03+0300",
    comments = "version: 1.4.2.Final, compiler: javac, environment: Java 14 (Oracle Corporation)"
)
@Component
public class DocumentMapperImpl implements DocumentMapper {

    @Override
    public Document fromEntityToDo(DocumentEntity documentEntity) {
        if ( documentEntity == null ) {
            return null;
        }

        Document document = new Document();

        if ( documentEntity.getId() != null ) {
            document.setId( documentEntity.getId() );
        }
        document.setFileName( documentEntity.getFileName() );
        document.setFileUrl( documentEntity.getFileUrl() );
        document.setCreatedAt( documentEntity.getCreatedAt() );
        document.setCreatedBy( documentEntity.getCreatedBy() );

        return document;
    }

    @Override
    public DocumentEntity fromDoToEntity(Document document) {
        if ( document == null ) {
            return null;
        }

        DocumentEntity documentEntity = new DocumentEntity();

        documentEntity.setId( document.getId() );
        documentEntity.setCreatedAt( document.getCreatedAt() );
        documentEntity.setFileName( document.getFileName() );
        documentEntity.setFileUrl( document.getFileUrl() );
        documentEntity.setCreatedBy( document.getCreatedBy() );

        return documentEntity;
    }

    @Override
    public List<Document> fromEntityListToDoList(List<DocumentEntity> documentEntities) {
        if ( documentEntities == null ) {
            return null;
        }

        List<Document> list = new ArrayList<Document>( documentEntities.size() );
        for ( DocumentEntity documentEntity : documentEntities ) {
            list.add( fromEntityToDo( documentEntity ) );
        }

        return list;
    }
}
