package com.tiber.property.status;

import com.fasterxml.jackson.annotation.JsonCreator;

public enum UnderwritingModelStatus {
    NEW,
    IN_PROGRESS,
    REVIEW,
    UNDERWRITING_RESULT_OFFER,
    UNDERWRITING_RESULT_DO_NOT_BUY,
    UNDERWRITING_RESULT_PASS,
    UNDERWRITING_RESULT_PRICE_TO_YIELD,
    FINAL_GOOD_TO_GO,
    CLOSING,
    UNKNOWN;

    @JsonCreator
    public static UnderwritingModelStatus get(String string) {
        if (string == null) {
            return UNKNOWN;
        }
        for (UnderwritingModelStatus value : UnderwritingModelStatus.values()) {
            if (value.name().equalsIgnoreCase(string)) {
                return value;
            }
        }
        return UNKNOWN;
    }
}
