package com.tiber.property.repository;

import com.tiber.property.entity.UnderwritingNoteEntity;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface UnderwritingNoteRepository extends JpaRepository<UnderwritingNoteEntity, Integer> {
}
