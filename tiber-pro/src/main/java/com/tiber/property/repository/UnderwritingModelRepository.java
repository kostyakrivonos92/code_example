package com.tiber.property.repository;

import com.tiber.property.entity.UnderwritingModelEntity;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.Collection;
import java.util.List;
import java.util.Optional;

@Repository
public interface UnderwritingModelRepository extends JpaRepository<UnderwritingModelEntity, Long> {

    List<UnderwritingModelEntity> findByPropertyUuid(String propertyUuid);

    Optional<UnderwritingModelEntity> findByUuid(String uuid);

    Optional<UnderwritingModelEntity> findByDealUuid(String dealUuid);

    Collection<UnderwritingModelEntity> findAllByPropertyUuidIn(Collection<String> propertyUuidCollection);

    boolean existsByPropertyUuid(String propertyUuid);
}
