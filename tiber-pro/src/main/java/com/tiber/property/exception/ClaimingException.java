package com.tiber.property.exception;

import com.tiber.common.exception.BadOperationException;

public class ClaimingException extends BadOperationException {

    public ClaimingException(String propertyUuid, String responsibleAnalystUuid, String claimedByAnalystUuid) {
        super(String.format("Error claiming deal for property %s to user %s by user %s",
                propertyUuid, responsibleAnalystUuid, claimedByAnalystUuid));
        this.addContextValue("Property UUID", propertyUuid)
                .addContextValue("Responsible User", responsibleAnalystUuid)
                .addContextValue("Claimed By", claimedByAnalystUuid);
    }
}
