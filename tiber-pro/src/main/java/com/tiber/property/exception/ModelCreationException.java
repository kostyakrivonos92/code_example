package com.tiber.property.exception;

import com.tiber.common.exception.BadOperationException;

public class ModelCreationException extends BadOperationException {

    public ModelCreationException(String message) {
        super(message);
    }

    public ModelCreationException(String message, Throwable cause) {
        super(message, cause);
    }
}
