package com.tiber.property.exception;

import com.tiber.common.exception.NotFoundException;

public class UnderwritingModelNotFoundException extends NotFoundException  {
    public UnderwritingModelNotFoundException(String uuid) {
        super("Underwriting model not found");
        this.addContextValue("UUID", uuid);
    }

    public UnderwritingModelNotFoundException(String message, Throwable cause) {
        super(message, cause);
    }
}
