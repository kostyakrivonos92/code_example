package com.tiber.property.domain;

import lombok.Data;

import java.math.BigDecimal;

@Data
public class StrategyDependentInitialData {

    private BigDecimal rent;

    private BigDecimal rehabOverride;

    private BigDecimal uwRehab;

    private BigDecimal projectedSalesPrice;
}
