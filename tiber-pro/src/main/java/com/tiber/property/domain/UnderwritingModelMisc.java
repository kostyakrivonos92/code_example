package com.tiber.property.domain;

import com.tiber.shared.util.ConstructionQuality;
import com.tiber.shared.util.RenovationType;
import lombok.Data;

@Data
public class UnderwritingModelMisc {

    private RenovationType strategy;
    private String hoa;
    private int leaseUpTime;
    private ConstructionQuality constructionQuality;

}
