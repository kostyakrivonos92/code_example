package com.tiber.property.domain;

import com.tiber.shared.util.AnalogueRenovationTag;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.math.BigDecimal;
import java.time.LocalDate;
import java.util.Objects;

@Data
@NoArgsConstructor
public class PropertyAnalogue {

    private String uuid;

    private String propertyUuid;

    private String address;

    private String city;

    private String state;

    private String zip;

    private int bedrooms;

    private int bathrooms;

    private int halfBathrooms;

    private int areaSqFt;

    private int yearBuilt;

    private BigDecimal price;

    private boolean isRent;

    private double latitude;

    private double longitude;

    private int daysOnMarket;

    private LocalDate closeDate;

    private float distance;

    private AnalogueRenovationTag renovationTag;

    private String compsLabel;

    private String googleUrl;

    private String zillowUrl;

    private String mlsUrl;

    public PropertyAnalogue(DealSummary dealSummary) {
        this.uuid = dealSummary.getUuid();
        this.propertyUuid = dealSummary.getPropertyUuid();
        this.address = dealSummary.getAddress();
        this.city = dealSummary.getCity();
        this.state = dealSummary.getState();
        this.zip = dealSummary.getZip();
        this.bedrooms = dealSummary.getBedrooms();
        this.bathrooms = dealSummary.getBathrooms();
        this.halfBathrooms = dealSummary.getHalfBathrooms();
        this.areaSqFt = dealSummary.getAreaSqFt();
        this.yearBuilt = dealSummary.getYearBuilt();
        this.price = BigDecimal.valueOf(dealSummary.getPrice());
        this.latitude = dealSummary.getLatitude();
        this.longitude = dealSummary.getLongitude();
        this.renovationTag = dealSummary.getRenovationTag();
        this.closeDate = dealSummary.getCloseDate();
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof PropertyAnalogue)) return false;
        PropertyAnalogue that = (PropertyAnalogue) o;
        return uuid != null && that.uuid != null && uuid.equals(that.uuid);
    }

    @Override
    public int hashCode() {
        return Objects.hash(uuid);
    }
}
