package com.tiber.property.domain;

import lombok.Data;

import java.util.List;

@Data
public class AnaloguesSelection {

    private List<String> spec;

    private List<String> stock;

    private List<String> all;
}
