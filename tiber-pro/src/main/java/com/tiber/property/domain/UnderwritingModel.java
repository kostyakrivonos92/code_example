package com.tiber.property.domain;

import com.tiber.property.status.UnderwritingModelStatus;
import com.tiber.shared.inspection.domain.PropertyInfo;
import com.tiber.shared.status.domain.StatusEvent;
import com.tiber.shared.util.RenovationType;
import lombok.Data;

import java.math.BigDecimal;
import java.time.Instant;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

//TODO to be updated
@Data
public class UnderwritingModel {

    private Long id;

    private String uuid;

    private String propertyUuid;

    private String dealUuid;

    private Instant salesDate;

    private int minYield;

    private BigDecimal ddRehab;

    private BigDecimal valuationPsp;

    private BigDecimal askingPrice;

    private BigDecimal offer;

    private BigDecimal allInBasis;

    private BigDecimal priceToYield;

    private RenovationType strategy;

    private Map<RenovationType, StrategyDependentResultingData> strategyDependentResultingDataMap = new HashMap<>();

    private Map<RenovationType, StrategyDependentInitialData> strategyDependentInitialDataMap = new HashMap<>();

    private UnderwritingModelLease lease = new UnderwritingModelLease();

    private UnderwritingModelMisc misc = new UnderwritingModelMisc();

    private UnderwritingModelTax tax = new UnderwritingModelTax();

    private List<PropertyAnalogue> propertyAnalogues;

    private AnaloguesSelection analoguesSelection;

    private Instant createdAt;

    private Instant updatedAt;

    private PropertyInfo propertyInfo;

    private List<StatusEvent<UnderwritingModelStatus>> statusEventList;

    //Temporal getter for case when we don't have NEW-status event
    public List<StatusEvent<UnderwritingModelStatus>> getStatusEventList() {
        return statusEventList == null
                ? Collections.singletonList(new StatusEvent<>(UnderwritingModelStatus.NEW, null, null, null))
                : statusEventList;
    }
}
