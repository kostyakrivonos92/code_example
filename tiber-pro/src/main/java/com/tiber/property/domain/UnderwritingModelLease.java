package com.tiber.property.domain;

import lombok.Data;
import java.time.Instant;

@Data
public class UnderwritingModelLease {

    private boolean lease;

    private int leaseAmount;

    private Instant leaseExpiration = Instant.now();

    private int allInBasis;

    private int inPlacePty;
}

