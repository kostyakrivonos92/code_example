package com.tiber.property.domain;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;

import java.util.List;

@Data
@Builder
@AllArgsConstructor
public class DealSummaryPage {

    private int page;

    private int pageSize;

    private long mainCounter;

    private List<DealSummary> deals;
}
