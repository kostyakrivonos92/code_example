package com.tiber.property.domain;

import com.tiber.shared.util.AnalogueRenovationTag;
import com.tiber.shared.vocabulary.DealStatus;
import lombok.Data;

import java.time.Instant;
import java.time.LocalDate;

@Data
public class DealSummary {

    private String uuid;

    private String propertyUuid;

    private String address;

    private String city;

    private String state;

    private String zip;

    private String msa;

    private String county;

    private Integer bedrooms;

    private Integer bathrooms;

    private Integer halfBathrooms;

    private Integer areaSqFt;

    private Integer yearBuilt;

    private Float price;

    private Float rent;

    private Float psp;

    private Float uwReno;

    private Instant purchaseDate;

    private Instant salesDate;

    //TODO replace with currentDealUuid, search model by claimed active deal UUID
    private String modelUuid;

    private double latitude;

    private double longitude;

    private AnalogueRenovationTag renovationTag;

    private DealStatus status;

    private LocalDate closeDate;

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        DealSummary that = (DealSummary) o;

        return uuid.equals(that.uuid);
    }

    @Override
    public int hashCode() {
        return uuid.hashCode();
    }
}
