package com.tiber.property.domain;

import lombok.Data;

@Data
public class UnderwritingModelTax {

    private int assessedValue;
    private float mileageRate;
    private int specialAssessment;
}
