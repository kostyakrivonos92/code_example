package com.tiber.property.domain;

import lombok.Data;

@Data
public class StrategyDependentResultingData {

    private float targetYield;

    private float loanToCostRatio;

    private float internalRateOfReturn;
}
