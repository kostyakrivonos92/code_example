package com.tiber.property.domain;

import lombok.Data;

import java.time.Instant;

@Data
public class Document {

    private long id;

    private String fileName;

    private String fileUrl;

    private Instant createdAt;

    private String createdBy;
}
