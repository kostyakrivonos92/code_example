package com.tiber.property.entity;

import com.tiber.shared.util.RenovationType;

import java.io.Serial;
import java.util.HashMap;

public class StrategyDependentInitialDataMap extends HashMap<RenovationType, StrategyDependentInitialDataEntity> {

    @Serial
    private static final long serialVersionUID = -3127623219315338277L;
}
