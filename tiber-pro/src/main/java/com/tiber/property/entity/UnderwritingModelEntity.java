package com.tiber.property.entity;

import com.tiber.shared.inspection.entity.PropertyInfoEntity;
import com.tiber.shared.inspection.entity.PropertyInfoEntityType;
import com.tiber.shared.status.entity.StatusEventEntityList;
import com.tiber.shared.status.entity.StatusEventEntityListType;
import com.tiber.shared.user.entity.ModifiableEntity;
import com.tiber.shared.util.RenovationType;
import lombok.Data;
import lombok.Getter;
import lombok.Setter;
import org.hibernate.annotations.DynamicInsert;
import org.hibernate.annotations.Type;
import org.hibernate.annotations.TypeDef;
import org.springframework.data.annotation.CreatedBy;
import org.springframework.data.annotation.LastModifiedBy;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.FetchType;
import javax.persistence.OneToMany;
import javax.persistence.OneToOne;
import javax.persistence.Table;
import java.io.Serial;
import java.math.BigDecimal;
import java.time.Instant;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

//TODO: should be updated when all requirements would be clarified

@Setter
@Getter
@Entity
@DynamicInsert
@TypeDef(name = "PropertyAnalogueListType", typeClass = PropertyAnalogueListType.class)
@TypeDef(name = "StrategyDependentInitialDataMapType", typeClass = StrategyDependentInitialDataMapType.class)
@TypeDef(name = "StrategyDependentResultingDataMapType", typeClass = StrategyDependentResultingDataMapType.class)
@TypeDef(name = "AnaloguesSelectionType", typeClass = AnaloguesSelectionType.class)
@TypeDef(name = "PropertyInfoEntityType", typeClass = PropertyInfoEntityType.class)
@TypeDef(name = "StatusEventEntityListType", typeClass = StatusEventEntityListType.class)
@Table(name = "underwriting_model", schema = "underwriting_data")
public class UnderwritingModelEntity extends ModifiableEntity<Long> {

    @Serial
    private static final long serialVersionUID = 7546719037851594120L;

    private String uuid;

    private String propertyUuid;

    private String dealUuid;

    private Instant salesDate;

    private int minYield;

    private int ddRehab;

    private int valuationPsp;

    private int askingPrice;

    private int offer;

    @Column(columnDefinition = "json")
    @Type(type = "PropertyInfoEntityType")
    private PropertyInfoEntity propertyInfo;

    private BigDecimal allInBasis;

    private BigDecimal priceToYield;

    @Enumerated(EnumType.STRING)
    private RenovationType strategy;

    @Column(columnDefinition = "json")
    @Type(type = "StrategyDependentResultingDataMapType")
    private Map<RenovationType, StrategyDependentResultingDataEntity> strategyDependentResultingDataMap = new HashMap<>();

    @Column(columnDefinition = "json")
    @Type(type = "StrategyDependentInitialDataMapType")
    private Map<RenovationType, StrategyDependentInitialDataEntity> strategyDependentInitialDataMap = new HashMap<>();

    @OneToMany(mappedBy = "underwritingModel", fetch = FetchType.LAZY)
    private List<UnderwritingNoteEntity> notes;

    @OneToMany(mappedBy = "underwritingModel", fetch = FetchType.LAZY)
    private List<DocumentEntity> documents;

    @OneToOne(fetch = FetchType.LAZY, cascade = CascadeType.ALL)
    private UnderwritingModelLeaseEntity lease;

    @OneToOne(fetch = FetchType.LAZY, cascade = CascadeType.ALL)
    private UnderwritingModelMiscEntity misc;

    @OneToOne(fetch = FetchType.LAZY, cascade = CascadeType.ALL)
    private UnderwritingModelTaxEntity tax;

    @Column(columnDefinition = "json")
    @Type(type = "PropertyAnalogueListType")
    private PropertyAnalogueEntityList propertyAnalogues;

    @Column(columnDefinition = "json")
    @Type(type = "AnaloguesSelectionType")
    private AnaloguesSelectionEntity analoguesSelection;

    @Column(columnDefinition = "json")
    @Type(type = "StatusEventEntityListType")
    private StatusEventEntityList statusEventList;

    @CreatedBy
    protected String createdBy;

    @LastModifiedBy
    protected String updatedBy;
}
