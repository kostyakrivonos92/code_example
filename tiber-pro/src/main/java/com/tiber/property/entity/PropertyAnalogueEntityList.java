package com.tiber.property.entity;

import java.io.Serial;
import java.io.Serializable;
import java.util.ArrayList;

public class PropertyAnalogueEntityList extends ArrayList<PropertyAnalogueEntity> implements Serializable {

    @Serial
    private static final long serialVersionUID = -5636321089830477478L;
}
