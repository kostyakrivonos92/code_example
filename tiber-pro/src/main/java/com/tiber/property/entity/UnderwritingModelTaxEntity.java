package com.tiber.property.entity;

import lombok.Data;

import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToOne;
import javax.persistence.Table;

//TODO: should be updated when all requirements would be clarified

@Data
@Entity
@Table(schema = "[underwriting_data]", name = "[tax]")
public class UnderwritingModelTaxEntity {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private long id;

    private int assessedValue;

    private float mileageRate;

    private int specialAssessment;

    @OneToOne(fetch = FetchType.LAZY, mappedBy = "tax")
    private UnderwritingModelEntity modelEntity;
}
