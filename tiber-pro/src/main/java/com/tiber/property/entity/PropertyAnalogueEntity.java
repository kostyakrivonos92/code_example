package com.tiber.property.entity;

import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.tiber.shared.util.AnalogueRenovationTag;
import com.tiber.shared.util.LocalDateDeserializer;
import com.tiber.shared.util.LocalDateSerializer;
import lombok.Data;

import java.io.Serial;
import java.io.Serializable;
import java.time.LocalDate;

@Data
public class PropertyAnalogueEntity implements Serializable {

    @Serial
    private static final long serialVersionUID = 607027122474482694L;

    private String uuid;

    private String propertyUuid;

    private String address;

    private String city;

    private String state;

    private String zip;

    private int bedrooms;

    private int bathrooms;

    private int halfBathrooms;

    private int areaSqFt;

    private int yearBuilt;

    private float price;

    private boolean isRent;

    private double latitude;

    private double longitude;

    private int daysOnMarket;

    @JsonDeserialize(using = LocalDateDeserializer.class)
    @JsonSerialize(using = LocalDateSerializer.class)
    private LocalDate closeDate;

    private float distance;

    private AnalogueRenovationTag renovationTag;

    private String compsLabel;

    private String googleUrl;

    private String zillowUrl;

    private String mlsUrl;
}
