package com.tiber.property.entity;

import lombok.Data;

import java.io.Serial;
import java.io.Serializable;
import java.util.List;

@Data
public class AnaloguesSelectionEntity implements Serializable {

    @Serial
    private static final long serialVersionUID = -1515203906154585096L;

    private List<String> spec;

    private List<String> stock;

    private List<String> all;
}
