package com.tiber.property.entity;

import com.tiber.shared.util.ConstructionQuality;
import com.tiber.shared.util.RenovationType;
import lombok.Data;

import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToOne;
import javax.persistence.Table;

//TODO: should be updated when all requirements would be clarified

@Data
@Entity
@Table(schema = "[underwriting_data]", name = "[misc]")
public class UnderwritingModelMiscEntity {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private long id;

    @Enumerated(EnumType.STRING)
    private RenovationType strategy = RenovationType.UNKNOWN;

    private int leaseUpTime;

    @Enumerated(EnumType.STRING)
    private ConstructionQuality constructionQuality = ConstructionQuality.OTHER;

    private String hoa;

    @OneToOne(fetch = FetchType.LAZY, mappedBy = "misc")
    private UnderwritingModelEntity modelEntity;
}
