package com.tiber.property.entity;

import com.tiber.shared.user.entity.ModifiableEntity;
import lombok.Data;
import org.hibernate.annotations.DynamicInsert;
import org.springframework.data.annotation.CreatedBy;

import javax.persistence.Entity;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import java.io.Serial;

@Data
@Entity
@DynamicInsert
@Table(name = "[document]", schema = "[underwriting_data]")
public class DocumentEntity extends ModifiableEntity<Long> {

    @Serial
    private static final long serialVersionUID = 3646919433731599930L;

    @ManyToOne
    @JoinColumn(name = "[model_id]", nullable = false)
    private UnderwritingModelEntity underwritingModel;

    private String fileName;

    private String fileUrl;

    @CreatedBy
    private String createdBy;
}
