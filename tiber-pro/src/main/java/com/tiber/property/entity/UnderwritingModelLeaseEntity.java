package com.tiber.property.entity;

import lombok.Data;
import lombok.Getter;
import lombok.Setter;

import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToOne;
import javax.persistence.Table;
import java.time.Instant;

//TODO: should be updated when all requirements would be clarified

@Getter
@Setter
@Entity
@Table(schema = "[underwriting_data]",name = "[lease]")
public class UnderwritingModelLeaseEntity {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private long id;

    private boolean lease;

    private int leaseAmount;

    private Instant leaseExpiration;

    private int allInBasis;

    private int inPlacePty;

    @OneToOne(fetch = FetchType.LAZY, mappedBy = "lease")
    private UnderwritingModelEntity modelEntity;
}
