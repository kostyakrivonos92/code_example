package com.tiber.property.entity;

import lombok.Data;

import java.io.Serial;
import java.io.Serializable;

@Data
public class StrategyDependentResultingDataEntity implements Serializable {

    @Serial
    private static final long serialVersionUID = 1348289855684235647L;

    private float targetYield;

    private float loanToCostRatio;

    private float internalRateOfReturn;
}
