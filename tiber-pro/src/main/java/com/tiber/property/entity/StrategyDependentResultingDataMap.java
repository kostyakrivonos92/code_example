package com.tiber.property.entity;

import com.tiber.shared.util.RenovationType;

import java.io.Serial;
import java.util.HashMap;

public class StrategyDependentResultingDataMap extends HashMap<RenovationType, StrategyDependentResultingDataEntity> {

    @Serial
    private static final long serialVersionUID = 2338656120795654206L;
}
