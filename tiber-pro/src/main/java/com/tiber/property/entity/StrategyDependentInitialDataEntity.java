package com.tiber.property.entity;

import lombok.Data;

import java.io.Serial;
import java.io.Serializable;
import java.math.BigDecimal;

@Data
public class StrategyDependentInitialDataEntity implements Serializable {

    @Serial
    private static final long serialVersionUID = 2596679519227549824L;

    private BigDecimal rent;

    private BigDecimal rehabOverride;

    private BigDecimal uwRehab;

    private BigDecimal projectedSalesPrice;
}
