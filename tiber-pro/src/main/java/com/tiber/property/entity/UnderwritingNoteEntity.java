package com.tiber.property.entity;

import com.tiber.shared.note.entity.BaseNote;
import lombok.Data;
import org.hibernate.annotations.DynamicInsert;

import javax.persistence.Entity;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import java.io.Serial;

@Data
@Entity
@DynamicInsert
@Table(name = "[note]", schema = "[underwriting_data]")
public class UnderwritingNoteEntity extends BaseNote {

    @Serial
    private static final long serialVersionUID = 2646919433731591420L;

    @ManyToOne
    @JoinColumn(name = "[model_id]", nullable = false)
    private UnderwritingModelEntity underwritingModel;

    private boolean isSystem;

}
