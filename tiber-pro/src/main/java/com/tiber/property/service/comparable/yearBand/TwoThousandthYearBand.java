package com.tiber.property.service.comparable.yearBand;

import org.springframework.stereotype.Component;

@Component
class TwoThousandthYearBand extends NumberedYearBand {

    private final int minYear = 2000;

    @Override
    public String getMinYearBuilt() {
        return Integer.toString(minYear);
    }

    @Override
    public String getMaxYearBuilt() {
        int maxYear = Integer.MAX_VALUE;
        return Integer.toString(maxYear);
    }

    @Override
    public boolean checkYear(int yearBuilt) {
        return yearBuilt >= minYear;
    }
}
