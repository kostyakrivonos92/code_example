package com.tiber.property.service;

import com.tiber.property.domain.PropertyAnalogue;
import com.tiber.property.mapper.PropertyAnalogueMapper;
import com.tiber.shared.propertylayer.service.PropertyLayerService;
import com.tiber.shared.searchCriteria.domain.BaseSearchCriteria;
import io.reactivex.rxjava3.core.Flowable;
import io.reactivex.rxjava3.core.Maybe;
import lombok.RequiredArgsConstructor;
import lombok.extern.log4j.Log4j2;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 * Default implementation of PropertyAnalogueService
 */
@Log4j2
@Service
@RequiredArgsConstructor(onConstructor_ = @Autowired)
class DefaultPropertyAnalogueService implements PropertyAnalogueService {

    private final PropertyAnalogueMapper propertyAnalogueMapper;

    private final PropertyLayerService propertyLayerService;

    @Override
    public Flowable<PropertyAnalogue> getPropertyAnalogueList(BaseSearchCriteria criteria) {
        return propertyLayerService.getProperties(criteria)
                .map(propertyAnalogueMapper::propertyAnalogueFromPropertyOnMarket);
    }

    @Override
    public Maybe<PropertyAnalogue> getPropertyAnalogueByUuid(String uuid) {
        return propertyLayerService.getPropertyByUuid(uuid)
                .map(propertyAnalogueMapper::propertyAnalogueFromPropertyOnMarket);
    }
}
