package com.tiber.property.service.comparable;

class SquareFeetBandCalculator {

    private final int bandPercent;
    private final int squareFeet;

    public SquareFeetBandCalculator(int bandPercent, int squareFeet) {
        this.bandPercent = bandPercent;
        this.squareFeet = squareFeet;
    }

    public String getMinSquareFeet() {
        return String.valueOf(squareFeet - squareFeet * bandPercent / 100);
    }

    public String getMaxSquareFeet() {
        return String.valueOf(squareFeet + squareFeet * bandPercent / 100);
    }

    public boolean isSquareFeetInBand(Integer targetSquareFeet) {
        int deltaSquareFeet = squareFeet * bandPercent / 100;
        return (targetSquareFeet >= (squareFeet - deltaSquareFeet)) && (targetSquareFeet <= (squareFeet + deltaSquareFeet));
    }
}
