package com.tiber.property.service.comparable.yearBand;

public interface YearBandService {

    YearBand getYearBand(int yearBuilt);

    YearBand getYearBands(int yearBuilt, int deltaYearBands);
}
