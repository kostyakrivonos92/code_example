package com.tiber.property.service.comparable;

import com.tiber.property.domain.PropertyAnalogue;
import com.tiber.shared.inspection.domain.PropertyInfo;

import java.util.List;


//TODO think about moving from List to Flowable
public interface ComparableListGatherer {

    List<PropertyAnalogue> gatherComparableListForProperty(String uuid, PropertyInfo propertyInfo);
}
