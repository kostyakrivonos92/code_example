package com.tiber.property.service;

import com.tiber.property.domain.AnaloguesSelection;
import com.tiber.property.domain.DealSummary;
import com.tiber.property.domain.Document;
import com.tiber.property.domain.PropertyAnalogue;
import com.tiber.property.domain.StrategyDependentInitialData;
import com.tiber.property.domain.UnderwritingModel;
import com.tiber.property.domain.UnderwritingModelLease;
import com.tiber.property.domain.UnderwritingModelMisc;
import com.tiber.property.domain.UnderwritingModelTax;
import com.tiber.property.entity.DocumentEntity;
import com.tiber.property.entity.UnderwritingModelEntity;
import com.tiber.property.entity.UnderwritingNoteEntity;
import com.tiber.property.exception.UnderwritingModelNotFoundException;
import com.tiber.property.mapper.DocumentMapper;
import com.tiber.property.mapper.PropertyInfoMapper;
import com.tiber.property.mapper.UnderwritingModelMapper;
import com.tiber.property.repository.DocumentRepository;
import com.tiber.property.repository.UnderwritingModelRepository;
import com.tiber.property.repository.UnderwritingNoteRepository;
import com.tiber.property.service.comparable.ComparableListGatherer;
import com.tiber.property.status.UnderwritingModelStatus;
import com.tiber.shared.exception.ObjectProcessingException;
import com.tiber.shared.file.service.ManageFileService;
import com.tiber.shared.inspection.domain.PropertyInfo;
import com.tiber.shared.inspection.domain.PropertyInfoTable;
import com.tiber.shared.note.domain.Note;
import com.tiber.shared.note.domain.NotePage;
import com.tiber.shared.propertylayer.domain.PropertyOnMarket;
import com.tiber.shared.propertylayer.service.PropertyLayerService;
import com.tiber.shared.status.domain.StatusEvent;
import com.tiber.shared.underwriting.domain.UnderwritingModelExternal;
import com.tiber.shared.user.vocabulary.Status;
import com.tiber.shared.util.RenovationType;
import com.tiber.shared.vocabulary.inspection.InspectionType;
import io.reactivex.rxjava3.annotations.NonNull;
import lombok.AllArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.math.BigDecimal;
import java.nio.file.Path;
import java.time.Instant;
import java.util.Collection;
import java.util.Collections;
import java.util.List;
import java.util.Map;
import java.util.Objects;
import java.util.Optional;
import java.util.UUID;
import java.util.stream.Collectors;

@Slf4j
@Service
@AllArgsConstructor(onConstructor_ = @Autowired)
public class DefaultUnderwritingModelService implements UnderwritingModelService {

    private final UnderwritingModelRepository underwritingModelRepository;

    private final UnderwritingNoteRepository underwritingNoteRepository;

    private final UnderwritingModelMapper underwritingModelMapper;

    private final AveragePriceCalculationService averagePriceCalculationService;

    private final DocumentRepository documentRepository;

    private final ManageFileService mediaFileService;

    private final DocumentMapper documentMapper;

    private final ComparableListGatherer comparableListGatherer;

    private final PropertyLayerService propertyLayerService;

    private final PropertyInfoMapper propertyInfoMapper;

    private final TiberBudgetInspectionService tiberBudgetInspectionService;

    private final RentalCalculationService rentalCalculationService;

    @Override
    @Transactional
    public List<UnderwritingModel> findByPropertyUuid(String propertyUuid) {
        return underwritingModelRepository.findByPropertyUuid(propertyUuid).stream()
                .map(underwritingModelMapper::toDomain).collect(Collectors.toList());
    }

    @Override
    @Transactional //Think about implementing different DO to optimize lazy-loading
    public Collection<UnderwritingModel> findByPropertyUuidCollection(Collection<String> propertyUuid) {
        return underwritingModelRepository.findAllByPropertyUuidIn(propertyUuid)
                .stream()
                .map(underwritingModelMapper::toDomain)
                .collect(Collectors.toList());
    }

    @Override
    public Optional<UnderwritingModel> findByUuid(String uuid) {
        return underwritingModelRepository.findByUuid(uuid)
                .map(underwritingModelMapper::toDomain);
    }

    @Override
    public Optional<UnderwritingModelExternal> findExternalByDealUuid(String dealUuid) {
        return underwritingModelRepository.findByDealUuid(dealUuid)
                .map(underwritingModelMapper::underwritingModelEntityToUnderwritingModelExternal);
    }

    @Override
    @Transactional
    public Optional<UnderwritingModel> create(String propertyUuid, DealSummary dealSummary) {
        UnderwritingModel underwritingModel = new UnderwritingModel();
        underwritingModel.setPropertyUuid(propertyUuid);
        underwritingModel.setDealUuid(dealSummary.getUuid());
        BigDecimal askingPrice = Objects.isNull(dealSummary.getPrice()) ? null : BigDecimal.valueOf(dealSummary.getPrice());
        underwritingModel.setAskingPrice(askingPrice); //TODO: FIXME on NULL value
        underwritingModel.setStrategy(defaultStrategy);
        underwritingModel.setSalesDate(Instant.now());
        return create(underwritingModel);
    }

    private Optional<UnderwritingModel> create(UnderwritingModel underwritingModel) {
        underwritingModel.setUuid(UUID.randomUUID().toString());
        return Optional.of(underwritingModel)
                .filter(underwritingModelLocal -> !underwritingModelRepository.existsByPropertyUuid(underwritingModelLocal.getPropertyUuid()))
                .map(underwritingModelLocal -> {
                    PropertyOnMarket property = propertyLayerService.getPropertyByUuid(underwritingModelLocal.getPropertyUuid()).blockingGet();
                    underwritingModelLocal.setPropertyInfo(propertyInfoMapper.publicPropertyToDomain(property));
                    StatusEvent<UnderwritingModelStatus> underwritingModelStatusEvent =
                            new StatusEvent<>(UnderwritingModelStatus.NEW, null, null, null);
                    underwritingModelLocal.setStatusEventList(Collections.singletonList(underwritingModelStatusEvent));
                    return evaluateModel(underwritingModelLocal);
                })
                .map(underwritingModelMapper::toEntity)
                .map(underwritingModelRepository::save)
                .map(underwritingModelMapper::toDomain);
    }


    @Override
    public UnderwritingModel update(UnderwritingModel underwritingModel) {

        @SuppressWarnings("redundant") final UnderwritingModel updatedUnderwritingModel = underwritingModelRepository.findByUuid(underwritingModel.getUuid())
                .map(underwritingModelMapper::toDomain)
                .map(underwritingModelLocal -> {
                    //TODO update when we know what we can update and what we can't or what is recalculated
                    //Editable fields updates. Editable fields list according to current design.
                    UnderwritingModelTax newUnderwritingModelTax = underwritingModel.getTax();
                    underwritingModelLocal.getTax().setAssessedValue(newUnderwritingModelTax.getAssessedValue());
                    underwritingModelLocal.getTax().setMileageRate(newUnderwritingModelTax.getMileageRate());
                    underwritingModelLocal.getTax().setSpecialAssessment(newUnderwritingModelTax.getSpecialAssessment());

                    UnderwritingModelMisc newUnderwritingModelMisc = underwritingModel.getMisc();
                    underwritingModelLocal.getMisc().setStrategy(newUnderwritingModelMisc.getStrategy());
                    underwritingModelLocal.getMisc().setLeaseUpTime(newUnderwritingModelMisc.getLeaseUpTime());
                    underwritingModelLocal.getMisc().setConstructionQuality(newUnderwritingModelMisc.getConstructionQuality());

                    UnderwritingModelLease newUnderwritingModelLease = underwritingModel.getLease();
                    underwritingModelLocal.getLease().setLease(newUnderwritingModelLease.isLease());
                    underwritingModelLocal.getLease().setLeaseAmount(newUnderwritingModelLease.getLeaseAmount());
                    underwritingModelLocal.getLease().setLeaseExpiration(newUnderwritingModelLease.getLeaseExpiration());

                    underwritingModelLocal.setOffer(underwritingModel.getOffer());
                    final RenovationType strategy = underwritingModel.getStrategy();
                    underwritingModelLocal.setStrategy(strategy);
                    underwritingModelLocal.setStrategyDependentInitialDataMap(underwritingModel.getStrategyDependentInitialDataMap());

                    Optional.ofNullable(underwritingModel.getStrategy())
                            .ifPresent(underwritingModelLocal::setStrategy);

                    Optional.ofNullable(underwritingModel.getStrategyDependentInitialDataMap())
                            .ifPresent(underwritingModelLocal::setStrategyDependentInitialDataMap);

                    return rentalCalculationService.calculate(underwritingModelLocal);
                })
                .map(underwritingModelMapper::toEntity)
                .map(underwritingModelRepository::save)
                .map(underwritingModelMapper::toDomain)
                .orElseThrow(() ->
                        new ObjectProcessingException("Model", String.format("Cannot find model for property with id %s", underwritingModel.getPropertyUuid())));
        return updatedUnderwritingModel;
    }


    @Override
    public Note createNote(String underwritingModelUuid, Note note) {
        return underwritingModelRepository.findByUuid(underwritingModelUuid)
                .map(underwritingModelEntity -> {
                    UnderwritingNoteEntity newNoteEntity = underwritingModelMapper.noteToUnderwritingNoteEntity(note);
                    newNoteEntity.setUnderwritingModel(underwritingModelEntity);
                    return underwritingNoteRepository.save(newNoteEntity);
                })
                .map(underwritingModelMapper::underwritingNoteEntityToNote)
                .orElseThrow(() -> new UnderwritingModelNotFoundException(underwritingModelUuid));
    }

    @Override
    public NotePage getNotesForModel(String underwritingModelUuid, boolean includeSystemNotes, int page, int pageSize) {
        return underwritingModelRepository.findByUuid(underwritingModelUuid)
                .map(modelEntity -> modelEntity.getNotes()
                        .stream()
                        .filter(noteEntity -> includeSystemNotes || !noteEntity.isSystem())
                        .sorted((noteEntity1, noteEntity2) ->
                                -noteEntity1.getCreatedAt().compareTo(noteEntity2.getCreatedAt()))
                        .skip(((long) page) * pageSize)
                        .limit(pageSize)
                        .map(underwritingModelMapper::underwritingNoteEntityToNote)
                        .collect(Collectors.toList()))
                .map(notes -> new NotePage(page, pageSize, notes))
                .orElseThrow(() -> new UnderwritingModelNotFoundException(underwritingModelUuid));
    }

    @Override
    public Document createDocument(String underwritingModelUuid, String fileName, Path documentUrl) {
        return underwritingModelRepository.findByUuid(underwritingModelUuid)
                .map(underwritingModelEntity -> {
                    Path documentPath = mediaFileService.uploadFile(underwritingModelUuid, fileName, documentUrl);

                    Document document = new Document();
                    document.setFileName(documentPath.getFileName().toString());
                    document.setFileUrl(documentPath.toString());

                    DocumentEntity documentEntity = documentMapper.fromDoToEntity(document);
                    documentEntity.setUnderwritingModel(underwritingModelEntity);
                    return documentRepository.save(documentEntity);
                })
                .map(documentMapper::fromEntityToDo)
                .orElseThrow(() -> new UnderwritingModelNotFoundException(underwritingModelUuid));
    }

    @Override
    public @NonNull List<Document> getDocumentsForModel(String underwritingModelUuid) {
        return underwritingModelRepository.findByUuid(underwritingModelUuid)
                .map(modelEntity -> {
                    List<DocumentEntity> documents = modelEntity.getDocuments().stream()
                            .filter(documentEntity -> documentEntity.getStatus().equals(Status.ACTIVE))
                            .collect(Collectors.toList());

                    return documentMapper.fromEntityListToDoList(documents);
                })
                .orElseThrow(() -> new UnderwritingModelNotFoundException(underwritingModelUuid));
    }

    @Override
    public void deleteDocument(String underwritingModelUuid, long id) {
        underwritingModelRepository.findByUuid(underwritingModelUuid)
                .ifPresentOrElse(underwritingModelEntity ->
                                Optional.ofNullable(underwritingModelEntity.getDocuments())
                                        .ifPresent(documentEntities ->
                                                documentEntities.stream()
                                                        .filter(documentEntity -> documentEntity.getId().equals(id))
                                                        .forEach(documentEntity -> {
                                                            mediaFileService.deleteFile(documentEntity.getFileUrl());
                                                            documentEntity.setStatus(Status.DELETED);
                                                            documentRepository.save(documentEntity);
                                                        })),
                        () -> {
                            throw new UnderwritingModelNotFoundException(underwritingModelUuid);
                        }
                );
    }

    /**
     * Saving logic is next: if one or a few of "spec", "stock" or "all" selections are not provided
     * in the AnaloguesSelection we are keeping current values
     *
     * @param underwritingModelUuid Underwriting model UUID
     * @param newAnaloguesSelection Analogue selection to set
     * @return saved underwriting model
     */
    @Override
    public UnderwritingModel setAnaloguesSelection(String underwritingModelUuid, AnaloguesSelection newAnaloguesSelection) {
        @SuppressWarnings("redundant")
        UnderwritingModel savedUnderwritingModel = underwritingModelRepository.findByUuid(underwritingModelUuid)
                .map(underwritingModelMapper::toDomain)
                .map(underwritingModel -> {
                    Collection<String> availablePropertyUuidCollection = underwritingModel.getPropertyAnalogues().stream()
                            .map(PropertyAnalogue::getUuid)
                            .collect(Collectors.toList());
                    final AnaloguesSelection currentAnaloguesSelection = underwritingModel.getAnaloguesSelection();
                    final AnaloguesSelection analoguesSelectionToSave = new AnaloguesSelection();
                    Optional.ofNullable(newAnaloguesSelection.getSpec())
                            .filter(availablePropertyUuidCollection::containsAll)
                            .ifPresentOrElse(analoguesSelectionToSave::setSpec,
                                    () -> analoguesSelectionToSave.setSpec(currentAnaloguesSelection.getSpec()));
                    Optional.ofNullable(newAnaloguesSelection.getStock())
                            .filter(availablePropertyUuidCollection::containsAll)
                            .ifPresentOrElse(analoguesSelectionToSave::setStock,
                                    () -> analoguesSelectionToSave.setStock(currentAnaloguesSelection.getStock()));
                    Optional.ofNullable(newAnaloguesSelection.getAll())
                            .filter(availablePropertyUuidCollection::containsAll)
                            .ifPresentOrElse(analoguesSelectionToSave::setAll,
                                    () -> analoguesSelectionToSave.setAll(currentAnaloguesSelection.getAll()));
                    underwritingModel.setAnaloguesSelection(analoguesSelectionToSave);

                    Map<RenovationType, StrategyDependentInitialData> strategyDependentInitialData = averagePriceCalculationService
                            .getAnalogueCalculatedIndexesByStrategy(underwritingModel.getPropertyAnalogues(), analoguesSelectionToSave);
                    underwritingModel.setStrategyDependentInitialDataMap(strategyDependentInitialData);

                    return rentalCalculationService.calculate(underwritingModel);
                })
                .map(underwritingModelMapper::toEntity)
                .map(underwritingModelRepository::save)
                .map(underwritingModelMapper::toDomain)
                .orElseThrow(() -> new UnderwritingModelNotFoundException(underwritingModelUuid));
        return savedUnderwritingModel;
    }

    @Override
    public PropertyInfoTable getUnderwritingModelInspectionTable(String underwritingModelUuid) {
        UnderwritingModel model = underwritingModelRepository
                .findByUuid(underwritingModelUuid)
                .map(underwritingModelMapper::toDomain)
                .orElseThrow(() -> new UnderwritingModelNotFoundException(underwritingModelUuid));

        return createInspectionTable(model);
    }

    @Override
    @Transactional
    public PropertyInfoTable updateUnderwritingModelInspection(String underwritingModelUuid, PropertyInfo underwritingModelInspection) {
        UnderwritingModel model = underwritingModelRepository.findByUuid(underwritingModelUuid)
                .map(underwritingModelMapper::toDomain)
                .orElseThrow(() -> new UnderwritingModelNotFoundException(underwritingModelUuid));

        int bedrooms = underwritingModelInspection.getBedrooms();
        if (bedrooms >= 0) {
            model.getPropertyInfo().setBedrooms(bedrooms);
        }

        int bathrooms = underwritingModelInspection.getBathrooms();
        if (bathrooms >= 0) {
            model.getPropertyInfo().setBathrooms(bathrooms);
        }

        int halfBathrooms = underwritingModelInspection.getHalfBathrooms();
        if (halfBathrooms >= 0) {
            model.getPropertyInfo().setHalfBathrooms(halfBathrooms);
        }

        int areaSqFt = underwritingModelInspection.getAreaSqFt();
        if (areaSqFt >= 0) {
            model.getPropertyInfo().setAreaSqFt(areaSqFt);
        }

        int yearBuilt = underwritingModelInspection.getYearBuilt();
        if (yearBuilt >= 0) {
            model.getPropertyInfo().setYearBuilt(yearBuilt);
        }

        final UnderwritingModelEntity underwritingModelEntity = underwritingModelMapper.toEntity(evaluateModel(model));
        final UnderwritingModelEntity savedModelEntity = underwritingModelRepository.save(underwritingModelEntity);

        return createInspectionTable(underwritingModelMapper.toDomain(savedModelEntity));
    }

    @Override
    public Optional<UnderwritingModel> applyStatusEvent(String underwritingModelUuid, StatusEvent<UnderwritingModelStatus> statusEvent) {
        //TODO Check if status is appropriate for current model before applying
        return underwritingModelRepository.findByUuid(underwritingModelUuid)
                .map(underwritingModelMapper::toDomain)
                .map(underwritingModel -> {
                    underwritingModel.getStatusEventList().add(statusEvent);
                    return underwritingModel;
                })
                .map(underwritingModelMapper::toEntity)
                .map(underwritingModelRepository::save)
                .map(underwritingModelMapper::toDomain);
    }

    @Override
    public Optional<List<StatusEvent<UnderwritingModelStatus>>> getStatusEventList(String underwritingModelUuid) {
        return findByUuid(underwritingModelUuid)
                .map(UnderwritingModel::getStatusEventList)
                .map(list -> {
                    list.sort((event1, event2) -> event2.getEmittedAt().compareTo(event1.getEmittedAt()));
                    return list;
                });
    }

    @Override
    public UnderwritingModel evaluate(String underwritingModelUuid) {
        @SuppressWarnings("redundant") final UnderwritingModel updatedUnderwritingModel = underwritingModelRepository.findByUuid(underwritingModelUuid)
                .map(underwritingModelMapper::toDomain)
                .map(this::evaluateModel)
                .map(underwritingModelMapper::toEntity)
                .map(underwritingModelRepository::save)
                .map(underwritingModelMapper::toDomain)
                .orElseThrow(() -> new UnderwritingModelNotFoundException(underwritingModelUuid));
        return updatedUnderwritingModel;
    }

    private UnderwritingModel evaluateModel(UnderwritingModel underwritingModelLocal) {
        List<PropertyAnalogue> propertyAnalogues = comparableListGatherer
                .gatherComparableListForProperty(underwritingModelLocal.getPropertyUuid(), underwritingModelLocal.getPropertyInfo());
        underwritingModelLocal.setPropertyAnalogues(propertyAnalogues);
        underwritingModelLocal.setAnaloguesSelection(new AnaloguesSelection());
        Map<RenovationType, StrategyDependentInitialData> strategyDependentInitialData = averagePriceCalculationService.getAnalogueCalculatedIndexesByStrategy(propertyAnalogues, underwritingModelLocal.getAnaloguesSelection());
        underwritingModelLocal.setStrategyDependentInitialDataMap(strategyDependentInitialData);
        return rentalCalculationService.calculate(underwritingModelLocal);
    }

    private PropertyInfoTable createInspectionTable(UnderwritingModel model) {
        PropertyOnMarket property = propertyLayerService.getPropertyByUuid(model.getPropertyUuid()).blockingGet();

        return PropertyInfoTable.builder()
                .proInspection(model.getPropertyInfo())
                .publicInspection(propertyInfoMapper.publicPropertyToDomain(property))
                .preAcquisitionInspection(tiberBudgetInspectionService.getInspectionDataByPropertyUuidAndInspectionType(model.getPropertyUuid(), InspectionType.PRE_ACQUISITION).blockingGet())
                .postRenoInspection(tiberBudgetInspectionService.getInspectionDataByPropertyUuidAndInspectionType(model.getPropertyUuid(), InspectionType.POST_RENOVATION).blockingGet())
                .build();
    }
}
