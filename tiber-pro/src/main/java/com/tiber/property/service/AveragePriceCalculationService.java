package com.tiber.property.service;

import com.tiber.property.domain.AnaloguesSelection;
import com.tiber.property.domain.PropertyAnalogue;
import com.tiber.property.domain.StrategyDependentInitialData;
import com.tiber.shared.util.RenovationType;

import java.util.List;
import java.util.Map;

//TODO Think about naming. For now it creates UnderwritingModelSpec but not the average price
public interface AveragePriceCalculationService {

    Map<RenovationType, StrategyDependentInitialData> getAnalogueCalculatedIndexesByStrategy(List<PropertyAnalogue> analogueList, AnaloguesSelection analoguesSelection);
}
