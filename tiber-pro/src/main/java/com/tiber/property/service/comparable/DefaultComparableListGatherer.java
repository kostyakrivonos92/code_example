package com.tiber.property.service.comparable;

import com.tiber.property.domain.PropertyAnalogue;
import com.tiber.shared.inspection.domain.PropertyInfo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

import java.util.*;

@Service
public class DefaultComparableListGatherer implements ComparableListGatherer {

    private final AnaloguesSearchLinkService analoguesSearchLinkService;

    @Value("${application.analogueQuantity}")
    private final Integer analogueQuantity;

    @Autowired
    public DefaultComparableListGatherer(AnaloguesSearchLinkService analoguesSearchLinkService,
                                         @Value("${application.analogueQuantity}") Integer analogueQuantity) {
        this.analoguesSearchLinkService = analoguesSearchLinkService;
        this.analogueQuantity = analogueQuantity;
    }

    @Override
    public List<PropertyAnalogue> gatherComparableListForProperty(String uuid, PropertyInfo propertyInfo) {
        List<PropertyAnalogue> result =  analoguesSearchLinkService.getAnaloguesForProperty(uuid, new LinkedList<>(), analogueQuantity, propertyInfo);
        return new ArrayList<>(new LinkedHashSet<>(result));
    }
}
