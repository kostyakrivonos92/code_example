package com.tiber.property.service.comparable.yearBand;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Comparator;
import java.util.List;
import java.util.Map;
import java.util.concurrent.atomic.AtomicInteger;
import java.util.function.Function;
import java.util.stream.Collectors;

@Service
class DefaultYearBandService implements YearBandService {

    private final Map<Integer, YearBand> yearBandsMap;

    @Autowired
    public DefaultYearBandService(List<YearBand> yearBandsList) {
        AtomicInteger number = new AtomicInteger(0);
        this.yearBandsMap = yearBandsList.stream()
                .sorted(Comparator.comparingInt(yearBand -> Integer.parseInt(yearBand.getMinYearBuilt())))
                .peek(yearBand -> yearBand.setNumber(number.getAndIncrement()))
                .collect(Collectors.toMap(YearBand::getNumber, Function.identity()));
    }


    @Override
    public YearBand getYearBand(int yearBuilt) {
        return yearBandsMap.values()
                .stream()
                .filter(yearBand -> yearBand.checkYear(yearBuilt))
                .findAny()
                .orElseThrow(() -> new IllegalStateException("Can't find appropriate year band"));
    }

    @Override
    public YearBand getYearBands(int yearBuilt, int deltaYearBands) {
        YearBand yearBand = getYearBand(yearBuilt);
        //Getting -1 year band min year
        String minYearBuilt = yearBand.getNumber() - deltaYearBands < 0
                ? yearBand.getMinYearBuilt()
                : yearBandsMap.get(yearBand.getNumber() - deltaYearBands).getMinYearBuilt();

        //Getting +1 year band max year
        String maxYearBuilt = yearBand.getNumber() + deltaYearBands > yearBandsMap.size() - 1
                ? yearBand.getMaxYearBuilt()
                : yearBandsMap.get(yearBand.getNumber() + deltaYearBands).getMaxYearBuilt();
        return new YearBand() {
            @Override
            public String getMinYearBuilt() {
                return minYearBuilt;
            }

            @Override
            public String getMaxYearBuilt() {
                return maxYearBuilt;
            }

            @Override
            public boolean checkYear(int yearBuilt) {
                throw new IllegalStateException("Year check can't be performed for a set of bands");
            }

            @Override
            public int getNumber() {
                throw new IllegalStateException("Set of bands can't have number");
            }

            @Override
            public void setNumber(int number) {
                throw new IllegalStateException("Set of bands can't have number");
            }
        };
    }

}
