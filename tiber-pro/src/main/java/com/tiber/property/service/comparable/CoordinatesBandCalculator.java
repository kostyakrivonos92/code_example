package com.tiber.property.service.comparable;

import static java.lang.Math.PI;

class CoordinatesBandCalculator {

    private final double longitude;

    private final double latitude;

    private final float distanceMile;

    private final double deltaInDegree;

    CoordinatesBandCalculator(double longitude, double latitude, float distanceMile) {
        this.longitude = longitude;
        this.latitude = latitude;
        this.distanceMile = distanceMile;
        deltaInDegree = getDeltaInDegree();
    }


    public String getMinLatitude() {
        double minLatitude = latitude - deltaInDegree;
        if (minLatitude < -90.0) {
            minLatitude = -90.0;
        }
        return String.valueOf(minLatitude);
    }

    public String getMaxLatitude() {
        double maxLatitude = latitude + deltaInDegree;
        if (maxLatitude > 90.0) {
            maxLatitude = 90.0;
        }
        return String.valueOf(maxLatitude);
    }

    public String getMinLongitude() {
        double minLongitude = longitude - deltaInDegree;
        if (minLongitude < -180.0) {
            minLongitude = minLongitude + 360.0;
        }
        return String.valueOf(minLongitude);
    }

    public String getMaxLongitude() {
        double maxLongitude = longitude + deltaInDegree;
        if (maxLongitude > 180.0) {
            maxLongitude = maxLongitude - 360.0;
        }
        return String.valueOf(maxLongitude);
    }

    private double getDeltaInDegree() {
        double earthRadiusMile = 3958.756;
        return 180 * distanceMile / PI / earthRadiusMile;
    }
}
