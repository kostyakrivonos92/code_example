package com.tiber.property.service.comparable;

import com.tiber.property.domain.PropertyAnalogue;
import com.tiber.property.service.PropertyAnalogueService;
import com.tiber.property.service.comparable.yearBand.YearBand;
import com.tiber.property.service.comparable.yearBand.YearBandService;
import com.tiber.shared.inspection.domain.PropertyInfo;
import com.tiber.shared.searchCriteria.domain.BaseSearchCriteria;
import org.javatuples.Pair;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.util.Collection;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashMap;
import java.util.List;
import java.util.Objects;
import java.util.stream.Collectors;
import java.util.stream.Stream;

import static com.tiber.shared.vocabulary.PropertyParameterName.CLOSE_DATE;
import static com.tiber.shared.vocabulary.PropertyParameterName.LATITUDE;
import static com.tiber.shared.vocabulary.PropertyParameterName.LONGITUDE;
import static com.tiber.shared.vocabulary.PropertyParameterName.YEAR_BUILT;

@Service("ok")
class OkAnaloguesSearchLinkService implements AnaloguesSearchLinkService {

    private final PropertyAnalogueService propertyAnalogueService;
    private final YearBandService yearBandService;
    private final Integer squareFeetPercent;
    private final Float distanceMile;
    private final Float filterDistanceMile;
    private final Integer edgeQuantityToApplyAdditionalFilters;
    private final Integer searchPeriodYears;
    private final Integer maxScore;
    private final int DELTA_YEAR_BANDS_FOR_OK_COMPS = 1;


    @Autowired
    OkAnaloguesSearchLinkService(PropertyAnalogueService propertyAnalogueService,
                                 YearBandService yearBandService,
                                 @Value("${application.okAnalogues.squareFeetPercent}") Integer squareFeetPercent,
                                 @Value("${application.okAnalogues.distanceMile}") Float distanceMile,
                                 @Value("${application.okAnalogues.filterDistanceMile}") Float filterDistanceMile,
                                 @Value("${application.okAnalogues.edgeQuantityToApplyAdditionalFilters}") Integer edgeQuantityToApplyAdditionalFilters,
                                 @Value("${application.okAnalogues.searchPeriodYears}") Integer searchPeriodYears,
                                 @Value("${application.okAnalogues.maxScore}") Integer maxScore) {
        this.propertyAnalogueService = propertyAnalogueService;
        this.yearBandService = yearBandService;
        this.squareFeetPercent = squareFeetPercent;
        this.distanceMile = distanceMile;
        this.filterDistanceMile = filterDistanceMile;
        this.edgeQuantityToApplyAdditionalFilters = edgeQuantityToApplyAdditionalFilters;
        this.searchPeriodYears = searchPeriodYears;
        this.maxScore = maxScore;
    }

    @Override
    public List<PropertyAnalogue> getAnaloguesForProperty(String uuid, List<PropertyAnalogue> currentComparableList, Integer analogueQuantity, PropertyInfo propertyInfo) {
        PropertyAnalogue basePropertyAnalogue = propertyAnalogueService.getPropertyAnalogueByUuid(uuid).blockingGet();
        if (Objects.isNull(basePropertyAnalogue)) {
            return Collections.emptyList();
        }
        if (propertyInfo != null) {
            basePropertyAnalogue.setBathrooms(propertyInfo.getBathrooms());
            basePropertyAnalogue.setBedrooms(propertyInfo.getBedrooms());
            basePropertyAnalogue.setAreaSqFt(propertyInfo.getAreaSqFt());
            basePropertyAnalogue.setYearBuilt(propertyInfo.getYearBuilt());
        }
        Collection<String> addedAnaloguesIds = currentComparableList.stream().map(PropertyAnalogue::getUuid).collect(Collectors.toList());
        final List<PropertyAnalogue> foundProperties = searchAnalogues(basePropertyAnalogue).stream()
                .filter(propertySummary -> !addedAnaloguesIds.contains(propertySummary.getUuid()))
                .filter(propertySummary -> !propertySummary.getUuid().equals(uuid))
                .peek(propertySummary -> propertySummary.setCompsLabel(OK_COMPS_LABEL))
                .collect(Collectors.toList());
        currentComparableList.addAll(foundProperties);
        return currentComparableList.stream()
                .limit(analogueQuantity).collect(Collectors.toList());
    }

    private Integer calculateScore(PropertyAnalogue basePropertyAnalogue, PropertyAnalogue propertyAnalogue) {
        int score = 0;
        final int onePoint = 1;
        final int twoPoints = 2;
        final int areaSquareFeetPercentForPoint = 15;

        int bedroomsDifference = Math.abs(basePropertyAnalogue.getBedrooms() - propertyAnalogue.getBedrooms());
        score += bedroomsDifference * twoPoints;

        int bathroomsDifference = Math.abs(basePropertyAnalogue.getBathrooms() - propertyAnalogue.getBathrooms());
        score += bathroomsDifference * twoPoints;

        int halfBathroomsDifference = Math.abs(basePropertyAnalogue.getHalfBathrooms() - propertyAnalogue.getHalfBathrooms());
        score += halfBathroomsDifference * onePoint;

        final int areaSqFt = basePropertyAnalogue.getAreaSqFt();
        int areaSquareFeetDifferencePercent = Math.abs(areaSqFt - propertyAnalogue.getAreaSqFt()) * 100 / areaSqFt;
        score += areaSquareFeetDifferencePercent / areaSquareFeetPercentForPoint * onePoint;

        YearBand baseYearBand = yearBandService.getYearBand(basePropertyAnalogue.getYearBuilt());
        YearBand yearBand = yearBandService.getYearBand(propertyAnalogue.getYearBuilt());
        int yearBandDifference = Math.abs(baseYearBand.getNumber() - yearBand.getNumber());
        score += yearBandDifference * twoPoints;

        score += propertyAnalogue.getDistance() / 0.2 * onePoint;

        return score;
    }

    private List<PropertyAnalogue> searchAnalogues(PropertyAnalogue basePropertyAnalogue) {

        List<Pair<PropertyAnalogue, Integer>> analogueScorePairList = propertyAnalogueService.getPropertyAnalogueList(getCriteria(basePropertyAnalogue))
                .map(propertyAnalogue -> Pair.with(propertyAnalogue, calculateScore(basePropertyAnalogue, propertyAnalogue)))
                .filter(analogueToScorePair -> analogueToScorePair.getValue1() <= maxScore)
                .doOnNext(analogueScorePair -> analogueScorePair.getValue0().setDistance(DistanceCalculator.getDistance(
                        basePropertyAnalogue.getLatitude(),
                        basePropertyAnalogue.getLongitude(),
                        analogueScorePair.getValue0().getLatitude(),
                        analogueScorePair.getValue0().getLongitude())))
                .collect(Collectors.toList())
                .blockingGet();
        SquareFeetBandCalculator squareFeetBandCalculator = new SquareFeetBandCalculator(squareFeetPercent, basePropertyAnalogue.getAreaSqFt());

        Stream<Pair<PropertyAnalogue, Integer>> stream = analogueScorePairList.size() < edgeQuantityToApplyAdditionalFilters
                ? analogueScorePairList.stream()
                : analogueScorePairList.stream()
                .filter(analogueScorePair -> squareFeetBandCalculator.isSquareFeetInBand(analogueScorePair.getValue1()))
                .filter(analogueScorePair -> filterDistanceMile >= analogueScorePair.getValue0().getDistance());
        return stream.sorted(Comparator.comparingInt(Pair::getValue1))
                .map(Pair::getValue0)
                .collect(Collectors.toList());
    }

    private BaseSearchCriteria getCriteria(PropertyAnalogue propertyAnalogue) {
        BaseSearchCriteria baseSearchCriteria = new BaseSearchCriteria();
        HashMap<String, String> graterThanOrEqualMap = new HashMap<>();
        YearBand yearBand = yearBandService.getYearBands(propertyAnalogue.getYearBuilt(), DELTA_YEAR_BANDS_FOR_OK_COMPS);
        graterThanOrEqualMap.put(YEAR_BUILT, yearBand.getMinYearBuilt());
        CoordinatesBandCalculator coordinatesBandCalculator = new CoordinatesBandCalculator(propertyAnalogue.getLongitude(),
                propertyAnalogue.getLatitude(),
                distanceMile);
        graterThanOrEqualMap.put(LONGITUDE, coordinatesBandCalculator.getMinLongitude());
        graterThanOrEqualMap.put(LATITUDE, coordinatesBandCalculator.getMinLatitude());
        graterThanOrEqualMap.put(CLOSE_DATE, LocalDate.now().minusYears(searchPeriodYears).format(DateTimeFormatter.ISO_DATE));
        baseSearchCriteria.setGreaterThanOrEqualMap(graterThanOrEqualMap);

        HashMap<String, String> lessThanOrEqualMap = new HashMap<>();
        lessThanOrEqualMap.put(LONGITUDE, coordinatesBandCalculator.getMaxLongitude());
        lessThanOrEqualMap.put(LATITUDE, coordinatesBandCalculator.getMaxLatitude());
        lessThanOrEqualMap.put(YEAR_BUILT, yearBand.getMaxYearBuilt());
        baseSearchCriteria.setLessThanOrEqualMap(lessThanOrEqualMap);
        return baseSearchCriteria;
    }

}
