package com.tiber.property.service.comparable;

import com.tiber.property.domain.PropertyAnalogue;
import com.tiber.property.service.PropertyAnalogueService;
import com.tiber.property.service.comparable.yearBand.YearBand;
import com.tiber.property.service.comparable.yearBand.YearBandService;
import com.tiber.shared.inspection.domain.PropertyInfo;
import com.tiber.shared.searchCriteria.domain.BaseSearchCriteria;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.util.Collection;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Objects;
import java.util.stream.Collectors;

import static com.tiber.shared.vocabulary.PropertyParameterName.BATHROOMS;
import static com.tiber.shared.vocabulary.PropertyParameterName.BEDROOMS;
import static com.tiber.shared.vocabulary.PropertyParameterName.CLOSE_DATE;
import static com.tiber.shared.vocabulary.PropertyParameterName.LATITUDE;
import static com.tiber.shared.vocabulary.PropertyParameterName.LONGITUDE;
import static com.tiber.shared.vocabulary.PropertyParameterName.YEAR_BUILT;

@Service("good")
class GoodAnaloguesSearchLinkService implements AnaloguesSearchLinkService {

    private final AnaloguesSearchLinkService analoguesSearchLinkService;
    private final PropertyAnalogueService propertyAnalogueService;
    private final YearBandService yearBandService;
    private final Float distanceMile;
    private final Integer searchPeriodYears;


    @Autowired
    GoodAnaloguesSearchLinkService(@Qualifier("ok") AnaloguesSearchLinkService analoguesSearchLinkService,
                                   PropertyAnalogueService propertyAnalogueService,
                                   YearBandService yearBandService,
                                   @Value("${application.goodAnalogues.distanceMile}") Float distanceMile,
                                   @Value("${application.goodAnalogues.searchPeriodYears}") Integer searchPeriodYears) {
        this.analoguesSearchLinkService = analoguesSearchLinkService;
        this.propertyAnalogueService = propertyAnalogueService;
        this.yearBandService = yearBandService;
        this.distanceMile = distanceMile;
        this.searchPeriodYears = searchPeriodYears;
    }

    @Override
    public List<PropertyAnalogue> getAnaloguesForProperty(String uuid, List<PropertyAnalogue> currentComparableList, Integer analogueQuantity, PropertyInfo propertyInfo) {
        Collection<String> addedAnaloguesIdList = currentComparableList.stream().map(PropertyAnalogue::getUuid).collect(Collectors.toList());
        PropertyAnalogue basePropertyAnalogue = propertyAnalogueService.getPropertyAnalogueByUuid(uuid).blockingGet();
        if (Objects.isNull(basePropertyAnalogue)) {
            return Collections.emptyList();
        }
        if (propertyInfo != null) {
            basePropertyAnalogue.setBathrooms(propertyInfo.getBathrooms());
            basePropertyAnalogue.setBedrooms(propertyInfo.getBedrooms());
            basePropertyAnalogue.setAreaSqFt(propertyInfo.getAreaSqFt());
            basePropertyAnalogue.setYearBuilt(propertyInfo.getYearBuilt());
        }
        final List<PropertyAnalogue> result = searchAnalogues(basePropertyAnalogue).stream()
                .filter(propertyAnalogue -> !propertyAnalogue.getUuid().equals(uuid))
                .filter(propertyAnalogue -> !addedAnaloguesIdList.contains(propertyAnalogue.getUuid()))
                .peek(propertyAnalogue -> propertyAnalogue.setCompsLabel(GOOD_COMPS_LABEL))
                .collect(Collectors.toList());
        currentComparableList.addAll(result);
        if (currentComparableList.size() >= analogueQuantity) {
            return currentComparableList.stream()
                    .limit(analogueQuantity).collect(Collectors.toList());
        }
        return analoguesSearchLinkService.getAnaloguesForProperty(uuid, currentComparableList, analogueQuantity, propertyInfo);
    }

    private List<PropertyAnalogue> searchAnalogues(PropertyAnalogue basePropertyAnalogue) {
        return propertyAnalogueService.getPropertyAnalogueList(getCriteria(basePropertyAnalogue))
                .doOnNext(propertyAnalogue -> propertyAnalogue.setDistance(
                        DistanceCalculator.getDistance(
                                basePropertyAnalogue.getLatitude(),
                                basePropertyAnalogue.getLongitude(),
                                propertyAnalogue.getLatitude(),
                                propertyAnalogue.getLongitude()))
                )
                .filter(propertyAnalogue -> propertyAnalogue.getDistance() <= distanceMile)
                .collect(Collectors.toList())
                .blockingGet();
    }

    private BaseSearchCriteria getCriteria(PropertyAnalogue propertyAnalogue) {
        BaseSearchCriteria baseSearchCriteria = new BaseSearchCriteria();
        HashMap<String, String> equalMap = new HashMap<>();
        equalMap.put(BEDROOMS, Integer.toString(propertyAnalogue.getBedrooms()));
        equalMap.put(BATHROOMS, Integer.toString(propertyAnalogue.getBathrooms()));
        baseSearchCriteria.setEqualMap(equalMap);

        HashMap<String, String> graterThanOrEqualMap = new HashMap<>();
        YearBand yearBand = yearBandService.getYearBand(propertyAnalogue.getYearBuilt());
        graterThanOrEqualMap.put(YEAR_BUILT, yearBand.getMinYearBuilt());
        CoordinatesBandCalculator coordinatesBandCalculator = new CoordinatesBandCalculator(propertyAnalogue.getLongitude(),
                propertyAnalogue.getLatitude(),
                distanceMile);
        graterThanOrEqualMap.put(LONGITUDE, coordinatesBandCalculator.getMinLongitude());
        graterThanOrEqualMap.put(LATITUDE, coordinatesBandCalculator.getMinLatitude());
        graterThanOrEqualMap.put(CLOSE_DATE, LocalDate.now().minusYears(searchPeriodYears).format(DateTimeFormatter.ISO_DATE));
        baseSearchCriteria.setGreaterThanOrEqualMap(graterThanOrEqualMap);

        HashMap<String, String> lessThanOrEqualMap = new HashMap<>();
        lessThanOrEqualMap.put(YEAR_BUILT, yearBand.getMaxYearBuilt());
        lessThanOrEqualMap.put(LONGITUDE, coordinatesBandCalculator.getMaxLongitude());
        lessThanOrEqualMap.put(LATITUDE, coordinatesBandCalculator.getMaxLatitude());
        baseSearchCriteria.setLessThanOrEqualMap(lessThanOrEqualMap);

        return baseSearchCriteria;
    }
}
