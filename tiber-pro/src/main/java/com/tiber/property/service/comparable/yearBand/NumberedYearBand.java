package com.tiber.property.service.comparable.yearBand;

abstract class NumberedYearBand implements YearBand {

    protected int number;

    @Override
    public int getNumber() {
        return number;
    }

    @Override
    public void setNumber(int number) {
        this.number = number;
    }
}
