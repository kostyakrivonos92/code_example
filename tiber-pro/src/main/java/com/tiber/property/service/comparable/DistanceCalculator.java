package com.tiber.property.service.comparable;

class DistanceCalculator {

    public static float getDistance(double latitude1, double longitude1, double latitude2, double longitude2) {
        final double earthRadiusMile = 3958.756;
        double centralCornerHaversin = getHaversin(Math.toRadians(latitude2 - latitude1))
                + Math.cos(Math.toRadians(latitude1)) * Math.cos(Math.toRadians(latitude2)) * getHaversin(Math.toRadians(longitude2 - longitude1));
        double distance = earthRadiusMile * 2 * Math.asin(Math.sqrt(centralCornerHaversin));
        return (float) distance;
    }

    private static double getHaversin(double cornerInRadians) {
        return Math.sin(cornerInRadians / 2) * Math.sin(cornerInRadians / 2);
    }
}
