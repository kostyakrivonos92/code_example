package com.tiber.property.service;

import com.tiber.shared.inspection.domain.PropertyInfo;
import com.tiber.shared.vocabulary.inspection.InspectionType;
import io.reactivex.rxjava3.core.Maybe;

public interface TiberBudgetInspectionService {

    Maybe<PropertyInfo> getInspectionDataByPropertyUuidAndInspectionType(String propertyUuid, InspectionType inspectionType);
}
