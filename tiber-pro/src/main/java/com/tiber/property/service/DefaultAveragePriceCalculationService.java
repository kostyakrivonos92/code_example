package com.tiber.property.service;

import com.tiber.property.domain.AnaloguesSelection;
import com.tiber.property.domain.PropertyAnalogue;
import com.tiber.property.domain.StrategyDependentInitialData;
import com.tiber.shared.util.AnalogueRenovationTag;
import com.tiber.shared.util.RenovationType;
import org.springframework.stereotype.Service;

import java.math.BigDecimal;
import java.math.RoundingMode;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Objects;
import java.util.Optional;
import java.util.function.Function;
import java.util.stream.Collectors;

@Deprecated
@Service
public class DefaultAveragePriceCalculationService implements AveragePriceCalculationService {

    @Override
    public Map<RenovationType, StrategyDependentInitialData> getAnalogueCalculatedIndexesByStrategy(List<PropertyAnalogue> analogueList,
                                                                                                    AnaloguesSelection analoguesSelection) {
        final Map<Boolean, List<PropertyAnalogue>> saleRentAnaloguesMap = analogueList.stream()
                .collect(Collectors.groupingBy(PropertyAnalogue::isRent));


        final List<PropertyAnalogue> propertyAnalogueListForSale = Optional.ofNullable(saleRentAnaloguesMap.get(false)).orElse(new ArrayList<>());
        final BigDecimal projectedSalesPriceAll = calculateAveragePrice(propertyAnalogueListForSale, analoguesSelection);
        final List<PropertyAnalogue> propertyAnalogueListForRent = Optional.ofNullable(saleRentAnaloguesMap.get(true)).orElse(new ArrayList<>());
        final BigDecimal rentPriceAll = calculateAveragePrice(propertyAnalogueListForRent, analoguesSelection);
        final StrategyDependentInitialData strategyDependentInitialData = new StrategyDependentInitialData();
        strategyDependentInitialData.setProjectedSalesPrice(projectedSalesPriceAll);
        strategyDependentInitialData.setRent(rentPriceAll);
        final Map<RenovationType, StrategyDependentInitialData> result = new HashMap<>();
        result.put(RenovationType.ALL, strategyDependentInitialData);

        final Collection<AnalogueRenovationTag> analogueSpecType = Arrays.asList(AnalogueRenovationTag.TIBER_SPEC, AnalogueRenovationTag.MARKET_SPEC);
        final StrategyDependentInitialData strategyDependentInitialDataSpec = getStrategyDependentInitialData(analoguesSelection,
                propertyAnalogueListForSale, propertyAnalogueListForRent, analogueSpecType);
        result.put(RenovationType.SPEC, strategyDependentInitialDataSpec);

        final Collection<AnalogueRenovationTag> analogueStockType = Arrays.asList(AnalogueRenovationTag.TIBER_STOCK, AnalogueRenovationTag.MARKET_STOCK);
        final StrategyDependentInitialData strategyDependentInitialDataStock = getStrategyDependentInitialData(analoguesSelection, propertyAnalogueListForSale, propertyAnalogueListForRent, analogueStockType);
        result.put(RenovationType.STOCK, strategyDependentInitialDataStock);

        return result;
    }

    private StrategyDependentInitialData getStrategyDependentInitialData(AnaloguesSelection analoguesSelection,
                                                                         List<PropertyAnalogue> propertyAnalogueListForSale,
                                                                         List<PropertyAnalogue> propertyAnalogueListForRent,
                                                                         Collection<AnalogueRenovationTag> analogueStockType) {
        final List<PropertyAnalogue> propertyAnalogueRentStockList = propertyAnalogueListForRent.stream()
                .filter(propertyAnalogue -> analogueStockType.contains(propertyAnalogue.getRenovationTag()))
                .collect(Collectors.toList());
        final BigDecimal rentPriceStock = calculateAveragePrice(propertyAnalogueRentStockList, analoguesSelection);
        final List<PropertyAnalogue> propertyAnalogueSaleStockList = propertyAnalogueListForSale.stream()
                .filter(propertyAnalogue -> analogueStockType.contains(propertyAnalogue.getRenovationTag()))
                .collect(Collectors.toList());
        final BigDecimal projectedSalesPriceStock = calculateAveragePrice(propertyAnalogueSaleStockList, analoguesSelection);
        final StrategyDependentInitialData strategyDependentInitialDataStock = new StrategyDependentInitialData();
        strategyDependentInitialDataStock.setProjectedSalesPrice(projectedSalesPriceStock);
        strategyDependentInitialDataStock.setRent(rentPriceStock);
        return strategyDependentInitialDataStock;
    }

    private BigDecimal calculateAveragePrice(List<PropertyAnalogue> propertyAnalogueList, AnaloguesSelection analoguesSelection) {
        final Map<String, PropertyAnalogue> stringListMap = propertyAnalogueList.stream()
                .collect(Collectors.toMap(PropertyAnalogue::getUuid, Function.identity()));
        final List<PropertyAnalogue> propertyAnaloguesToCalculate = Optional.ofNullable(analoguesSelection.getSpec())
                .filter(strings -> !strings.isEmpty())
                .map(list -> list.stream()
                        .map(stringListMap::get).collect(Collectors.toList()))
                .orElse(propertyAnalogueList);
        if (propertyAnaloguesToCalculate.isEmpty()) {
            return BigDecimal.ZERO;
        }
        final BigDecimal sum = propertyAnaloguesToCalculate.stream().filter(Objects::nonNull)
                .map(PropertyAnalogue::getPrice)
                .reduce(BigDecimal::add)
                .orElse(BigDecimal.ZERO);
        return sum.divide(BigDecimal.valueOf(propertyAnaloguesToCalculate.size()), RoundingMode.HALF_UP);
    }
}
