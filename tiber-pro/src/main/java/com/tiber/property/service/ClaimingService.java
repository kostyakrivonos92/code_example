package com.tiber.property.service;

import com.tiber.property.domain.DealSummary;

public interface ClaimingService {

    DealSummary claimActiveDealInProperty(String propertyUuid, String responsibleAnalystUuid, String claimedByAnalystUuid);
}
