package com.tiber.property.service;

import com.tiber.property.domain.PropertyAnalogue;
import com.tiber.shared.searchCriteria.domain.BaseSearchCriteria;
import io.reactivex.rxjava3.core.Flowable;
import io.reactivex.rxjava3.core.Maybe;

public interface PropertyAnalogueService {

    Flowable<PropertyAnalogue> getPropertyAnalogueList(BaseSearchCriteria criteria);

    Maybe<PropertyAnalogue> getPropertyAnalogueByUuid(String uuid);
}
