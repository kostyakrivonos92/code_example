package com.tiber.property.service.comparable.yearBand;

import org.springframework.stereotype.Component;

@Component
class SeventiesYearBand extends NumberedYearBand {

    private final int minYear = 1970;

    private final int maxYear = 1979;

    @Override
    public String getMinYearBuilt() {
        return Integer.toString(minYear);
    }

    @Override
    public String getMaxYearBuilt() {
        return Integer.toString(maxYear);
    }

    @Override
    public boolean checkYear(int yearBuilt) {
        return yearBuilt >= minYear && yearBuilt <= maxYear;
    }
}
