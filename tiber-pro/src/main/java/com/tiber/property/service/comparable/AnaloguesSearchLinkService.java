package com.tiber.property.service.comparable;

import com.tiber.property.domain.PropertyAnalogue;
import com.tiber.shared.inspection.domain.PropertyInfo;

import java.util.List;

//TODO think about moving from List to Flowable
interface AnaloguesSearchLinkService {
    String PERFECT_COMPS_LABEL = "PC";
    String GOOD_COMPS_LABEL = "GC";
    String OK_COMPS_LABEL = "OC";

    List<PropertyAnalogue> getAnaloguesForProperty(String uuid, List<PropertyAnalogue> currentComparableList, Integer analogueQuantity, PropertyInfo propertyInfo);
}
