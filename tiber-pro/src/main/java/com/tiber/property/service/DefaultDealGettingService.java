package com.tiber.property.service;

import com.tiber.property.domain.DealSummary;
import com.tiber.property.domain.DealSummaryPage;
import com.tiber.property.domain.UnderwritingModel;
import com.tiber.property.mapper.DealSummaryMapper;
import com.tiber.shared.file.domain.FileObject;
import com.tiber.shared.propertylayer.domain.Deal;
import com.tiber.shared.propertylayer.service.PropertyLayerService;
import com.tiber.shared.searchCriteria.domain.PagedBaseSearchCriteria;
import com.tiber.shared.status.domain.StatusEvent;
import com.tiber.shared.vocabulary.DealStatus;
import io.reactivex.rxjava3.core.Maybe;
import io.reactivex.rxjava3.core.Single;
import java.util.LinkedHashSet;
import lombok.RequiredArgsConstructor;
import lombok.extern.log4j.Log4j2;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.function.Function;
import java.util.stream.Collectors;

/**
 * Default implementation of DealGettingService
 */
@Log4j2
@Service
@RequiredArgsConstructor(onConstructor_ = @Autowired)
class DefaultDealGettingService implements DealGettingService {

    private final UnderwritingModelService underwritingModelService;

    private final DealSummaryMapper dealSummaryMapper;

    private final PropertyLayerService propertyLayerService;

    @Override
    public Single<DealSummaryPage> getDealSummaryPage(PagedBaseSearchCriteria criteria) {
        DealSummaryPage dealSummaryPage = new DealSummaryPage(criteria.getPage(), criteria.getPageSize(), 0L, new LinkedList<>());
        Single<DealSummaryPage> dealSummaryPageSingle = propertyLayerService.getPropertyPage(criteria)
                .map(propertyOnMarketPage -> {
                    dealSummaryPage.getDeals()
                            .addAll(dealSummaryMapper.mapToDealSummaryList(propertyOnMarketPage.getProperties()));
                    Map<String, DealSummary> dealSummaryMap = new LinkedHashSet<>(dealSummaryPage.getDeals()).stream()
                            .collect(Collectors.toMap(DealSummary::getPropertyUuid, Function.identity()));
                    underwritingModelService.findByPropertyUuidCollection(dealSummaryMap.keySet())
                            .forEach(underwritingModel -> {
                                DealSummary dealSummary = dealSummaryMap.get(underwritingModel.getPropertyUuid());
                                dealSummary.setSalesDate(underwritingModel.getSalesDate());
                                dealSummary.setModelUuid(underwritingModel.getUuid());
                            });
                    dealSummaryPage.setMainCounter(propertyOnMarketPage.getMainCounter());
                    dealSummaryPage.setPage(propertyOnMarketPage.getPage());
                    dealSummaryPage.setPageSize(propertyOnMarketPage.getPageSize());
                    return dealSummaryPage;
                });
        return dealSummaryPageSingle;
    }

    @Override
    public Maybe<DealSummary> getDealSummaryByPropertyUuid(String uuid) {
        List<UnderwritingModel> model = underwritingModelService.findByPropertyUuid(uuid);
        return propertyLayerService.getPropertyByUuid(uuid)
                .map(propertyOnMarket -> {
                    Deal latestDeal = propertyOnMarket.getLatestDeal();
                    DealSummary dealSummary = model.stream()
                            .filter(underwritingModel -> underwritingModel.getDealUuid().equals(latestDeal.getUuid()))
                            .findAny()
                            .map(underwritingModel -> dealSummaryMapper.mapToDealSummary(propertyOnMarket, underwritingModel))
                            .orElseGet(() -> dealSummaryMapper.dealSummaryFromPropertyOnMarket(propertyOnMarket));
                    return dealSummary;
                });
    }

    @Override
    public Maybe<DealSummary> claimActiveDealInProperty(String propertyUuid, String responsibleAnalystUuid, String claimedByAnalystUuid) {
        List<UnderwritingModel> model = underwritingModelService.findByPropertyUuid(propertyUuid);
        return propertyLayerService.claimActiveDealInProperty(propertyUuid, responsibleAnalystUuid, claimedByAnalystUuid)
                .map(propertyGeneral -> {
                    Deal latestDeal = propertyGeneral.getLatestDeal();
                    DealSummary dealSummary = model.stream()
                            .filter(underwritingModel -> underwritingModel.getDealUuid().equals(latestDeal.getUuid()))
                            .findAny()
                            .map(underwritingModel -> dealSummaryMapper.mapToDealSummary(propertyGeneral, underwritingModel))
                            .orElseGet(() -> dealSummaryMapper.dealSummaryFromPropertyOnMarket(propertyGeneral));
                    return dealSummary;
                });
    }

    @Override
    public Maybe<DealSummary> updateLatestDealStatus(String propertyUuid, DealStatus dealStatus,
                                                     String emittedByUuid, String comment, String addresseeUuid) {
        StatusEvent<DealStatus> dealStatusEvent = new StatusEvent<>(dealStatus, emittedByUuid, comment, addresseeUuid);
        return propertyLayerService.updateLatestDealStatus(propertyUuid, dealStatusEvent)
                .map(dealSummaryMapper::dealSummaryFromPropertyOnMarket);
    }

    @Override
    public Maybe<List<FileObject>> getFileObjectListByDealUuid(String propertyUuid) {
        return propertyLayerService.getPropertyByUuid(propertyUuid)
                .map(propertyOnMarket -> Optional.ofNullable(propertyOnMarket.getLatestDeal().getFiles()).orElse(new ArrayList<>()));
    }
}
