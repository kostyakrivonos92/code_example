package com.tiber.property.service;

import com.tiber.property.domain.StrategyDependentResultingData;
import com.tiber.property.domain.UnderwritingModel;
import com.tiber.shared.util.RenovationType;
import org.springframework.stereotype.Service;

import java.util.HashMap;
import java.util.Random;

@Service
public class DummyRentalCalculationService implements RentalCalculationService {
    @Override
    public UnderwritingModel calculate(UnderwritingModel underwritingModel) {
        final HashMap<RenovationType, StrategyDependentResultingData> renovationTypeStrategyDependentResultingDataHashMap =
                new HashMap<>();
        final StrategyDependentResultingData strategyDependentResultingData = new StrategyDependentResultingData();
        strategyDependentResultingData.setTargetYield(new Random().nextFloat());
        strategyDependentResultingData.setLoanToCostRatio(new Random().nextFloat());
        strategyDependentResultingData.setInternalRateOfReturn(new Random().nextFloat());
        renovationTypeStrategyDependentResultingDataHashMap.put(RenovationType.ALL,
                strategyDependentResultingData);
        renovationTypeStrategyDependentResultingDataHashMap.put(RenovationType.STOCK,
                strategyDependentResultingData);
        renovationTypeStrategyDependentResultingDataHashMap.put(RenovationType.SPEC,
                strategyDependentResultingData);
        underwritingModel.setStrategyDependentResultingDataMap(renovationTypeStrategyDependentResultingDataHashMap);
        return underwritingModel;
    }
}
