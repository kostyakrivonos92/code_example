package com.tiber.property.service;

import com.tiber.property.domain.DealSummary;
import com.tiber.property.domain.DealSummaryPage;
import com.tiber.shared.file.domain.FileObject;
import com.tiber.shared.searchCriteria.domain.PagedBaseSearchCriteria;
import com.tiber.shared.vocabulary.DealStatus;
import io.reactivex.rxjava3.core.Maybe;
import io.reactivex.rxjava3.core.Single;

import java.util.List;

//TODO rename to DealService when propertyLayer is run as separate application. For now named as "getting" because of injection issue
public interface DealGettingService {

    Single<DealSummaryPage> getDealSummaryPage(PagedBaseSearchCriteria criteria);

    Maybe<DealSummary> getDealSummaryByPropertyUuid(String uuid);

    Maybe<DealSummary> claimActiveDealInProperty(String propertyUuid, String responsibleAnalystUuid, String claimedByAnalystUuid);

    Maybe<DealSummary> updateLatestDealStatus(String propertyUuid, DealStatus dealStatus, String emittedByUuid, String comment, String addresseeUuid);

    Maybe<List<FileObject>> getFileObjectListByDealUuid(String propertyUuid);
}
