package com.tiber.property.service.comparable.yearBand;

public interface YearBand {

    String getMinYearBuilt();

    String getMaxYearBuilt();

    boolean checkYear(int yearBuilt);

    int getNumber();

    void setNumber(int number);
}
