package com.tiber.property.service.comparable;

import com.tiber.property.domain.PropertyAnalogue;
import com.tiber.property.service.PropertyAnalogueService;
import com.tiber.property.service.comparable.yearBand.YearBand;
import com.tiber.property.service.comparable.yearBand.YearBandService;
import com.tiber.shared.inspection.domain.PropertyInfo;
import com.tiber.shared.searchCriteria.domain.BaseSearchCriteria;
import org.javatuples.Pair;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

import java.math.BigDecimal;
import java.math.RoundingMode;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Comparator;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Objects;
import java.util.stream.Collectors;

import static com.tiber.shared.vocabulary.PropertyParameterName.BATHROOMS;
import static com.tiber.shared.vocabulary.PropertyParameterName.BEDROOMS;
import static com.tiber.shared.vocabulary.PropertyParameterName.LATITUDE;
import static com.tiber.shared.vocabulary.PropertyParameterName.LONGITUDE;
import static com.tiber.shared.vocabulary.PropertyParameterName.SQUARE_FEET;
import static com.tiber.shared.vocabulary.PropertyParameterName.YEAR_BUILT;

/**
 * @deprecated for now all tags are non-tagged according to requirements, don't need separate service.
 * Think about replace it with OtherAnaloguesSearchLinkService if needed
 */
@Service("non-tagged")
class NonTaggedAnaloguesSearchLinkService implements AnaloguesSearchLinkService {

    private final PropertyAnalogueService propertyAnalogueService;
    private final YearBandService yearBandService;
    private final Integer squareFeetPercent;
    private final Integer squareFeetPercentStepTwo;
    private final Integer squareFeetPercentStepThree;
    private final float[] stepThreeDistances;
    private final Integer analogueQuantity;
    private final Float distanceMile;

    private static final String NON_TAGGED_COMPS_LABEL = "NTC";
    private static final int STEP_ONE_NUMBER = 1;
    private static final int STEP_TWO_NUMBER = 2;
    private static final int STEP_THREE_NUMBER = 3;

    @Autowired
    public NonTaggedAnaloguesSearchLinkService(PropertyAnalogueService propertyAnalogueService,
                                               YearBandService yearBandService,
                                               @Value("${application.nonTaggedAnalogues.squareFeetPercent:10}") Integer squareFeetPercent,
                                               @Value("${application.nonTaggedAnalogues.squareFeetPercentStepTwo:15}") Integer squareFeetPercentStepTwo,
                                               @Value("${application.nonTaggedAnalogues.squareFeetPercentStepThree:20}") Integer squareFeetPercentStepThree,
                                               @Value("${application.nonTaggedAnalogues.stepThreeDistances:0.5,0.8,1,2}") float[] stepThreeDistances,
                                               @Value("${application.analogueQuantity:20}") Integer analogueQuantity,
                                               @Value("${application.nonTaggedAnalogues.distanceMile:2}") Float distanceMile) {
        this.yearBandService = yearBandService;
        this.propertyAnalogueService = propertyAnalogueService;
        this.squareFeetPercent = squareFeetPercent;
        this.squareFeetPercentStepTwo = squareFeetPercentStepTwo;
        this.squareFeetPercentStepThree = squareFeetPercentStepThree;
        this.analogueQuantity = analogueQuantity;
        this.distanceMile = distanceMile;
        this.stepThreeDistances = stepThreeDistances;
    }

    @Override
    public List<PropertyAnalogue> getAnaloguesForProperty(String uuid, List<PropertyAnalogue> currentComparableList, Integer analogueQuantity, PropertyInfo propertyInfo) {
        final PropertyAnalogue propertyToSearchAnalogs = propertyAnalogueService.getPropertyAnalogueByUuid(uuid).blockingGet();
        if (Objects.isNull(propertyToSearchAnalogs)) {
            return new ArrayList<>();
        }
        if (propertyInfo != null) {
            propertyToSearchAnalogs.setBathrooms(propertyInfo.getBathrooms());
            propertyToSearchAnalogs.setBedrooms(propertyInfo.getBedrooms());
            propertyToSearchAnalogs.setAreaSqFt(propertyInfo.getAreaSqFt());
            propertyToSearchAnalogs.setYearBuilt(propertyInfo.getYearBuilt());
        }
        ;
        List<PropertyAnalogue> nonTaggedComparablePropertiesList = new ArrayList<>();
        int missingItemsCount = this.analogueQuantity - currentComparableList.size();

        //step 1 search
        nonTaggedComparablePropertiesList.addAll(searchAnalogsStepOne(propertyToSearchAnalogs, currentComparableList));
        if (nonTaggedComparablePropertiesList.size() >= missingItemsCount) {
            currentComparableList.addAll(filterPropertiesByScores(nonTaggedComparablePropertiesList,
                    yearBandService.getYearBand(propertyToSearchAnalogs.getYearBuilt()),
                    missingItemsCount));
            return currentComparableList;
        }

        // step 2, additional, if total NTC < required items qty
        List<PropertyAnalogue> stepTwoResult = searchAnalogsStepTwo(propertyToSearchAnalogs, currentComparableList);
        stepTwoResult.removeAll(nonTaggedComparablePropertiesList);
        nonTaggedComparablePropertiesList.addAll(stepTwoResult);

        if (nonTaggedComparablePropertiesList.size() >= missingItemsCount) {
            currentComparableList.addAll(filterPropertiesByScores(
                    nonTaggedComparablePropertiesList,
                    yearBandService.getYearBand(propertyToSearchAnalogs.getYearBuilt()),
                    missingItemsCount));
            return currentComparableList;
        }

        //step 3 4-loops
        List<PropertyAnalogue> stepThreeResult = searchAnalogsStepThree(propertyToSearchAnalogs,
                nonTaggedComparablePropertiesList, currentComparableList);
        nonTaggedComparablePropertiesList.addAll(stepThreeResult);
        if (nonTaggedComparablePropertiesList.size() >= missingItemsCount) {
            currentComparableList.addAll(filterPropertiesByScores(
                    nonTaggedComparablePropertiesList,
                    yearBandService.getYearBand(propertyToSearchAnalogs.getYearBuilt()),
                    missingItemsCount));
            return currentComparableList;
        }

        //step 4 all ntc min distance
        int otherCompsMissingQty = missingItemsCount - nonTaggedComparablePropertiesList.size();
        List<PropertyAnalogue> otherNonTaggedComps = findOtherNonTaggedComps(propertyToSearchAnalogs,
                currentComparableList, nonTaggedComparablePropertiesList, otherCompsMissingQty);
        nonTaggedComparablePropertiesList.addAll(otherNonTaggedComps);
        currentComparableList.addAll(nonTaggedComparablePropertiesList);

        return currentComparableList;
    }

    private List<PropertyAnalogue> searchAnalogues(PropertyAnalogue property, float distance, int step) {
        return searchAnalogues(property, getSearchCriteriaForStep(property, distance, step));
    }

    private List<PropertyAnalogue> searchAnalogues(PropertyAnalogue property, BaseSearchCriteria criteria) {
        return propertyAnalogueService.getPropertyAnalogueList(criteria).
                doOnNext(propertyAnalogue -> propertyAnalogue.setDistance(
                        DistanceCalculator.getDistance(
                                property.getLatitude(),
                                property.getLongitude(),
                                propertyAnalogue.getLatitude(),
                                propertyAnalogue.getLongitude()))
                )
                .filter(propertyAnalogue -> propertyAnalogue.getDistance() <= distanceMile)
                .collect(Collectors.toList())
                .blockingGet();
    }

    // Step 1 criteria, primary
    private BaseSearchCriteria getCriteriaStepOne(PropertyAnalogue propertySummary, int squareFeetPercent, float distanceMile) {
        BaseSearchCriteria baseSearchCriteria = new BaseSearchCriteria();

        SquareFeetBandCalculator squareFeetBandCalculator = new SquareFeetBandCalculator(squareFeetPercent, propertySummary.getAreaSqFt());
        CoordinatesBandCalculator coordinatesBandCalculator = new CoordinatesBandCalculator(propertySummary.getLatitude(), propertySummary.getLongitude(), distanceMile);
        YearBand yearBand = yearBandService.getYearBands(propertySummary.getYearBuilt(), 1);

        //bedrooms & bathrooms eq map
        HashMap<String, String> equalMap = new HashMap<>();
        equalMap.put(BEDROOMS, Integer.toString(propertySummary.getBedrooms()));
        equalMap.put(BATHROOMS, Integer.toString(propertySummary.getBathrooms()));
        baseSearchCriteria.setEqualMap(equalMap);

        baseSearchCriteria.setLessThanOrEqualMap(getFilledRequiredMap(
                Pair.with(YEAR_BUILT, yearBand.getMaxYearBuilt()),
                Pair.with(SQUARE_FEET, squareFeetBandCalculator.getMinSquareFeet()),
                Pair.with(LATITUDE, coordinatesBandCalculator.getMinLatitude()),
                Pair.with(LONGITUDE, coordinatesBandCalculator.getMinLongitude())
        ));

        baseSearchCriteria.setGreaterThanOrEqualMap(getFilledRequiredMap(
                Pair.with(YEAR_BUILT, yearBand.getMaxYearBuilt()),
                Pair.with(SQUARE_FEET, squareFeetBandCalculator.getMaxSquareFeet()),
                Pair.with(LATITUDE, coordinatesBandCalculator.getMaxLatitude()),
                Pair.with(LONGITUDE, coordinatesBandCalculator.getMaxLongitude())
        ));
        return baseSearchCriteria;
    }

    // Step two criteria, additional if not enough comps from step 1
    private BaseSearchCriteria getCriteriaStepTwo(PropertyAnalogue propertySummary, int squareFeetPercent, float distanceMile) {
        BaseSearchCriteria baseSearchCriteria = new BaseSearchCriteria();

        SquareFeetBandCalculator squareFeetBandCalculator = new SquareFeetBandCalculator(squareFeetPercent, propertySummary.getAreaSqFt());
        CoordinatesBandCalculator coordinatesBandCalculator = new CoordinatesBandCalculator(propertySummary.getLatitude(), propertySummary.getLongitude(), distanceMile);

        baseSearchCriteria.setEqualMap(getFilledRequiredMap(
                Pair.with(BEDROOMS, Integer.toString(propertySummary.getBedrooms())),
                Pair.with(BATHROOMS, Integer.toString(propertySummary.getBathrooms()))
        ));

        baseSearchCriteria.setLessThanOrEqualMap(getFilledRequiredMap(
                Pair.with(SQUARE_FEET, squareFeetBandCalculator.getMinSquareFeet()),
                Pair.with(LATITUDE, coordinatesBandCalculator.getMinLatitude()),
                Pair.with(LONGITUDE, coordinatesBandCalculator.getMinLongitude())
        ));

        baseSearchCriteria.setGreaterThanOrEqualMap(getFilledRequiredMap(
                Pair.with(SQUARE_FEET, squareFeetBandCalculator.getMaxSquareFeet()),
                Pair.with(LATITUDE, coordinatesBandCalculator.getMaxLatitude()),
                Pair.with(LONGITUDE, coordinatesBandCalculator.getMaxLongitude())
        ));
        return baseSearchCriteria;
    }

    // Step three criteria, additional if not enough comps from step 2
    private BaseSearchCriteria getCriteriaStepThree(PropertyAnalogue propertySummary, int squareFeetPercent, float distanceMile) {
        BaseSearchCriteria baseSearchCriteria = new BaseSearchCriteria();

        SquareFeetBandCalculator squareFeetBandCalculator = new SquareFeetBandCalculator(squareFeetPercent, propertySummary.getAreaSqFt());
        CoordinatesBandCalculator coordinatesBandCalculator = new CoordinatesBandCalculator(propertySummary.getLatitude(), propertySummary.getLongitude(), distanceMile);

        baseSearchCriteria.setLessThanOrEqualMap(getFilledRequiredMap(
                Pair.with(BEDROOMS, String.valueOf(propertySummary.getBedrooms() - 1)),
                Pair.with(BATHROOMS, String.valueOf(propertySummary.getBathrooms() - 1)),
                Pair.with(SQUARE_FEET, squareFeetBandCalculator.getMinSquareFeet()),
                Pair.with(LATITUDE, coordinatesBandCalculator.getMinLatitude()),
                Pair.with(LONGITUDE, coordinatesBandCalculator.getMinLongitude())
        ));

        baseSearchCriteria.setGreaterThanOrEqualMap(getFilledRequiredMap(
                Pair.with(BEDROOMS, String.valueOf(propertySummary.getBedrooms() + 1)),
                Pair.with(BATHROOMS, String.valueOf(propertySummary.getBathrooms() + 1)),
                Pair.with(SQUARE_FEET, squareFeetBandCalculator.getMaxSquareFeet()),
                Pair.with(LATITUDE, coordinatesBandCalculator.getMaxLatitude()),
                Pair.with(LONGITUDE, coordinatesBandCalculator.getMaxLongitude())
        ));
        return baseSearchCriteria;
    }

    // Step three criteria, additional if not enough comps from step 3
    private BaseSearchCriteria getOtherNonTaggedCompsSearchCriteria(PropertyAnalogue propertySummary, float distanceMile) {
        BaseSearchCriteria baseSearchCriteria = new BaseSearchCriteria();

        CoordinatesBandCalculator coordinatesBandCalculator = new CoordinatesBandCalculator(propertySummary.getLatitude(), propertySummary.getLongitude(), distanceMile);

        baseSearchCriteria.setLessThanOrEqualMap(getFilledRequiredMap(
                Pair.with(LATITUDE, coordinatesBandCalculator.getMinLatitude()),
                Pair.with(LONGITUDE, coordinatesBandCalculator.getMinLongitude())
        ));

        baseSearchCriteria.setGreaterThanOrEqualMap(getFilledRequiredMap(
                Pair.with(LATITUDE, coordinatesBandCalculator.getMaxLatitude()),
                Pair.with(LONGITUDE, coordinatesBandCalculator.getMaxLongitude())
        ));
        return baseSearchCriteria;
    }

    @SafeVarargs
    private Map<String, String> getFilledRequiredMap(Pair<String, String>... equalValues) {
        HashMap<String, String> map = new HashMap<>();
        if (equalValues != null) {
            Arrays.stream(equalValues).forEach(item -> map.put(item.getValue0(), item.getValue1()));
        }
        return map;
    }

    private List<PropertyAnalogue> filterPropertiesByScores(List<PropertyAnalogue> results, YearBand exactYearBand, int missingItemsCount) {
        List<PropertyAnalogue> selectedNonTaggedPropertiesList = new ArrayList<>();
        if (results.size() > missingItemsCount) {
            List<PropertyAnalogue> sameYearBandProps = results.stream()
                    .filter(item -> !exactYearBand.checkYear(item.getYearBuilt()))
                    .collect(Collectors.toList());
            results.removeAll(sameYearBandProps);
            selectedNonTaggedPropertiesList.addAll(sameYearBandProps);

            if (!results.isEmpty() && selectedNonTaggedPropertiesList.size() < missingItemsCount) {
                List<PropertyAnalogue> areaSqftOrderedResults = results.stream()
                        .sorted(Comparator.comparing(PropertyAnalogue::getAreaSqFt))
                        .limit(missingItemsCount - selectedNonTaggedPropertiesList.size())
                        .collect(Collectors.toList());
                results.removeAll(areaSqftOrderedResults);
                selectedNonTaggedPropertiesList.addAll(areaSqftOrderedResults);
            }

            if (!results.isEmpty() && selectedNonTaggedPropertiesList.size() < missingItemsCount) {
                List<PropertyAnalogue> priceToSquareFeetList = results.stream()
                        .collect(Collectors.toMap(item -> item, item -> item.getPrice().divide(new BigDecimal(item.getAreaSqFt()), RoundingMode.HALF_UP)))
                        .entrySet().stream().sorted(Map.Entry.comparingByValue())
                        .limit(results.size() - selectedNonTaggedPropertiesList.size())
                        .map(Map.Entry::getKey)
                        .collect(Collectors.toList()).stream()
                        .limit(missingItemsCount - selectedNonTaggedPropertiesList.size())
                        .collect(Collectors.toList());
                results.removeAll(priceToSquareFeetList);
                selectedNonTaggedPropertiesList.addAll(priceToSquareFeetList);
            }
        } else {
            selectedNonTaggedPropertiesList = results;
        }
        return selectedNonTaggedPropertiesList;
    }

    private BaseSearchCriteria getSearchCriteriaForStep(PropertyAnalogue propertySummary, Float distanceMile, int stepNumber) {
        BaseSearchCriteria searchCriteria = switch (stepNumber) {
            case STEP_ONE_NUMBER -> getCriteriaStepOne(propertySummary, squareFeetPercent, distanceMile);
            case STEP_TWO_NUMBER -> getCriteriaStepTwo(propertySummary, squareFeetPercentStepTwo, distanceMile);
            case STEP_THREE_NUMBER -> getCriteriaStepThree(propertySummary, squareFeetPercentStepThree, distanceMile);
            default -> null;
        };
        return searchCriteria;
    }

    private void setLabel(PropertyAnalogue item) {
        item.setCompsLabel(NON_TAGGED_COMPS_LABEL);
    }

    private List<PropertyAnalogue> prepareResultList(List<PropertyAnalogue> properties, String propertyToSearchUuid) {
        return properties.stream()
                .filter(propertyAnalogue -> !propertyAnalogue.getUuid().equals(propertyToSearchUuid))
                .peek(this::setLabel)
                .collect(Collectors.toList());
    }

    private List<PropertyAnalogue> searchAnalogsStepOne(PropertyAnalogue propertyToSearchAnalogs, List<PropertyAnalogue> currentComparableList) {
        List<PropertyAnalogue> result = prepareResultList(
                searchAnalogues(propertyToSearchAnalogs, this.distanceMile, STEP_ONE_NUMBER),
                propertyToSearchAnalogs.getUuid());
        result.removeAll(currentComparableList);
        return result;
    }

    private List<PropertyAnalogue> searchAnalogsStepTwo(PropertyAnalogue propertyToSearchAnalogs, List<PropertyAnalogue> currentComparableList) {
        List<PropertyAnalogue> result = prepareResultList(
                searchAnalogues(propertyToSearchAnalogs, this.distanceMile, STEP_TWO_NUMBER),
                propertyToSearchAnalogs.getUuid());
        result.removeAll(currentComparableList);
        return result;
    }

    private List<PropertyAnalogue> searchAnalogsStepThree(PropertyAnalogue propertyToSearchAnalogs,
                                                          final List<PropertyAnalogue> currentNonTaggedAnalogs,
                                                          final List<PropertyAnalogue> resultsFromPreviousSteps) {
        int missingNonTaggedResults = this.analogueQuantity - currentNonTaggedAnalogs.size() - resultsFromPreviousSteps.size();
        List<PropertyAnalogue> otherNonTaggedComps = new ArrayList<>();
        for (float selectedDistance : stepThreeDistances) {
            otherNonTaggedComps.addAll(prepareResultList(
                    searchAnalogues(propertyToSearchAnalogs, selectedDistance, STEP_THREE_NUMBER),
                    propertyToSearchAnalogs.getUuid()));
            otherNonTaggedComps.removeAll(currentNonTaggedAnalogs);
            otherNonTaggedComps.removeAll(resultsFromPreviousSteps);
            if (otherNonTaggedComps.size() >= missingNonTaggedResults) {
                break;
            }
        }

        if (otherNonTaggedComps.size() >= missingNonTaggedResults) {
            otherNonTaggedComps = filterPropertiesByScores(otherNonTaggedComps,
                    yearBandService.getYearBand(propertyToSearchAnalogs.getYearBuilt()), missingNonTaggedResults);
        }
        return otherNonTaggedComps;
    }

    private List<PropertyAnalogue> findOtherNonTaggedComps(PropertyAnalogue propertyToSearchAnalogs, List<PropertyAnalogue> previousAnalogsList,
                                                           List<PropertyAnalogue> nonTaggedAnalogsList, int missingResultsQuantity) {
        List<PropertyAnalogue> otherNonTaggedAnalogs = searchAnalogues(propertyToSearchAnalogs,
                getOtherNonTaggedCompsSearchCriteria(propertyToSearchAnalogs, distanceMile));
        otherNonTaggedAnalogs.removeAll(previousAnalogsList);
        otherNonTaggedAnalogs.removeAll(nonTaggedAnalogsList);
        return otherNonTaggedAnalogs.stream()
                .sorted((o1, o2) -> Float.compare(o1.getDistance(), o2.getDistance()))
                .peek(this::setLabel)
                .limit(missingResultsQuantity)
                .collect(Collectors.toList());
    }
}
