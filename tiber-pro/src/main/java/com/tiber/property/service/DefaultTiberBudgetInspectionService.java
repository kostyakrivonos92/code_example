package com.tiber.property.service;

import com.tiber.shared.events.EventBus;
import com.tiber.shared.events.InspectionEvent;
import com.tiber.shared.events.InspectionShortInfoCompleteResponseEvent;
import com.tiber.shared.events.InspectionShortInfoRequestEvent;
import com.tiber.shared.events.InspectionShortInfoResponseEvent;
import com.tiber.shared.inspection.domain.PropertyInfo;
import com.tiber.shared.vocabulary.inspection.InspectionType;
import io.reactivex.rxjava3.core.Maybe;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.UUID;
import java.util.concurrent.CompletableFuture;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.TimeUnit;

@Slf4j
@Service
@RequiredArgsConstructor(onConstructor_ = @Autowired)
public class DefaultTiberBudgetInspectionService implements TiberBudgetInspectionService {

    private final EventBus<InspectionEvent> eventBus;

    private final ExecutorService pushExecutor = Executors.newSingleThreadExecutor();

    private final int timoutSeconds = 30;

    @Override
    public Maybe<PropertyInfo> getInspectionDataByPropertyUuidAndInspectionType(String propertyUuid, InspectionType inspectionType) {
        InspectionShortInfoRequestEvent propertyInfoEvent =
                new InspectionShortInfoRequestEvent(UUID.randomUUID().toString(), propertyUuid, inspectionType);
        @SuppressWarnings("redundant")
        Maybe<PropertyInfo> propertyGeneralMaybe = eventBus.events()
                .filter(event -> event.getId().equals(propertyInfoEvent.getId()))
                .takeUntil(event -> event instanceof InspectionShortInfoCompleteResponseEvent)
                .ofType(InspectionShortInfoResponseEvent.class)
                .map(InspectionShortInfoResponseEvent::getPropertyInfo)
                .take(timoutSeconds, TimeUnit.SECONDS)
                .firstElement()
                .doOnSubscribe(disposable -> CompletableFuture.runAsync(() -> eventBus.push(propertyInfoEvent), pushExecutor));
        return propertyGeneralMaybe;
    }
}
