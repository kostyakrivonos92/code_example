package com.tiber.property.service;

import com.tiber.property.domain.UnderwritingModel;

public interface RentalCalculationService {

    UnderwritingModel calculate(UnderwritingModel underwritingModel);
}
