package com.tiber.property.service.comparable.yearBand;

import org.springframework.stereotype.Component;

@Component
class BeforeFiftiesYearBand extends NumberedYearBand {

    private final int maxYear = 1949;

    @Override
    public String getMinYearBuilt() {
        return "0";
    }

    @Override
    public String getMaxYearBuilt() {
        return Integer.toString(maxYear);
    }

    @Override
    public boolean checkYear(int yearBuilt) {
        return yearBuilt <= maxYear;
    }
}
