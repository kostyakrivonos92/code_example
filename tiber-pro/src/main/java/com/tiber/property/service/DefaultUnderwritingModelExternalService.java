package com.tiber.property.service;

import com.tiber.property.mapper.UnderwritingModelMapper;
import com.tiber.property.repository.UnderwritingModelRepository;
import com.tiber.shared.underwriting.domain.UnderwritingModelExternal;
import com.tiber.shared.underwriting.service.UnderwritingModelExternalService;
import java.util.Optional;
import lombok.RequiredArgsConstructor;
import lombok.extern.log4j.Log4j2;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/*
    TODO: this service should be refactored or eliminated. Theis is temporary solution to replace RX-based implementation
 */
@Log4j2
@Service
@RequiredArgsConstructor(onConstructor_ = @Autowired)
class DefaultUnderwritingModelExternalService implements UnderwritingModelExternalService {

    private final UnderwritingModelRepository underwritingModelRepository;
    private final UnderwritingModelMapper underwritingModelMapper;

    @Override
    public Optional<UnderwritingModelExternal> getModelForDealUuid(String dealUuid) {
        return underwritingModelRepository.findByDealUuid(dealUuid)
                .map(underwritingModelMapper::underwritingModelEntityToUnderwritingModelExternal);
    }
}
