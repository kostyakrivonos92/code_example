package com.tiber.property.service.comparable;

import com.tiber.property.domain.PropertyAnalogue;
import com.tiber.property.service.PropertyAnalogueService;
import com.tiber.property.service.comparable.yearBand.YearBand;
import com.tiber.property.service.comparable.yearBand.YearBandService;
import com.tiber.shared.inspection.domain.PropertyInfo;
import com.tiber.shared.searchCriteria.domain.BaseSearchCriteria;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Primary;
import org.springframework.stereotype.Service;

import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Objects;
import java.util.stream.Collectors;

import static com.tiber.shared.vocabulary.PropertyParameterName.BATHROOMS;
import static com.tiber.shared.vocabulary.PropertyParameterName.BEDROOMS;
import static com.tiber.shared.vocabulary.PropertyParameterName.CLOSE_DATE;
import static com.tiber.shared.vocabulary.PropertyParameterName.LATITUDE;
import static com.tiber.shared.vocabulary.PropertyParameterName.LONGITUDE;
import static com.tiber.shared.vocabulary.PropertyParameterName.SQUARE_FEET;
import static com.tiber.shared.vocabulary.PropertyParameterName.YEAR_BUILT;

@Service
@Primary
class PerfectAnaloguesSearchLinkService implements AnaloguesSearchLinkService {

    private final AnaloguesSearchLinkService analoguesSearchLinkService;
    private final PropertyAnalogueService propertyAnalogueService;
    private final YearBandService yearBandService;
    private final Integer squareFeetPercent;
    private final Float distanceMile;
    private final Integer searchPeriodYears;

    @Autowired
    PerfectAnaloguesSearchLinkService(@Qualifier("good") AnaloguesSearchLinkService analoguesSearchLinkService,
                                      PropertyAnalogueService propertyAnalogueService,
                                      YearBandService yearBandService,
                                      @Value("${application.perfectAnalogues.squareFeetPercent}") Integer squareFeetPercent,
                                      @Value("${application.perfectAnalogues.distanceMile}") Float distanceMile,
                                      @Value("${application.perfectAnalogues.searchPeriodYears}") Integer searchPeriodYears) {
        this.analoguesSearchLinkService = analoguesSearchLinkService;
        this.propertyAnalogueService = propertyAnalogueService;
        this.yearBandService = yearBandService;
        this.squareFeetPercent = squareFeetPercent;
        this.distanceMile = distanceMile;
        this.searchPeriodYears = searchPeriodYears;
    }

    @Override
    public List<PropertyAnalogue> getAnaloguesForProperty(String uuid, List<PropertyAnalogue> currentComparableList, Integer analogueQuantity, PropertyInfo propertyInfo) {
        PropertyAnalogue basePropertyAnalogue = propertyAnalogueService.getPropertyAnalogueByUuid(uuid).blockingGet();
        if (Objects.isNull(basePropertyAnalogue)) {
            return Collections.emptyList();
        }

        if (propertyInfo != null) {
            basePropertyAnalogue.setBathrooms(propertyInfo.getBathrooms());
            basePropertyAnalogue.setBedrooms(propertyInfo.getBedrooms());
            basePropertyAnalogue.setAreaSqFt(propertyInfo.getAreaSqFt());
            basePropertyAnalogue.setYearBuilt(propertyInfo.getYearBuilt());
        }

        final List<PropertyAnalogue> result = searchAnalogues(basePropertyAnalogue).stream()
                .filter(propertyAnalogue -> !propertyAnalogue.getUuid().equals(uuid))
                .peek(item -> item.setCompsLabel(PERFECT_COMPS_LABEL))
                .collect(Collectors.toList());
        if (result.size() >= analogueQuantity) {
            return result.stream()
                    .limit(analogueQuantity)
                    .collect(Collectors.toList());
        }
        return analoguesSearchLinkService.getAnaloguesForProperty(uuid, result, analogueQuantity, propertyInfo);
    }

    private List<PropertyAnalogue> searchAnalogues(PropertyAnalogue basePropertyAnalogue) {
        return propertyAnalogueService.getPropertyAnalogueList(getCriteria(basePropertyAnalogue))
                .doOnNext(propertyAnalogue -> propertyAnalogue.setDistance(
                        DistanceCalculator.getDistance(
                                basePropertyAnalogue.getLatitude(),
                                basePropertyAnalogue.getLongitude(),
                                propertyAnalogue.getLatitude(),
                                propertyAnalogue.getLongitude()))
                )
                .filter(propertyAnalogue -> propertyAnalogue.getDistance() <= distanceMile)
                .collect(Collectors.toList())
                .blockingGet();
    }

    private BaseSearchCriteria getCriteria(PropertyAnalogue propertySummary) {
        BaseSearchCriteria baseSearchCriteria = new BaseSearchCriteria();
        HashMap<String, String> equalMap = new HashMap<>();
        equalMap.put(BEDROOMS, Integer.toString(propertySummary.getBedrooms()));
        equalMap.put(BATHROOMS, Integer.toString(propertySummary.getBathrooms()));
        baseSearchCriteria.setEqualMap(equalMap);

        HashMap<String, String> graterThanOrEqualMap = new HashMap<>();
        YearBand yearBand = yearBandService.getYearBand(propertySummary.getYearBuilt());
        graterThanOrEqualMap.put(YEAR_BUILT, yearBand.getMinYearBuilt());
        SquareFeetBandCalculator squareFeetBandCalculator = new SquareFeetBandCalculator(squareFeetPercent, propertySummary.getAreaSqFt());
        graterThanOrEqualMap.put(SQUARE_FEET, squareFeetBandCalculator.getMinSquareFeet());
        CoordinatesBandCalculator coordinatesBandCalculator = new CoordinatesBandCalculator(propertySummary.getLongitude(),
                propertySummary.getLatitude(),
                distanceMile);
        graterThanOrEqualMap.put(LONGITUDE, coordinatesBandCalculator.getMinLongitude());
        graterThanOrEqualMap.put(LATITUDE, coordinatesBandCalculator.getMinLatitude());
        graterThanOrEqualMap.put(CLOSE_DATE, LocalDate.now().minusYears(searchPeriodYears).format(DateTimeFormatter.ISO_DATE));
        baseSearchCriteria.setGreaterThanOrEqualMap(graterThanOrEqualMap);

        HashMap<String, String> lessThanOrEqualMap = new HashMap<>();
        lessThanOrEqualMap.put(YEAR_BUILT, yearBand.getMaxYearBuilt());
        lessThanOrEqualMap.put(SQUARE_FEET, squareFeetBandCalculator.getMaxSquareFeet());
        lessThanOrEqualMap.put(LONGITUDE, coordinatesBandCalculator.getMaxLongitude());
        lessThanOrEqualMap.put(LATITUDE, coordinatesBandCalculator.getMaxLatitude());
        baseSearchCriteria.setLessThanOrEqualMap(lessThanOrEqualMap);
        return baseSearchCriteria;
    }
}