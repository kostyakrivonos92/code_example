package com.tiber.property.service;

import com.tiber.property.domain.AnaloguesSelection;
import com.tiber.property.domain.DealSummary;
import com.tiber.property.domain.Document;
import com.tiber.shared.note.domain.Note;
import com.tiber.shared.note.domain.NotePage;
import com.tiber.property.domain.UnderwritingModel;
import com.tiber.property.status.UnderwritingModelStatus;
import com.tiber.shared.inspection.domain.PropertyInfo;
import com.tiber.shared.inspection.domain.PropertyInfoTable;
import com.tiber.shared.status.domain.StatusEvent;
import com.tiber.shared.underwriting.domain.UnderwritingModelExternal;
import com.tiber.shared.util.RenovationType;

import java.nio.file.Path;
import java.util.Collection;
import java.util.List;
import java.util.Optional;

public interface UnderwritingModelService {

    RenovationType defaultStrategy = RenovationType.SPEC;

    List<UnderwritingModel> findByPropertyUuid(String propertyUuid);

    Collection<UnderwritingModel> findByPropertyUuidCollection(Collection<String> propertyUuid);

    Optional<UnderwritingModel> findByUuid(String uuid);

    Optional<UnderwritingModelExternal> findExternalByDealUuid(String dealUuid);

    Optional<UnderwritingModel> create(String propertyUuid, DealSummary dealSummary);

    UnderwritingModel update(UnderwritingModel underwritingModel);

    Note createNote(String underwritingModelUuid, Note note);

    NotePage getNotesForModel(String underwritingModelUuid, boolean includeSystemNotes, int page, int pageSize);

    Document createDocument(String underwritingModelUuid, String fileName, Path documentUrl);

    List<Document> getDocumentsForModel(String underwritingModelUuid);

    void deleteDocument(String underwritingModelUuid, long id);

    UnderwritingModel setAnaloguesSelection(String underwritingModelUuid, AnaloguesSelection analoguesSelection);

    PropertyInfoTable getUnderwritingModelInspectionTable(String underwritingModelUuid);

    PropertyInfoTable updateUnderwritingModelInspection(String underwritingModelUuid, PropertyInfo underwritingModelInspection);

    Optional<UnderwritingModel> applyStatusEvent(String underwritingModelUuid, StatusEvent<UnderwritingModelStatus> statusEvent);

    Optional<List<StatusEvent<UnderwritingModelStatus>>> getStatusEventList(String underwritingModelUuid);

    UnderwritingModel evaluate(String underwritingModelUuid);
}
