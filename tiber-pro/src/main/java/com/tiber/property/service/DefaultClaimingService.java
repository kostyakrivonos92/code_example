package com.tiber.property.service;

import com.tiber.property.domain.DealSummary;
import com.tiber.property.exception.ModelCreationException;
import com.tiber.shared.exception.NoActiveDealException;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Optional;

@Slf4j
@Service
@RequiredArgsConstructor(onConstructor_ = @Autowired)
public class DefaultClaimingService implements ClaimingService {

    private final DealGettingService dealGettingService;

    private final UnderwritingModelService underwritingModelService;

    @Override
    public DealSummary claimActiveDealInProperty(String propertyUuid, String responsibleAnalystUuid, String claimedByAnalystUuid) {
        log.info("Claiming deal for property {}", propertyUuid);
        DealSummary dealSummary = Optional.ofNullable(
                dealGettingService.claimActiveDealInProperty(propertyUuid, responsibleAnalystUuid, claimedByAnalystUuid)
                        .blockingGet())
                .orElseThrow(() -> new NoActiveDealException(propertyUuid));

        if (dealSummary.getModelUuid() == null) {
            log.info("Creating model for deal {} for property {}", dealSummary.getUuid(), propertyUuid);
            underwritingModelService.create(dealSummary.getPropertyUuid(), dealSummary)
                    .map(underwritingModel -> {
                        dealSummary.setModelUuid(underwritingModel.getUuid());
                        return dealSummary;
                    })
                    .orElseThrow(() -> new ModelCreationException("Cannot create model for selected property."));
        }
        return dealSummary;
    }
}
