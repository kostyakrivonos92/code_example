package com.tiber.property.mapper;

import com.tiber.property.domain.PropertyAnalogue;
import com.tiber.shared.propertylayer.domain.PropertyOnMarket;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;

@Mapper(componentModel = "spring")
public interface PropertyAnalogueMapper {

    @Mapping(source = "latestDeal.closeDate", target = "closeDate")
    @Mapping(source = "latestDeal.price", target = "price")
    @Mapping(source = "latestDeal.renovationTag", target = "renovationTag")
    @Mapping(source = "latestDeal.uuid", target = "uuid")
    @Mapping(source = "uuid", target = "propertyUuid")
    PropertyAnalogue propertyAnalogueFromPropertyOnMarket(PropertyOnMarket propertyOnMarket);
}
