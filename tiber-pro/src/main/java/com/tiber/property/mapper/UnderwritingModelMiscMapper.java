package com.tiber.property.mapper;

import com.tiber.property.domain.UnderwritingModelMisc;
import com.tiber.property.entity.UnderwritingModelMiscEntity;
import org.mapstruct.Mapper;

@Mapper(componentModel = "spring")
public interface UnderwritingModelMiscMapper {

    UnderwritingModelMisc toDomain(UnderwritingModelMiscEntity source);

    UnderwritingModelMiscEntity toEntity(UnderwritingModelMisc source);
}
