package com.tiber.property.mapper;

import com.tiber.shared.inspection.domain.PropertyInfo;
import com.tiber.shared.inspection.entity.PropertyInfoEntity;
import com.tiber.shared.propertylayer.domain.PropertyOnMarket;
import org.mapstruct.Mapper;

@Mapper(componentModel = "spring")
public interface PropertyInfoMapper {

    PropertyInfo propertyInfoDoToEntity(PropertyInfoEntity entity);

    PropertyInfoEntity propertyInfoEntityToDo(PropertyInfo propertyInfo);

    PropertyInfo publicPropertyToDomain(PropertyOnMarket propertyOnMarket);
}
