package com.tiber.property.mapper;

import com.tiber.property.domain.Document;
import com.tiber.property.entity.DocumentEntity;
import org.mapstruct.Mapper;

import java.util.List;

@Mapper(componentModel = "spring")
public interface DocumentMapper {

    Document fromEntityToDo(DocumentEntity documentEntity);

    DocumentEntity fromDoToEntity(Document document);

    List<Document> fromEntityListToDoList(List<DocumentEntity> documentEntities);
}
