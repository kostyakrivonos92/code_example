package com.tiber.property.mapper;

import com.tiber.property.domain.AnaloguesSelection;
import com.tiber.property.domain.UnderwritingModel;
import com.tiber.property.entity.AnaloguesSelectionEntity;
import com.tiber.property.entity.UnderwritingModelEntity;
import com.tiber.property.entity.UnderwritingNoteEntity;
import com.tiber.property.status.UnderwritingModelStatus;
import com.tiber.shared.note.domain.Note;
import com.tiber.shared.underwriting.domain.UnderwritingModelExternal;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;

@Mapper(
        componentModel = "spring",
        uses = {
                UnderwritingModelMiscMapper.class,
                UnderwritingModelLeaseMapper.class,
                UnderwritingModelTaxMapper.class
        }
)
public interface UnderwritingModelMapper {

    UnderwritingModel toDomain(UnderwritingModelEntity entity);

    UnderwritingModelEntity toEntity(UnderwritingModel model);

    AnaloguesSelectionEntity analoguesSelectionToAnaloguesSelectionEntity(AnaloguesSelection analoguesSelection);

    UnderwritingNoteEntity noteToUnderwritingNoteEntity(Note note);

    Note underwritingNoteEntityToNote(UnderwritingNoteEntity note);

    default UnderwritingModelStatus statusToUnderwritingModelStatus(String value) {
        return UnderwritingModelStatus.get(value);
    }

    @Mapping(target = "projectedSalesPrice",
            expression = "java(underwritingModelEntity.getStrategyDependentInitialDataMap()" +
            ".get(underwritingModelEntity.getStrategy()).getProjectedSalesPrice())")
    @Mapping(target = "bedroom", source = "underwritingModelEntity.propertyInfo.bedrooms")
    @Mapping(target = "bathroom", source = "underwritingModelEntity.propertyInfo.bathrooms")
    UnderwritingModelExternal underwritingModelEntityToUnderwritingModelExternal(UnderwritingModelEntity underwritingModelEntity);
}
