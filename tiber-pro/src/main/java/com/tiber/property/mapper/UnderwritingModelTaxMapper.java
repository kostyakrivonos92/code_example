package com.tiber.property.mapper;

import com.tiber.property.domain.UnderwritingModelTax;
import com.tiber.property.entity.UnderwritingModelTaxEntity;
import org.mapstruct.Mapper;

@Mapper(componentModel = "spring")
public interface UnderwritingModelTaxMapper {

    UnderwritingModelTax toDomain(UnderwritingModelTaxEntity source);

    UnderwritingModelTaxEntity toEntity(UnderwritingModelTax source);
}
