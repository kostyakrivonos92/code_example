package com.tiber.property.mapper;

import com.tiber.property.domain.DealSummary;
import com.tiber.property.domain.UnderwritingModel;
import com.tiber.shared.propertylayer.domain.PropertyOnMarket;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;

import java.util.List;

@Mapper(componentModel = "spring")
public interface DealSummaryMapper {

    List<DealSummary> mapToDealSummaryList(List<PropertyOnMarket> properties);

    @Mapping(source = "uuid", target = "propertyUuid")
    @Mapping(source = "latestDeal.uuid", target = "uuid")
    @Mapping(source = "latestDeal.price", target = "price")
    @Mapping(source = "latestDeal.status", target = "status")
    @Mapping(source = "latestDeal.closeDate", target = "closeDate")
    @Mapping(source = "latestDeal.renovationTag", target = "renovationTag")
    DealSummary dealSummaryFromPropertyOnMarket(PropertyOnMarket propertyOnMarket);

    default DealSummary mapToDealSummary(PropertyOnMarket propertyOnMarket, UnderwritingModel underwritingModel) {
        DealSummary dealSummary = dealSummaryFromPropertyOnMarket(propertyOnMarket);
        dealSummary.setSalesDate(underwritingModel.getSalesDate());
        dealSummary.setModelUuid(underwritingModel.getUuid());
        return dealSummary;
    }
}
