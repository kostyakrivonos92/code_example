package com.tiber.property.mapper;

import com.tiber.property.domain.UnderwritingModelLease;
import com.tiber.property.entity.UnderwritingModelLeaseEntity;
import org.mapstruct.Mapper;

@Mapper(componentModel = "spring")
public interface UnderwritingModelLeaseMapper {

    UnderwritingModelLease toDomain(UnderwritingModelLeaseEntity source);

    UnderwritingModelLeaseEntity toEntity(UnderwritingModelLease source);
}
