package com.artbay.marketplace.marketplacemodule.controller;

import com.artbay.marketplace.common.client.partner.internal.PartnerInternalClient;
import com.artbay.marketplace.common.mapper.UserDtoMapper;
import com.artbay.marketplace.common.model.ShortUserInfoDTO;
import com.artbay.marketplace.marketplacemodule.model.Artwork;
import com.artbay.marketplace.marketplacemodule.service.ArtworkService;
import com.artbay.marketplace.marketplacemodule.service.CollectionService;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.UUID;

@Slf4j
@RestController
@RequestMapping("/users")
@RequiredArgsConstructor
public class UserController {

    private final PartnerInternalClient partnerInternalClient;
    private final UserDtoMapper userDtoMapper;

    private final CollectionService collectionService;
    private final ArtworkService artworkService;

    @GetMapping("/{userId}")
    public ShortUserInfoDTO getUserProfile(@PathVariable UUID userId) {
        ShortUserInfoDTO dto = userDtoMapper.partnerToUserInfo(partnerInternalClient.getPartnerById(userId));
        dto.setCollectionsCount(collectionService.getCollections(userId).size());
        dto.setNftsCount((int) artworkService.getArtworks(userId)
                                   .stream()
                                   .filter(Artwork::getPublishedToMarketplace)
                                   .count());
        return dto;
    }

    @GetMapping("/username/{username}")
    public ShortUserInfoDTO getUserProfile(@PathVariable String username) {
        ShortUserInfoDTO dto = userDtoMapper.partnerToUserInfo(partnerInternalClient.getPartnerByUsername(username));
        dto.setCollectionsCount(collectionService.getCollections(dto.getId()).size());
        dto.setNftsCount((int) artworkService.getArtworks(dto.getId())
                                   .stream()
                                   .filter(Artwork::getPublishedToMarketplace)
                                   .count());
        return dto;
    }

}
