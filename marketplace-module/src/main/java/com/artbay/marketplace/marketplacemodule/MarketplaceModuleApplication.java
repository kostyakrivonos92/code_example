package com.artbay.marketplace.marketplacemodule;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.autoconfigure.domain.EntityScan;
import org.springframework.cloud.client.discovery.EnableDiscoveryClient;
import org.springframework.cloud.openfeign.EnableFeignClients;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;

@SpringBootApplication
@EnableDiscoveryClient
@EnableFeignClients("com.artbay.marketplace.common.client")
@EntityScan(basePackages = {"com.artbay.marketplace.marketplacemodule.model", "com.artbay.marketplace.staking.model"})
@EnableJpaRepositories(basePackages = {
    "com.artbay.marketplace.marketplacemodule.repository",
    "com.artbay.marketplace.staking.repository"
})
@ComponentScan({"com.artbay.marketplace.marketplacemodule", "com.artbay.marketplace.staking"})
public class MarketplaceModuleApplication {

    public static void main(String[] args) {
        SpringApplication.run(MarketplaceModuleApplication.class, args);
    }

}
