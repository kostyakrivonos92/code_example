package com.artbay.marketplace.marketplacemodule.config;

import com.artbay.marketplace.common.config.CommonAuthConfig;
import com.artbay.marketplace.common.config.JpaAuditingConfig;
import net.kaczmarzyk.spring.data.jpa.web.SpecificationArgumentResolver;
import com.artbay.marketplace.common.config.CommonConfig;
import org.springframework.context.annotation.Configuration;
import org.springframework.web.method.support.HandlerMethodArgumentResolver;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurer;

import java.util.List;
import org.springframework.context.annotation.Import;

@Configuration
@Import({CommonConfig.class, JpaAuditingConfig.class, CommonAuthConfig.class})
public class MarketplaceModuleConfig implements WebMvcConfigurer {

    @Override
    public void addArgumentResolvers(List<HandlerMethodArgumentResolver> argumentResolvers) {
        argumentResolvers.add(new SpecificationArgumentResolver());
    }
}