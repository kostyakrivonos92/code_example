package com.artbay.marketplace.marketplacemodule.util;

import com.artbay.marketplace.common.client.partner.internal.PartnerInternalClient;
import com.artbay.marketplace.common.client.referral.internal.ReferralInternalClient;
import com.artbay.marketplace.common.model.ArtworkDTO;
import com.artbay.marketplace.common.model.PartnerDTO;
import com.artbay.marketplace.marketplacemodule.model.Artwork;
import com.artbay.marketplace.marketplacemodule.repository.ArtworkRepository;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Component;

import java.util.Arrays;
import java.util.List;
import java.util.Objects;
import java.util.UUID;
import java.util.stream.Collectors;

import static com.artbay.marketplace.common.model.enums.ArtworkVerificationStatus.EMPTY;
import static com.artbay.marketplace.common.model.enums.ArtworkVerificationStatus.VERIFIED;

@Component
@RequiredArgsConstructor
public class UserLevelUtil {

    private final ReferralInternalClient transactionService;
    private final ArtworkRepository artworkRepository;
    private final PartnerInternalClient partnerInternalClient;

    public void openReferralLevel(Artwork artwork, PartnerDTO buyer) {
        if (buyer.isHasNftKey() && artwork.getLevel() > buyer.getReferralLevel()) {
            transactionService.accrualHiddenRewardsForNextLevel(buyer, artwork.getLevel());
            buyer.setReferralLevel(artwork.getLevel());
            partnerInternalClient.updatePartner(buyer);
        }
    }

    public void loweringRefLevelForSeller(ArtworkDTO artwork, UUID sellerId) {
        if (artwork.getStatus().equals(VERIFIED) && !artwork.getPublishedToMarketplace() ||
                artwork.getStatus().equals(EMPTY)) {
            int loweredLevel = 0;
            PartnerDTO seller = partnerInternalClient.getPartnerById(sellerId);
            List<Artwork> artworks = artworkRepository.findAllByCurrentOwnerIdAndStatusInAndImportedIsFalse(
                    sellerId,
                    Arrays.asList(VERIFIED, EMPTY)
                )
                                         .stream()
                                         .filter(art -> !art.getId().equals(artwork.getId()))
                                         .collect(Collectors.toList());
            if (seller.isHasNftKey() && artwork.getFirstSale() && !artworks.isEmpty()) {
                var artsUpLevelCount = artworks.stream()
                                           .filter(art -> art.getLevel() > artwork.getLevel())
                                           .count();

                var artsLevelCount = artworks.stream()
                                         .filter(art -> Objects.equals(art.getLevel(), artwork.getLevel()))
                                         .count();

                if (artsUpLevelCount == 0 && artsLevelCount == 1) {
                    var levels = artworks.stream()
                                     .map(Artwork::getLevel)
                                     .distinct()
                                     .sorted()
                                     .collect(Collectors.toList());

                    if (levels.size() > 1) {
                        loweredLevel = levels.get(levels.size() - 2);
                    }
                    seller.setReferralLevel(loweredLevel);
                }
            } else {
                seller.setReferralLevel(loweredLevel);
            }
            partnerInternalClient.updatePartner(seller);
        }
    }

}
