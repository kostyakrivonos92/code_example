package com.artbay.marketplace.marketplacemodule.model.mapper;

import com.artbay.marketplace.common.client.partner.internal.PartnerInternalClient;
import com.artbay.marketplace.marketplacemodule.model.rating.UserVoteMetadata;
import com.artbay.marketplace.marketplacemodule.model.rating.dto.UserVoteMetadataDTO;
import org.mapstruct.*;
import org.springframework.beans.factory.annotation.Autowired;

import java.util.Optional;

@Mapper(
    unmappedTargetPolicy = ReportingPolicy.IGNORE,
    nullValuePropertyMappingStrategy = NullValuePropertyMappingStrategy.IGNORE,
    nullValueCheckStrategy = NullValueCheckStrategy.ALWAYS
)
public abstract class UserVoteMetadataMapper {

    @Autowired
    private PartnerInternalClient partnerInternalClient;

    @Mapping(target = "mysteryBoxCount", ignore = true)
    public abstract UserVoteMetadataDTO entityToDtoAuto(UserVoteMetadata userVoteMetadata);

    public UserVoteMetadataDTO entityToDto(UserVoteMetadata userVoteMetadata) {
        var userVoteMetadataDTO = entityToDtoAuto(userVoteMetadata);
        var allBoxesCount = partnerInternalClient.getMysteryBoxCount(userVoteMetadata.getPartnerId());
        var chromeBoxCount = Optional.ofNullable(allBoxesCount.get(1));
        userVoteMetadataDTO.setMysteryBoxCount(chromeBoxCount.orElse(0L).intValue());
        return userVoteMetadataDTO;
    }

}
