package com.artbay.marketplace.marketplacemodule.controller;

import com.artbay.marketplace.common.client.marketplace.MarketplaceNftKeyClient;
import com.artbay.marketplace.common.client.referral.ReferralExchangeRatesClient;
import com.artbay.marketplace.common.model.NFTKeyDTO;
import com.artbay.marketplace.marketplacemodule.model.mapper.NFTKeyMapper;
import com.artbay.marketplace.marketplacemodule.service.NftKeyService;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.Map;
import java.util.Optional;
import java.util.UUID;

import static com.artbay.marketplace.common.utils.constants.ReferralConstants.NFT_KEY_VALUE;

@Slf4j
@RestController
@RequestMapping("/nft-key")
@RequiredArgsConstructor
public class NftKeyController implements MarketplaceNftKeyClient {

    private final NftKeyService nftKeyService;
    private final NFTKeyMapper nftKeyMapper;

    private final ReferralExchangeRatesClient referralExchangeRatesClient;

    @Override
    public Optional<NFTKeyDTO> findNftKeyEligibleForPurchase(UUID referrerId) {
        return Optional.of(nftKeyMapper.entityToDto(nftKeyService.findReferrerKeyOnSale(referrerId)));
    }

    @Override
    public void releaseKey(UUID keyId) {
        nftKeyService.releaseKey(keyId);
    }

    @Override
    public NFTKeyDTO getRandomNFTKeyOnSale() {
        return nftKeyMapper.entityToDto(nftKeyService.getRandomNftKeyOrThrow());
    }

    @Override
    public Map<String, Integer> getSoldNftKeyCount() {
        return Map.of(COUNT, nftKeyService.getSoldNftKeyCount());
    }

    @GetMapping("/price")
    public Map<String, Number> getCurrentNftKeyPrice() {
        return Map.of(
            "usdPrice",
            NFT_KEY_VALUE,
            "artzPrice",
            NFT_KEY_VALUE * referralExchangeRatesClient.getUsdArtzPair().getPrice()
        );
    }

}
