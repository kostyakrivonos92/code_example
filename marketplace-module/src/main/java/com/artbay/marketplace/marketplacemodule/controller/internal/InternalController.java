package com.artbay.marketplace.marketplacemodule.controller.internal;

import com.artbay.marketplace.common.client.marketplace.internal.MarketplaceInternalClient;
import com.artbay.marketplace.common.model.ArtworkDTO;
import com.artbay.marketplace.common.model.NFTKeyDTO;
import com.artbay.marketplace.common.model.enums.ArtworkVerificationStatus;
import com.artbay.marketplace.common.model.request.ImportArtworkRequest;
import com.artbay.marketplace.marketplacemodule.model.mapper.ArtworkMapper;
import com.artbay.marketplace.marketplacemodule.model.mapper.NFTKeyMapper;
import com.artbay.marketplace.marketplacemodule.service.*;
import com.artbay.marketplace.marketplacemodule.service.RatingService;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;
import java.util.UUID;

@Slf4j
@RestController
@RequestMapping("/internal")
@RequiredArgsConstructor
public class InternalController implements MarketplaceInternalClient {

    private final ArtworkService artworkService;
    private final NftKeyService nftKeyService;
    private final ImportExportNftService importExportNftService;
    private final RatingService ratingService;
    private final ArtUpBoosterService artUpBoosterService;

    private final NFTKeyMapper nftKeyMapper;
    private final ArtworkMapper artworkMapper;

    @Override
    public NFTKeyDTO getAndBookRandomNFTKeyOnSale(UUID buyerId) {
        log.info("Getting and booking random key on sale");
        return nftKeyService.getAndBookRandomNftKey(buyerId);
    }

    @Override
    public NFTKeyDTO getKeyById(UUID keyId) {
        log.info("Getting NFT key by id={}", keyId);
        return nftKeyService.getNftKeyById(keyId);
    }

    @Override
    public void updateNftKey(NFTKeyDTO keyDto) {
        log.info("Received key for update {}", keyDto);
        nftKeyService.updateNftKey(nftKeyMapper.dtoToEntity(keyDto));
    }

    @Override
    public ArtworkDTO updateArtwork(@RequestBody ArtworkDTO artworkDTO) {
        return artworkMapper.entityToDto(artworkService.internalUpdateArtwork(artworkDTO));
    }

    public List<ArtworkDTO> getArtworksByOwnerIdAndStatuses(UUID ownerId, List<ArtworkVerificationStatus> status) {
        var artworks = artworkService.getArtworksByOwnerIdAndStatuses(ownerId, status);
        return artworkMapper.entityToDto(artworks);
    }

    @Override
    public List<ArtworkDTO> getArtworksByArtist(UUID artistId) {
        return artworkMapper.entityToDto(artworkService.getArtworksByArtistInternal(artistId));
    }

    @Override
    public ArtworkDTO getArtworkById(UUID artworkId) {
        return artworkMapper.entityToDto(artworkService.getArtworkById(artworkId));
    }

    @Override
    public void importNft(ImportArtworkRequest request) {
        importExportNftService.importNft(request.getTokenAddress(), request.getArtist(), request.getPartner());
    }

    @Override
    public void createPurchaseActivityHistory(UUID artworkId, UUID buyerId, String buyerName, Double price) {
        artworkService.createArtworkActivityHistory(buyerId, artworkId, buyerName, price);
    }

    @Override
    public void addVoteDaysForRating(UUID userId, int numOfAdditionalDays) {
        ratingService.addVoteDaysForRating(userId, numOfAdditionalDays);
    }

    @Override
    public void createMysteryBoxPrizeArtUpBooster(@RequestParam UUID userId, @RequestParam int artUpLevel){
        artUpBoosterService.createMysteryBoxPrizeArtUpBooster(userId, artUpLevel);
    }

    @Override
    public ArtworkDTO accrualMysteryBoxPrizeNft(@RequestParam UUID userId, @RequestParam int nftLevel){
        return artworkMapper.entityToDto(artworkService.mysteryBoxNftPrize(userId, nftLevel));
    }

}
