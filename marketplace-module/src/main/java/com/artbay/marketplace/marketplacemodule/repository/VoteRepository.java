package com.artbay.marketplace.marketplacemodule.repository;

import com.artbay.marketplace.marketplacemodule.model.enums.VoteType;
import com.artbay.marketplace.marketplacemodule.model.rating.Vote;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import java.time.Instant;
import java.util.List;
import java.util.UUID;

public interface VoteRepository extends JpaRepository<Vote, UUID> {

    Vote findFirstByPartnerIdAndArtworkIdOrderByCreationDateDesc(UUID partnerId, UUID artworkId);

    @Query(
        "select count(id) from Vote where artworkId = :artworkId " +
            "and voteType <> com.artbay.marketplace.marketplacemodule.model.enums.VoteType.SKIP"
    )
    int countVotesForArtwork(UUID artworkId);

    List<Vote> findByArtworkId(UUID artworkId);

    Page<Vote> findByPartnerIdAndVoteTypeNot(UUID userId, VoteType voteType, Pageable pageable);

    List<Vote> findAllByPartnerId(UUID userId);

    List<Vote> findAllByPartnerIdAndAndCreationDateAfter(UUID userId, Instant creationDate);

}