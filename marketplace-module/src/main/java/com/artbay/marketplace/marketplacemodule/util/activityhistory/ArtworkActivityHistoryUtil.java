package com.artbay.marketplace.marketplacemodule.util.activityhistory;

import com.artbay.marketplace.marketplacemodule.model.activityhistory.ArtworkActivityHistory;
import com.artbay.marketplace.marketplacemodule.model.enums.ActivityHistoryOperationType;
import lombok.experimental.UtilityClass;

import java.util.UUID;

@UtilityClass
public class ArtworkActivityHistoryUtil {

    public static ArtworkActivityHistory buildCreateArtworkArtworkActivityHistory(
        UUID artistId,
        String username,
        UUID artworkId
    ) {
        return ArtworkActivityHistory.builder()
                   .userId(artistId)
                   .username(username)
                   .relatedNftId(artworkId)
                   .operationType(ActivityHistoryOperationType.CREATED)
                   .build();
    }

    public static ArtworkActivityHistory buildPurchaseArtworkArtworkActivityHistory(
        UUID partnerId,
        String username,
        UUID artworkId,
        Double price
    ) {
        return ArtworkActivityHistory.builder()
                   .userId(partnerId)
                   .username(username)
                   .relatedNftId(artworkId)
                   .price(price)
                   .operationType(ActivityHistoryOperationType.PURCHASE)
                   .build();
    }

    public static ArtworkActivityHistory buildExportArtworkArtworkActivityHistory(
        UUID partnerId,
        String username,
        UUID artworkId
    ) {
        return ArtworkActivityHistory.builder()
                   .userId(partnerId)
                   .username(username)
                   .relatedNftId(artworkId)
                   .operationType(ActivityHistoryOperationType.EXPORT)
                   .build();
    }

    public static ArtworkActivityHistory buildImportArtworkArtworkActivityHistory(
        UUID partnerId,
        String username,
        UUID artworkId
    ) {
        return ArtworkActivityHistory.builder()
                   .userId(partnerId)
                   .username(username)
                   .relatedNftId(artworkId)
                   .operationType(ActivityHistoryOperationType.IMPORT)
                   .build();
    }

    public static ArtworkActivityHistory buildLevelUpArtworkActivityHistory(
        UUID partnerId,
        String username,
        UUID artworkId,
        int newLevel
    ) {
        return ArtworkActivityHistory.builder()
                   .userId(partnerId)
                   .username(username)
                   .relatedNftId(artworkId)
                   .previousLevel(newLevel - 1)
                   .newLevel(newLevel)
                   .operationType(ActivityHistoryOperationType.LEVEL_UP)
                   .build();
    }

}
