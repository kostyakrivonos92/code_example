package com.artbay.marketplace.marketplacemodule.service;

import com.artbay.marketplace.common.client.partner.internal.PartnerInternalClient;
import com.artbay.marketplace.common.client.referral.ReferralTransactionClient;
import com.artbay.marketplace.common.exception.ErrorCode;
import com.artbay.marketplace.common.exception.GlobalException;
import com.artbay.marketplace.common.model.PartnerDTO;
import com.artbay.marketplace.common.model.enums.ArtworkOwnerRole;
import com.artbay.marketplace.common.model.mysterybox.MysteryBoxDto;
import com.artbay.marketplace.marketplacemodule.model.Artwork;
import com.artbay.marketplace.marketplacemodule.model.enums.VoteType;
import com.artbay.marketplace.marketplacemodule.model.rating.UserVoteMetadata;
import com.artbay.marketplace.marketplacemodule.model.rating.Vote;
import com.artbay.marketplace.marketplacemodule.repository.ArtworkRepository;
import com.artbay.marketplace.marketplacemodule.repository.UserVoteMetadataRepository;
import com.artbay.marketplace.marketplacemodule.repository.VoteRepository;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.context.MessageSource;
import org.springframework.context.i18n.LocaleContextHolder;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Service;

import java.time.Instant;
import java.util.Optional;
import java.util.UUID;
import java.util.function.Predicate;
import java.util.stream.Collectors;

import static com.artbay.marketplace.common.model.enums.TransactionEntityType.ACTIVITY_POINTS_BALANCE;
import static com.artbay.marketplace.common.utils.constants.PartnerRankConstants.ACTIVITY_POINTS_TO_PARTNER_RANK;
import static com.artbay.marketplace.common.utils.constants.RatingConstants.*;
import static com.artbay.marketplace.marketplacemodule.model.enums.VoteType.SKIP;
import static java.time.Instant.now;
import static java.time.temporal.ChronoUnit.DAYS;

@Slf4j
@Service
@RequiredArgsConstructor
public class RatingService {

    private final ArtworkService artworkService;

    private final ArtworkRepository artworkRepository;
    private final VoteRepository voteRepository;
    private final UserVoteMetadataRepository userVoteMetadataRepository;

    private final PartnerInternalClient partnerInternalClient;
    private final ReferralTransactionClient referralTransactionClient;

    @Qualifier("activeMessageSource")
    private final MessageSource messageSource;

    private static final Predicate<Vote> SUITABLE_RANDOM_ARTWORK_PREDICATE =
        vote -> vote == null || (vote.getCreationDate().isBefore(now().minus(1, DAYS)));

    public Page<Vote> getLastVotes(UUID userId, Pageable pageable) {
        return voteRepository.findByPartnerIdAndVoteTypeNot(userId, SKIP, pageable);
    }

    public UserVoteMetadata getUserVoteMetadata(UUID partnerId) {
        var votingMetadataOptional = userVoteMetadataRepository.findById(partnerId);
        if (votingMetadataOptional.isEmpty()) {
            var partner = partnerInternalClient.getPartnerById(partnerId);
            int maxVotes = calculateMaxVotes(partner.getId());
            Instant votingEndDate = calculateVotingEndDate(partner);
            var metadataToCreate = UserVoteMetadata.builder()
                                       .partnerId(partnerId)
                                       .maxVotes(maxVotes)
                                       .availableVotes(maxVotes)
                                       .restorePeriodSeconds(SECONDS_IN_DAY / maxVotes)
                                       .votingEndDate(votingEndDate)
                                       .votesUntilMysteryBox(VOTES_TO_UNLOCK_MYSTERY_BOX)
                                       .isTrial(false)
                                       .isTrialPassed(false)
                                       .build();
            if (votingEndDate.isAfter(now())) {
                metadataToCreate.setIsTrialPassed(true);
            }
            votingMetadataOptional = Optional.of(userVoteMetadataRepository.save(metadataToCreate));
        } else {
            var metadata = votingMetadataOptional.get();
            if (metadata.getAvailableVotes() < metadata.getMaxVotes()) {
                var maxVotes = calculateMaxVotes(partnerId);
                if (maxVotes > metadata.getMaxVotes()) {
                    metadata.setMaxVotes(maxVotes);
                    metadata.setAvailableVotes(maxVotes);
                    metadata.setRestorePeriodSeconds(SECONDS_IN_DAY / maxVotes);
                } else {
                    var timeSinceCheckpoint =
                        now().getEpochSecond() - metadata.getRestoreVotesCheckpointDate().getEpochSecond();
                    var votesToRestore = (int) (timeSinceCheckpoint / metadata.getRestorePeriodSeconds());
                    if (votesToRestore > 0) {
                        metadata.setAvailableVotes(Math.min(
                            metadata.getMaxVotes(),
                            metadata.getAvailableVotes() + votesToRestore
                        ));
                        metadata.setRestoreVotesCheckpointDate(now());
                    }
                }
                metadata = userVoteMetadataRepository.save(metadata);
                votingMetadataOptional = Optional.of(metadata);
            }
        }
        return votingMetadataOptional.get();
    }

    public void addVoteDaysForRating(UUID userId, int numOfAdditionalDays) {
        var partner = partnerInternalClient.getPartnerById(userId);
        var votingMetadata = getUserVoteMetadata(userId);
        int maxVotes = calculateMaxVotes(partner.getId());

        if (votingMetadata.getVotingEndDate().isBefore(now())) {
            votingMetadata.setVotingEndDate(now().plus(numOfAdditionalDays, DAYS));
        } else {
            votingMetadata.setVotingEndDate(
                votingMetadata.getVotingEndDate().plus(numOfAdditionalDays, DAYS)
            );
        }

        votingMetadata.setMaxVotes(maxVotes);
        votingMetadata.setAvailableVotes(maxVotes);
        votingMetadata.setRestorePeriodSeconds(SECONDS_IN_DAY / maxVotes);
        votingMetadata.setIsTrial(false);
        votingMetadata.setIsTrialPassed(true);
        userVoteMetadataRepository.save(votingMetadata);
    }

    public void activateTrial(UUID userId) {
        var votingMetadata = getUserVoteMetadata(userId);
        if (votingMetadata.getIsTrial()) {
            throw new GlobalException(
                ErrorCode.ERROR_VALIDATION,
                messageSource.getMessage(
                    "RATING_TRIAL_ALREADY_ACTIVATED",
                    null,
                    LocaleContextHolder.getLocale()
                )
            );
        }
        if (votingMetadata.getIsTrialPassed()) {
            throw new GlobalException(
                ErrorCode.ERROR_VALIDATION,
                messageSource.getMessage(
                    "RATING_TRIAL_ALREADY_PASSED",
                    null,
                    LocaleContextHolder.getLocale()
                )
            );
        }

        votingMetadata.setIsTrial(true);
        votingMetadata.setMaxVotes(TRIAL_VOTES_LIMIT);
        votingMetadata.setAvailableVotes(votingMetadata.getMaxVotes());
        votingMetadata.setRestorePeriodSeconds(SECONDS_IN_DAY / votingMetadata.getMaxVotes());
        votingMetadata.setVotingEndDate(now().plus(TRIAL_DAYS_LIMIT, DAYS));
        userVoteMetadataRepository.save(votingMetadata);
    }

    public Artwork getArtworkForVoting(UUID userId) {
        var votingMetadata = getUserVoteMetadata(userId);

        isTrialSuggestion(votingMetadata);
        isEnoughVotes(votingMetadata);
        isDateValid(votingMetadata);

        //        var votedArtworkIds = voteRepository.findAllByPartnerId(userId)
        //                                  .stream()
        //                                  .map(vote -> vote.getArtworkId().toString())
        //                                  .collect(Collectors.toList());

        var votedArtworkIds = voteRepository.findAllByPartnerIdAndAndCreationDateAfter(userId, now().minus(1, DAYS))
                                  .stream()
                                  .map(vote -> vote.getArtworkId().toString())
                                  .collect(Collectors.toList());

        if (votedArtworkIds.isEmpty()) {
            votedArtworkIds.add("0");
        }
        var maybeRandomArtwork = artworkRepository.findHundredEligibleForRating(votedArtworkIds);

        var artwork = maybeRandomArtwork.orElseThrow(
            () -> new GlobalException(
                ErrorCode.NO_AVAILABLE_ARTWORKS_FOR_RATING,
                messageSource.getMessage(
                    "NO_AVAILABLE_ARTWORKS_FOR_RATING",
                    null,
                    LocaleContextHolder.getLocale()
                )
            )
        );

        var vote = voteRepository.findFirstByPartnerIdAndArtworkIdOrderByCreationDateDesc(userId, artwork.getId());
        if (SUITABLE_RANDOM_ARTWORK_PREDICATE.test(vote)) {
            return artwork;
        }

        throw new GlobalException(
            ErrorCode.NO_AVAILABLE_ARTWORKS_FOR_RATING,
            messageSource.getMessage(
                "NO_AVAILABLE_ARTWORKS_FOR_RATING",
                null,
                LocaleContextHolder.getLocale()
            )
        );
    }

    public Vote addVote(Vote vote) {
        if (vote.getVoteType() == SKIP) {
            voteRepository.save(vote);
            return vote;
        }

        var votingMetadata = getUserVoteMetadata(vote.getPartnerId());

        if (votingMetadata.getAvailableVotes().equals(votingMetadata.getMaxVotes())) {
            votingMetadata.setRestoreVotesCheckpointDate(now());
        }

        isEnoughVotes(votingMetadata);
        isDateValid(votingMetadata);
        isVotedForArtwork(vote);

        voteRepository.save(vote);

        votingMetadata.setAvailableVotes(votingMetadata.getAvailableVotes() - 1);
        votingMetadata.setVotesUntilMysteryBox(votingMetadata.getVotesUntilMysteryBox() - 1);
        userVoteMetadataRepository.save(votingMetadata);

        if (votingMetadata.getVotesUntilMysteryBox() <= 0) {
            vote.setMysteryBoxWin(true);
            vote = voteRepository.save(vote);
            partnerInternalClient.createMysteryBox(
                MysteryBoxDto.builder()
                    .ownerId(vote.getPartnerId())
                    .level(1) //should always be 1st level
                    .price(0D)
                    .build(),
                vote.getPartnerId()
            );
            votingMetadata.setVotesUntilMysteryBox(VOTES_TO_UNLOCK_MYSTERY_BOX);
            userVoteMetadataRepository.save(votingMetadata);
        }

        int votesNumberForArtwork = voteRepository.countVotesForArtwork(vote.getArtworkId());
        if (votesNumberForArtwork >= RATING_MIN_VOTES) {
            var artwork = artworkService.getArtworkById(vote.getArtworkId());
            var votesForArtwork = voteRepository.findByArtworkId(vote.getArtworkId());
            var positiveVotes = votesForArtwork.stream().filter(v -> v.getVoteType() == VoteType.POSITIVE).count();
            var negativeVotes = votesForArtwork.stream().filter(v -> v.getVoteType() == VoteType.NEGATIVE).count();
            double ratingPercentage = Math.round((double) positiveVotes / (positiveVotes + negativeVotes) * 100);
            artwork.setRating(ratingPercentage / 100);
            artworkService.save(artwork);
        }
        return vote;
    }

    @Scheduled(fixedRate = 3600000)
    public void cancelTrial() {
        var votingMetadataList = userVoteMetadataRepository.findAllByIsTrialTrue();
        votingMetadataList.forEach(votingMetadata -> {
            if (votingMetadata.getVotingEndDate().isBefore(now())) {
                votingMetadata.setIsTrial(false);
                votingMetadata.setIsTrialPassed(true);
                votingMetadata.setMaxVotes(calculateMaxVotes(votingMetadata.getPartnerId()));
                votingMetadata.setAvailableVotes(0);
                votingMetadata.setRestorePeriodSeconds(SECONDS_IN_DAY / votingMetadata.getMaxVotes());
                votingMetadata.setVotingEndDate(now());
                userVoteMetadataRepository.save(votingMetadata);
            }
        });
    }

    private void isVotedForArtwork(Vote vote) {
        var voteForArtwork = voteRepository.findFirstByPartnerIdAndArtworkIdOrderByCreationDateDesc(vote.getPartnerId(), vote.getArtworkId());
        if (voteForArtwork != null && voteForArtwork.getVoteType() != SKIP &&
                voteForArtwork.getCreationDate().isAfter(now().minus(1, DAYS))) {
            throw new GlobalException(
                ErrorCode.ERROR_VALIDATION,
                messageSource.getMessage(
                    "ALREADY_VOTED_FOR_ARTWORK",
                    null,
                    LocaleContextHolder.getLocale()
                )
            );
        }
    }

    private void isTrialSuggestion(UserVoteMetadata votingMetadata) {
        if (!votingMetadata.getIsTrial() && !votingMetadata.getIsTrialPassed()) {
            throw new GlobalException(
                ErrorCode.RATING_TRIAL_SUGGESTION,
                messageSource.getMessage(
                    "RATING_TRIAL_SUGGESTION",
                    null,
                    LocaleContextHolder.getLocale()
                )
            );
        }
    }

    private void isEnoughVotes(UserVoteMetadata votingMetadata) {
        if (votingMetadata.getAvailableVotes() <= 0) {
            throw new GlobalException(
                ErrorCode.NO_AVAILABLE_VOTES_FOR_RATING,
                messageSource.getMessage(
                    "NO_AVAILABLE_VOTES",
                    null,
                    LocaleContextHolder.getLocale()
                )
            );
        }
    }

    private void isDateValid(UserVoteMetadata votingMetadata) {
        if (votingMetadata.getVotingEndDate().isBefore(now())) {
            throw new GlobalException(
                ErrorCode.NO_AVAILABLE_DAYS_FOR_RATING,
                messageSource.getMessage(
                    "NO_AVAILABLE_DAYS_FOR_RATING",
                    null,
                    LocaleContextHolder.getLocale()
                )
            );
        }
    }

    private Instant calculateVotingEndDate(PartnerDTO partner) {
        var artworks = artworkService.getArtworks(partner.getId());
        var days = 0;
        if (!artworks.isEmpty()) {
            days += artworks.stream()
                        .filter(artwork -> artwork.getOwnerRole().equals(ArtworkOwnerRole.PARTNER))
                        .filter(artwork -> artwork.getLevel() != null)
                        .mapToInt(Artwork::getLevel)
                        .filter(level -> level > 0)
                        .map(level -> ARTWORK_LEVEL_TO_VOTING_DAYS.floorEntry(level).getValue())
                        .sum();
        }
        if (partner.isHasNftKey()) {
            days += NFT_KEY_VOTING_DAYS;
        }
        return now().plus(days, DAYS);
    }

    private int calculateMaxVotes(UUID partnerId) {
        double activityPoints = referralTransactionClient.getTotalEarningsForSpecificEntityType(
            partnerId,
            ACTIVITY_POINTS_BALANCE
        );
        var rank = ACTIVITY_POINTS_TO_PARTNER_RANK.floorEntry((int) activityPoints).getValue();
        return PARTNER_RANK_TO_MAX_VOTES.get(rank);
    }

}
