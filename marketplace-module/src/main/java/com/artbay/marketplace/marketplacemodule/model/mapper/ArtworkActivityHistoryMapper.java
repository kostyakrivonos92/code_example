package com.artbay.marketplace.marketplacemodule.model.mapper;

import com.artbay.marketplace.common.client.partner.internal.PartnerInternalClient;
import com.artbay.marketplace.marketplacemodule.model.activityhistory.ArtworkActivityHistory;
import com.artbay.marketplace.marketplacemodule.model.activityhistory.dto.ArtworkActivityHistoryDto;
import org.mapstruct.*;
import org.springframework.beans.factory.annotation.Autowired;

@Mapper(
    unmappedTargetPolicy = ReportingPolicy.IGNORE,
    nullValuePropertyMappingStrategy = NullValuePropertyMappingStrategy.IGNORE,
    nullValueCheckStrategy = NullValueCheckStrategy.ALWAYS
)
public abstract class ArtworkActivityHistoryMapper {

    @Autowired
    private PartnerInternalClient partnerInternalClient;

    @Mapping(source = "userId", target = "user.id")
    @Mapping(source = "username", target = "user.username")
    public abstract ArtworkActivityHistoryDto entityToDto(ArtworkActivityHistory entity);

    @AfterMapping
    public void fillAvatarLink(
        @MappingTarget final ArtworkActivityHistoryDto dto,
        final ArtworkActivityHistory entity
    ) {
        dto.getUser().setAvatarLink(partnerInternalClient.getPartnerById(entity.getUserId()).getAvatarLink());
    }

}
