package com.artbay.marketplace.marketplacemodule.model.mapper;

import com.artbay.marketplace.marketplacemodule.model.rating.Vote;
import com.artbay.marketplace.marketplacemodule.model.rating.dto.ShortArtworkInfoDto;
import com.artbay.marketplace.marketplacemodule.model.rating.dto.VoteDTO;
import com.artbay.marketplace.marketplacemodule.repository.ArtworkRepository;
import com.artbay.marketplace.marketplacemodule.repository.VoteRepository;
import org.mapstruct.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;

import java.util.List;

@Mapper(
    unmappedTargetPolicy = ReportingPolicy.IGNORE,
    nullValuePropertyMappingStrategy = NullValuePropertyMappingStrategy.IGNORE,
    nullValueCheckStrategy = NullValueCheckStrategy.ALWAYS
)
public abstract class VoteMapper {

    @Autowired
    private ArtworkRepository artworkRepository;
    @Autowired
    private VoteRepository voteRepository;

    public VoteDTO entityToDto(Vote vote) {
        if(vote == null) {
            return VoteDTO.builder().build();
        }
        int votesCount = voteRepository.countVotesForArtwork(vote.getArtworkId());
        var artwork = artworkRepository.getById(vote.getArtworkId());
        return VoteDTO.builder()
                   .id(vote.getId())
                   .voteType(vote.getVoteType())
                   .artwork(
                       ShortArtworkInfoDto.builder()
                           .artworkId(artwork.getId())
                           .artworkName(artwork.getName())
                           .rating(artwork.getRating())
                           .thumbnailUrl(artwork.getThumbnailFilepath())
                           .votesCount(votesCount)
                           .build()
                   )
                   .mysteryBoxWin(vote.getMysteryBoxWin())
                   .build();
    }

    public abstract List<VoteDTO> entityToDto(List<Vote> votes);

    @Mapping(target = "partnerId", expression = "java(com.artbay.marketplace.common.utils.ControllerUtil.getUserId())")
    @Mapping(target = "creationDate", expression = "java(java.time.Instant.now())")
    @Mapping(target = "artworkId", source = "artwork.artworkId")
    @Mapping(target = "mysteryBoxWin", ignore = true)
    public abstract Vote dtoToEntity(VoteDTO voteDto);

    public Page<VoteDTO> entityToDto(Page<Vote> page) {
        return new PageImpl<>(
            entityToDto(page.getContent()),
            page.getPageable(),
            page.getTotalElements()
        );
    }

}
