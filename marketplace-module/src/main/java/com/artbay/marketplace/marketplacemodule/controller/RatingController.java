package com.artbay.marketplace.marketplacemodule.controller;

import com.artbay.marketplace.common.model.ArtworkDTO;
import com.artbay.marketplace.marketplacemodule.model.mapper.ArtworkMapper;
import com.artbay.marketplace.marketplacemodule.model.mapper.UserVoteMetadataMapper;
import com.artbay.marketplace.marketplacemodule.model.mapper.VoteMapper;
import com.artbay.marketplace.marketplacemodule.model.rating.dto.UserVoteMetadataDTO;
import com.artbay.marketplace.marketplacemodule.model.rating.dto.VoteDTO;
import com.artbay.marketplace.marketplacemodule.service.RatingService;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;

import static com.artbay.marketplace.common.utils.ControllerUtil.getUserId;

@Slf4j
@RestController
@RequestMapping("/rating")
@RequiredArgsConstructor
@PreAuthorize("hasAuthority('user')")
public class RatingController {

    private final RatingService ratingService;

    private final UserVoteMetadataMapper userVoteMetadataMapper;
    private final VoteMapper voteMapper;
    private final ArtworkMapper artworkMapper;

    @GetMapping("/history")
    public Page<VoteDTO> getLastVotes(Pageable pageable) {
        return voteMapper.entityToDto(ratingService.getLastVotes(getUserId(), pageable));
    }

    @GetMapping("/current-artwork")
    public ArtworkDTO getArtworkForVoting() {
        return artworkMapper.entityToDto(ratingService.getArtworkForVoting(getUserId()));
    }

    @GetMapping("/voting-metadata")
    public UserVoteMetadataDTO getVotingMetadata() {
        return userVoteMetadataMapper.entityToDto(ratingService.getUserVoteMetadata(getUserId()));
    }

    @GetMapping("/activate-trial")
    public void activateTrial() {
        ratingService.activateTrial(getUserId());
    }

    @PostMapping("/vote")
    public VoteDTO addVote(@RequestBody @Valid VoteDTO voteDto) {
        var vote = voteMapper.dtoToEntity(voteDto);
        return voteMapper.entityToDto(ratingService.addVote(vote));
    }

}
