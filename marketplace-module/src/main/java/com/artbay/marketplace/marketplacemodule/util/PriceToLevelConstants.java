package com.artbay.marketplace.marketplacemodule.util;

import lombok.AccessLevel;
import lombok.NoArgsConstructor;
import org.springframework.stereotype.Component;

import java.util.NavigableMap;
import java.util.TreeMap;

@Component
@NoArgsConstructor(access = AccessLevel.PRIVATE)
public class PriceToLevelConstants {

    public static final NavigableMap<Double, Integer> ARTWORK_PRICE_TO_LEVEL_MAP = new TreeMap<>();
    static {
        ARTWORK_PRICE_TO_LEVEL_MAP.put(40.0, 1);
        ARTWORK_PRICE_TO_LEVEL_MAP.put(80.0, 2);
        ARTWORK_PRICE_TO_LEVEL_MAP.put(160.0, 3);
        ARTWORK_PRICE_TO_LEVEL_MAP.put(320.0, 4);
        ARTWORK_PRICE_TO_LEVEL_MAP.put(640.0, 5);
        ARTWORK_PRICE_TO_LEVEL_MAP.put(1280.0, 6);
        ARTWORK_PRICE_TO_LEVEL_MAP.put(2560.0, 7);
        ARTWORK_PRICE_TO_LEVEL_MAP.put(5120.0, 8);
        ARTWORK_PRICE_TO_LEVEL_MAP.put(10240.0, 9);
        ARTWORK_PRICE_TO_LEVEL_MAP.put(20480.0, 10);
    }

    public static final NavigableMap<Integer, Double> ARTWORK_LEVEL_TO_PRICE_MAP = new TreeMap<>();
    static {
        ARTWORK_LEVEL_TO_PRICE_MAP.put(1, 40.0);
        ARTWORK_LEVEL_TO_PRICE_MAP.put(2, 80.0);
        ARTWORK_LEVEL_TO_PRICE_MAP.put(3, 160.0);
        ARTWORK_LEVEL_TO_PRICE_MAP.put(4, 320.0);
        ARTWORK_LEVEL_TO_PRICE_MAP.put(5, 640.0);
        ARTWORK_LEVEL_TO_PRICE_MAP.put(6, 1280.0);
        ARTWORK_LEVEL_TO_PRICE_MAP.put(7, 2560.0);
        ARTWORK_LEVEL_TO_PRICE_MAP.put(8, 5120.0);
        ARTWORK_LEVEL_TO_PRICE_MAP.put(9, 10240.0);
        ARTWORK_LEVEL_TO_PRICE_MAP.put(10, 20480.0);
    }

}