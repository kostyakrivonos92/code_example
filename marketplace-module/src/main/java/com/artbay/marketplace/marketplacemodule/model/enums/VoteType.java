package com.artbay.marketplace.marketplacemodule.model.enums;

public enum VoteType {

    POSITIVE, NEGATIVE, SKIP

}
