package com.artbay.marketplace.marketplacemodule.model;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.*;
import java.util.UUID;

@Data
@Builder
@AllArgsConstructor
@NoArgsConstructor
@Entity
@Table(name = "nft_keys")
public class NFTKey {

    @Id
    @GeneratedValue
    @Column(name = "id")
    private UUID id;

    @Column(name = "name")
    private String name;

    @Column(name = "current_owner_id")
    private UUID currentOwnerId;

    //tech details
    @Column(name = "nft_metadata")
    private String nftMetadata;
    @Column(name = "filepath")
    private String filepath;
    @Column(name = "token_address")
    private String tokenAddress;

    @Column(name = "on_sale")
    private Boolean onSale;

    @Column(name = "exported")
    @Builder.Default
    private Boolean exported = false;
    @Column(name = "imported")
    @Builder.Default
    private Boolean imported = false;

}
