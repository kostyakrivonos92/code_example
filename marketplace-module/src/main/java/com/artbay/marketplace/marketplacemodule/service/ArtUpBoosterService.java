package com.artbay.marketplace.marketplacemodule.service;

import com.artbay.marketplace.common.client.partner.internal.PartnerInternalClient;
import com.artbay.marketplace.common.client.referral.ReferralExchangeRatesClient;
import com.artbay.marketplace.common.client.referral.internal.ReferralInternalClient;
import com.artbay.marketplace.common.exception.ErrorCode;
import com.artbay.marketplace.common.exception.GlobalException;
import com.artbay.marketplace.common.model.PartnerDTO;
import com.artbay.marketplace.marketplacemodule.model.ArtUpBooster;
import com.artbay.marketplace.marketplacemodule.model.Artwork;
import com.artbay.marketplace.marketplacemodule.model.response.ArtUpBoosterPageResponse;
import com.artbay.marketplace.marketplacemodule.repository.ArtUpBoosterRepository;
import com.artbay.marketplace.marketplacemodule.repository.ArtworkRepository;
import com.artbay.marketplace.marketplacemodule.repository.ArtworkActivityHistoryRepository;
import com.artbay.marketplace.marketplacemodule.util.ArtUpBoosterPriceConstants;
import com.artbay.marketplace.marketplacemodule.util.UserLevelUtil;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.context.MessageSource;
import org.springframework.context.i18n.LocaleContextHolder;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.stereotype.Service;

import java.util.*;
import java.util.stream.Collectors;

import static com.artbay.marketplace.common.model.enums.ArtworkVerificationStatus.DELETED;
import static com.artbay.marketplace.common.model.enums.ArtworkVerificationStatus.REVOKED;
import static com.artbay.marketplace.marketplacemodule.util.ArtUpBoosterPriceConstants.PRICE_ART_UP;
import static com.artbay.marketplace.marketplacemodule.util.ArtUpBoosterPriceConstants.PRICE_ART_UP_BOOSTER;
import static com.artbay.marketplace.marketplacemodule.util.activityhistory.ArtworkActivityHistoryUtil.buildLevelUpArtworkActivityHistory;
import static com.artbay.marketplace.marketplacemodule.util.PriceToLevelConstants.ARTWORK_LEVEL_TO_PRICE_MAP;
import static java.util.stream.Collectors.groupingBy;

@Slf4j
@Service
@RequiredArgsConstructor
public class ArtUpBoosterService {

    private final PartnerInternalClient partnerInternalClient;
    private final ReferralInternalClient referralInternalClient;
    private final ReferralExchangeRatesClient referralExchangeRatesClient;

    private final ArtworkService artworkService;
    private final ArtworkActivityHistoryRepository artworkActivityHistoryRepository;
    private final ArtUpBoosterRepository artUpBoosterRepository;
    private final ArtworkRepository artworkRepository;

    private final UserLevelUtil userLevelUtil;
    private final ArtUpBoosterPriceConstants artUpBoosterPriceConstants;

    @Qualifier("activeMessageSource")
    private final MessageSource messageSource;

    public List<ArtUpBoosterPageResponse> getAvailableForUseArtUpBoosters(UUID partnerId) {
        List<ArtUpBoosterPageResponse> responseList = new ArrayList<>();

        Map<Integer, Long> availableForUseArtupsLevelCount = artUpBoosterRepository.findAllByOwnerId(partnerId)
                                                                 .stream()
                                                                 .filter(artUpBooster -> !artUpBooster.getUsed())
                                                                 .collect(groupingBy(
                                                                     ArtUpBooster::getLevel,
                                                                     Collectors.counting()
                                                                 ));
        initArtUpMap().forEach((level, isAvailable) -> {
            ArtUpBoosterPageResponse response;

            if (availableForUseArtupsLevelCount.containsKey(level) && isAvailable) {
                response = new ArtUpBoosterPageResponse(
                    level,
                    availableForUseArtupsLevelCount.get(level),
                    PRICE_ART_UP_BOOSTER.get(level),
                    true
                );
            } else {
                response = new ArtUpBoosterPageResponse(
                    level,
                    0L,
                    PRICE_ART_UP_BOOSTER.get(level),
                    isAvailable
                );
            }

            responseList.add(response);
        });
        return responseList;
    }

    public List<ArtUpBoosterPageResponse> getArtUpPrice() {
        return artUpBoosterPriceConstants.getArtUpPrice();
    }

    public void buyArtUpBooster(UUID partnerId, int artUpLevel) {
        int artUpPrice = PRICE_ART_UP_BOOSTER.get(artUpLevel);
        ArtUpBooster artUp = ArtUpBooster.builder()
                                 .price(artUpPrice)
                                 .ownerId(partnerId)
                                 .level(artUpLevel)
                                 .used(false)
                                 .build();

        referralInternalClient.buyArtUpBooster(partnerId, artUpPrice, artUpLevel);
        artUpBoosterRepository.save(artUp);
    }

    public void buyArtUpLevelByArtz(UUID partnerId, UUID artworkId, int artUpLevel) {
        double upLevelPriceArtz =
            PRICE_ART_UP.get(artUpLevel) / referralExchangeRatesClient.getArtzUsdPair().getPrice();
        Artwork artwork = artworkService.getArtworkById(artworkId);
        if (artwork.getCurrentOwnerId().equals(partnerId) && !artwork.getRootOwnerId().equals(partnerId) &&
                artwork.getPublishedToMarketplace().equals(false) && !artwork.getStatus().equals(REVOKED) &&
                artwork.getLevel() == artUpLevel) {
            PartnerDTO partner = partnerInternalClient.getPartnerById(partnerId);
            referralInternalClient.buyArtUpLevelByArtz(partnerId, upLevelPriceArtz, artUpLevel);
            userLevelUtil.openReferralLevel(artwork, partnerInternalClient.getPartnerById(partnerId));
            artwork.setLevel(artwork.getLevel() + 1);
            artwork.setPrice(ARTWORK_LEVEL_TO_PRICE_MAP.get(artwork.getLevel()));
            artworkService.save(artwork);
            artworkActivityHistoryRepository.save(
                buildLevelUpArtworkActivityHistory(
                    partner.getId(),
                    partner.getUsername(),
                    artwork.getId(),
                    artwork.getLevel()
                )
            );
        }
    }

    public void applyArtUpBooster(Integer artUpLevel, UUID artworkId, UUID partnerId) {
        Artwork artwork = artworkService.getArtworkById(artworkId);
        if (artwork.getCurrentOwnerId().equals(partnerId) && !artwork.getRootOwnerId().equals(partnerId) &&
                artwork.getImported().equals(false) && !artwork.getStatus().equals(REVOKED)) {
            Optional<ArtUpBooster> maybeArtUp =
                artUpBoosterRepository.findAllByOwnerId(partnerId)
                    .stream()
                    .filter(artUpBooster -> Objects.equals(artUpBooster.getLevel(), artUpLevel))
                    .filter(artUpBooster -> !artUpBooster.getUsed())
                    .findFirst();

            if (maybeArtUp.isPresent()) {
                ArtUpBooster artUp = maybeArtUp.get();

                if (!Objects.equals(artwork.getLevel(), artUpLevel) && artwork.getPublishedToMarketplace()) {
                    throw new GlobalException(
                        ErrorCode.ERROR_VALIDATION,
                        messageSource.getMessage(
                            "LOW_ARTUP_LEVEL",
                            null,
                            LocaleContextHolder.getLocale()
                        )
                    );
                } else if (!artwork.getEligibleForPromotion()) {
                    throw new GlobalException(
                        ErrorCode.ERROR_VALIDATION,
                        messageSource.getMessage(
                            "USE_ARTUP_NOT_AVAILABLE",
                            null,
                            LocaleContextHolder.getLocale()
                        )
                    );
                }
                artUp.setUsed(true);
                artUpBoosterRepository.save(artUp);

                artwork.setLevel(artwork.getLevel() + 1);
                artwork.setEligibleForPromotion(false);
                artwork.setPrice(ARTWORK_LEVEL_TO_PRICE_MAP.get(artwork.getLevel()));
                artwork = artworkService.save(artwork);

                PartnerDTO partner = partnerInternalClient.getPartnerById(partnerId);

                artworkActivityHistoryRepository.save(
                    buildLevelUpArtworkActivityHistory(
                        partner.getId(),
                        partner.getUsername(),
                        artwork.getId(),
                        artwork.getLevel()
                    )
                );

                if (partner.getReferralLevel() < artwork.getLevel() && partner.isHasNftKey()) {
                    partner.setReferralLevel(artwork.getLevel());
                    partnerInternalClient.updatePartner(partner);
                    referralInternalClient.accrualHiddenRewardsForNextLevel(partner, artUpLevel);
                }
            } else {
                log.error("No art up booster found for partner {} and level {}", partnerId, artUpLevel);
                throw new GlobalException(
                    ErrorCode.INSUFFICIENT_FUNDS,
                    messageSource.getMessage(
                        "CANT_USE_ART_UP",
                        null,
                        LocaleContextHolder.getLocale()
                    )
                );
            }
        } else {
            throw new GlobalException(
                ErrorCode.BAD_REQUEST,
                messageSource.getMessage(
                    "UNABLE_APPLY_ARTUP_TO_NFT",
                    null,
                    LocaleContextHolder.getLocale()
                )
            );
        }
    }

    public Page<Artwork> getEligibleForPromotionArtworks(
        Specification<Artwork> specification,
        Pageable pageable,
        UUID userId
    ) {
        Specification<Artwork> artworksSpec = (root, query, criteriaBuilder) -> criteriaBuilder.and(
            criteriaBuilder.notEqual(root.get("status"), DELETED),
            criteriaBuilder.equal(root.get("currentOwnerId"), userId)
        );
        return artworkRepository.findAll(specification.and(artworksSpec), pageable);
    }

    public void createMysteryBoxPrizeArtUpBooster(UUID userId, int artUpLevel){
        ArtUpBooster artUp = ArtUpBooster.builder()
                                 .ownerId(userId)
                                 .level(artUpLevel)
                                 .used(false)
                                 .build();

        artUpBoosterRepository.save(artUp);
    }

    private Map<Integer, Boolean> initArtUpMap() {
        Map<Integer, Boolean> artUpMap = new HashMap<>();
        for (int i = 1; i < 10; i++) {
            artUpMap.put(i, true);
        }
        return artUpMap;
    }

}
