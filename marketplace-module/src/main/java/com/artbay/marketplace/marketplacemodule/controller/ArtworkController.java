package com.artbay.marketplace.marketplacemodule.controller;

import com.artbay.marketplace.common.exception.ErrorCode;
import com.artbay.marketplace.common.exception.GlobalException;
import com.artbay.marketplace.common.model.ArtworkDTO;
import com.artbay.marketplace.common.model.response.ExportNftResponse;
import com.artbay.marketplace.common.twofactor.Required2fa;
import com.artbay.marketplace.common.validation.LoadableFile;
import com.artbay.marketplace.marketplacemodule.model.Artwork;
import com.artbay.marketplace.marketplacemodule.model.activityhistory.dto.ArtworkActivityHistoryDto;
import com.artbay.marketplace.marketplacemodule.model.mapper.ArtworkActivityHistoryMapper;
import com.artbay.marketplace.marketplacemodule.model.mapper.ArtworkMapper;
import com.artbay.marketplace.marketplacemodule.model.mapper.UserInfoMapper;
import com.artbay.marketplace.marketplacemodule.service.ArtworkService;
import com.artbay.marketplace.marketplacemodule.service.ImportExportNftService;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import net.kaczmarzyk.spring.data.jpa.domain.*;
import net.kaczmarzyk.spring.data.jpa.web.annotation.And;
import net.kaczmarzyk.spring.data.jpa.web.annotation.Spec;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.http.HttpStatus;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

import javax.validation.Valid;
import java.util.List;
import java.util.Map;
import java.util.NavigableMap;
import java.util.UUID;
import java.util.stream.Collectors;

import static com.artbay.marketplace.common.utils.ControllerUtil.getUserId;
import static com.artbay.marketplace.marketplacemodule.util.PriceToLevelConstants.ARTWORK_PRICE_TO_LEVEL_MAP;

@Slf4j
@RestController
@RequestMapping("/artworks")
@RequiredArgsConstructor
@Validated
public class ArtworkController {

    private final ArtworkService artworkService;
    private final ImportExportNftService importExportNftService;

    private final ArtworkMapper artworkMapper;
    private final UserInfoMapper userInfoMapper;
    private final ArtworkActivityHistoryMapper artworkActivityHistoryMapper;

    @GetMapping
    public Page<ArtworkDTO> getArtworks(
        @And({
            @Spec(path = "publishedToMarketplace", constVal = "true", valueInSpEL = true, spec = EqualIgnoreCase.class),
            @Spec(path = "collection.id", params = "collectionId", spec = Equal.class),
            @Spec(path = "currentOwnerId", params = "currentOwner", spec = Equal.class),
            @Spec(path = "rootOwnerId", params = "rootOwner", spec = Equal.class),
            @Spec(path = "name", params = "name", spec = LikeIgnoreCase.class),
            @Spec(path = "price", params = "minPrice", spec = GreaterThanOrEqual.class),
            @Spec(path = "price", params = "maxPrice", spec = LessThanOrEqual.class),
            @Spec(path = "level", params = "levels", paramSeparator = ',', spec = In.class),
            @Spec(path = "exported", constVal = "false", spec = Equal.class),
            @Spec(path = "status", constVal = "VERIFIED", spec = Equal.class),
            @Spec(path = "firstSale", params = "firstSale", spec = EqualIgnoreCase.class),
            @Spec(path = "promoted", params = "promoted", spec = EqualIgnoreCase.class)
        }) Specification<Artwork> specification, Pageable pageable
    ) {
        return artworkMapper.artworksToDtoPage(artworkService.searchArtworks(
            specification,
            pageable
        ));
    }

    @GetMapping("/promoted")
    public List<ArtworkDTO> getPromotedArtworks(
        @And({
            @Spec(path = "publishedToMarketplace", constVal = "true", valueInSpEL = true, spec = EqualIgnoreCase.class),
            @Spec(path = "promoted", constVal = "true", valueInSpEL = true, spec = EqualIgnoreCase.class),
            @Spec(path = "status", constVal = "VERIFIED", spec = Equal.class),
            @Spec(path = "status", constVal = "DELETED", spec = NotEqual.class)
        }) Specification<Artwork> specification
    ) {
        return artworkMapper.entityListToDtoListWithAvatar(artworkService.getPromotedArtworks(
            specification
        ));
    }

    @PostMapping
    @ResponseStatus(HttpStatus.CREATED)
    @PreAuthorize("hasAuthority('artist_allowed_artwork_creation')")
    public ArtworkDTO createArtwork(
        @Valid @LoadableFile(permittedTypes = {"image",
                                               "video"}) @RequestPart(value = "file") MultipartFile multipartFile,
        @Valid @RequestPart("artwork") ArtworkDTO artwork
    ) {
        if (artwork.getName().getBytes().length > 32) {
            throw new GlobalException(ErrorCode.ERROR_VALIDATION, "Artwork name is too large");
        }
        return artworkMapper.entityToDto(
            artworkService.createArtwork(
                multipartFile,
                artwork,
                getUserId()
            )
        );
    }

    @PutMapping("/purchase/{artworkId}")
    @PreAuthorize("hasAuthority('user_allowed_purchase')")
    public void purchaseArtwork(@PathVariable UUID artworkId) {
        artworkService.purchaseArtwork(artworkId, getUserId());
    }

    @GetMapping("/my-network")
    @PreAuthorize("hasAuthority('user')")
    public List<ArtworkDTO> getArtworksFromPartnerNetwork() {
        return artworkMapper.entityToDto(artworkService.getArtworksFromPartnerNetwork(getUserId()));
    }

    @GetMapping("/{artworkId}")
    public ArtworkDTO getArtworkById(@PathVariable UUID artworkId) {
        ArtworkDTO artwork = artworkMapper.entityToDtoWithAvatar(artworkService.getArtworkById(artworkId));
        return userInfoMapper.setUserInfoToArtwork(artwork);
    }

    @GetMapping("/artwork-activity-history/{artworkId}")
    public List<ArtworkActivityHistoryDto> getArtworkActivityHistory(@PathVariable UUID artworkId) {
        return artworkService.getArtworkHistory(artworkId)
                   .stream()
                   .map(artworkActivityHistoryMapper::entityToDto)
                   .collect(Collectors.toList());
    }

    @GetMapping(value = "/artist/{artistId}")
    public Page<ArtworkDTO> getArtworksByArtist(
        @PathVariable UUID artistId,
        @RequestParam UUID artworkId,
        Pageable pageable
    ) {
        return artworkMapper.artworksToDtoPage(artworkService.getArtworksByArtist(artistId, artworkId, pageable));
    }

    @GetMapping("/price-to-level")
    @PreAuthorize("hasAuthority('user')")
    public NavigableMap<Double, Integer> getLevelPriceMap() {
        return ARTWORK_PRICE_TO_LEVEL_MAP;
    }

    @Required2fa
    @PutMapping("/export")
    @PreAuthorize("hasAnyAuthority('artist_allowed_withdrawal', 'user_allowed_withdrawal')")
    public ExportNftResponse exportArtwork(
        @RequestParam(required = false) UUID nftId,
        @RequestParam(required = false) String toWalletPublicKey
    ) {
        return importExportNftService.export(nftId, getUserId(), toWalletPublicKey);
    }

    @GetMapping("/{artworkId}/revoke-from-marketplace")
    @PreAuthorize("hasAnyAuthority('user, artist_verified')")
    public void revokeArtworkFromMarketplace(@PathVariable UUID artworkId) {
        artworkService.revokeArtworkFromMarketplace(getUserId(), artworkId);
    }

    @GetMapping("/on-verification-count")
    @PreAuthorize("hasAuthority('artist_allowed_artwork_creation')")
    public Map<String, Integer> getArtworkVerificationsCount() {
        int activeVerificationsCount = artworkService.getArtworksByRootOwnerIdAndNotVerified(getUserId())
                                           .size();
        return Map.of("count", activeVerificationsCount);
    }


}
