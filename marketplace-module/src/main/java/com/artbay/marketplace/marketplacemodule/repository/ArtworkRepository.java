package com.artbay.marketplace.marketplacemodule.repository;

import com.artbay.marketplace.common.model.enums.ArtworkVerificationStatus;
import com.artbay.marketplace.common.model.enums.CollectionStatus;
import com.artbay.marketplace.marketplacemodule.model.Artwork;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import java.util.Collection;
import java.util.List;
import java.util.Optional;
import java.util.UUID;

@Repository
public interface ArtworkRepository extends JpaRepository<Artwork, UUID>, JpaSpecificationExecutor<Artwork> {

    List<Artwork> findAllByCurrentOwnerIdIn(List<UUID> userIds);

    List<Artwork> findAllByRootOwnerId(UUID rootOwnerId);

    Page<Artwork> findAllByRootOwnerIdAndCurrentOwnerIdAndPublishedToMarketplaceIsTrue(
        UUID rootOwnerId,
        UUID currentOwnerId,
        Pageable pageable
    );

    List<Artwork> findAllByRootOwnerIdAndStatus(UUID artistId, ArtworkVerificationStatus status);

    /**
     * Dirty hack for getting artworks that were sold by a specific artist
     *
     * @param artistId       - usually equal to currentOwnerId
     * @param currentOwnerId - usually equal to artistId
     * @return list of artworks that were sold by a specific artist
     */
    Page<Artwork> findAllByRootOwnerIdAndCurrentOwnerIdNotAndFirstSale(
        UUID artistId,
        UUID currentOwnerId,
        boolean firstSale,
        Pageable pageable
    );

    List<Artwork> findAllByRootOwnerIdAndCurrentOwnerIdNotAndFirstSaleIsTrue(UUID artistId, UUID currentOwnerId);

    List<Artwork> findAllByCurrentOwnerIdAndPublishedToMarketplaceFalseAndStatus(
        UUID userId,
        ArtworkVerificationStatus status
    );

    List<Artwork> findAllByCurrentOwnerIdAndLevel(UUID currentRootId, int level);

    List<Artwork> findAllByCurrentOwnerIdAndStatusInAndImportedIsFalse(
        UUID currentOwnerId,
        List<ArtworkVerificationStatus> status
    );

    Optional<Artwork> findByTokenAddress(String tokenAddress);

    @Query(
        nativeQuery = true,
        value = "SELECT * FROM artworks a " +
                    "WHERE a.first_sale = true AND a.exported = false AND a.status = 'VERIFIED' " +
                    "AND cast(a.id as text) NOT IN :votedArtworkIds " +
                    "ORDER BY RANDOM() LIMIT 1"
    )
    Optional<Artwork> findHundredEligibleForRating(@Param("votedArtworkIds") Collection<String> votedArtworkIds);

    boolean existsArtworksByCollection_IdAndStatusNot(UUID collectionId, CollectionStatus status);

}
