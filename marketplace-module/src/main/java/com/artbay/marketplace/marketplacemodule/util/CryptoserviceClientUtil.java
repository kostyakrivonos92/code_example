package com.artbay.marketplace.marketplacemodule.util;

import com.artbay.marketplace.common.exception.ErrorCode;
import com.artbay.marketplace.common.exception.GlobalException;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.context.MessageSource;
import org.springframework.context.i18n.LocaleContextHolder;
import org.springframework.stereotype.Component;

@Slf4j
@Component
@RequiredArgsConstructor
public class CryptoserviceClientUtil {

    private final ObjectMapper objectMapper;

    @Qualifier("activeMessageSource")
    private final MessageSource messageSource;

    public String getBlockchainTxIdFromResponse(String transferTokensResponse) {
        try {
            String transactionId = objectMapper.readTree(transferTokensResponse).get("data").get("txid").asText();

            if (transactionId == null) {
                log.error(
                    "[REFERRAL] Unable to parse blockchain transaction id from response: {}",
                    transferTokensResponse
                );
                throw new GlobalException(
                    ErrorCode.ERROR_VALIDATION,
                    messageSource.getMessage(
                        "UNABLE_TO_PARSE_TRANSACTION_ID",
                        null,
                        LocaleContextHolder.getLocale()
                    )
                );
            }
            return transactionId;
        } catch (JsonProcessingException e) {
            log.error("Unable to parse blockchain transaction id from response: {}", transferTokensResponse);
            throw new GlobalException(
                ErrorCode.ERROR_VALIDATION,
                messageSource.getMessage(
                    "UNABLE_TO_PARSE_TRANSACTION_ID",
                    null,
                    LocaleContextHolder.getLocale()
                )
            );
        }
    }

}
