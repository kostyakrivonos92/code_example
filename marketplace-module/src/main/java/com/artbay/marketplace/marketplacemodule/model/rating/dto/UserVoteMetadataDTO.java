package com.artbay.marketplace.marketplacemodule.model.rating.dto;

import lombok.Data;

import java.time.Instant;
import java.util.UUID;

@Data
public class UserVoteMetadataDTO {

    private UUID partnerId;
    private Integer maxVotes;
    private Integer availableVotes;
    private Integer votesUntilMysteryBox;
    private Instant votingEndDate;
    private Integer mysteryBoxCount;
    private Boolean isTrial;
    private Boolean isTrialPassed;

}
