package com.artbay.marketplace.marketplacemodule.model.activityhistory.dto;

import com.artbay.marketplace.common.model.ShortUserInfoDTO;
import com.artbay.marketplace.marketplacemodule.model.enums.ActivityHistoryOperationType;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.time.Instant;

@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
public class ArtworkActivityHistoryDto {

    private ActivityHistoryOperationType operationType;
    private Double price;
    private ShortUserInfoDTO user;
    private Instant operationDate;
    private Integer previousLevel;
    private Integer newLevel;

}
