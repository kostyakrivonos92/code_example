package com.artbay.marketplace.marketplacemodule.util;

import com.artbay.marketplace.common.client.referral.ReferralExchangeRatesClient;
import com.artbay.marketplace.marketplacemodule.model.response.ArtUpBoosterPageResponse;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Component;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

@Component
@RequiredArgsConstructor
public class ArtUpBoosterPriceConstants {

    private final ReferralExchangeRatesClient referralExchangeRatesClient;

    public static final int PRICE_L1_ART_UP = 80;
    public static final int PRICE_L2_ART_UP = 130;
    public static final int PRICE_L3_ART_UP = 220;
    public static final int PRICE_L4_ART_UP = 340;
    public static final int PRICE_L5_ART_UP = 440;
    public static final int PRICE_L6_ART_UP = 680;
    public static final int PRICE_L7_ART_UP = 1100;
    public static final int PRICE_L8_ART_UP = 1800;
    public static final int PRICE_L9_ART_UP = 2500;

    public static final Map<Integer, Integer> PRICE_ART_UP_BOOSTER = Map.of(
        1, PRICE_L1_ART_UP,
        2, PRICE_L2_ART_UP,
        3, PRICE_L3_ART_UP,
        4, PRICE_L4_ART_UP,
        5, PRICE_L5_ART_UP,
        6, PRICE_L6_ART_UP,
        7, PRICE_L7_ART_UP,
        8, PRICE_L8_ART_UP,
        9, PRICE_L9_ART_UP
    );

    public static final int PRICE_UP_L1 = 40;
    public static final int PRICE_UP_L2 = 80;
    public static final int PRICE_UP_L3 = 160;
    public static final int PRICE_UP_L4 = 320;
    public static final int PRICE_UP_L5 = 640;
    public static final int PRICE_UP_L6 = 1280;
    public static final int PRICE_UP_L7 = 2560;
    public static final int PRICE_UP_L8 = 5120;
    public static final int PRICE_UP_L9 = 10240;

    public static final Map<Integer, Integer> PRICE_ART_UP = Map.of(
        1, PRICE_UP_L1,
        2, PRICE_UP_L2,
        3, PRICE_UP_L3,
        4, PRICE_UP_L4,
        5, PRICE_UP_L5,
        6, PRICE_UP_L6,
        7, PRICE_UP_L7,
        8, PRICE_UP_L8,
        9, PRICE_UP_L9
    );

    public List<ArtUpBoosterPageResponse> getArtUpPrice() {
        List<ArtUpBoosterPageResponse> responseList = new ArrayList<>();
        var artzUsdPair= referralExchangeRatesClient.getArtzUsdPair().getPrice();
        for (int level = 1; level < 10; level = level + 1) {
            ArtUpBoosterPageResponse response;
            double upLevelPriceArtz = PRICE_ART_UP.get(level) / artzUsdPair;
            response = new ArtUpBoosterPageResponse(
                level,
                0L,
                (int) upLevelPriceArtz,
                true
            );

            responseList.add(response);
        }
        return responseList;
    }

}
