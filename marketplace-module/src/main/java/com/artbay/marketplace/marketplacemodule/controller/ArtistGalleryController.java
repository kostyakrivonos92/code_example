package com.artbay.marketplace.marketplacemodule.controller;

import com.artbay.marketplace.common.model.ArtworkDTO;
import com.artbay.marketplace.marketplacemodule.model.Artwork;
import com.artbay.marketplace.marketplacemodule.model.mapper.ArtworkMapper;
import com.artbay.marketplace.marketplacemodule.service.ArtworkService;
import io.swagger.annotations.ApiOperation;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import net.kaczmarzyk.spring.data.jpa.domain.*;
import net.kaczmarzyk.spring.data.jpa.web.annotation.And;
import net.kaczmarzyk.spring.data.jpa.web.annotation.Spec;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.http.HttpStatus;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.Map;
import java.util.UUID;

import static com.artbay.marketplace.common.utils.ControllerUtil.getUserId;

@Slf4j
@RestController
@RequestMapping("/artist/gallery")
@RequiredArgsConstructor
public class ArtistGalleryController {

    private final ArtworkService artworkService;

    private final ArtworkMapper artworkMapper;

    /**
     * @return list of artworks that are published to marketplace
     */
    @GetMapping("/artworks")
    @PreAuthorize("hasAuthority('artist_verified')")
    @ApiOperation(value = "Search artworks, request may be missing for get all artworks." +
                              "For sort by 'creationDate' you must concat in url '?sort=creationDate,asc or desc'.")
    public Page<ArtworkDTO> getArtistArtworks(
        @And(
            {
                @Spec(path = "currentOwnerId", params = "currentOwner", spec = Equal.class),
                @Spec(path = "name", params = "name", spec = LikeIgnoreCase.class),
                @Spec(path = "publishedToMarketplace", params = "published", spec = EqualIgnoreCase.class),
                @Spec(path = "status", params = "status", paramSeparator = ',', spec = In.class),
                @Spec(path = "price", params = "minPrice", spec = GreaterThanOrEqual.class),
                @Spec(path = "price", params = "maxPrice", spec = LessThanOrEqual.class),
                @Spec(path = "level", params = "levels", paramSeparator = ',', spec = In.class),
                @Spec(path = "imported", params = "imported", defaultVal = "false", spec = EqualIgnoreCase.class),
                @Spec(path = "firstSale", params = "firstSale", spec = EqualIgnoreCase.class)
            }
        )
        Specification<Artwork> specification,
        Pageable pageable
    ) {
        return artworkMapper.artworksToDtoPage(
            artworkService.getArtistMyArtworks(
                getUserId(),
                specification,
                pageable
            )
        );
    }

    @GetMapping("/tab-counters")
    @PreAuthorize("hasAuthority('artist_verified')")
    public Map<String, Long> tabCounters() {
        return artworkService.getArtistTabCounters(getUserId());
    }

    @GetMapping("/sold-artworks")
    @PreAuthorize("hasAuthority('artist_verified')")
    public Page<ArtworkDTO> getSoldArtworks(Pageable pageable) {
        return artworkMapper.artworksToDtoPage(artworkService.getSoldArtworks(getUserId(), pageable));
    }

    @GetMapping("/eligible-for-export")
    @PreAuthorize("hasAuthority('artist_verified')")
    public List<ArtworkDTO> getArtworksEligibleForExport() {
        return artworkMapper.entityToDto(artworkService.getArtworksEligibleForExport(getUserId()));
    }

    @PutMapping("/sell")
    @ResponseStatus(HttpStatus.OK)
    @PreAuthorize("hasAuthority('artist_allowed_selling')")
    public void publishToMarketplace(@RequestBody ArtworkDTO artwork) {
        artworkService.publishToMarketplaceArtist(getUserId(), artwork);
    }

    @GetMapping("/{artworkId}/revoke-from-marketplace")
    @PreAuthorize("hasAuthority('artist_verified')")
    public void revokeArtworkFromMarketplace(@PathVariable UUID artworkId) {
        artworkService.revokeArtworkFromMarketplace(getUserId(), artworkId);
    }

    @DeleteMapping("/delete/{artworkId}")
    @PreAuthorize("hasAnyAuthority('artist_verified')")
    public void deleteArtwork(@PathVariable UUID artworkId) {
        artworkService.deleteArtwork(artworkId, getUserId());
    }

}
