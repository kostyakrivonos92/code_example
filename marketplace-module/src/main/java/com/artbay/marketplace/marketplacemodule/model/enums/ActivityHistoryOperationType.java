package com.artbay.marketplace.marketplacemodule.model.enums;

public enum ActivityHistoryOperationType {
    CREATED,
    SALES,
    LEVEL_UP,
    PURCHASE,
    EXPORT,
    IMPORT,
    EXCHANGE,
    PLATFORM_ACCRUAL
}
