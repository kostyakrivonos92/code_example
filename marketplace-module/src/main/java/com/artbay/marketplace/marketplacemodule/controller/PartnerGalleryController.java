package com.artbay.marketplace.marketplacemodule.controller;

import com.artbay.marketplace.common.model.ArtworkDTO;
import com.artbay.marketplace.common.model.NFTKeyDTO;
import com.artbay.marketplace.marketplacemodule.model.Artwork;
import com.artbay.marketplace.marketplacemodule.model.mapper.ArtworkMapper;
import com.artbay.marketplace.marketplacemodule.service.ArtworkService;
import com.artbay.marketplace.marketplacemodule.service.NftKeyService;
import io.swagger.annotations.ApiOperation;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import net.kaczmarzyk.spring.data.jpa.domain.*;
import net.kaczmarzyk.spring.data.jpa.web.annotation.And;
import net.kaczmarzyk.spring.data.jpa.web.annotation.Spec;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.http.HttpStatus;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;

import java.util.Map;
import java.util.UUID;

import static com.artbay.marketplace.common.utils.ControllerUtil.getUserId;

@Slf4j
@RestController
@RequestMapping("/partner/gallery")
@RequiredArgsConstructor
public class PartnerGalleryController {

    private final ArtworkService artworkService;
    private final NftKeyService nftKeyService;

    private final ArtworkMapper artworkMapper;

    @GetMapping("/nft-key")
    @PreAuthorize("hasAuthority('user')")
    public NFTKeyDTO getNftKey() {
        return nftKeyService.getNftKey(getUserId());
    }

    @GetMapping("/artworks")
    @PreAuthorize("hasAuthority('user')")
    @ApiOperation(value = "Search artworks, request may be missing for get all artworks." +
                              "For sort by 'creationDate' you must concat in url '?sort=creationDate,asc or desc'.")
    public Page<ArtworkDTO> getPartnerArtworks(
        @And({
            @Spec(path = "currentOwnerId", params = "currentOwner", spec = Equal.class),
            @Spec(path = "name", params = "name", spec = LikeIgnoreCase.class),
            @Spec(path = "publishedToMarketplace", params = "published", spec = EqualIgnoreCase.class),
            @Spec(path = "status", params = "status", paramSeparator = ',', spec = In.class),
            @Spec(path = "price", params = "minPrice", spec = GreaterThanOrEqual.class),
            @Spec(path = "price", params = "maxPrice", spec = LessThanOrEqual.class),
            @Spec(path = "level", params = "levels", paramSeparator = ',', spec = In.class),
            @Spec(path = "imported", params = "imported", paramSeparator = ',', defaultVal = "false", spec = In.class),
            @Spec(path = "firstSale", params = "firstSale", spec = EqualIgnoreCase.class)})
        Specification<Artwork> specification,
        Pageable pageable
    ) {
        return artworkMapper.artworksToDtoPage(artworkService.getPartnerMyArtworks(
            getUserId(),
            specification,
            pageable
        ));
    }

    @GetMapping("/tab-counters")
    @PreAuthorize("hasAuthority('user')")
    public Map<String, Long> tabCounters() {
        return artworkService.getPartnerTabCounters(getUserId());
    }

    @GetMapping("/{artworkId}/revoke-from-marketplace")
    @PreAuthorize("hasAuthority('user')")
    public void revokeArtworkFromMarketplace(@PathVariable UUID artworkId) {
        artworkService.revokeArtworkFromMarketplace(getUserId(), artworkId);
    }

    @PutMapping("/sell")
    @ResponseStatus(HttpStatus.OK)
    @PreAuthorize("hasAuthority('user_allowed_selling')")
    public void publishToMarketplace(@RequestBody ArtworkDTO artwork) {
        artworkService.publishToMarketplacePartner(getUserId(), artwork);
    }

    @PutMapping("/check-price")
    @PreAuthorize("hasAuthority('user')")
    public Map<String, Boolean> checkPriceToLevel(@RequestBody ArtworkDTO artwork) {
        return Map.of("check", artworkService.checkArtworkPriceToLevel(artwork, getUserId()));
    }

    @GetMapping("/average-price/{art-level}")
    @PreAuthorize("hasAuthority('user')")
    public Map<String, Double> getAverageArtworkPrice(@PathVariable(value = "art-level") Integer artLevel) {
        return Map.of("price", artworkService.getAveragePrice(artLevel, getUserId()));
    }

    @GetMapping("/check-level/{art-level}")
    @PreAuthorize("hasAuthority('user')")
    public Map<String, Boolean> checkArtworkLevel(@PathVariable(value = "art-level") int artworkLevel) {
        return Map.of("check", artworkService.checkArtworkLevel(getUserId(), artworkLevel));
    }

}
