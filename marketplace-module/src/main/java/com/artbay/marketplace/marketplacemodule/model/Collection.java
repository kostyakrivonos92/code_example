package com.artbay.marketplace.marketplacemodule.model;

import com.artbay.marketplace.common.model.enums.CollectionStatus;
import lombok.*;
import org.springframework.data.annotation.CreatedDate;
import org.springframework.data.jpa.domain.support.AuditingEntityListener;

import javax.persistence.*;
import java.time.Instant;
import java.util.Set;
import java.util.UUID;

@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
@Entity
@EntityListeners(AuditingEntityListener.class)
@Table(name = "collections")
public class Collection {

    @Id
    @GeneratedValue
    @Column(name = "id")
    private UUID id;

    @Column(name = "name")
    private String name;
    @Column(name = "family")
    private String family;
    @Column(name = "description")
    private String description;

    /**
     * Both of these params are the links to the pics
     */
    @Column(name = "icon")
    private String icon;
    @Column(name = "thumbnail_icon")
    private String thumbnailIcon;
    @Column(name = "cover")
    private String cover;
    @Column(name = "thumbnail_cover")
    private String thumbnailCover;

    @EqualsAndHashCode.Exclude
    @OneToMany(fetch = FetchType.LAZY, mappedBy = "collection")
    private Set<Artwork> artworks;
    @Column(name = "creator_id")
    private UUID creatorId;

    @CreatedDate
    @Column(name = "creation_date")
    private Instant creationDate;

    @Enumerated(EnumType.STRING)
    @Column(name = "status")
    private CollectionStatus status;

}
