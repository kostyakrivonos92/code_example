package com.artbay.marketplace.marketplacemodule.model.mapper;

import com.artbay.marketplace.common.model.NFTKeyDTO;
import com.artbay.marketplace.marketplacemodule.model.NFTKey;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;
import org.mapstruct.Named;

import java.util.List;

@Mapper
public interface NFTKeyMapper {

    NFTKeyDTO entityToDto(NFTKey entity);

    @Named("entityToDtoForKeyPage")
    @Mapping(target = "id", ignore = true)
    @Mapping(target = "currentOwnerId", ignore = true)
    @Mapping(target = "nftMetadata", ignore = true)
    @Mapping(target = "onSale", ignore = true)
    NFTKeyDTO entityToDtoForKeyPage(NFTKey entity);

    List<NFTKeyDTO> entityToDto(List<NFTKey> entities);

    NFTKey dtoToEntity(NFTKeyDTO key);

}
