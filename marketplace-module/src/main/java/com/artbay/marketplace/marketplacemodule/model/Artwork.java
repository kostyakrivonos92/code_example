package com.artbay.marketplace.marketplacemodule.model;

import com.artbay.marketplace.common.model.enums.ArtworkOwnerRole;
import com.artbay.marketplace.common.model.enums.ArtworkVerificationStatus;
import lombok.*;
import org.springframework.data.annotation.CreatedDate;
import org.springframework.data.annotation.LastModifiedDate;
import org.springframework.data.jpa.domain.support.AuditingEntityListener;

import javax.persistence.*;
import javax.validation.constraints.Max;
import javax.validation.constraints.Min;
import java.time.Instant;
import java.util.UUID;

@Getter
@Setter
@Builder
@AllArgsConstructor
@NoArgsConstructor
@Entity
@EntityListeners(AuditingEntityListener.class)
@Table(name = "artworks")
public class Artwork {

    @Id
    @GeneratedValue
    @Column(name = "id")
    private UUID id;

    @Column(name = "name")
    private String name;

    @Column(name = "description", length = 1000)
    private String description;

    @Enumerated(EnumType.STRING)
    @Column(name = "status")
    private ArtworkVerificationStatus status;

    @Enumerated(EnumType.STRING)
    @Column(name = "owner_role")
    private ArtworkOwnerRole ownerRole;

    //add validation to keep consistency between art level and price
    @Column(name = "price", columnDefinition = "double default 0")
    private Double price;

    @Min(value = 0, message = "Artwork level should be between 0 and 10")
    @Max(value = 10, message = "Artwork level should be between 0 and 10")
    @Column(name = "level", columnDefinition = "integer default 0")
    private Integer level;
    @Min(value = 0, message = "Artwork level should be between 0 and 10")
    @Max(value = 10, message = "Artwork level should be between 0 and 10")
    @Column(name = "initial_level", columnDefinition = "integer default 0")
    private Integer initialLevel;
    @Min(value = 0, message = "Artwork level should be between 0 and 10")
    @Max(value = 10, message = "Artwork level should be between 0 and 10")
    @Column(name = "approved_level", columnDefinition = "integer default 0")
    private Integer approvedLevel;

    @Column(name = "root_owner_id")
    private UUID rootOwnerId;
    @Column(name = "current_owner_id")
    private UUID currentOwnerId;
    @Column(name = "last_buyer_id")
    private UUID lastBuyerId;

    @EqualsAndHashCode.Exclude
    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "collection_id")
    private Collection collection;

    //tech details
    @Column(name = "nft_metadata", length = 3000)
    private String nftMetadata;
    @Column(name = "nft_metadata_link")
    private String nftMetadataLink;
    @Column(name = "filepath")
    private String filepath;
    @Column(name = "thumbnail_filepath")
    private String thumbnailFilepath;
    @Column(name = "token_address")
    private String tokenAddress;
    @Column(name = "ipfs_hash")
    private String ipfsHash;

    @Column(name = "published_to_marketplace")
    private Boolean publishedToMarketplace;
    @Column(name = "promoted")
    @Builder.Default
    private Boolean promoted = false;
    @Column(name = "exported")
    @Builder.Default
    private Boolean exported = false;
    @Column(name = "imported")
    @Builder.Default
    private Boolean imported = false;
    @Column(name = "first_sale")
    @Builder.Default
    private Boolean firstSale = false; //defines if the artwork was sold at least once (has token address)
    @Column(name = "external")
    @Builder.Default
    private Boolean external = false;
    @Column(name = "is_staked")
    @Builder.Default
    private Boolean isStaked = false;

    @Column(name = "eligible_for_promotion")
    private Boolean eligibleForPromotion;

    @CreatedDate
    @Column(name = "creation_date")
    private Instant creationDate;

    @LastModifiedDate
    @Column(name = "last_modified_date")
    private Instant lastModifiedDate;

    @Column(name = "rating", columnDefinition = "double default 0.0")
    @Builder.Default
    private Double rating = 0.0;

}
