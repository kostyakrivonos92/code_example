package com.artbay.marketplace.marketplacemodule.model;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.*;
import javax.validation.constraints.Max;
import javax.validation.constraints.Min;
import java.util.UUID;

@Data
@Builder
@AllArgsConstructor
@NoArgsConstructor
@Entity
@Table(name = "artup_boosters")
public class ArtUpBooster {

    @Id
    @GeneratedValue
    @Column(name = "id")
    private UUID id;

    //add validation to keep consistency between art level and price
    @Column(name = "price", columnDefinition = "integer default 0.0")
    private Integer price;

    @Min(value = 1, message = "ArtUp level should be between 1 and 9")
    @Max(value = 9, message = "ArtUp level should be between 1 and 9")
    @Column(name = "level", columnDefinition = "integer default 0")
    private Integer level;

    @Column(name = "owner_id")
    private UUID ownerId;

    //Defines if artup was used
    @Column(name = "used")
    @Builder.Default
    private Boolean used = false;

}
