package com.artbay.marketplace.marketplacemodule.service;

import com.amazonaws.util.StringUtils;
import com.artbay.marketplace.common.exception.ErrorCode;
import com.artbay.marketplace.common.exception.GlobalException;
import com.artbay.marketplace.common.model.CollectionDTO;
import com.artbay.marketplace.common.model.enums.ArtworkVerificationStatus;
import com.artbay.marketplace.common.model.enums.CollectionStatus;
import com.artbay.marketplace.common.service.S3Service;
import com.artbay.marketplace.common.utils.FilePathGenerator;
import com.artbay.marketplace.marketplacemodule.model.Collection;
import com.artbay.marketplace.marketplacemodule.model.mapper.CollectionMapper;
import com.artbay.marketplace.marketplacemodule.repository.ArtworkRepository;
import com.artbay.marketplace.marketplacemodule.repository.CollectionRepository;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.io.FilenameUtils;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.MessageSource;
import org.springframework.context.i18n.LocaleContextHolder;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;

import java.io.File;
import java.io.IOException;
import java.util.List;
import java.util.Objects;
import java.util.UUID;
import java.util.stream.Collectors;

import static com.artbay.marketplace.common.model.enums.CollectionStatus.ACTIVE;
import static com.artbay.marketplace.common.model.enums.CollectionStatus.DELETED;
import static com.artbay.marketplace.common.utils.FilePathGenerator.generateCloudinaryLink;
import static com.artbay.marketplace.common.utils.FilePathGenerator.generateCollectionsFileName;

@Slf4j
@Service
@RequiredArgsConstructor
public class CollectionService {

    @Value("${cloud.aws.default-bucket}")
    private String defaultBucketName;
    @Value("${app.cloudinary.delivery.url}")
    private String cloudinaryDeliveryUrl;

    private final S3Service s3Service;

    private final CollectionRepository collectionRepository;
    private final ArtworkRepository artworkRepository;
    private final CollectionMapper collectionMapper;

    @Qualifier("activeMessageSource")
    private final MessageSource messageSource;

    public Page<Collection> getMyCollections(UUID userId, Pageable pageable) {
        var pageCollections = collectionRepository.findAllByCreatorId(userId, pageable);
        var collectionsList = pageCollections.stream()
                                  .filter(collection -> collection.getStatus().equals(ACTIVE))
                                  .collect(Collectors.toList());

        return new PageImpl<>(collectionsList, pageCollections.getPageable(), pageCollections.getTotalElements());
    }

    public Page<Collection> getCollections(Specification<Collection> specification, Pageable pageable) {
        Specification<Collection> collectionsSpec = (root, query, criteriaBuilder) -> criteriaBuilder.and(
            criteriaBuilder.notEqual(root.get("status"), CollectionStatus.DELETED)
        );
        var collections = collectionRepository.findAll(specification.and(collectionsSpec), pageable);
        collections.forEach(
            collection -> collection.setArtworks(
                collection.getArtworks()
                    .stream()
                    .filter(artwork -> artwork.getStatus() == ArtworkVerificationStatus.VERIFIED)
                    .collect(Collectors.toSet())
            )
        );
        return collections;
    }

    public List<Collection> getCollections(UUID creatorId) {
        return collectionRepository.findAllByCreatorId(creatorId);
    }

    public Collection getCollection(UUID collectionId) {
        Collection collection = collectionRepository.getById(collectionId);
        collection.setArtworks(
            collection.getArtworks()
                .stream()
                .filter(artwork -> artwork.getStatus() == ArtworkVerificationStatus.VERIFIED)
                .collect(Collectors.toSet())
        );
        return collection;
    }

    public Collection createCollection(
        MultipartFile icon,
        MultipartFile cover,
        CollectionDTO collectionDTO,
        UUID creatorId
    ) {
        Collection collection = collectionMapper.dtoToEntity(collectionDTO);
        collection.setCreatorId(creatorId);
        uploadFile(icon, cover, collection);
        collection.setStatus(ACTIVE);
        return collectionRepository.save(collection);
    }

    public Collection updateCollection(
        UUID collectionId,
        MultipartFile icon,
        MultipartFile cover,
        String description,
        UUID userId
    ) {
        Collection collection = getCollection(collectionId);
        if (collection.getCreatorId().equals(userId)) {
            uploadFile(icon, cover, collection);
            if (!StringUtils.isNullOrEmpty(description)) {
                collection.setDescription(description);
            }
            return collectionRepository.save(collection);
        } else {
            throw new GlobalException(
                ErrorCode.ERROR_VALIDATION,
                messageSource.getMessage(
                    "NOT_ROOT_TO_EDIT",
                    null,
                    LocaleContextHolder.getLocale()
                )
            );
        }
    }

    public void deleteCollection(UUID collectionId, UUID userId) {
        var collection = collectionRepository.findById(collectionId)
                             .orElseThrow(() -> new GlobalException(
                                 ErrorCode.BAD_REQUEST,
                                 messageSource.getMessage(
                                     "COLLECTION_NOT_FOUND",
                                     null,
                                     LocaleContextHolder.getLocale()
                                 )
                             ));
        if (collection.getCreatorId().equals(userId) && collection.getStatus().equals(ACTIVE)) {
            checkCollectionForDelete(collectionId);
            collection.setStatus(DELETED);
            collectionRepository.save(collection);
            log.info("[MARKETPLACE] User by id: {}, deleted collection by id: {} .", userId, collectionId);
        } else {
            throw new GlobalException(ErrorCode.BAD_REQUEST, messageSource.getMessage(
                "UNABLE_DELETE_COLLECTION",
                null,
                LocaleContextHolder.getLocale()
            ));
        }
    }

    private void uploadFile(MultipartFile icon, MultipartFile cover, Collection collection) {
        if (Objects.nonNull(icon)) {
            String tempIconName =
                "icon" + UUID.randomUUID() + "." + FilenameUtils.getExtension(icon.getOriginalFilename());
            File tempIconFile = new File(System.getProperty("java.io.tmpdir"), tempIconName);
            String iconPath = uploadArtworkToS3(icon, tempIconFile);
            collection.setIcon(generateCloudinaryLink(cloudinaryDeliveryUrl, iconPath));
            collection.setThumbnailIcon(FilePathGenerator.generateThumbnailLink(
                cloudinaryDeliveryUrl,
                iconPath,
                false
            ));
        }

        if (Objects.nonNull(cover)) {
            String tempCoverName =
                "cover" + UUID.randomUUID() + "." + FilenameUtils.getExtension(cover.getOriginalFilename());
            File tempCoverFile = new File(System.getProperty("java.io.tmpdir"), tempCoverName);
            String coverPath = uploadArtworkToS3(cover, tempCoverFile);
            collection.setCover(generateCloudinaryLink(cloudinaryDeliveryUrl, coverPath));
            collection.setThumbnailCover(FilePathGenerator.generateThumbnailLink(
                cloudinaryDeliveryUrl,
                coverPath,
                false
            ));
        }
    }

    private String uploadArtworkToS3(MultipartFile file, File tempFile) {
        try {
            file.transferTo(tempFile);
            String path = generateCollectionsFileName(tempFile.getName());
            s3Service.upload(defaultBucketName, path, tempFile);
            return path;
        } catch (IOException e) {
            log.error("Error with file IO ops");
            throw new GlobalException(ErrorCode.UNKNOWN_ERROR, messageSource.getMessage(
                "UNABLE_TO_PERFORM_FILE",
                null,
                LocaleContextHolder.getLocale()
            ));
        }
    }

    private void checkCollectionForDelete(UUID collectionId) {
        if (artworkRepository.existsArtworksByCollection_IdAndStatusNot(collectionId, DELETED)) {
            throw new GlobalException(
                ErrorCode.BAD_REQUEST,
                messageSource.getMessage(
                    "UNABLE_DELETE_COLLECTION_HAS_NFT",
                    null,
                    LocaleContextHolder.getLocale()
                )
            );
        }
    }

}