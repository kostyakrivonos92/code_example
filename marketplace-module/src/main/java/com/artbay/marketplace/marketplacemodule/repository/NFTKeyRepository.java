package com.artbay.marketplace.marketplacemodule.repository;

import com.artbay.marketplace.marketplacemodule.model.NFTKey;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.Optional;
import java.util.UUID;

@Repository
public interface NFTKeyRepository extends JpaRepository<NFTKey, UUID> {

    NFTKey getByCurrentOwnerId(UUID currentOwnerId);

    Optional<NFTKey> findFirstByCurrentOwnerId(UUID currentOwnerId);

    int countAllByCurrentOwnerIdIsNotNull();

    Optional<NFTKey> findFirstByCurrentOwnerIdIsNullAndOnSaleTrue();

    Optional<NFTKey> findByTokenAddress(String tokenAddress);

}
