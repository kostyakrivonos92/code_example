package com.artbay.marketplace.marketplacemodule.model.mapper;

import com.artbay.marketplace.common.client.partner.internal.PartnerInternalClient;
import com.artbay.marketplace.common.mapper.UserDtoMapper;
import com.artbay.marketplace.common.model.ArtworkDTO;
import com.artbay.marketplace.common.model.CollectionDTO;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

@Service
@RequiredArgsConstructor
public class UserInfoMapper {

    private final PartnerInternalClient partnerInternalClient;
    private final UserDtoMapper userDtoMapper;

    public ArtworkDTO setUserInfoToArtwork(ArtworkDTO artwork) {
        if (artwork.getRootOwnerId() != null) {
            artwork.setRootOwner(userDtoMapper.setOwnersInfo(partnerInternalClient.getPartnerByIdIsArtist(artwork.getRootOwnerId())));
        }
        if (artwork.getCurrentOwnerId() != null) {
            artwork.setCurrentOwner(userDtoMapper.setOwnersInfo(partnerInternalClient.getPartnerById(artwork.getCurrentOwnerId())));
        }
        return artwork;
    }

    public CollectionDTO setUserInfoToCollection(CollectionDTO collection) {
        collection.setUserInfo(userDtoMapper.setOwnersInfo(partnerInternalClient.getPartnerById(collection.getCreatorId())));
        return collection;
    }

}
