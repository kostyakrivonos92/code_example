package com.artbay.marketplace.marketplacemodule.model.activityhistory;

import com.artbay.marketplace.marketplacemodule.model.enums.ActivityHistoryOperationType;
import lombok.*;
import org.springframework.data.annotation.CreatedDate;
import org.springframework.data.jpa.domain.support.AuditingEntityListener;

import javax.persistence.*;
import java.time.Instant;
import java.util.UUID;

@Getter
@Setter
@Entity
@Builder
@AllArgsConstructor
@NoArgsConstructor
@Table(name = "artwork_activity_history")
@EntityListeners(AuditingEntityListener.class)
public class ArtworkActivityHistory {

    @Id
    @GeneratedValue
    @Column(name = "id")
    private UUID id;

    @Column(name = "related_nft_id")
    private UUID relatedNftId;

    @Enumerated(EnumType.STRING)
    @Column(name = "operation_type")
    private ActivityHistoryOperationType operationType;
    @Column(name = "user_id")
    private UUID userId;
    @Column(name = "username")
    private String username;

    @Column(name = "price")
    private Double price;
    @Column(name = "previous_level")
    private Integer previousLevel;
    @Column(name = "new_level")
    private Integer newLevel;

    @CreatedDate
    @Column(name = "operation_date")
    private Instant operationDate;
}
