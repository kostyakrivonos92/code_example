package com.artbay.marketplace.marketplacemodule.repository;

import com.artbay.marketplace.marketplacemodule.model.activityhistory.ArtworkActivityHistory;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.UUID;

@Repository
public interface ArtworkActivityHistoryRepository extends JpaRepository<ArtworkActivityHistory, UUID> {

    List<ArtworkActivityHistory> findAllByRelatedNftIdOrderByOperationDateDesc(UUID artworkId);
}
