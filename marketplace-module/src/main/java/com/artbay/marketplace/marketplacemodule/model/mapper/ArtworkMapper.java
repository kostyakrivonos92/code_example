package com.artbay.marketplace.marketplacemodule.model.mapper;

import com.artbay.marketplace.common.client.partner.internal.PartnerInternalClient;
import com.artbay.marketplace.common.model.ArtworkDTO;
import com.artbay.marketplace.marketplacemodule.model.Artwork;
import org.mapstruct.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;

import java.util.List;

import static com.artbay.marketplace.common.utils.RoundingUtil.roundArtz;

@Mapper(
    unmappedTargetPolicy = ReportingPolicy.IGNORE,
    nullValuePropertyMappingStrategy = NullValuePropertyMappingStrategy.IGNORE,
    nullValueCheckStrategy = NullValueCheckStrategy.ALWAYS
)
public abstract class ArtworkMapper {

    @Autowired
    private PartnerInternalClient partnerInternalClient;

    @Mapping(target = "avatarLink", ignore = true)
    @Mapping(target = "image", source = "filepath")
    @Mapping(target = "collectionId", source = "entity.collection.id")
    @Mapping(target = "thumbnailImage", source = "thumbnailFilepath")
    @Named("noAvatar")
    @Mapping(target = "price", source = "entity", qualifiedByName = "roundPrice")
    public abstract ArtworkDTO entityToDto(Artwork entity);

    @Mapping(target = "avatarLink", source = "entity", qualifiedByName = "setAvatarLink")
    @Mapping(target = "image", source = "filepath")
    @Mapping(target = "collectionId", source = "entity.collection.id")
    @Mapping(target = "thumbnailImage", source = "thumbnailFilepath")
    @Mapping(target = "price", source = "entity", qualifiedByName = "roundPrice")
    public abstract ArtworkDTO entityToDtoWithAvatar(Artwork entity);

    @Mapping(target = "avatarLink", source = "entity", qualifiedByName = "setAvatarLink")
    @Mapping(target = "price", source = "entity", qualifiedByName = "roundPrice")
    @Mapping(target = "image", source = "filepath")
    @Mapping(target = "collectionId", source = "entity.collection.id")
    @Mapping(target = "thumbnailImage", source = "thumbnailFilepath")
    public abstract List<ArtworkDTO> entityListToDtoListWithAvatar(List<Artwork> entityList);

    @Mapping(target = "ipfsHash", ignore = true)
    @Mapping(target = "filepath", source = "image")
    @Mapping(target = "thumbnailFilepath", source = "thumbnailImage")
    public abstract Artwork dtoToEntity(ArtworkDTO dto);

    @IterableMapping(qualifiedByName = "noAvatar")
    @Mapping(target = "price", source = "entity", qualifiedByName = "roundPrice")
    public abstract List<ArtworkDTO> entityToDto(List<Artwork> entities);

    @Mapping(target = "price", source = "entity", qualifiedByName = "roundPrice")
    public Page<ArtworkDTO> artworksToDtoPage(Page<Artwork> artworks) {
        return new PageImpl<>(
            entityToDto(artworks.getContent()),
            artworks.getPageable(),
            artworks.getTotalElements()
        );
    }

    public abstract Artwork updateArtwork(@MappingTarget Artwork entity, ArtworkDTO artworkDTO);

    @Named("setAvatarLink")
    public String setAvatarLink(Artwork artwork) {
        return partnerInternalClient.getPartnerById(artwork.getRootOwnerId()).getAvatarLink();
    }

    @Named("roundPrice")
    public Double roundPrice(Artwork artwork){
        return artwork.getPrice() == null ? 0 : roundArtz(artwork.getPrice());
    }

}
