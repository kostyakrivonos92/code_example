package com.artbay.marketplace.marketplacemodule.service;

import com.artbay.marketplace.common.annotation.IncludesWithdrawalOps;
import com.artbay.marketplace.common.client.CryptoServiceClient;
import com.artbay.marketplace.common.client.artist.internal.ArtistInternalClient;
import com.artbay.marketplace.common.client.partner.internal.PartnerInternalClient;
import com.artbay.marketplace.common.client.referral.internal.ReferralInternalClient;
import com.artbay.marketplace.common.exception.ErrorCode;
import com.artbay.marketplace.common.exception.GlobalException;
import com.artbay.marketplace.common.model.ArtistDTO;
import com.artbay.marketplace.common.model.ArtworkDTO;
import com.artbay.marketplace.common.model.NFTKeyDTO;
import com.artbay.marketplace.common.model.PartnerDTO;
import com.artbay.marketplace.common.model.enums.ArtworkOwnerRole;
import com.artbay.marketplace.common.model.response.ExportNftResponse;
import com.artbay.marketplace.marketplacemodule.model.Artwork;
import com.artbay.marketplace.marketplacemodule.model.NFTKey;
import com.artbay.marketplace.marketplacemodule.model.mapper.ArtworkMapper;
import com.artbay.marketplace.marketplacemodule.model.mapper.NFTKeyMapper;
import com.artbay.marketplace.marketplacemodule.repository.ArtworkActivityHistoryRepository;
import com.artbay.marketplace.marketplacemodule.repository.ArtworkRepository;
import com.artbay.marketplace.marketplacemodule.repository.NFTKeyRepository;
import com.artbay.marketplace.marketplacemodule.util.CryptoserviceClientUtil;
import com.artbay.marketplace.marketplacemodule.util.UserLevelUtil;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.MessageSource;
import org.springframework.context.i18n.LocaleContextHolder;
import org.springframework.stereotype.Service;

import java.util.UUID;

import static com.artbay.marketplace.common.model.enums.ArtworkVerificationStatus.*;
import static com.artbay.marketplace.common.utils.FilePathGenerator.generateExternalThumbnailLink;
import static com.artbay.marketplace.marketplacemodule.util.activityhistory.ArtworkActivityHistoryUtil.buildExportArtworkArtworkActivityHistory;
import static com.artbay.marketplace.marketplacemodule.util.activityhistory.ArtworkActivityHistoryUtil.buildImportArtworkArtworkActivityHistory;

@Slf4j
@Service
@RequiredArgsConstructor
public class ImportExportNftService {

    @Value("${app.wallet.platform.private-key}")
    private String platformPrivateKey;
    @Value("${app.cloudinary.delivery.url}")
    private String cloudinaryDeliveryUrl;

    private final CryptoServiceClient cryptoServiceClient;
    private final ReferralInternalClient referralInternalClient;
    private final PartnerInternalClient partnerInternalClient;
    private final ArtistInternalClient artistInternalClient;

    private final ArtworkRepository artworkRepository;
    private final NFTKeyRepository nftKeyRepository;
    private final ArtworkActivityHistoryRepository artworkActivityHistoryRepository;

    private final ObjectMapper objectMapper;
    private final NFTKeyMapper nftKeyMapper;
    private final ArtworkMapper artworkMapper;

    private final CryptoserviceClientUtil cryptoserviceClientUtil;
    private final UserLevelUtil userLevelUtil;

    @Qualifier("activeMessageSource")
    private final MessageSource messageSource;

    public void importNft(String tokenAddress, ArtistDTO artist, PartnerDTO partner) {
        log.info("[MARKETPLACE] Import nft by partner.");
        var userId = getUserId(artist, partner);
        var optionalArtwork = artworkRepository.findByTokenAddress(tokenAddress);
        var optionalNftKey = nftKeyRepository.findByTokenAddress(tokenAddress);
        UUID importedNftId = null;

        if (optionalArtwork.isPresent() && optionalArtwork.get().getStatus().equals(REVOKED)) {
            optionalArtwork.get().setExported(false);
            artworkRepository.save(optionalArtwork.get());
        } else if (optionalArtwork.isPresent()) {
            importedNftId = importArtworkFlow(optionalArtwork.get(), userId, artist, partner);
        } else if (optionalNftKey.isPresent()) {
            importedNftId = importNftKeyFlow(optionalNftKey.get(), userId);
        } else {
            String walletPrivateKey;
            ArtworkOwnerRole role;
            if (artist != null) {
                walletPrivateKey = artist.getCryptowalletPrivateKey();
                role = ArtworkOwnerRole.ARTIST;
            } else {
                walletPrivateKey = partner.getCryptowalletPrivateKey();
                role = ArtworkOwnerRole.PARTNER;
            }
            var metadata = cryptoServiceClient.getNftTokenMetadata(walletPrivateKey, tokenAddress);
            if (metadata != null && !metadata.isEmpty()) {
                importedNftId = importExternalNft(metadata, tokenAddress, userId, role).getId();
            }
        }
        artworkActivityHistoryRepository.save(
            buildImportArtworkArtworkActivityHistory(userId, partner.getUsername(), importedNftId)
        );
    }

    public ExportNftResponse export(UUID nftId, UUID userId, String toWalletPublicKey) {
        var optionalArtwork = artworkRepository.findById(nftId);
        var optionalNftKey = nftKeyRepository.findById(nftId);

        String txId = null;

        if (optionalArtwork.isPresent() && optionalArtwork.get().getCurrentOwnerId().equals(userId)) {
            txId = exportArtwork(artworkMapper.entityToDto(optionalArtwork.get()), userId, toWalletPublicKey);
            log.info("[MARKETPLACE] Export artwork.");
        } else if (optionalNftKey.isPresent() && optionalNftKey.get().getCurrentOwnerId().equals(userId)) {
            txId = exportNftKey(nftKeyMapper.entityToDto(optionalNftKey.get()), userId, toWalletPublicKey);
            log.info("[MARKETPLACE] Export nft-key.");
        } else {
            throw new GlobalException(
                ErrorCode.BAD_REQUEST,
                messageSource.getMessage(
                    "UNABLE_TO_EXPORT_FOR_USER",
                    null,
                    LocaleContextHolder.getLocale()
                )
            );
        }
        return ExportNftResponse.builder()
                   .txId(txId)
                   .build();
    }

    private UUID importArtworkFlow(Artwork artwork, UUID userId, ArtistDTO artist, PartnerDTO partner) {
        if (artwork.getFirstSale()) {
            artwork.setExported(false);
            artwork.setImported(false);
            artwork.setEligibleForPromotion(false);
            artwork.setCurrentOwnerId(userId);
            artwork.setOwnerRole(ArtworkOwnerRole.PARTNER);
            if (artwork.getLastBuyerId().equals(userId)) {
                userLevelUtil.openReferralLevel(artwork, partner);
            } else {
                artwork.setStatus(VERIFIED);
                artwork.setImported(true);
            }
            if (artist != null) {
                transferNftFromArtistToPartner(artist, partner, artwork.getTokenAddress());
            }
        } else if (!artwork.getFirstSale() && artist != null) {
            artwork.setOwnerRole(ArtworkOwnerRole.ARTIST);
            artwork.setEligibleForPromotion(false);
            artwork.setExported(false);
            artwork.setCurrentOwnerId(userId);
            artwork.setStatus(EMPTY);
            if (artwork.getRootOwnerId() == null) {
                artwork.setRootOwnerId(artist.getId());
            } else {
                artwork.setExported(false);
                artwork.setStatus(REVOKED);
            }
        } else {
            artwork.setOwnerRole(ArtworkOwnerRole.PARTNER);
            artwork.setCurrentOwnerId(userId);
            artwork.setExported(false);
            artwork.setEligibleForPromotion(false);
            artwork.setPublishedToMarketplace(false);
            artwork.setStatus(REJECTED);
        }
        return artworkRepository.save(artwork).getId();
    }

    private UUID importNftKeyFlow(NFTKey nftKey, UUID userId) {
        if (nftKey.getCurrentOwnerId().equals(userId)) {
            nftKey.setExported(false);
        } else {
            nftKey.setExported(false);
            nftKey.setCurrentOwnerId(userId);
        }
        return nftKeyRepository.save(nftKey).getId();
    }

    private UUID getUserId(ArtistDTO artist, PartnerDTO partner) {
        if (artist != null) {
            return artist.getId();
        } else if (partner != null) {
            return partner.getId();
        } else {
            throw new GlobalException(
                ErrorCode.ERROR_VALIDATION,
                messageSource.getMessage(
                    "USER_NOT_FOUND_ON_PLATFORM",
                    null,
                    LocaleContextHolder.getLocale()
                )
            );
        }
    }

    private void transferNftFromArtistToPartner(ArtistDTO artist, PartnerDTO partner, String tokenAddress) {
        try {
            cryptoServiceClient.transferNft(
                tokenAddress,
                artist.getCryptowalletPrivateKey(),
                platformPrivateKey,
                partner.getCryptowalletPublicKey()
            );
        } catch (Exception e) {
            log.error("[REFERRAL] Unable to import nft.", e);
            throw new GlobalException(ErrorCode.ERROR_VALIDATION, "Unable to import nft.");
        }
    }

    private Artwork importExternalNft(String metadata, String tokenAddress, UUID ownerId, ArtworkOwnerRole role) {
        try {
            JsonNode metadataNode = objectMapper.readTree(metadata).get("data").get("metadata");
            var name = metadataNode.get("name").asText();
            var description =
                metadataNode.get("description") == null ? null : metadataNode.get("description").asText();
            var category = metadataNode.findPath("category").asText();
            String filepath = null;
            String thumbnailFilepath = null;
            if (category.contains("image")) {
                filepath = metadataNode.get("image").asText();
                if (filepath.contains("arweave")) {
                    thumbnailFilepath = filepath;
                } else {
                    thumbnailFilepath = generateExternalThumbnailLink(
                        cloudinaryDeliveryUrl,
                        filepath
                    );
                }
            } else if (category.contains("video")) {
                filepath = getFilePath(metadataNode);
                thumbnailFilepath = filepath;
            } else if (category.isEmpty()) {
                filepath = getFilePath(metadataNode);
                if (filepath.contains("arweave")) {
                    thumbnailFilepath = filepath;
                } else {
                    thumbnailFilepath = generateExternalThumbnailLink(
                        cloudinaryDeliveryUrl,
                        filepath
                    );
                }
            }

            UUID rootOwner = null;
            var artworkStatus = REJECTED;
            if (role.equals(ArtworkOwnerRole.ARTIST)) {
                rootOwner = ownerId;
                artworkStatus = EMPTY;
            }

            var artwork = Artwork.builder()
                              .name(name)
                              .description(description)
                              .status(artworkStatus)
                              .ownerRole(role)
                              .external(true)
                              .imported(false)
                              .rootOwnerId(rootOwner)
                              .currentOwnerId(ownerId)
                              .nftMetadata(metadataNode.toString())
                              .tokenAddress(tokenAddress)
                              .publishedToMarketplace(false)
                              .eligibleForPromotion(false)
                              .filepath(filepath)
                              .thumbnailFilepath(thumbnailFilepath)
                              .build();
            return artworkRepository.save(artwork);
        } catch (JsonProcessingException e) {
            log.error("Error with import external nft.");
            throw new GlobalException(
                ErrorCode.UNKNOWN_ERROR,
                messageSource.getMessage(
                    "ERROR_IMPORT_EXTERNAL_NFT",
                    null,
                    LocaleContextHolder.getLocale()
                )
            );
        }
    }

    private String getFilePath(JsonNode metadataNode) {
        var files = metadataNode.get("properties").get("files").findParents("uri");
        var video = files.stream()
                        .filter(f -> f.get("type").asText().contains("video"))
                        .findFirst();

        if (video.isPresent()) {
            return video.get().get("uri").asText();
        } else {
            var image = files.stream()
                            .filter(f -> f.get("type").asText().contains("image"))
                            .findFirst();
            return image.get().get("uri").asText();
        }
    }

    private String exportArtwork(ArtworkDTO artwork, UUID userId, String toWalletPublicKey) {
        if (artwork.getPublishedToMarketplace()) {
            throw new GlobalException(
                ErrorCode.BAD_REQUEST,
                messageSource.getMessage(
                    "UNABLE_TO_EXPORT_ARTWORK_ON_MARKETPLACE",
                    null,
                    LocaleContextHolder.getLocale()
                )
            );
        }

        ArtistDTO artistToImport = artistInternalClient.getArtistByWallet(toWalletPublicKey);
        PartnerDTO partnerToImport = partnerInternalClient.getPartnerByWallet(toWalletPublicKey);
        checkToWalletPublicKey(userId, artistToImport, partnerToImport, artwork);

        String fromWalletPrivateKey;
        String fromWalletPublicKey;
        var tokenAddress = artwork.getTokenAddress();
        if (artwork.getOwnerRole().equals(ArtworkOwnerRole.ARTIST)) {
            ArtistDTO artist = artistInternalClient.getArtistById(userId);
            fromWalletPrivateKey = artist.getCryptowalletPrivateKey();
            fromWalletPublicKey = artist.getCryptowalletPublicKey();
            checkNotTransferToOwnWallet(fromWalletPublicKey, toWalletPublicKey);
            if (tokenAddress == null) {
                tokenAddress = mintNftForExport(artwork, userId, artist);
                artwork.setTokenAddress(tokenAddress);
                artworkRepository.save(artworkMapper.dtoToEntity(artwork));
            }
        } else {
            PartnerDTO partner = partnerInternalClient.getPartnerById(userId);
            fromWalletPrivateKey = partner.getCryptowalletPrivateKey();
            fromWalletPublicKey = partner.getCryptowalletPublicKey();
            checkNotTransferToOwnWallet(fromWalletPublicKey, toWalletPublicKey);
            userLevelUtil.loweringRefLevelForSeller(artwork, userId);
        }
        String txId = transferNftForExport(
            artwork,
            null,
            fromWalletPrivateKey,
            toWalletPublicKey
        );

        artwork.setExported(true);
        artworkRepository.save(artworkMapper.dtoToEntity(artwork));
        createExportArtworkActivityHistory(userId, artwork.getId());
        if (artistToImport != null || partnerToImport != null) {
            if (artistToImport != null) {
                partnerToImport = partnerInternalClient.getPartnerById(artistToImport.getId());
            }
            importNft(tokenAddress, artistToImport, partnerToImport);
        }
        return txId;
    }

    private void checkToWalletPublicKey(UUID userId, ArtistDTO artist, PartnerDTO partner, ArtworkDTO artwork) {
        if (artwork.getImported().equals(true) && (artist != null && artist.getId().equals(userId) ||
                                                       partner != null && partner.getId().equals(userId))) {
            throw new GlobalException(
                ErrorCode.BAD_REQUEST,
                messageSource.getMessage(
                    "UNABLE_EXPORT_NFT_TO_INTERNAL_WALLET",
                    null,
                    LocaleContextHolder.getLocale()
                )
            );
        }
    }

    private void checkNotTransferToOwnWallet(String fromWalletPublicKey, String toWalletPublicKey) {
        if (fromWalletPublicKey.equals(toWalletPublicKey)) {
            throw new GlobalException(
                ErrorCode.BAD_REQUEST,
                messageSource.getMessage(
                    "UNABLE_EXPORT_NFT_TO_INTERNAL_WALLET",
                    null,
                    LocaleContextHolder.getLocale()
                )
            );
        }
    }

    @IncludesWithdrawalOps
    private String transferNftForExport(
        ArtworkDTO artwork,
        NFTKeyDTO nftKey,
        String fromWalletPrivateKey,
        String toWalletPublicKey
    ) {
        if (fromWalletPrivateKey != null) {
            String tokenAddress;
            if (artwork != null) {
                tokenAddress = artwork.getTokenAddress();
            } else {
                tokenAddress = nftKey.getTokenAddress();
            }

            String transferNftResponse = cryptoServiceClient.transferNft(
                tokenAddress,
                fromWalletPrivateKey,
                platformPrivateKey,
                toWalletPublicKey
            );
            return cryptoserviceClientUtil.getBlockchainTxIdFromResponse(transferNftResponse);
        } else {
            throw new GlobalException(
                ErrorCode.BAD_REQUEST,
                messageSource.getMessage(
                    "UNABLE_TO_EXPORT_ARTWORK",
                    null,
                    LocaleContextHolder.getLocale()
                )
            );
        }
    }

    private String mintNftForExport(ArtworkDTO artwork, UUID userId, ArtistDTO artist) {
        if (artwork.getTokenAddress() == null) {
            artwork = referralInternalClient.mintNftForExport(
                artwork.getId(),
                userId,
                artist.getCryptowalletPrivateKey(),
                artist.getCryptowalletPublicKey()
            );
        }
        return artwork.getTokenAddress();
    }

    private String exportNftKey(NFTKeyDTO nftKey, UUID userId, String toWalletPublicKey) {
        if (nftKey.getCurrentOwnerId().equals(userId)) {
            var partner = partnerInternalClient.getPartnerById(userId);
            String txId = transferNftForExport(
                null,
                nftKey,
                partner.getCryptowalletPrivateKey(),
                toWalletPublicKey
            );

            nftKey.setExported(true);
            nftKeyRepository.save(nftKeyMapper.dtoToEntity(nftKey));
            createExportArtworkActivityHistory(userId, nftKey.getId());
            return txId;
        } else {
            throw new GlobalException(
                ErrorCode.BAD_REQUEST,
                messageSource.getMessage(
                    "UNABLE_TO_EXPORT_ARTWORK",
                    null,
                    LocaleContextHolder.getLocale()
                )
            );
        }
    }

    private void createExportArtworkActivityHistory(UUID userId, UUID nftId) {
        var user = partnerInternalClient.getPartnerById(userId);
        artworkActivityHistoryRepository.save(
            buildExportArtworkArtworkActivityHistory(user.getId(), user.getUsername(), nftId)
        );
    }

}
