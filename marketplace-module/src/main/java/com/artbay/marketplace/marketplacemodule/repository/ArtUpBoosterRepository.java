package com.artbay.marketplace.marketplacemodule.repository;

import com.artbay.marketplace.marketplacemodule.model.ArtUpBooster;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.UUID;

@Repository

public interface ArtUpBoosterRepository extends JpaRepository<ArtUpBooster, UUID> {
    List<ArtUpBooster> findAllByOwnerId(UUID ownerId);

}
