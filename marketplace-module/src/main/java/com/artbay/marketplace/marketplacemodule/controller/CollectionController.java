package com.artbay.marketplace.marketplacemodule.controller;

import com.artbay.marketplace.common.model.CollectionDTO;
import com.artbay.marketplace.common.validation.LoadableFile;
import com.artbay.marketplace.marketplacemodule.model.Collection;
import com.artbay.marketplace.marketplacemodule.model.mapper.CollectionMapper;
import com.artbay.marketplace.marketplacemodule.model.mapper.UserInfoMapper;
import com.artbay.marketplace.marketplacemodule.service.CollectionService;
import io.swagger.annotations.ApiOperation;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import net.kaczmarzyk.spring.data.jpa.domain.Equal;
import net.kaczmarzyk.spring.data.jpa.web.annotation.And;
import net.kaczmarzyk.spring.data.jpa.web.annotation.Spec;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.http.HttpStatus;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

import javax.validation.Valid;
import java.util.UUID;

import static com.artbay.marketplace.common.utils.ControllerUtil.getUserId;

@Slf4j
@RestController
@RequestMapping("/collections")
@RequiredArgsConstructor
@Validated
public class CollectionController {

    private final CollectionService collectionService;

    private final CollectionMapper collectionMapper;
    private final UserInfoMapper userInfoMapper;

    @GetMapping
    @ApiOperation("Get collections by current user(creator)")
    @PreAuthorize("hasAnyAuthority('user', 'artist_verified')")
    public Page<CollectionDTO> getMyCollections(Pageable pageable) {
        return collectionMapper.collectionsToDtoPage(collectionService.getMyCollections(getUserId(), pageable))
                   .map(userInfoMapper::setUserInfoToCollection);
    }

    @GetMapping("/all")
    @ApiOperation("get all collection by parameters id/creator")
    public Page<CollectionDTO> getCollections(
        @And({
            @Spec(path = "id", params = "id", spec = Equal.class),
            @Spec(path = "creatorId", params = "creator", spec = Equal.class)
        }) Specification<Collection> specification, Pageable pageable
    ) {
        return collectionMapper.collectionsToDtoPage(collectionService.getCollections(specification, pageable))
                   .map(userInfoMapper::setUserInfoToCollection);
    }

    @GetMapping("/{collectionId}")
    public CollectionDTO getCollection(@PathVariable UUID collectionId) {
        CollectionDTO collection = collectionMapper.entityToDto(collectionService.getCollection(collectionId));
        return userInfoMapper.setUserInfoToCollection(collection);
    }

    @PostMapping
    @ResponseStatus(HttpStatus.CREATED)
    @PreAuthorize("hasAnyAuthority('user', 'artist_verified')")
    public CollectionDTO createCollection(
        @Valid @LoadableFile(permittedTypes = {"image"}) @RequestPart(required = false,
                                                                      value = "icon") MultipartFile icon,
        @Valid @LoadableFile(permittedTypes = {"image"}) @RequestPart(required = false,
                                                                      value = "cover") MultipartFile cover,
        @Valid @RequestPart CollectionDTO collection
    ) {
        CollectionDTO dto = collectionMapper.entityToDto(collectionService.createCollection(
            icon,
            cover,
            collection,
            getUserId()
        ));
        return userInfoMapper.setUserInfoToCollection(dto);
    }

    @PutMapping("/{collectionId}")
    @PreAuthorize("hasAnyAuthority('user', 'artist_verified')")
    public CollectionDTO updateCollection(
        @PathVariable UUID collectionId,
        @Valid @LoadableFile(permittedTypes = {"image"})
        @RequestPart(required = false, value = "icon") MultipartFile icon,
        @Valid @LoadableFile(permittedTypes = {"image"})
        @RequestPart(required = false, value = "cover") MultipartFile cover,
        @Valid @RequestPart(required = false) String description
    ) {
        return collectionMapper.entityToDto(collectionService.updateCollection(
            collectionId,
            icon,
            cover,
            description,
            getUserId()
        ));
    }

    @DeleteMapping("/delete/{collectionId}")
    @PreAuthorize("hasAnyAuthority('artist_verified')")
    public void deleteCollection(@PathVariable UUID collectionId) {
        collectionService.deleteCollection(collectionId, getUserId());
    }

}
