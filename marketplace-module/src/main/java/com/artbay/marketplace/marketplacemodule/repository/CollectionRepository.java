package com.artbay.marketplace.marketplacemodule.repository;

import com.artbay.marketplace.marketplacemodule.model.Collection;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.UUID;

@Repository
public interface CollectionRepository extends JpaRepository<Collection, UUID>, JpaSpecificationExecutor<Collection> {

    Page<Collection> findAllByCreatorId(UUID creatorId, Pageable pageable);

    List<Collection> findAllByCreatorId(UUID creatorId);
}
