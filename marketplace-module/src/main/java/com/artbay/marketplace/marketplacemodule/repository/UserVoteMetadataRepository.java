package com.artbay.marketplace.marketplacemodule.repository;

import com.artbay.marketplace.marketplacemodule.model.rating.UserVoteMetadata;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;
import java.util.UUID;

public interface UserVoteMetadataRepository extends JpaRepository<UserVoteMetadata, UUID> {

    List<UserVoteMetadata> findAllByIsTrialTrue();

}