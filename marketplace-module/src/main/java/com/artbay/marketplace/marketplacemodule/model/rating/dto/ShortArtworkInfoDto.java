package com.artbay.marketplace.marketplacemodule.model.rating.dto;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.validation.constraints.NotNull;
import java.util.UUID;

@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class ShortArtworkInfoDto {

    @NotNull
    private UUID artworkId;
    private String artworkName;
    private String thumbnailUrl;
    private Double rating;
    private Integer votesCount;

}
