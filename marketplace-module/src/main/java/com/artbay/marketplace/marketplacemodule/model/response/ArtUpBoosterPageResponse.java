package com.artbay.marketplace.marketplacemodule.model.response;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
public class ArtUpBoosterPageResponse {

    private Integer level;
    private Long availableCount;
    private Integer price;
    private boolean isAvailableForPurchase;

}
