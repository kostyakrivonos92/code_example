package com.artbay.marketplace.marketplacemodule.service;


import com.artbay.marketplace.common.client.CryptoServiceClient;
import com.artbay.marketplace.common.client.artist.internal.ArtistInternalClient;
import com.artbay.marketplace.common.client.partner.internal.PartnerInternalClient;
import com.artbay.marketplace.common.client.referral.ReferralRootClient;
import com.artbay.marketplace.common.client.referral.internal.ReferralInternalClient;
import com.artbay.marketplace.common.enums.FileType;
import com.artbay.marketplace.common.exception.ErrorCode;
import com.artbay.marketplace.common.exception.ErrorMessage;
import com.artbay.marketplace.common.exception.GlobalException;
import com.artbay.marketplace.common.model.ArtistDTO;
import com.artbay.marketplace.common.model.ArtworkDTO;
import com.artbay.marketplace.common.model.PartnerDTO;
import com.artbay.marketplace.common.model.enums.ArtworkOwnerRole;
import com.artbay.marketplace.common.model.enums.ArtworkVerificationStatus;
import com.artbay.marketplace.common.service.S3Service;
import com.artbay.marketplace.common.utils.FileUtils;
import com.artbay.marketplace.marketplacemodule.model.Artwork;
import com.artbay.marketplace.marketplacemodule.model.Collection;
import com.artbay.marketplace.marketplacemodule.model.activityhistory.ArtworkActivityHistory;
import com.artbay.marketplace.marketplacemodule.model.mapper.ArtworkMapper;
import com.artbay.marketplace.marketplacemodule.repository.ArtworkActivityHistoryRepository;
import com.artbay.marketplace.marketplacemodule.repository.ArtworkRepository;
import com.artbay.marketplace.marketplacemodule.repository.CollectionRepository;
import com.artbay.marketplace.marketplacemodule.util.PostmarkUtil;
import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.node.ObjectNode;
import liquibase.repackaged.org.apache.commons.text.StringEscapeUtils;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.io.FilenameUtils;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.MessageSource;
import org.springframework.context.i18n.LocaleContextHolder;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;

import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
import java.time.Instant;
import java.util.*;
import java.util.stream.Collectors;

import static com.artbay.marketplace.common.model.enums.ArtworkOwnerRole.ARTIST;
import static com.artbay.marketplace.common.model.enums.ArtworkVerificationStatus.*;
import static com.artbay.marketplace.common.utils.FilePathGenerator.*;
import static com.artbay.marketplace.marketplacemodule.util.PriceToLevelConstants.ARTWORK_LEVEL_TO_PRICE_MAP;
import static com.artbay.marketplace.marketplacemodule.util.PriceToLevelConstants.ARTWORK_PRICE_TO_LEVEL_MAP;
import static com.artbay.marketplace.marketplacemodule.util.activityhistory.ArtworkActivityHistoryUtil.buildCreateArtworkArtworkActivityHistory;
import static com.artbay.marketplace.marketplacemodule.util.activityhistory.ArtworkActivityHistoryUtil.buildPurchaseArtworkArtworkActivityHistory;

@Slf4j
@Service
@RequiredArgsConstructor
public class ArtworkService {

    private static final String METADATA_COLLECTION_NODE_TEMPLATE = "{\"name\": \"%s\",\"family\": \"%s\"}";

    @Value("${cloud.aws.default-bucket}")
    private String defaultBucketName;
    @Value("${app.cloudinary.delivery.url}")
    private String cloudinaryDeliveryUrl;
    @Value("${app.ipfs.metadata.template}")
    private String ipfsMetadataTemplate;
    @Value("${app.mystery-box.prize.nft.email}")
    private String prizeNftEmail;

    private final S3Service s3Service;

    private final ArtworkRepository artworkRepository;
    private final CollectionRepository collectionRepository;
    private final ArtworkActivityHistoryRepository artworkActivityHistoryRepository;

    private final CryptoServiceClient cryptoServiceClient;
    private final ArtistInternalClient artistInternalClient;
    private final PartnerInternalClient partnerInternalClient;
    private final ReferralRootClient referralRootClient;
    private final ReferralInternalClient referralInternalClient;

    private final ArtworkMapper artworkMapper;
    private final ObjectMapper objectMapper;

    private final PostmarkUtil postmarkUtil;

    @Qualifier("activeMessageSource")
    private final MessageSource messageSource;

    /**
     * This method can be used both for artists and partners
     *
     * @param specification specification generated in controller
     * @return list of artworks that are published to marketplace
     */
    public Page<Artwork> getArtistMyArtworks(
        UUID currentOwnerId,
        Specification<Artwork> specification,
        Pageable pageable
    ) {
        Specification<Artwork> onlyArtistArtworksSpec = (root, query, criteriaBuilder) -> criteriaBuilder.and(
            criteriaBuilder.equal(root.get("currentOwnerId"), currentOwnerId),
            criteriaBuilder.equal(root.get("ownerRole"), ARTIST),
            criteriaBuilder.notEqual(root.get("status"), DELETED),
            criteriaBuilder.equal(root.get("exported"), false)
        );
        return artworkRepository.findAll(specification.and(onlyArtistArtworksSpec), pageable);
    }

    public Page<Artwork> getPartnerMyArtworks(
        UUID currentOwnerId,
        Specification<Artwork> specification,
        Pageable pageable
    ) {
        Specification<Artwork> onlyPartnerArtworksSpec = (root, query, criteriaBuilder) -> criteriaBuilder.and(
            criteriaBuilder.isNotNull(root.get("currentOwnerId")),
            criteriaBuilder.equal(root.get("currentOwnerId"), currentOwnerId),
            criteriaBuilder.equal(root.get("ownerRole"), ArtworkOwnerRole.PARTNER),
            criteriaBuilder.notEqual(root.get("status"), DELETED),
            criteriaBuilder.equal(root.get("exported"), false)
        );
        return artworkRepository.findAll(specification.and(onlyPartnerArtworksSpec), pageable);
    }

    /**
     * This method is relevant ONLY for artists
     *
     * @param artistId artist's FusionAuth ID
     * @return page of sold artworks
     */
    public Page<Artwork> getSoldArtworks(UUID artistId, Pageable pageable) {
        return artworkRepository.findAllByRootOwnerIdAndCurrentOwnerIdNotAndFirstSale(
            artistId,
            artistId,
            true,
            pageable
        );
    }

    /**
     * This method can be used both for artists and partners
     *
     * @param userId artist or partner id
     * @return list of artworks that can be exported
     */
    public List<Artwork> getArtworksEligibleForExport(UUID userId) {
        return artworkRepository.findAllByCurrentOwnerIdAndPublishedToMarketplaceFalseAndStatus(
            userId,
            VERIFIED
        );
    }

    public void purchaseArtwork(UUID artworkId, UUID buyerId) {
        referralInternalClient.purchaseArtwork(artworkId, buyerId);
    }

    public Artwork createArtwork(MultipartFile file, ArtworkDTO artworkDto, UUID artistId) {
        ArtistDTO artist = artistInternalClient.getArtistById(artistId);
        if (artist.getTrustLevel() != 0) {
            Artwork artwork = artworkMapper.dtoToEntity(artworkDto);
            if (Objects.nonNull(file)) {
                if (artworkDto.getCollectionId() != null) {
                    Collection collection = collectionRepository.findById(artworkDto.getCollectionId())
                                                .orElseThrow(() -> new GlobalException(
                                                    ErrorCode.NOT_FOUND,
                                                    "Collection not found by id: " +
                                                        artworkDto.getCollectionId()
                                                ));
                    artwork.setCollection(collection);
                }
                artwork.setRootOwnerId(artistId);
                artwork.setCurrentOwnerId(artistId);
                artwork.setStatus(ArtworkVerificationStatus.EMPTY);
                artwork.setOwnerRole(ARTIST);
                artwork.setEligibleForPromotion(true);
                artwork.setExternal(false);
                artwork.setLastModifiedDate(Instant.now());
                setLevels(artwork, getLevelFromPrice(artworkDto));
                isEligibleForPublishingArt(artwork, artist, artworkDto.getPublishedToMarketplace());

                artwork = artworkRepository.save(artwork);

                String tempFileName = UUID.randomUUID() + "." + FilenameUtils.getExtension(file.getOriginalFilename());
                File tempFile = new File(System.getProperty("java.io.tmpdir"), tempFileName);
                String filepath = uploadArtworkToS3(file, tempFile);
                artwork.setFilepath(generateCloudinaryLink(cloudinaryDeliveryUrl, filepath));
                boolean isVideo = file.getOriginalFilename().toLowerCase().contains("mp4");
                artwork.setThumbnailFilepath(generateThumbnailLink(
                    cloudinaryDeliveryUrl,
                    filepath,
                    isVideo
                ));

                //metadata
                String mimeType = FileUtils.detectFileMimeType(tempFile);
                FileType fileType = FileUtils.detectFileType(tempFile);
                artwork.setNftMetadata(buildMetadata(artist, artwork, fileType, mimeType));
                log.info("Uploading NFT metadata {} to IPFS", artwork.getNftMetadata());
                String metadataLink = getMetadataLink(artwork, artist, tempFile);
                artwork.setNftMetadataLink(metadataLink);
                try {
                    Files.delete(tempFile.toPath());
                } catch (IOException e) {
                    log.warn("Unable to delete file {}", tempFile.getName());
                }
            } else {
                throw new GlobalException(
                    ErrorCode.BAD_REQUEST,
                    messageSource.getMessage(
                        "FILE_NOT_FOUND",
                        null,
                        LocaleContextHolder.getLocale()
                    )
                );
            }

            Artwork createdArtwork = artworkRepository.save(artwork);
            artworkActivityHistoryRepository.save(
                buildCreateArtworkArtworkActivityHistory(artist.getId(), artist.getUsername(), createdArtwork.getId())
            );
            return createdArtwork;
        } else {
            throw new GlobalException(
                ErrorCode.BAD_REQUEST,
                messageSource.getMessage("SOMETHING_VERY_STRANGE", null, LocaleContextHolder.getLocale())
            );
        }
    }

    public Page<Artwork> searchArtworks(Specification<Artwork> specification, Pageable pageable) {
        return artworkRepository.findAll(specification, pageable);
    }

    public List<Artwork> getPromotedArtworks(Specification<Artwork> specification) {
        var artworks = artworkRepository.findAll(specification);
        ArrayList<Artwork> listArtworks = new ArrayList<>(artworks);
        Collections.shuffle(listArtworks);
        return listArtworks.stream()
                   .limit(8)
                   .collect(Collectors.toList());
    }

    public List<Artwork> getArtworks(UUID ownerId) {
        return artworkRepository.findAllByCurrentOwnerIdIn(List.of(ownerId));
    }

    public List<ArtworkActivityHistory> getArtworkHistory(UUID artworkId) {
        return artworkActivityHistoryRepository.findAllByRelatedNftIdOrderByOperationDateDesc(artworkId);
    }

    public List<Artwork> getArtworksFromPartnerNetwork(UUID partnerId) {
        List<UUID> referralNetworkUserIds = referralRootClient.getReferralNetworkUserIdsForPartner(partnerId);
        return artworkRepository.findAllByCurrentOwnerIdIn(referralNetworkUserIds);
    }

    public Artwork getArtworkById(UUID artworkId) {
        return artworkRepository.findById(artworkId)
                   .orElseThrow(() -> new GlobalException(
                       ErrorCode.NOT_FOUND,
                       ErrorMessage.ARTWORK_NOT_FOUND
                   ));
    }

    public List<Artwork> getArtworksByIds(List<UUID> artworkIds) {
        return artworkRepository.findAllById(artworkIds);
    }

    public Artwork internalUpdateArtwork(ArtworkDTO artworkDTO) {
        Artwork artwork = artworkMapper.dtoToEntity(artworkDTO);
        return artworkRepository.save(artwork);
    }

    public Map<String, Long> getPartnerTabCounters(UUID partnerId) {
        Specification<Artwork> specification = (root, query, criteriaBuilder) -> criteriaBuilder.and(
            criteriaBuilder.isNotNull(root.get("currentOwnerId")),
            criteriaBuilder.equal(root.get("currentOwnerId"), partnerId),
            criteriaBuilder.equal(root.get("ownerRole"), ArtworkOwnerRole.PARTNER),
            criteriaBuilder.equal(root.get("exported"), false)
        );

        List<Artwork> artworks = artworkRepository.findAll(specification);
        long available = artworks.stream()
                             .filter(artwork -> artwork.getStatus() == ArtworkVerificationStatus.VERIFIED ||
                                                    artwork.getStatus() == ArtworkVerificationStatus.EMPTY)
                             .filter(artwork -> !artwork.getPublishedToMarketplace())
                             .filter(artwork -> artwork.getImported().equals(false))
                             .count();
        long onSale = artworks.stream()
                          .filter(artwork -> artwork.getStatus() == ArtworkVerificationStatus.VERIFIED)
                          .filter(Artwork::getPublishedToMarketplace)
                          .count();
        long imported = artworks.stream()
                            .filter(artwork -> artwork.getStatus() == ArtworkVerificationStatus.VERIFIED)
                            .filter(Artwork::getImported)
                            .filter(artwork -> !artwork.getPublishedToMarketplace())
                            .count();
        long rejected = artworks.stream()
                            .filter(artwork -> artwork.getStatus() == REJECTED)
                            .filter(artwork -> artwork.getImported().equals(false))
                            .count();
        long restricted = artworks.stream()
                              .filter(artwork -> artwork.getStatus() == REVOKED)
                              .filter(artwork -> artwork.getImported().equals(false))
                              .count();
        return Map.of("all", available + onSale,
                      "available", available,
                      "onSale", onSale,
                      "imported", imported,
                      "rejected", rejected,
                      "restricted", restricted
        );
    }

    public Map<String, Long> getArtistTabCounters(UUID artistId) {
        Specification<Artwork> specification = (root, query, criteriaBuilder) -> criteriaBuilder.and(
            criteriaBuilder.equal(root.get("currentOwnerId"), artistId),
            criteriaBuilder.equal(root.get("exported"), false)
        );

        List<Artwork> artworks = artworkRepository.findAll(specification);
        long available = artworks.stream()
                             .filter(artwork -> artwork.getStatus() == ArtworkVerificationStatus.VERIFIED
                                                    || artwork.getStatus() == EMPTY)
                             .filter(artwork -> !artwork.getPublishedToMarketplace())
                             .filter(artwork -> !artwork.getFirstSale())
                             .filter(artwork -> artwork.getOwnerRole().equals(ARTIST))
                             .count();
        long onSale = artworks.stream()
                          .filter(artwork -> artwork.getStatus() == ArtworkVerificationStatus.VERIFIED)
                          .filter(Artwork::getPublishedToMarketplace)
                          .filter(artwork -> !artwork.getFirstSale())
                          .filter(artwork -> artwork.getOwnerRole().equals(ARTIST))
                          .count();
        long imported = artworks.stream()
                            .filter(artwork -> artwork.getStatus() == ArtworkVerificationStatus.VERIFIED)
                            .filter(artwork -> artwork.getOwnerRole().equals(ARTIST))
                            .filter(Artwork::getImported)
                            .count();
        long pending = artworks.stream()
                           .filter(artwork -> artwork.getStatus() == IN_PROGRESS)
                           .filter(artwork -> !artwork.getFirstSale())
                           .filter(artwork -> artwork.getOwnerRole().equals(ARTIST))
                           .count();
        long declined = artworks.stream()
                            .filter(artwork -> artwork.getStatus() == DECLINED)
                            .filter(artwork -> !artwork.getFirstSale())
                            .filter(artwork -> artwork.getOwnerRole().equals(ARTIST))
                            .count();
        long sold = artworkRepository.findAllByRootOwnerIdAndCurrentOwnerIdNotAndFirstSaleIsTrue(artistId, artistId)
                        .size();
        long restricted = artworks.stream()
                              .filter(artwork -> artwork.getStatus() == REVOKED)
                              .filter(artwork -> !artwork.getFirstSale())
                              .filter(artwork -> artwork.getOwnerRole().equals(ARTIST))
                              .count();
        return Map.of("all", available + onSale,
                      "available", available,
                      "onSale", onSale,
                      "imported", imported,
                      "pending", pending,
                      "declined", declined,
                      "sold", sold,
                      "restricted", restricted
        );
    }

    public void revokeArtworkFromMarketplace(UUID userId, UUID artworkId) {
        Artwork artwork = artworkRepository.getById(artworkId);
        if (referralInternalClient.checkMintMarker(artworkId)) {
            throw new GlobalException(
                ErrorCode.ERROR_VALIDATION,
                messageSource.getMessage("UNABLE_TO_REVOKE_NFT", null, LocaleContextHolder.getLocale())
            );
        }

        if (artwork.getStatus().equals(VERIFIED) &&
                ((artwork.getCurrentOwnerId() == null && artwork.getRootOwnerId().equals(userId)) ||
                     artwork.getCurrentOwnerId().equals(userId))) {
            artwork.setPublishedToMarketplace(false);
            artworkRepository.save(artwork);
        }
    }

    public List<Artwork> getArtworksByRootOwnerIdAndNotVerified(UUID rootOwnerId) {
        return artworkRepository.findAllByRootOwnerIdAndStatus(rootOwnerId, IN_PROGRESS);
    }

    public void publishToMarketplacePartner(UUID userId, ArtworkDTO dto) {
        Artwork artwork = artworkRepository.findById(dto.getId())
                              .orElseThrow(() -> new GlobalException(
                                  ErrorCode.NOT_FOUND,
                                  ErrorMessage.ARTWORK_NOT_FOUND
                              ));

        if (artwork.getCurrentOwnerId().equals(userId) && artwork.getStatus().equals(VERIFIED)) {
            artwork.setPublishedToMarketplace(true);
            artwork.setPrice(dto.getPrice());
            artworkRepository.save(artwork);
            PartnerDTO partner = partnerInternalClient.getPartnerById(userId);
            postmarkUtil.sendPublishNftOnMarketplaceEmail(partner.getEmail(), false, artwork);
        } else {
            throw new GlobalException(
                ErrorCode.BAD_REQUEST,
                messageSource.getMessage("UNABLE_TO_PUBLISHED_NFT", null, LocaleContextHolder.getLocale())
            );
        }
    }

    public void publishToMarketplaceArtist(UUID userId, ArtworkDTO dto) {
        Artwork artwork = artworkRepository.findById(dto.getId())
                              .orElseThrow(() -> new GlobalException(
                                  ErrorCode.NOT_FOUND,
                                  ErrorMessage.ARTWORK_NOT_FOUND
                              ));
        if ((artwork.getRootOwnerId().equals(userId) && artwork.getCurrentOwnerId().equals(userId)) &&
                !artwork.getStatus().equals(REVOKED) && !artwork.getStatus().equals(REJECTED)) {
            artwork = artworkMapper.updateArtwork(artwork, dto);
            ArtistDTO artist = artistInternalClient.getArtistById(userId);

            if (artist.getTrustLevel() == 0) {
                throw new GlobalException(
                    ErrorCode.BAD_REQUEST,
                    messageSource.getMessage(
                        "USER_IS_LOCKED",
                        null,
                        LocaleContextHolder.getLocale()
                    )
                );
            }
            setLevels(artwork, getLevelFromPrice(dto));
            isEligibleForPublishingPublishArt(artwork, artist);

            artworkRepository.save(artwork);
            postmarkUtil.sendPublishNftOnMarketplaceEmail(artist.getEmail(), true, artwork);
        } else {
            throw new GlobalException(
                ErrorCode.BAD_REQUEST,
                messageSource.getMessage("UNABLE_TO_PUBLISHED_NFT", null, LocaleContextHolder.getLocale())
            );
        }
    }

    public Double getAveragePrice(int artLevel, UUID userId) {
        var arts = artworkRepository.findAllByCurrentOwnerIdAndLevel(userId, artLevel);
        if (!arts.isEmpty()) {
            return arts.stream()
                       .mapToDouble(Artwork::getPrice)
                       .average()
                       .orElse(Double.NaN);
        } else {
            return 0.0;
        }
    }

    public Boolean checkArtworkPriceToLevel(ArtworkDTO dto, UUID userId) {
        Artwork artwork = artworkRepository.findById(dto.getId())
                              .orElseThrow(() -> new GlobalException(
                                  ErrorCode.NOT_FOUND,
                                  ErrorMessage.ARTWORK_NOT_FOUND
                              ));
        if (artwork.getCurrentOwnerId().equals(userId)) {
            if (dto.getPrice() >= getPriceFromLevel(artwork.getLevel())) {
                return true;
            } else {
                throw new GlobalException(
                    ErrorCode.PRICE_LIMIT,
                    "Exceeding the price limit for artwork level " + artwork.getLevel()
                );
            }
        } else {
            throw new GlobalException(ErrorCode.BAD_REQUEST, "User is not the current owner of the artwork.");
        }
    }

    public Artwork save(Artwork artwork) {
        return artworkRepository.save(artwork);
    }

    public Boolean checkArtworkLevel(UUID partnerId, int level) {
        List<Artwork> artworks = artworkRepository.findAllByCurrentOwnerIdAndStatusInAndImportedIsFalse(
            partnerId,
            Arrays.asList(VERIFIED, EMPTY)
        );

        var artsUpLevelCount = artworks.stream()
                                   .filter(art -> art.getLevel() > level)
                                   .count();
        var artsLevelCount = artworks.stream()
                                 .filter(art -> art.getLevel() == level)
                                 .count();

        if (artsUpLevelCount > 1 || artsLevelCount > 1) {
            return true;
        } else {
            return !artworks.isEmpty();
        }
    }

    public List<Artwork> getArtworksByOwnerIdAndStatuses(UUID ownerId, List<ArtworkVerificationStatus> status) {
        return artworkRepository.findAllByCurrentOwnerIdAndStatusInAndImportedIsFalse(ownerId, status);
    }

    public List<Artwork> getArtworksByArtistInternal(UUID artistId) {
        return artworkRepository.findAllByRootOwnerId(artistId);
    }

    public Page<Artwork> getArtworksByArtist(UUID artistId, UUID artworkId, Pageable pageable) {
        var page = artworkRepository.findAllByRootOwnerIdAndCurrentOwnerIdAndPublishedToMarketplaceIsTrue(
            artistId,
            artistId,
            pageable
        );
        var list = page.stream()
                       .filter(art -> art.getPublishedToMarketplace().equals(true) && !art.getId().equals(artworkId))
                       .collect(Collectors.toList());
        return new PageImpl<>(list, page.getPageable(), page.getTotalElements());
    }

    public void createArtworkActivityHistory(UUID buyerId, UUID artworkId, String buyerName, double price) {
        artworkActivityHistoryRepository.save(
            buildPurchaseArtworkArtworkActivityHistory(
                buyerId,
                buyerName,
                artworkId,
                price
            )
        );
    }

    public void deleteArtwork(UUID artworkId, UUID userId) {
        var artwork = getArtworkById(artworkId);
        if (artwork.getTokenAddress() == null && artwork.getRootOwnerId().equals(userId) &&
                artwork.getCurrentOwnerId().equals(userId)) {
            checkArtworkForDelete(artwork);
            artwork.setStatus(ArtworkVerificationStatus.DELETED);
            artworkRepository.save(artwork);
            log.info("[MARKETPLACE] User by id: {}, deleted artwork by id: {} .", userId, artworkId);
        }
    }

    public Artwork mysteryBoxNftPrize(UUID userId, int nftLevel) {
        var nftPoolUser = partnerInternalClient.getPartnerByEmail(prizeNftEmail);

        var artList = artworkRepository.findAllByCurrentOwnerIdAndLevel(nftPoolUser.getId(), nftLevel);
        Artwork prizeNft = null;
        if (!artList.isEmpty()) {
            prizeNft = artList.get(0);
            referralInternalClient.accrualPrizeNft(userId, nftPoolUser.getId(), prizeNft.getId());
            return prizeNft;
        } else {
            return prizeNft;
        }
    }

    private void isEligibleForPublishingArt(Artwork artwork, ArtistDTO artist, boolean publishedToMarketplace) {
        var trustLevel = artist.getTrustLevel();
        var approvedLevel = artwork.getApprovedLevel();
        var initialLevel = artwork.getInitialLevel();
        if (publishedToMarketplace) {
            if (approvedLevel != 0) {
                checkLevelForVerificationFlow(
                    artwork,
                    (initialLevel <= approvedLevel),
                    trustLevel,
                    initialLevel
                );
            } else {
                checkLevelForVerificationFlow(
                    artwork,
                    (initialLevel <= trustLevel),
                    trustLevel,
                    initialLevel
                );
            }
        } else {
            artwork.setInitialLevel(0);
            artwork.setLevel(0);
            artwork.setPublishedToMarketplace(artwork.getPublishedToMarketplace());
        }
    }

    private void isEligibleForPublishingPublishArt(Artwork artwork, ArtistDTO artist) {
        var trustLevel = artist.getTrustLevel();
        var approvedLevel = artwork.getApprovedLevel();
        var initialLevel = artwork.getInitialLevel();
        if (!artwork.getStatus().equals(REVOKED) && !artwork.getStatus().equals(IN_PROGRESS)) {
            if (artwork.getStatus().equals(VERIFIED)) {
                checkLevelForVerificationFlow(
                    artwork,
                    (initialLevel <= approvedLevel),
                    trustLevel,
                    initialLevel
                );
            } else {
                isEligibleForPublishingArt(artwork, artist, true);
            }
        } else {
            throw new GlobalException(
                ErrorCode.ERROR_VALIDATION,
                messageSource.getMessage("ARTWORK_INCORRECT_STATUS", null, LocaleContextHolder.getLocale())
            );
        }
    }

    private void checkLevelForVerificationFlow(
        Artwork artwork,
        boolean checkedLevel,
        int trustLevel,
        int initialLevel
    ) {
        if (checkedLevel) {
            if (artwork.getApprovedLevel() == 0) {
                artwork.setApprovedLevel(trustLevel);
            }
            artwork.setPublishedToMarketplace(true);
            artwork.setStatus(VERIFIED);
        } else {
            artwork.setPublishedToMarketplace(false);
            checkOnExceedingLimits(artwork.getRootOwnerId(), trustLevel, initialLevel);
            artwork.setStatus(IN_PROGRESS);
        }
    }

    private void checkOnExceedingLimits(UUID artistId, int trustLevel, int initialLevel) {
        if (checkArtVerificationCount(artistId)) {
            throw new GlobalException(
                ErrorCode.EXCEEDING_LIMIT,
                messageSource.getMessage(
                    "EXCEED_NUMBER_OF_ARTWORKS_FOR_VERIFICATION",
                    null,
                    LocaleContextHolder.getLocale()
                )
            );
        }
        var maxArtLevel = trustLevel + 5;
        if (initialLevel > maxArtLevel) {
            throw new GlobalException(
                ErrorCode.EXCEEDING_LIMIT,
                messageSource.getMessage(
                    "EXCEED_THE_MAXIMUM_ART_LEVEL",
                    new Object[]{maxArtLevel},
                    LocaleContextHolder.getLocale()
                )
            );
        }
    }

    private Boolean checkArtVerificationCount(UUID artistId) {
        var artworks = getArtworksByRootOwnerIdAndNotVerified(artistId);
        int count = 0;
        if (!artworks.isEmpty()) {
            count = artworks.size();
        }
        return count >= 5;
    }

    private String buildMetadata(ArtistDTO artist, Artwork artwork, FileType filetype, String mimeType) {
        String metadata = String.format(
            ipfsMetadataTemplate,
            StringEscapeUtils.escapeJava(artwork.getName()),
            StringEscapeUtils.escapeJava(artwork.getDescription()),
            artist.getCryptowalletPublicKey(),
            artwork.getId(),
            artwork.getFilepath(),
            mimeType,
            filetype.getTypeName(),
            artist.getCryptowalletPublicKey()
        );
        try {
            JsonNode metadataRootNode = objectMapper.readTree(metadata);
            if (artwork.getCollection() != null) {
                String collectionJsonString = String.format(
                    METADATA_COLLECTION_NODE_TEMPLATE,
                    artwork.getCollection().getName(),
                    artwork.getCollection().getFamily()
                );
                JsonNode collectionNode = objectMapper.readTree(collectionJsonString);
                ((ObjectNode) metadataRootNode).set("collection", collectionNode);
            }
            return metadataRootNode.toString();
        } catch (IOException e) {
            log.error("Error parsing/editing metadata while creating artwork: {}", metadata, e);
        }
        return null;
    }

    private String getMetadataLink(Artwork artwork, ArtistDTO artist, File tempFile) {
        String metadataLink;
        try {
            String hashJson = cryptoServiceClient.uploadFileToIPFS(
                artist.getCryptowalletPrivateKey(),
                artwork.getNftMetadata(),
                tempFile
            );
            String hash = objectMapper.readTree(hashJson).findValue("hash").asText();
            //TODO: add proper retry mechanism
            metadataLink = getMetadataLinkFromCryptoservice(artist.getCryptowalletPrivateKey(), hash);
        } catch (IOException e) {
            log.error("Unable to upload image to IPFS, error with getting metadata from IPFS", e);
            throw new GlobalException(
                ErrorCode.ERROR_CRYPTO_SERVICE,
                messageSource.getMessage("UNABLE_TO_UPLOAD_IMAGE", null, LocaleContextHolder.getLocale())
            );
        }
        return metadataLink;
    }

    private int getLevelFromPrice(ArtworkDTO dto) {
        if (dto.getPublishedToMarketplace()) {
            if (dto.getPrice() > 0) {
                return ARTWORK_PRICE_TO_LEVEL_MAP.floorEntry(dto.getPrice()).getValue();
            } else {
                throw new GlobalException(
                    ErrorCode.BAD_REQUEST,
                    "Price not specified. Please indicate the price of the artwork."
                );
            }
        } else {
            return 0;
        }
    }

    private double getPriceFromLevel(int price) {
        return ARTWORK_LEVEL_TO_PRICE_MAP.floorEntry(price)
                   .getValue();
    }

    private String uploadArtworkToS3(MultipartFile file, File tempFile) {
        try {
            file.transferTo(tempFile);
            String path = generateArtworkFileName(tempFile.getName().toLowerCase());
            s3Service.upload(defaultBucketName, path, tempFile);
            return path;
        } catch (IOException e) {
            log.error("Error with file IO ops");
            throw new GlobalException(
                ErrorCode.UNKNOWN_ERROR,
                messageSource.getMessage("UNABLE_TO_PERFORM_FILE", null, LocaleContextHolder.getLocale())
            );
        }
    }

    private String getMetadataLinkFromCryptoservice(String privateKey, String hash) throws IOException {
        for (int retryCount = 0; retryCount < 10; retryCount++) {
            log.info("Getting metadata link, retry count: {}", retryCount);
            String metadataLinkJson = cryptoServiceClient.getFileLinkByHash(privateKey, hash);

            JsonNode dataNode = objectMapper.readTree(metadataLinkJson).findValue("data");

            if (dataNode != null) {
                JsonNode metadataNode = objectMapper.readTree(metadataLinkJson)
                                            .findValue("data")
                                            .findValue("metadata");
                if (metadataNode != null) {
                    return metadataNode.asText();
                }
            }

            try {
                Thread.sleep(5000);
            } catch (InterruptedException e) {
                log.error("wtf", e);
                Thread.currentThread().interrupt();
                throw new GlobalException(
                    ErrorCode.UNKNOWN_ERROR,
                    messageSource.getMessage("SOMETHING_VERY_STRANGE", null, LocaleContextHolder.getLocale())
                );
            }
        }
        throw new GlobalException(
            ErrorCode.ERROR_CRYPTO_SERVICE,
            messageSource.getMessage("UNABLE_TO_GET_METADATA_LINK", null, LocaleContextHolder.getLocale())
        );
    }

    private void setLevels(Artwork artwork, int level) {
        if (artwork.getApprovedLevel() == null) {
            artwork.setApprovedLevel(0);
        }
        artwork.setInitialLevel(level);
        artwork.setLevel(level);
    }

    private void checkArtworkForDelete(Artwork artwork) {
        if (artwork.getPublishedToMarketplace().equals(true) &&
                referralInternalClient.checkMintMarker(artwork.getId())) {
            throw new GlobalException(ErrorCode.BAD_REQUEST, messageSource.getMessage(
                "UNABLE_TO_DELETE_NFT",
                null,
                LocaleContextHolder.getLocale()
            ));
        }
    }

}
