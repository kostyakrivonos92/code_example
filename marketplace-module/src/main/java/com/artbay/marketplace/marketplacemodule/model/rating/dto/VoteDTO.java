package com.artbay.marketplace.marketplacemodule.model.rating.dto;

import com.artbay.marketplace.marketplacemodule.model.enums.VoteType;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.validation.constraints.NotNull;
import java.util.UUID;

@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class VoteDTO {

    private UUID id;
    @NotNull
    private ShortArtworkInfoDto artwork;
    private VoteType voteType;
    @Builder.Default
    private boolean mysteryBoxWin = false;

}
