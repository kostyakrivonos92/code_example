package com.artbay.marketplace.marketplacemodule.service;

import com.artbay.marketplace.common.exception.ErrorCode;
import com.artbay.marketplace.common.exception.GlobalException;
import com.artbay.marketplace.common.model.NFTKeyDTO;
import com.artbay.marketplace.marketplacemodule.model.NFTKey;
import com.artbay.marketplace.marketplacemodule.model.mapper.NFTKeyMapper;
import com.artbay.marketplace.marketplacemodule.repository.NFTKeyRepository;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.context.MessageSource;
import org.springframework.context.i18n.LocaleContextHolder;
import org.springframework.data.domain.Example;
import org.springframework.stereotype.Service;

import javax.transaction.Transactional;
import java.util.Optional;
import java.util.UUID;

@Slf4j
@Service
@RequiredArgsConstructor
public class NftKeyService {

    private final NFTKeyRepository nftKeyRepository;
    private final NFTKeyMapper nftKeyMapper;

    @Qualifier("activeMessageSource")
    private final MessageSource messageSource;

    public NFTKeyDTO getNftKey(UUID partnerId) {
        return nftKeyMapper.entityToDtoForKeyPage(nftKeyRepository.getByCurrentOwnerId(partnerId));
    }

    public int getSoldNftKeyCount() {
        return nftKeyRepository.countAllByCurrentOwnerIdIsNotNull();
    }

    public NFTKeyDTO getNftKeyById(UUID keyId) {
        return nftKeyMapper.entityToDto(nftKeyRepository.getById(keyId));
    }

    @Transactional
    public NFTKey getRandomNftKeyOrThrow() {
        Optional<NFTKey> maybeKey = nftKeyRepository.findFirstByCurrentOwnerIdIsNullAndOnSaleTrue();
        if (maybeKey.isPresent()) {
            return maybeKey.get();
        }
        throw new GlobalException(ErrorCode.NO_AVAILABLE_KEYS, messageSource.getMessage(
            "NFT_KEYS_SOLD_OUT",
            null,
            LocaleContextHolder.getLocale()
        ));
    }

    @Transactional
    public NFTKeyDTO getAndBookRandomNftKey(UUID buyerId) {
        Optional<NFTKey> maybeBookedKey = nftKeyRepository.findFirstByCurrentOwnerId(buyerId);
        NFTKey key;
        if (maybeBookedKey.isPresent()) {
            key = maybeBookedKey.get();
        } else {
            key = getRandomNftKeyOrThrow();
            key.setCurrentOwnerId(buyerId);
            key.setOnSale(false);
            key = nftKeyRepository.save(key);
            log.info("Booked NFT key {} for user {}", key, buyerId);
        }
        return nftKeyMapper.entityToDto(key);
    }

    public void updateNftKey(NFTKey key) {
        log.info("Updating NFT key {}", key);
        nftKeyRepository.save(key);
    }

    public void releaseKey(UUID keyId) {
        NFTKey key = nftKeyRepository.getById(keyId);
        key.setCurrentOwnerId(null);
        key.setOnSale(true);
        nftKeyRepository.save(key);
    }

    public NFTKey findReferrerKeyOnSale(UUID referrerId) {
        Example<NFTKey> example = Example.of(
            NFTKey.builder()
                .currentOwnerId(referrerId)
                .onSale(true)
                .build()
        );
        Optional<NFTKey> maybeKey = nftKeyRepository.findAll(example).stream().findAny();
        if (maybeKey.isPresent()) {
            NFTKey key = maybeKey.get();
            key.setOnSale(false);
            nftKeyRepository.save(key);
            return key;
        }
        return null;
    }

}
