package com.artbay.marketplace.marketplacemodule.controller;

import com.artbay.marketplace.common.model.ArtworkDTO;
import com.artbay.marketplace.marketplacemodule.model.Artwork;
import com.artbay.marketplace.marketplacemodule.model.mapper.ArtworkMapper;
import com.artbay.marketplace.marketplacemodule.model.response.ArtUpBoosterPageResponse;
import com.artbay.marketplace.marketplacemodule.service.ArtUpBoosterService;
import io.swagger.annotations.ApiOperation;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import net.kaczmarzyk.spring.data.jpa.domain.Equal;
import net.kaczmarzyk.spring.data.jpa.domain.EqualIgnoreCase;
import net.kaczmarzyk.spring.data.jpa.domain.In;
import net.kaczmarzyk.spring.data.jpa.domain.LikeIgnoreCase;
import net.kaczmarzyk.spring.data.jpa.web.annotation.And;
import net.kaczmarzyk.spring.data.jpa.web.annotation.Spec;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.UUID;

import static com.artbay.marketplace.common.utils.ControllerUtil.getUserId;

@Slf4j
@RestController
@RequestMapping("/art-up-booster")
@RequiredArgsConstructor
public class ArtUpBoosterController {

    private final ArtUpBoosterService artUpBoosterService;

    private final ArtworkMapper artworkMapper;

    @GetMapping("/available")
    @PreAuthorize("hasAuthority('user')")
    @ApiOperation(value = "Get available artup boosters")
    public List<ArtUpBoosterPageResponse> getAvailableArtUpBoosters() {
        return artUpBoosterService.getAvailableForUseArtUpBoosters(getUserId());
    }

    @PostMapping("/buy")
    @PreAuthorize("hasAuthority('user')")
    @ApiOperation(value = "Buy art up booster. Parameter artUpLevel its available ArtUpBoosterLevel")
    public void buyArtUpBooster(@RequestParam int artUpLevel) {
        artUpBoosterService.buyArtUpBooster(getUserId(), artUpLevel);
    }

    @GetMapping("/art-up-price")
    @PreAuthorize("hasAuthority('user')")
    @ApiOperation(value = "Get price for art up level by artz")
    public List<ArtUpBoosterPageResponse> getArtUpPrice() {
        return artUpBoosterService.getArtUpPrice();
    }

    @PostMapping("/artz")
    @PreAuthorize("hasAuthority('user')")
    @ApiOperation(value = "Up artwork level by artz.")
    public void buyArtUpLevelByArtz(@RequestParam UUID artworkId, @RequestParam int artUpLevel) {
        artUpBoosterService.buyArtUpLevelByArtz(getUserId(), artworkId, artUpLevel);
    }

    @PostMapping("/apply")
    @PreAuthorize("hasAuthority('user')")
    @ApiOperation(value = "Apply art up booster. Required params: artUpLevel, artworkId")
    public void applyArtUpBooster(@RequestParam Integer artUpLevel, @RequestParam UUID artworkId) {
        artUpBoosterService.applyArtUpBooster(artUpLevel, artworkId, getUserId());
    }

    @GetMapping("/eligible-for-promotion")
    public Page<ArtworkDTO> getEligibleForPromotionArtworks(
        @And({
            @Spec(path = "imported", params = "imported", spec = EqualIgnoreCase.class),
            @Spec(path = "level", params = "levels", paramSeparator = ',', spec = In.class),
            @Spec(path = "name", params = "name", spec = LikeIgnoreCase.class),
            @Spec(path = "publishedToMarketplace", constVal = "false", spec = Equal.class),
            @Spec(path = "status", constVal = "VERIFIED", spec = Equal.class)
        }) Specification<Artwork> specification, Pageable pageable
    ) {
        return artworkMapper.artworksToDtoPage(artUpBoosterService.getEligibleForPromotionArtworks(
            specification,
            pageable,
            getUserId()
        ));
    }

}
