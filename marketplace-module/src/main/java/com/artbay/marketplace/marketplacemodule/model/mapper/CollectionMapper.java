package com.artbay.marketplace.marketplacemodule.model.mapper;

import com.artbay.marketplace.common.model.CollectionDTO;
import com.artbay.marketplace.marketplacemodule.model.Collection;
import org.mapstruct.*;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;

import java.util.List;

@Mapper(
    unmappedTargetPolicy = ReportingPolicy.IGNORE,
    nullValuePropertyMappingStrategy = NullValuePropertyMappingStrategy.IGNORE,
    nullValueCheckStrategy = NullValueCheckStrategy.ALWAYS
)
public interface CollectionMapper {

    @Mapping(target = "artworksCount", source = "entity", qualifiedByName = "setArtworksCount")
    CollectionDTO entityToDto(Collection entity);

    Collection dtoToEntity(CollectionDTO dto);

    List<CollectionDTO> entityListToDtoList(List<Collection> collectionList);

    default Page<CollectionDTO> collectionsToDtoPage(Page<Collection> collections) {
        return new PageImpl<>(
            entityListToDtoList(collections.getContent()),
            collections.getPageable(),
            collections.getTotalElements()
        );
    }

    @Named("setArtworksCount")
    default int getArtworksCount(Collection collection) {
        var artworks = collection.getArtworks();
        if (artworks == null || artworks.isEmpty()) {
            return 0;
        } else {
            return artworks.size();
        }
    }

}
