package com.artbay.marketplace.marketplacemodule.model.rating;

import com.artbay.marketplace.marketplacemodule.model.enums.VoteType;
import lombok.*;
import org.springframework.data.annotation.CreatedDate;
import org.springframework.data.jpa.domain.support.AuditingEntityListener;

import javax.persistence.*;
import java.time.Instant;
import java.util.UUID;

@Getter
@Setter
@Builder
@AllArgsConstructor
@NoArgsConstructor
@Entity
@EntityListeners(AuditingEntityListener.class)
@Table(name = "votes")
public class Vote {

    @Id
    @GeneratedValue
    @Column(name = "id")
    private UUID id;

    @Column(name = "partner_id")
    private UUID partnerId;

    @Column(name = "artwork_id")
    private UUID artworkId;

    @Enumerated(EnumType.STRING)
    @Column(name = "vote_type")
    private VoteType voteType;

    @Column(name = "mystery_box_win")
    @Builder.Default
    private Boolean mysteryBoxWin = false;

    @CreatedDate
    @Column(name = "creation_date")
    private Instant creationDate;

}
