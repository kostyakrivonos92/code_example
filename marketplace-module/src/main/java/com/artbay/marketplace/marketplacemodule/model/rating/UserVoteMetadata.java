package com.artbay.marketplace.marketplacemodule.model.rating;

import lombok.*;
import org.springframework.data.annotation.CreatedDate;
import org.springframework.data.annotation.LastModifiedDate;
import org.springframework.data.jpa.domain.support.AuditingEntityListener;

import javax.persistence.*;
import java.time.Instant;
import java.util.UUID;

@Getter
@Setter
@Builder
@AllArgsConstructor
@NoArgsConstructor
@Entity
@EntityListeners(AuditingEntityListener.class)
@Table(name = "user_votes_metadata")
public class UserVoteMetadata {

    @Id
    @Column(name = "partner_id")
    private UUID partnerId;

    @Column(name = "max_votes")
    private Integer maxVotes;

    @Column(name = "available_votes")
    private Integer availableVotes;

    @Column(name = "restore_period_seconds")
    private Integer restorePeriodSeconds;

    @Column(name = "votes_until_mystery_box")
    private Integer votesUntilMysteryBox;

    @Column(name = "voting_end_date")
    private Instant votingEndDate;

    @Column(name = "is_trial")
    private Boolean isTrial;

    @Column(name = "is_trial_passed")
    private Boolean isTrialPassed;

    @Column(name = "restore_votes_checkpoint_date")
    private Instant restoreVotesCheckpointDate;

    @CreatedDate
    @Column(name = "creation_date")
    private Instant creationDate;
    @LastModifiedDate
    @Column(name = "last_modified_date")
    private Instant lastModifiedDate;

}
