package com.artbay.marketplace.marketplacemodule.util;

import com.artbay.marketplace.common.service.PostmarkService;
import com.artbay.marketplace.marketplacemodule.model.Artwork;
import com.wildbit.java.postmark.client.data.model.templates.TemplatedMessage;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

import java.util.Map;

@Slf4j
@Component
@RequiredArgsConstructor
public class PostmarkUtil {

    @Value("${app.postmark.from}")
    private String postmarkFromEmail;
    @Value("${app.postmark.message-stream}")
    private String postmarkMessageStream;
    @Value("${app.postmark.publish-nft-on-marketplace.template-id}")
    private Integer publishOnMarketplaceTemplateId;

    private final PostmarkService postmarkService;


    public void sendPublishNftOnMarketplaceEmail(String email, boolean isArtist, Artwork artwork) {
        String galleryLink = "";
        if(isArtist){
            galleryLink = "https://artozo.com/api/marketplace/artist/gallery/artworks";
        } else {
            galleryLink = "https://artozo.com/api/marketplace/partner/gallery/artworks";
        }
        var data = Map.of(
            "NFT_name",
            artwork.getName(),
            "gallery_link",
            galleryLink
        );
        TemplatedMessage message = new TemplatedMessage(
            postmarkFromEmail,
            email,
            publishOnMarketplaceTemplateId,
            postmarkMessageStream
        );
        message.setTemplateModel(data);
        postmarkService.sendEmail(message);

    }
}
