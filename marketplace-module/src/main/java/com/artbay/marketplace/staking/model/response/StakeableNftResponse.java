package com.artbay.marketplace.staking.model.response;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.UUID;

@Data
@Builder
@AllArgsConstructor
@NoArgsConstructor
public class StakeableNftResponse {

    private UUID artworkId;
    private String artworkName;
    private double artworkRating;
    private int artworkBaseWeight;
    private int annualPercentageRate;
    private double artworkPurchasePrice;

}
