package com.artbay.marketplace.staking.service;

import com.artbay.marketplace.common.client.CryptoServiceClient;
import com.artbay.marketplace.common.client.partner.internal.PartnerInternalClient;
import com.artbay.marketplace.common.client.referral.internal.ReferralInternalClient;
import com.artbay.marketplace.common.exception.ErrorCode;
import com.artbay.marketplace.common.exception.GlobalException;
import com.artbay.marketplace.marketplacemodule.service.ArtworkService;
import com.artbay.marketplace.staking.model.HourlyStakedNftStats;
import com.artbay.marketplace.staking.model.HourlyStakingStats;
import com.artbay.marketplace.staking.model.StakedNft;
import com.artbay.marketplace.staking.model.Staking;
import com.artbay.marketplace.staking.model.response.StakeableNftResponse;
import com.artbay.marketplace.staking.repository.StakingRepository;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Service;

import java.time.Duration;
import java.time.Instant;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.UUID;
import java.util.stream.Collectors;

import static com.artbay.marketplace.staking.util.StakingConstants.ARTWORK_LEVEL_TO_BASE_WEIGHT;
import static com.artbay.marketplace.staking.util.StakingConstants.HOURS_IN_YEAR;
import static java.time.Instant.now;

@Slf4j
@Service
@RequiredArgsConstructor
public class StakingService {

    @Value("${app.artz.token.address}")
    private String artzTokenAddress;
    @Value("${app.wallet.platform.private-key}")
    private String platformPrivateKey;

    private final ArtworkService artworkService; // replace with marketplace internal client
    private final HourlyStakingStatsService hourlyStakingStatsService;
    private final StakedNftService stakedNftService;
    private final StakingRepository stakingRepository;

    private final PartnerInternalClient partnerInternalClient;
    private final ReferralInternalClient referralInternalClient;
    private final CryptoServiceClient cryptoServiceClient;

    public List<Staking> getRelatedStakings() {
        var announcedStakings = stakingRepository.findAllByAnnouncementDateAfter(now());
        var relatedStakings = new ArrayList<>(announcedStakings);

        var twoLastFinishedStakings = stakingRepository.findTwoFinished();
        relatedStakings.addAll(twoLastFinishedStakings);

        return relatedStakings;
    }

    public List<StakeableNftResponse> getStakableNfts(UUID partnerId, UUID stakingId) {
        var staking = stakingRepository.getById(stakingId);

        var eligibleArtworks = artworkService.getArtworks(partnerId);

        eligibleArtworks = eligibleArtworks.stream()
                               .filter(artwork ->
                                           !artwork.getIsStaked() && !artwork.getPublishedToMarketplace() &&
                                               staking.getMinArtworkLevel() <= artwork.getLevel() &&
                                               staking.getMinArtworkRating() <= artwork.getRating()
                               )
                               .collect(Collectors.toList());

        var lastHourStats = hourlyStakingStatsService.getLastHourStats(stakingId);

        double aprCoefficient;
        if (staking.getStartDate().isAfter(now()) && lastHourStats != null) {
            aprCoefficient = lastHourStats.getRewardPerWeight() * HOURS_IN_YEAR * 100;
        } else {
            double totalStakingWeight = stakedNftService.findAllByStakingId(stakingId)
                                            .stream()
                                            .map(StakedNft::getBaseStakingWeight)
                                            .reduce(Integer::sum)
                                            .orElse(1);
            aprCoefficient = staking.getHourlyPrizeAmount() / totalStakingWeight * HOURS_IN_YEAR * 100;
        }


        return eligibleArtworks.stream()
                   .map(
                       artwork -> {
                           double lastPurchasePrice = referralInternalClient.getArtworkLastPurchasePrice(
                               artwork.getId(),
                               artwork.getCurrentOwnerId()
                           );
                           int baseWeight = (int) Math.floor(
                               artwork.getRating() * ARTWORK_LEVEL_TO_BASE_WEIGHT.get(artwork.getLevel())
                           );
                           return StakeableNftResponse.builder()
                                      .artworkId(artwork.getId())
                                      .artworkName(artwork.getName())
                                      .artworkRating(artwork.getRating())
                                      .artworkBaseWeight(baseWeight)
                                      .annualPercentageRate(
                                          (int) Math.round(aprCoefficient * baseWeight / lastPurchasePrice)
                                      )
                                      .artworkPurchasePrice(lastPurchasePrice)
                                      .build();
                       }
                   )
                   .collect(Collectors.toList());
    }

    public List<StakeableNftResponse> getStakedNfts(UUID userId, UUID stakingId) {
        var staking = stakingRepository.getById(stakingId);
        var lastHourStats = hourlyStakingStatsService.getLastHourStats(stakingId);
        double aprCoefficient;
        if (staking.getStartDate().isAfter(now()) && lastHourStats != null) {
            aprCoefficient = lastHourStats.getRewardPerWeight() * HOURS_IN_YEAR * 100;
        } else {
            double totalStakingWeight = stakedNftService.findAllByStakingId(stakingId)
                                            .stream()
                                            .map(StakedNft::getBaseStakingWeight)
                                            .reduce(Integer::sum)
                                            .orElse(1);
            aprCoefficient = staking.getHourlyPrizeAmount() / totalStakingWeight * HOURS_IN_YEAR * 100;
        }


        var stakedNfts = stakedNftService.findAllStakedForUser(userId, stakingId);
        return stakedNfts.stream()
                   .map(stakedNft -> {
                       double lastPurchasePrice = referralInternalClient.getArtworkLastPurchasePrice(
                           stakedNft.getArtworkId(),
                           stakedNft.getUserId()
                       );
                       var artwork = artworkService.getArtworkById(stakedNft.getArtworkId());
                       //TODO: add accumulated reward
                       return StakeableNftResponse.builder()
                                  .artworkId(stakedNft.getArtworkId())
                                  .artworkName(artwork.getName())
                                  .artworkRating(artwork.getRating())
                                  .artworkBaseWeight(stakedNft.getBaseStakingWeight())
                                  .annualPercentageRate((int) Math.round(
                                      aprCoefficient * stakedNft.getBaseStakingWeight() / lastPurchasePrice)
                                  )
                                  .artworkPurchasePrice(lastPurchasePrice)
                                  .build();
                   })
                   .collect(Collectors.toList());
    }

    public void stakeArtwork(UUID partnerId, UUID stakingId, UUID artworkId) {
        var partner = partnerInternalClient.getPartnerById(partnerId);
        var artwork = artworkService.getArtworkById(artworkId);
        var staking = stakingRepository.getById(stakingId);

        //Validations
        if (now().isBefore(staking.getAnnouncementDate())) {
            throw new GlobalException(ErrorCode.ERROR_VALIDATION, "Staking has not been announced yet");
        }

        if (artwork.getPublishedToMarketplace()) {
            throw new GlobalException(ErrorCode.ERROR_VALIDATION, "Artwork is already published to marketplace");
        }

        if (staking.getMinArtworkLevel() > artwork.getLevel()) {
            throw new GlobalException(ErrorCode.ERROR_VALIDATION, "Artwork level is too low");
        }

        if (staking.getMinPartnerLevel() > partner.getReferralLevel()) {
            throw new GlobalException(ErrorCode.ERROR_VALIDATION, "Partner level is too low");
        }

        if (staking.getNftKeyRequired() && !partner.isHasNftKey()) {
            throw new GlobalException(ErrorCode.ERROR_VALIDATION, "Partner does not have NFT key");
        }

        try {
            cryptoServiceClient.transferNft(
                artwork.getTokenAddress(),
                partner.getCryptowalletPrivateKey(),
                platformPrivateKey,
                staking.getCryptowalletPublicKey()
            );
        } catch (Exception e) {
            log.error("Error while transferring NFT to staking", e);
            throw new GlobalException(ErrorCode.ERROR_VALIDATION, "Failed to transfer NFT");
        }

        stakedNftService.save(
            StakedNft.builder()
                .userId(partner.getId())
                .artworkId(artworkId)
                .stakingId(stakingId)
                .baseStakingWeight(
                    (int) Math.floor(
                        artwork.getRating() * ARTWORK_LEVEL_TO_BASE_WEIGHT.get(artwork.getLevel())
                    )
                )
                .build()
        );

        artwork.setIsStaked(true);
        artworkService.save(artwork);
    }

    public void unstakeArtwork(UUID userId, UUID stakingId, UUID artworkId) {
        var staking = stakingRepository.getById(stakingId);
        var stakedNft = stakedNftService.getStakedNft(userId, stakingId, artworkId);

        var nftStakedMillis = Duration.between(stakedNft.getCreationDate(), now()).toMillis();

        if (nftStakedMillis < staking.getMinStakingPeriodMillis()) {
            throw new GlobalException(ErrorCode.ERROR_VALIDATION, "NFT is not staked long enough");
        }

        var partner = partnerInternalClient.getPartnerById(userId);
        var artwork = artworkService.getArtworkById(artworkId);

        try {
            cryptoServiceClient.transferTokens(
                artzTokenAddress,
                staking.getCryptowalletPrivateKey(),
                platformPrivateKey,
                partner.getCryptowalletPublicKey(),
                stakedNft.getAccumulatedReward()
            );
        } catch (Exception e) {
            log.error("Error while transferring staking reward to partner", e);
            throw new GlobalException(ErrorCode.ERROR_VALIDATION, "Failed to transfer staking reward");
        }

        try {
            cryptoServiceClient.transferNft(
                artwork.getTokenAddress(),
                staking.getCryptowalletPrivateKey(),
                platformPrivateKey,
                partner.getCryptowalletPublicKey()
            );
        } catch (Exception e) {
            log.error("Error while transferring NFT from staking to partner", e);
            throw new GlobalException(ErrorCode.ERROR_VALIDATION, "Failed to transfer NFT from staking to partner");
        }

        stakedNft.setClaimedReward(stakedNft.getAccumulatedReward());
        stakedNftService.save(stakedNft);

        artwork.setIsStaked(false);
        artworkService.save(artwork);
    }

    @Scheduled(cron = "0 0 * ? * *")
    public void calculateStakingMath() {
        log.info("[STAKING] Calculating staking math");
        var currentDate = now();
        var stakings = stakingRepository.findAllByStartDateBeforeAndEndDateAfter(currentDate, currentDate);
        stakings.parallelStream().forEach(staking -> processStakingForStakingMath(staking, currentDate));
    }

    private void processStakingForStakingMath(Staking staking, Instant currentDate) {
        log.info("[STAKING] Calculating staking math for staking {}, date: {}", staking.getId(), currentDate);

        var stakedNfts = stakedNftService.findAllByStakingId(staking.getId());
        var realStakingWeightMap = new HashMap<StakedNft, Double>();

        //update base and real weights
        stakedNfts.forEach(stakedNft -> {
            var artwork = artworkService.getArtworkById(stakedNft.getArtworkId());
            stakedNft.setBaseStakingWeight((int) Math.round(
                artwork.getRating() * ARTWORK_LEVEL_TO_BASE_WEIGHT.get(artwork.getLevel())));
            stakedNftService.save(stakedNft);
            double realStakingWeightMultiplier;


            if (Duration.between(stakedNft.getCreationDate(), currentDate).toHours() > 1) {
                realStakingWeightMultiplier = 1;
            } else {
                realStakingWeightMultiplier =
                    (double) Duration.between(stakedNft.getCreationDate(), currentDate).toMinutes() / 60;
            }

            realStakingWeightMap.put(stakedNft, stakedNft.getBaseStakingWeight() * realStakingWeightMultiplier);
        });

        var totalBaseStakingWeight = stakedNfts
                                         .parallelStream()
                                         .map(StakedNft::getBaseStakingWeight)
                                         .reduce(0, Integer::sum);

        var totalRealStakingWeight = realStakingWeightMap.values().stream().mapToDouble(Double::doubleValue).sum();

        var hourlyRewardPerWeight = staking.getHourlyPrizeAmount() / totalRealStakingWeight;

        var hourlyStakingStats = hourlyStakingStatsService.saveHourlyStakingStats(
            HourlyStakingStats.builder()
                .stakingId(staking.getId())
                .totalBaseStakingWeight(totalBaseStakingWeight)
                .totalRealStakingWeight(totalRealStakingWeight)
                .totalStakedNfts(stakedNfts.size())
                .rewardPerWeight(hourlyRewardPerWeight)
                .build()
        );

        stakedNfts.forEach(stakedNft -> {
            HourlyStakedNftStats hourlyStakedNftStats = hourlyStakingStatsService.saveStakedNftStats(
                HourlyStakedNftStats.builder()
                    .stakedNftId(stakedNft.getId())
                    .hourlyStakingStats(hourlyStakingStats)
                    .realStakingWeight(realStakingWeightMap.get(stakedNft))
                    .accumulatedReward(realStakingWeightMap.get(stakedNft) * hourlyRewardPerWeight)
                    .claimedReward(0d)
                    .build()
            );
            stakedNft.setAccumulatedReward(
                stakedNft.getAccumulatedReward() + hourlyStakedNftStats.getAccumulatedReward()
            );
            stakedNftService.save(stakedNft);
        });
    }

}
