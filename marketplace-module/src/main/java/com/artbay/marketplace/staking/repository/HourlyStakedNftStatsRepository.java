package com.artbay.marketplace.staking.repository;

import com.artbay.marketplace.staking.model.HourlyStakedNftStats;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.UUID;

@Repository
public interface HourlyStakedNftStatsRepository extends JpaRepository<HourlyStakedNftStats, UUID> {
}
