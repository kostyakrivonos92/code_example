package com.artbay.marketplace.staking.model;

import com.artbay.marketplace.common.model.enums.PartnerRank;
import lombok.*;
import org.springframework.data.annotation.CreatedDate;
import org.springframework.data.annotation.LastModifiedDate;
import org.springframework.data.jpa.domain.support.AuditingEntityListener;

import javax.persistence.*;
import javax.validation.constraints.Min;
import java.time.Instant;
import java.util.UUID;

@Getter
@Setter
@Builder
@AllArgsConstructor
@NoArgsConstructor
@Entity
@EntityListeners(AuditingEntityListener.class)
@Table(name = "stakings")
public class Staking {

    @Id
    @GeneratedValue
    @Column(name = "id")
    private UUID id;

    @Column(name = "name")
    private String name;

    @Column(name = "announcement_date")
    private Instant announcementDate;
    @Column(name = "start_date")
    private Instant startDate;
    @Column(name = "end_date")
    private Instant endDate;

    @Column(name = "prize_fund")
    private Double prizeFund;
    @Column(name = "hourly_prize_amount")
    private Double hourlyPrizeAmount;

    @Column(name = "min_staking_period", columnDefinition = "bigint default 86400000")
    @Min(value = 86400000, message = "Minimum staking period is 24 hours")
    @Builder.Default
    private Long minStakingPeriodMillis = 86400000L;
    @Column(name = "nft_key_required")
    private Boolean nftKeyRequired;
    @Column(name = "min_partner_level")
    private Integer minPartnerLevel;
    @Column(name = "min_collector_rank")
    private PartnerRank minCollectorRank;
    @Column(name = "min_artwork_level")
    private Integer minArtworkLevel;
    @Column(name = "min_artwork_rating")
    private Double minArtworkRating;

    @Column(name = "cryptowallet_public_key")
    private String cryptowalletPublicKey;
    @Column(name = "cryptowallet_private_key")
    private String cryptowalletPrivateKey;

    @CreatedDate
    @Column(name = "creation_date")
    private Instant creationDate;
    @LastModifiedDate
    @Column(name = "last_modified_date")
    private Instant lastModifiedDate;

}
