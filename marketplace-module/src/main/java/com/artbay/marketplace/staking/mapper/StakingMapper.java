package com.artbay.marketplace.staking.mapper;

import com.artbay.marketplace.common.model.StakingDTO;
import com.artbay.marketplace.staking.model.Staking;
import org.mapstruct.*;

import java.util.List;

@Mapper(
    unmappedTargetPolicy = ReportingPolicy.IGNORE,
    nullValuePropertyMappingStrategy = NullValuePropertyMappingStrategy.IGNORE,
    nullValueCheckStrategy = NullValueCheckStrategy.ALWAYS
)
public interface StakingMapper {

    StakingDTO entityToDto(Staking staking);

    @Mapping(target = "hourlyPrizeAmount", ignore = true)
    Staking dtoToEntity(StakingDTO stakingDTO);

    List<StakingDTO> entityToDto(List<Staking> stakings);

    List<Staking> dtoToEntity(List<StakingDTO> stakingDTOs);

}
