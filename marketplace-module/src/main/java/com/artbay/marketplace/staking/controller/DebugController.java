package com.artbay.marketplace.staking.controller;

import com.artbay.marketplace.common.client.CryptoServiceClient;
import com.artbay.marketplace.common.model.StakingDTO;
import com.artbay.marketplace.staking.mapper.StakingMapper;
import com.artbay.marketplace.staking.repository.StakingRepository;
import com.artbay.marketplace.staking.service.StakingService;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.web.bind.annotation.*;

import java.time.Duration;

@Slf4j
@RestController
@RequestMapping("/debug/staking")
@RequiredArgsConstructor
public class DebugController {

    private final StakingService stakingService;
    private final StakingRepository stakingRepository;
    private final CryptoServiceClient cryptoServiceClient;
    private final StakingMapper stakingMapper;

    @PostMapping
    public StakingDTO createStaking(@RequestBody StakingDTO stakingDTO) {
        var walletResponse = cryptoServiceClient.createWallet();
        var staking = stakingMapper.dtoToEntity(stakingDTO);
        staking.setHourlyPrizeAmount(
            staking.getPrizeFund() / Duration.between(staking.getStartDate(), staking.getEndDate()).toHours()
        );
        staking.setCryptowalletPublicKey(walletResponse.getPublicKey());
        staking.setCryptowalletPrivateKey(walletResponse.getPrivateKey());
        staking = stakingRepository.save(staking);

        return stakingMapper.entityToDto(staking);
    }

    @GetMapping("/calc")
    public void calculateMath() {
        stakingService.calculateStakingMath();
    }

}
