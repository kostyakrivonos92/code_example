package com.artbay.marketplace.staking.repository;

import com.artbay.marketplace.staking.model.Staking;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import java.time.Instant;
import java.util.List;
import java.util.UUID;

@Repository
public interface StakingRepository extends JpaRepository<Staking, UUID> {

    List<Staking> findAllByAnnouncementDateAfter(Instant announcementDate);

    @Query(
        nativeQuery = true,
        value = "SELECT * FROM stakings s ORDER BY s.end_date LIMIT 2"
    )
    List<Staking> findTwoFinished();

    List<Staking> findAllByStartDateBeforeAndEndDateAfter(Instant startDate, Instant endDate);

}
