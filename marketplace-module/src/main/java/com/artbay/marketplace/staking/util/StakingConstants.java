package com.artbay.marketplace.staking.util;

import lombok.experimental.UtilityClass;

import java.util.Map;

@UtilityClass
public class StakingConstants {

    public static final int HOURS_IN_YEAR = 8760;

    public static final int L1_BASE_STAKING_VALUE = 100;
    public static final int L2_BASE_STAKING_VALUE = 200;
    public static final int L3_BASE_STAKING_VALUE = 400;
    public static final int L4_BASE_STAKING_VALUE = 800;
    public static final int L5_BASE_STAKING_VALUE = 1600;
    public static final int L6_BASE_STAKING_VALUE = 3200;
    public static final int L7_BASE_STAKING_VALUE = 6400;
    public static final int L8_BASE_STAKING_VALUE = 12800;
    public static final int L9_BASE_STAKING_VALUE = 25600;
    public static final int L10_BASE_STAKING_VALUE = 51200;

    public static final Map<Integer, Integer> ARTWORK_LEVEL_TO_BASE_WEIGHT = Map.of(
        1, L1_BASE_STAKING_VALUE,
        2, L2_BASE_STAKING_VALUE,
        3, L3_BASE_STAKING_VALUE,
        4, L4_BASE_STAKING_VALUE,
        5, L5_BASE_STAKING_VALUE,
        6, L6_BASE_STAKING_VALUE,
        7, L7_BASE_STAKING_VALUE,
        8, L8_BASE_STAKING_VALUE,
        9, L9_BASE_STAKING_VALUE,
        10, L10_BASE_STAKING_VALUE
    );

}
