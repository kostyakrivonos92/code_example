package com.artbay.marketplace.staking.repository;

import com.artbay.marketplace.staking.model.StakedNft;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.UUID;

@Repository
public interface StakedNftRepository extends JpaRepository<StakedNft, UUID> {

    StakedNft findTopByUserIdAndStakingIdAndArtworkId(UUID userId, UUID stakingId, UUID artworkId);

    List<StakedNft> findAllByStakingId(UUID stakingId);

    List<StakedNft> findAllByUserIdAndStakingId(UUID userId, UUID stakingId);

}
