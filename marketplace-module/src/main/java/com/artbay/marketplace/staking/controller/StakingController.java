package com.artbay.marketplace.staking.controller;

import com.artbay.marketplace.common.model.StakingDTO;
import com.artbay.marketplace.staking.mapper.StakingMapper;
import com.artbay.marketplace.staking.model.response.StakeableNftResponse;
import com.artbay.marketplace.staking.service.StakingService;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.UUID;

import static com.artbay.marketplace.common.utils.ControllerUtil.getUserId;

@Slf4j
@RestController
@RequestMapping("/staking")
@RequiredArgsConstructor
public class StakingController {

    private final StakingService stakingService;
    private final StakingMapper stakingMapper;

    @GetMapping("/related")
    public List<StakingDTO> getRelatedStakings() {
        return stakingMapper.entityToDto(stakingService.getRelatedStakings());
    }

    @GetMapping("/{stakingId}/stakeable-nfts")
    public List<StakeableNftResponse> getStakableNfts(@PathVariable UUID stakingId) {
        return stakingService.getStakableNfts(getUserId(), stakingId);
    }

    @GetMapping("/{stakingId}/staked-nfts")
    public List<StakeableNftResponse> getStakedNfts(@PathVariable UUID stakingId) {
        return stakingService.getStakedNfts(getUserId(), stakingId);
    }

    @PostMapping("/{stakingId}/stake/{artworkId}")
    public void stakeArtwork(@PathVariable UUID stakingId, @PathVariable UUID artworkId) {
        stakingService.stakeArtwork(getUserId(), stakingId, artworkId);
    }

    @PostMapping("/{stakingId}/unstake/{artworkId}")
    public void unstakeArtwork(@PathVariable UUID stakingId, @PathVariable UUID artworkId) {
        stakingService.unstakeArtwork(getUserId(), stakingId, artworkId);
    }

}
