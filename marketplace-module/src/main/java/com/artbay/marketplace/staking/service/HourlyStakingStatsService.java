package com.artbay.marketplace.staking.service;

import com.artbay.marketplace.staking.model.HourlyStakedNftStats;
import com.artbay.marketplace.staking.model.HourlyStakingStats;
import com.artbay.marketplace.staking.repository.HourlyStakedNftStatsRepository;
import com.artbay.marketplace.staking.repository.HourlyStakingStatsRepository;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;

import java.util.UUID;

@Slf4j
@Service
@RequiredArgsConstructor
public class HourlyStakingStatsService {

    private final HourlyStakingStatsRepository hourlyStakingStatsRepository;
    private final HourlyStakedNftStatsRepository hourlyStakedNftStatsRepository;

    public HourlyStakingStats saveHourlyStakingStats(HourlyStakingStats hourlyStakingStats) {
        return hourlyStakingStatsRepository.save(hourlyStakingStats);
    }

    public HourlyStakedNftStats saveStakedNftStats(HourlyStakedNftStats hourlyStakedNftStats) {
        return hourlyStakedNftStatsRepository.save(hourlyStakedNftStats);
    }

    public HourlyStakingStats getLastHourStats(UUID stakingId) {
        return hourlyStakingStatsRepository.findTopByStakingIdOrderByCreationDateDesc(stakingId);
    }

}
