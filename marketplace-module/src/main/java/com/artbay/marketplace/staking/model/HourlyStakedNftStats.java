package com.artbay.marketplace.staking.model;

import lombok.*;
import org.springframework.data.annotation.CreatedDate;
import org.springframework.data.jpa.domain.support.AuditingEntityListener;

import javax.persistence.*;
import java.time.Instant;
import java.util.UUID;

@Getter
@Setter
@Builder
@AllArgsConstructor
@NoArgsConstructor
@Entity
@EntityListeners(AuditingEntityListener.class)
@Table(name = "hourly_staked_nft_stats")
public class HourlyStakedNftStats {

    @Id
    @GeneratedValue
    @Column(name = "id")
    private UUID id;

    @Column(name = "staked_nft_id")
    private UUID stakedNftId;

    @ManyToOne(optional = false)
    @JoinColumn(name = "hourly_staking_stats_id", nullable = false)
    private HourlyStakingStats hourlyStakingStats;

    @Column(name = "real_staking_weight")
    private Double realStakingWeight;

    @Column(name = "accumulated_reward")
    private Double accumulatedReward;

    @Column(name = "claimed_reward")
    private Double claimedReward;

    @CreatedDate
    @Column(name = "creation_date")
    private Instant creationDate;

}
