package com.artbay.marketplace.staking.service;

import com.artbay.marketplace.staking.model.StakedNft;
import com.artbay.marketplace.staking.repository.StakedNftRepository;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.UUID;

@Slf4j
@Service
@RequiredArgsConstructor
public class StakedNftService {

    private final StakedNftRepository stakedNftRepository;

    public StakedNft save(StakedNft stakedNft) {
        return stakedNftRepository.save(stakedNft);
    }

    public StakedNft getStakedNft(UUID userId, UUID stakingId, UUID artworkId) {
        return stakedNftRepository.findTopByUserIdAndStakingIdAndArtworkId(userId, stakingId, artworkId);
    }

    public List<StakedNft> findAllByStakingId(UUID stakingId) {
        return stakedNftRepository.findAllByStakingId(stakingId);
    }

    public List<StakedNft> findAllStakedForUser(UUID userId, UUID stakingId) {
        return stakedNftRepository.findAllByUserIdAndStakingId(userId, stakingId);
    }

}
