package com.artbay.marketplace.staking.repository;

import com.artbay.marketplace.staking.model.HourlyStakingStats;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.UUID;

@Repository
public interface HourlyStakingStatsRepository extends JpaRepository<HourlyStakingStats, UUID> {

    HourlyStakingStats findTopByStakingIdOrderByCreationDateDesc(UUID stakingId);

}
