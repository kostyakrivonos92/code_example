package com.artbay.marketplace.staking.model;

import lombok.*;
import org.springframework.data.annotation.CreatedDate;
import org.springframework.data.jpa.domain.support.AuditingEntityListener;

import javax.persistence.*;
import java.time.Instant;
import java.util.List;
import java.util.UUID;

@Getter
@Setter
@Builder
@AllArgsConstructor
@NoArgsConstructor
@Entity
@EntityListeners(AuditingEntityListener.class)
@Table(name = "hourly_staking_stats")
public class HourlyStakingStats {

    @Id
    @GeneratedValue
    @Column(name = "id")
    private UUID id;

    @Column(name = "staking_id")
    private UUID stakingId;

    @Column(name = "total_base_staking_weight")
    private Integer totalBaseStakingWeight;

    @Column(name = "total_real_staking_weight")
    private Double totalRealStakingWeight;

    @Column(name = "total_staked_nfts")
    private Integer totalStakedNfts;

    @Column(name = "reward_per_weight")
    private Double rewardPerWeight;

    @OneToMany(mappedBy = "hourlyStakingStats", cascade = CascadeType.ALL, orphanRemoval = true)
    private List<HourlyStakedNftStats> hourlyStakedNftStats;

    @CreatedDate
    @Column(name = "creation_date")
    private Instant creationDate;

}
