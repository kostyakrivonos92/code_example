package com.artbay.marketplace.staking.model;

import lombok.*;
import org.springframework.data.annotation.CreatedDate;
import org.springframework.data.annotation.LastModifiedDate;
import org.springframework.data.jpa.domain.support.AuditingEntityListener;

import javax.persistence.*;
import java.time.Instant;
import java.util.List;
import java.util.UUID;

@Getter
@Setter
@Builder
@AllArgsConstructor
@NoArgsConstructor
@Entity
@EntityListeners(AuditingEntityListener.class)
@Table(name = "staked_nfts")
public class StakedNft {

    @Id
    @GeneratedValue
    @Column(name = "id")
    private UUID id;

    @Column(name = "user_id")
    private UUID userId;

    @Column(name = "artwork_id")
    private UUID artworkId;

    @Column(name = "staking_id")
    private UUID stakingId;

    @Column(name = "base_staking_weight")
    private Integer baseStakingWeight;

    @Column(name = "accumulated_reward", columnDefinition = "double default 0.0")
    @Builder.Default
    private Double accumulatedReward = 0.0;

    @Column(name = "claimed_reward", columnDefinition = "double default 0.0")
    private Double claimedReward = 0.0;

    @OneToMany(mappedBy = "stakedNftId", cascade = CascadeType.ALL, orphanRemoval = true)
    private List<HourlyStakedNftStats> hourlyStats;

    @CreatedDate
    @Column(name = "creation_date")
    private Instant creationDate;
    @LastModifiedDate
    @Column(name = "last_modified_date")
    private Instant lastModifiedDate;

}
